import { Col, Row, Card } from "react-bootstrap";
import { toAbsoluteUrl } from "src/_metronic/helpers";
import { AddressBlock } from "./AddressBlock";

interface Props {
  mga: any;
}

//TODO: Use Memo
export const languages: any = {
  en: {
    name: "English",
    flag: toAbsoluteUrl("/media/svg/flags/226-united-states.svg"),
  },
  es: {
    lang: "es",
    name: "Spanish",
    flag: toAbsoluteUrl("/media/svg/flags/128-spain.svg"),
  },
};

export const MGADetails = ({ mga }: Props) => {
  return (
    <Card className="mb-4 p-8 overview-section">
      <Card.Title as="h3" className="text-primary">
        Business Details
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>{mga && <AddressBlock address={mga.business?.address} />}</h4>
            <span>Address </span>
          </Col>
          <Col>
            {mga && <h4>{mga.business.website || "-"}</h4>}
            <span>Website</span>
          </Col>
          <Col>
            <span>{mga && <h4>{mga.contact.name || "-"}</h4>}</span>
            <span>Contact Name</span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
