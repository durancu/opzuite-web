import { Formik } from "formik";
import { isEqual } from "lodash";
import { useMemo } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useFilterUIContext } from "../../../../../../../components/Filter";
import * as filterActions from "../../../../../../../components/Filter/_redux/filterActions";
import { useDashboardsResultDetailUIContext } from "../DashboardsResultDetailUIContext";
import { dashboardResultDetailValidationSchema } from "./DashboardResulDetailFormValidation";
import { FilterForm } from "./form/FilterForm";

const prepareFilter = (queryParams: any, values: any) => ({
  ...queryParams,
  ...values,
});

export function DashboardsResultDetailFilterComponent() {
  // Dashboards UI Context
  const dashboardsUIContext: any = useDashboardsResultDetailUIContext();
  const { togglePanel } = useFilterUIContext();
  const dispatch = useAppDispatch();

  const { filterForEdit } = useSelector(
    (state: any) => ({
      filterForEdit: state.filters.filterForEdit,
    }),
    shallowEqual
  );

  const dashboardsUIProps: any = useMemo(() => {
    return {
      setQueryParams: dashboardsUIContext.setQueryParams,
      queryParams: dashboardsUIContext.queryParams,
    };
  }, [dashboardsUIContext]);

  const applyFilter = (values: any) => {
    const newQueryParams = prepareFilter(dashboardsUIProps.queryParams, values);

    if (!isEqual(newQueryParams, dashboardsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      dashboardsUIProps.setQueryParams(newQueryParams);
    }
  };

  const initialValues = {
    type: "",
    model: "sale",
    groupByModel: "",
    dateFrom: null,
    dateTo: null,
    filters: [],
  };

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={dashboardResultDetailValidationSchema}
      initialValues={filterForEdit || initialValues}
      onSubmit={(values) => {
        applyFilter(values);
        dispatch(filterActions.setFilter(values));
        togglePanel(false);
      }}
      onReset={() => {
        applyFilter(initialValues);
        dispatch(filterActions.setFilter(initialValues));
        togglePanel(false);
      }}
    >
      {() => {
        return <FilterForm />;
      }}
    </Formik>
  );
}
