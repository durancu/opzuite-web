import { Fragment, useMemo } from "react";
import { Col, Row } from "react-bootstrap";
import { useFormikContext } from "formik";
import { DatePickerCustom } from "src/app/partials/controls";
import {
  FormAutoComplete,
  // FormInput,
  // FormMultiselect,
  // FormSelect,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const FilterForm = () => {
  const catalog = useGetCatalog();

  const { values } = useFormikContext<any>();

  const groupOptions = useMemo(() => {
    return (
      catalog?.reportFilterModel.find(({ id }: any) => id === values.model)
        ?.groupByFields || []
    );
  }, [catalog, values.model]);

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="model"
            labelText="Source"
            options={catalog?.reportFilterModel}
            labelField="name"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            disabled={!values.model}
            name="groupByModel"
            labelText="Group by"
            options={groupOptions}
            labelField="name"
          />
        </Col>
      </Row>
      <Row className="mb-6 pt-4">
        <Col>
          {/* <FormMultiselect
            selectionKey="field"
            labelField="field"
            value={values.filters}
            disabled={!values.model}
            onChange={(_, value) => {
              setFieldValue("filters", value);
            }}
            options={
              catalog?.reportFilterModel
                .find(({ id }: any) => id === values.model)
                ?.filterFields.map(({ id, name }: any) => ({
                  field: id,
                  operator: "=",
                  value: "",
                  name,
                })) || []
            }
          /> */}
        </Col>
      </Row>

      <Row className="mb-4">
        {/* <Col>
          <FieldArray
            name="filters"
            render={() => {
              return values?.filters?.map((filter: any, index: number) => {
                const nameField = `filters.${index}`;
                let options: any[] | undefined = [];
                switch (filter.field) {
                  case "type":
                    if (values.model === "sale") {
                      options = catalog?.saleTypes;
                    } else if (values.model === "customer") {
                      options = catalog?.customerTypes;
                    }
                    break;
                  case "country":
                    options = catalog?.countries;
                    break;
                  case "seller":
                    options = catalog?.users;
                    break;
                  case "location":
                    options = catalog?.locations;
                    break;
                  case "carrier":
                    options = catalog?.carriers;
                    break;
                  case "broker":
                    options = catalog?.brokers;
                    break;
                  case "customer":
                    options = catalog?.customers;
                    break;
                  case "status":
                    options = catalog?.saleStatus;
                    break;
                  default:
                    break;
                }

                return (
                  <Row key={index} className="mb-4">
                    <Col>
                      <h6>Field</h6>
                      <p>{values.filters[index].field}</p>
                    </Col>
                    <Col>
                      <FormInput
                        name={`${nameField}.operator`}
                        labelText="Operator"
                        disabled
                      />
                    </Col>
                    <Col>
                      <FormSelect
                        name={`${nameField}.value`}
                        labelText="Value"
                        options={options || []}
                      />
                    </Col>
                  </Row>
                );
              });
            }}
          />
        </Col> */}
      </Row>

      <Row className="mb-6 pt-6">
        <Col sm="6">
          <DatePickerCustom label="Date from" name="dateFrom" />
        </Col>
        <Col sm="6">
          <DatePickerCustom label="Date to" name="dateTo" />
        </Col>
      </Row>
    </Fragment>
  );
};
