import { Field, FieldArray, useFormikContext } from "formik";
import { capitalize, isEmpty, startCase } from "lodash";
import { Fragment, useEffect, useState } from "react";
import { Accordion, Col, Form, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { processCoveragesByCommissionStatus } from "src/app/components/functions/processCoveragesByCommissionStatus";
import { Sale } from "src/app/components/interfaces/Sale";
import { useGetUserAuth } from "src/app/hooks";
import { AutoCompleteNoFormik } from "src/app/partials/controls";
import ItemDeleteModal from "../modalForm/ItemDeleteModal";
import { AddCoverage } from "./AddCoverage";
import { ButtonModalAction } from "./ButtonModalAction";
import { CoverageItem } from "./CoverageItem";
import ModalCoverageForm from "./ModalCoverageForm";
import objectPath from "object-path";
import { MissingFieldsAlert } from "src/app/components";

type MODAL_ACTION = "EDIT" | "REPLACE" | "ADD" | "";

export const RepeaterPolicyCoverages = () => {
  const [index, setIndex] = useState<any>();
  const [modal, toggleModal] = useState(false);
  const [modalAction, setModalAction] = useState<MODAL_ACTION>("");
  const [commissionPlans, setCommissionPlans] = useState([]);
  const [recalculateInitialBasePremium, setRecalculateInitialBasePremium] =
    useState<boolean>(false);
  const [insuredByBroker, setInsuredByBroker] = useState(true);
  const [carrier, setCarrier] = useState("");
  const [broker, setBroker] = useState("");

  const user = useGetUserAuth();
  const { brokers, carriers } = useGetCatalog();
  const { values, errors, setFieldValue } = useFormikContext<Sale>();

  useEffect(() => {
    if (user?.company) {
      setCommissionPlans(
        user?.company?.settings?.insurersCommissionPlans || []
      );
    }
  }, [user?.company]);

  useEffect(() => {
    if (recalculateInitialBasePremium) {
      (async () => {
        await processCoveragesByCommissionStatus(
          setFieldValue,
          commissionPlans,
          values.items,
          values.endorsements,
          user?.id || ""
        ).then((results: any) => {
          setFieldValue("endorsements.0.amount", results.totalPremium);
          setFieldValue(
            "endorsements.0.items.0.amount",
            results.totalDownPayment
          );
          setRecalculateInitialBasePremium(false);
        });
      })();
    }
  }, [
    values.items,
    values.endorsements,
    setFieldValue,
    recalculateInitialBasePremium,
    commissionPlans,
    user?.id,
  ]);

  useEffect(() => {
    if (values?.items && values?.items.length) {
      if (values.items[0].broker) {
        setInsuredByBroker(true);
        setBroker(values?.items[0].broker);
      } else if (values.items[0].carrier) {
        setInsuredByBroker(false);
        setCarrier(values?.items[0].carrier);
      }
    }
  }, [values?.items]);

  const setAllCarriers = ({ target }: any): void => {
    setCarrier(target.value);
    const newItems: any[] = values.items.map((element: any) => {
      return { ...element, carrier: target.value, broker: "" };
    });

    setFieldValue("items", newItems);
  };

  const setAllBrokers = ({ target }: any): void => {
    setBroker(target.value);
    const newItems: any[] = values.items.map((element: any) => {
      return { ...element, carrier: "", broker: target.value };
    });

    setFieldValue("items", newItems);
  };

  const clearElement = (): void => {
    const newItems: any[] = values.items.map((element: any) => {
      return { ...element, carrier: "", broker: "" };
    });

    setFieldValue("items", newItems);
  };

  function formatCoverageLabel(item: any) {
    return item
      ? ` ${startCase(capitalize(item.lineOfBusiness))} / ${item.name} / ${
          item.premium
        }`
      : "";
  }

  return (
    <Fragment>
      <h3 className="mb-4 text-primary">Coverages</h3>

      <Row className="mb-4 d-flex align-items-start">
        <Col className="align-self-center">
          <Field
            as={Form.Check}
            checked={insuredByBroker}
            name="insuredByBroker"
            label="This policy is binded to a MGA/Broker"
            onChange={() => {
              setBroker("");
              setCarrier("");
              clearElement();
              setInsuredByBroker(!insuredByBroker);
            }}
          />
        </Col>
        <Col hidden={!insuredByBroker}>
          <AutoCompleteNoFormik
            name="broker"
            label="MGA/Broker"
            labelField="name"
            options={brokers}
            handleChange={setAllBrokers}
            value={broker}
          />
        </Col>
        <Col hidden={insuredByBroker}>
          <AutoCompleteNoFormik
            name="carrier"
            label="Carrier"
            labelField="name"
            options={carriers}
            handleChange={setAllCarriers}
            value={carrier}
          />
        </Col>
        <Col md={8}>
          <AddCoverage
            onAdd={(lastItemIndex) => {
              setModalAction("ADD");
              setIndex(lastItemIndex);
              toggleModal(true);
            }}
            isProviderSelected={
              (insuredByBroker && !isEmpty(broker)) ||
              (!insuredByBroker && !isEmpty(carrier))
            }
            binder={insuredByBroker ? broker : carrier}
          />
        </Col>
      </Row>

      <ModalCoverageForm
        index={index}
        show={modal}
        onHide={() => toggleModal(false)}
        actionType={modalAction}
        recalculateInitialBasePremium={setRecalculateInitialBasePremium}
        insuredByBroker={insuredByBroker}
      />

      <FieldArray
        name="items"
        render={() => (
          <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
            <div className="mb-3 mt-6">
              {values.items?.length > 0 &&
                values.items.map((item: any, index: number) => {
                  return (
                    <Accordion.Item
                      key={index}
                      eventKey={index.toString()}
                      className="mb-6 border"
                      hidden={item.deleted}
                    >
                      <Accordion.Button className="px-6 py-4">
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <h4 className="card-title">
                            <span className="fw-bolder me-2">Coverage: </span>
                            <span>{formatCoverageLabel(item)}</span>
                          </h4>

                          <div className="d-flex justify-content-between align-items-center gap-3">
                            <MissingFieldsAlert
                              show={objectPath.get(errors, "items")}
                            />
                            <ButtonModalAction
                              actions={() => {
                                setModalAction("EDIT");
                                setIndex(index);
                                toggleModal(true);
                              }}
                              tooltipText="Edit"
                              actionName="edit"
                            />
                            <ItemDeleteModal
                              itemTitle={item.name || ""}
                              index={index}
                              item={item}
                              recalculateInitialBasePremium={
                                setRecalculateInitialBasePremium
                              }
                            />
                            {/* <ButtonModalEndorsementAction
                                className="mr-2"
                                actionName="ADD"
                                tooltipText="Endorse"
                                anotherActions={() => {
                                  setCoverageCode(item.code);
                                }}
                              />
                              <ButtonModalAction
                                className="mr-2"
                                actions={() => {
                                  setModalAction("REPLACE");
                                  setPolicyEditDetails({
                                    policyToEditIndex: index,
                                    policyToEdit: item,
                                  });
                                  toggleModal(true);
                                }}
                                tooltipText="Renew"
                                actionName="renew"
                              /> */}
                          </div>
                        </div>
                      </Accordion.Button>
                      <Accordion.Body className="p-0">
                        <CoverageItem
                          item={item}
                          isProviderSelected={
                            (insuredByBroker && !isEmpty(broker)) ||
                            (!insuredByBroker && !isEmpty(carrier))
                          }
                        />
                      </Accordion.Body>
                    </Accordion.Item>
                  );
                })}
            </div>
          </Accordion>
        )}
      />
    </Fragment>
  );
};

export default RepeaterPolicyCoverages;
