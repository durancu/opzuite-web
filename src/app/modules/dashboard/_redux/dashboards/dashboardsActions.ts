import { PayloadAction } from "@reduxjs/toolkit";
import { Dispatch } from "redux";
import { variantAccordingToHttpCode } from "../../../../components/Feedback";
import { feedbacksSlice } from "../../../../components/Feedback/_redux/feedbackSlice";
import * as requestFromServer from "./dashboardCrud";
import { callTypes, dashboardsSlice } from "./dashboardsSlice";

const { actions } = dashboardsSlice;

export const fetchDashboards =
  (queryParams: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.list } as any));
    return requestFromServer
      .findDashboards(queryParams)
      .then((response) => {
        const { totalCount, entities } = response.data;
        dispatch(actions.dashboardsFetched({ totalCount, entities } as any));
      })
      .catch((error) => {
        error.clientMessage = "Can't find dashboards";
        dispatch(
          actions.catchError({ error, callType: callTypes.list } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const fetchDashboard =
  (code: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> | any => {
    if (!code) {
      return dispatch(
        actions.dashboardFetched({ dashboardForEdit: undefined } as any)
      );
    }

    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .getDashboardById(code)
      .then((response) => {
        const dashboard = response.data;
        dispatch(
          actions.dashboardFetched({ dashboardForEdit: dashboard } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't find dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const fetchDashboardChartData =
  (code: string, params: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .getDashboardChartData(code, params)
      .then((response) => {
        const chart = response.data;
        dispatch(actions.dashboardCharDataFetched({ chart: chart } as any));
        return chart;
      })
      .catch((error) => {
        error.clientMessage = "Can't find dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const deleteDashboard =
  (code: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .deleteDashboard(code)
      .then((response) => {
        dispatch(actions.dashboardDeleted());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Dashboard deleted successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't delete dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const createDashboard =
  (dashboardForCreation: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .createDashboard(dashboardForCreation)
      .then((response) => {
        const dashboard = response.data;
        dispatch(actions.dashboardCreated());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Dashboard created successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
        return dashboard;
      })
      .catch((error) => {
        error.clientMessage = "Can't create dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };
export const resultDashboard =
  (queryParams: any, code: string) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .dashboardResults(queryParams, code)
      .then((response) => {
        const dashboard = response.data?.dashboard;
        dispatch(actions.dashboardResult({ dashboard } as any));
        return response.data;
      })
      .catch((error) => {
        error.clientMessage = "Can't load dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const updateDashboard =
  (dashboard: any) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .updateDashboard(dashboard)
      .then((response) => {
        dispatch(actions.dashboardUpdated());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Dashboard updated successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't update dashboard";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const updateDashboardsStatus =
  (ids: string[], status: string) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .updateStatusForDashboards(ids, status)
      .then(() => {
        dispatch(actions.dashboardsStatusUpdated({ ids, status } as any));
      })
      .catch((error) => {
        error.clientMessage = "Can't update dashboards status";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const deleteDashboards =
  (ids: string[]) =>
  (dispatch: Dispatch): Promise<PayloadAction | void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .deleteDashboards(ids)
      .then((response) => {
        dispatch(actions.dashboardsDeleted());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Dashboards deleted successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't delete dashboards";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };
