import { useGetCatalog } from "../Catalog/hooks";
import { FormAutoComplete } from "../forms/FormAutoComplete";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
  disabled?: boolean;
}

export function CustomerAutocompleteField({
  helperText,
  helperTextId = "FORM.LABELS.HELPER.CUSTOMER",
  labelTextId = "FORM.LABELS.CUSTOMER",
  fieldName = "customer",
  disabled = false,
}: Props) {
  const { customers } = useGetCatalog(false);

  return (
    <FormAutoComplete
      name={fieldName}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={customers || []}
      disabled={disabled}
    />
  );
}
