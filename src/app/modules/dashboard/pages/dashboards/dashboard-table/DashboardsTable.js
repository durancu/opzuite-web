import { useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import {
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  getHandlerTableChange,
  getSelectRow,
  sortCaret,
} from "src/app/components";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as filterActions from "src/app/components/Filter/_redux/filterActions";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import * as actions from "../../../_redux/dashboards/dashboardsActions";
import { useDashboardsUIContext } from "../DashboardsUIContext";
import * as uiHelpers from "../DashboardsUIHelpers";

export const DashboardsTable = () => {
  // Dashboards UI Context
  const {
    ids,
    setIds,
    queryParams,
    setQueryParams,
    openDetailDashboardPage,
    openEditDashboardPage,
    openDeleteDashboardDialog,
  } = useDashboardsUIContext();

  const dispatch = useDispatch();

  // Getting current state of dashboards list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.dashboards }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Dashboards Redux state
  useEffect(() => {
    // clear selections list
    setIds([]);
    // server call by queryParams
    dispatch(actions.fetchDashboards(queryParams));
    return () => {
      dispatch(filterActions.clearFilter());
    };
  }, [queryParams, dispatch, setIds]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(listLoading);
  }, [listLoading, setIsLoading]);

  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.DashboardNameColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: openDetailDashboardPage,
      },
    },
    /* {
      dataField: "description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
    }, */
    {
      dataField: "createdBy",
      text: "Author",
      sort: true,
      sortCaret: sortCaret,
      formatter: columnFormatters.AuthorNameColumnFormatter,
    },
    {
      dataField: "updatedAt",
      text: "Last Update",
      sortCaret: sortCaret,
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "scope",
      text: "Scope",
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      headerAlign: "right",
      align: "right",
      // formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditDashboardPage: openEditDashboardPage,
        openDeleteDashboardDialog: openDeleteDashboardDialog,
        openDetailDashboardPage: openDetailDashboardPage,
      },
      classes: "text-right pr-0",
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={[{ dataField: "soldAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(setQueryParams)}
                selectRow={getSelectRow({
                  entities,
                  ids: ids,
                  setIds: setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
};
