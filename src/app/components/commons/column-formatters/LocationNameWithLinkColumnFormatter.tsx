import { Link } from "react-router-dom";

export function LocationNameWithLinkColumnFormatter(locationName: any, location: any) {
  return (
    <>
      <Link
        className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
        to={`/manager/locations/${location?.code}/detail`}
      >
        {location.name}
      </Link>
    </>
  );
}
