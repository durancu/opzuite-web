export const AmountFooterTotal = (columnData: any) =>
  columnData.reduce((acc: any, item: any) => acc + (item || 0), 0);
