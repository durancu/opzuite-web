import { FormHelperText } from "@mui/material";
import { useIntl } from "react-intl";
import { DatePickerCustom } from "../../partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "../../partials/controls/forms/DateRange/dateFactory";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName: string;
  disabled?: boolean;
}

export function DatePickerField({
  helperText,
  helperTextId,
  labelTextId = "DEFAULT",
  labelText,
  fieldName,
  disabled = false,
}: Props) {
  const intl = useIntl();

  return (
    <>
      <DatePickerCustom
        name={fieldName}
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
        format={DEFAULT_DATEPICKER_FORMAT}
        disabled={disabled}
      />
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
