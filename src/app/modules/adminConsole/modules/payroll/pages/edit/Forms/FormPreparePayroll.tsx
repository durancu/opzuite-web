import { Field, useFormikContext } from "formik";
import moment from "moment";
import { Fragment, useEffect, useState } from "react";
import { Col, Row, Form } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import * as payrollActions from "../../../_redux/payrollsActions";

const FormPreparePayroll = () => {
  const { values, setFieldValue, errors } = useFormikContext<any>();
  const [payPeriod, setPayPeriod] = useState<number>(-1);

  const dispatch = useAppDispatch();

  const { currentState, payrollAvailableDates } = useSelector(
    (state: any) => ({
      currentState: state.catalogs,
      payrollAvailableDates: state.payrolls.payrollAvailableDates,
    }),

    shallowEqual
  );
  const { entities /* listLoading */ }: any = currentState;

  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  useEffect(() => {
    setPayPeriod(-1);
    values.location &&
      dispatch(payrollActions.getPayrollAvailableDates(values.location));
  }, [dispatch, values.location, setPayPeriod]);

  return (
    <Fragment>
      <Row>
        <Col>
          <Form.Group>
            <Form.Label>Location</Form.Label>
            <Field as={Form.Select} name="location">
              {entities === null ? (
                <option>No location</option>
              ) : (
                entities.locations.map(({ business, id }: any) => (
                  <option key={id} value={id}>
                    {business["name"]}
                  </option>
                ))
              )}
            </Field>
          </Form.Group>
        </Col>
        <Col
          hidden={
            !values.location ||
            !payrollAvailableDates ||
            payrollAvailableDates.length === 0
          }
        >
          <Form.Group>
            <Form.Label htmlFor="payPeriod">Pay Period</Form.Label>
            <Field
              as={Form.Select}
              name="payPeriod"
              value={payPeriod}
              disabled={!values.location}
              isInvalid={
                errors.payPeriodStartedAt || errors.payPeriodEndedAt
                  ? true
                  : false
              }
              onChange={(e: any) => {
                setPayPeriod(parseFloat(e.target.value));
                if (e.target.value > -1) {
                  setFieldValue(
                    "payPeriodStartedAt",
                    payrollAvailableDates[e.target.value]["payPeriodStartedAt"]
                  );
                  setFieldValue(
                    "payPeriodEndedAt",
                    payrollAvailableDates[e.target.value]["payPeriodEndedAt"]
                  );
                  dispatch(
                    payrollActions.initializePayroll(
                      payrollAvailableDates[e.target.value]
                    )
                  );
                }
              }}
            >
              <option value="-1">Choose a payroll date range</option>
              {payrollAvailableDates &&
                payrollAvailableDates.map(
                  (
                    { payPeriodEndedAt, payPeriodStartedAt }: any,
                    index: any
                  ) => (
                    <option key={index} value={index}>
                      {`${moment(payPeriodStartedAt).format(
                        "MM/DD/yyyy"
                      )} to ${moment(payPeriodEndedAt).format("MM/DD/yyyy")}`}
                    </option>
                  )
                )}
            </Field>
            <Form.Text className="d-block text-danger mt-1">
              <>{errors.payPeriodStartedAt || errors.payPeriodEndedAt}</>
            </Form.Text>
          </Form.Group>
        </Col>
      </Row>
      <Row
        hidden={
          !values.location ||
          (payrollAvailableDates && payrollAvailableDates.length !== 0)
        }
      >
        <span className="text-danger">
          <small>
            No available date ranges for this location. <br />
            Go to Payrolls table page and select the one you want to edit.
          </small>
        </span>
      </Row>
    </Fragment>
  );
};

export default FormPreparePayroll;
