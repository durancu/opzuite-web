import { isEqual, isFunction } from "lodash";
import { createContext, useCallback, useContext, useState } from "react";
import { BASE_FILTER_OPTIONS } from "src/app/constants/tables";

interface TableMenuActions {
  edit: (code: string, type?: any) => void;
  details: (code: string) => void;
  delete: (code: string) => void;
  renew?: (code: string) => void;
  certificates?: (code: string) => void;
  preview?: (code: string) => void;
  changePassword?: (id: string) => void;
  changeIP?: (code: string) => void;
}

interface Props {
  children: any;
  menuActions?: TableMenuActions | Record<string, any>;
  defaultFilter?: QueryParams;
  context?: string;
}

const FilterUIContext = createContext({
  ids: [] as string[],
  setIds: (value: any) => value,
  context: "",
  showPanel: false,
  togglePanel: (toggle: boolean) => toggle,
  queryParams: {} as QueryParams,
  updateQueryParams: (value: any) => value,
  menuActions: {} as TableMenuActions,
  defaultFilter: {} as Record<string, QueryParams>,
});

export const useFilterUIContext = () => useContext(FilterUIContext);

export const FilterUIProvider = ({
  context,
  menuActions = {},
  defaultFilter = BASE_FILTER_OPTIONS,
  children,
}: Props) => {
  const [ids, setIds] = useState([]);
  const [showPanel, setshowPanel] = useState(false);
  const [queryParams, setQueryParams] = useState(defaultFilter);

  const togglePanel = (hide: boolean) => setshowPanel(hide);

  const updateQueryParams = useCallback((params: any) => {
    setQueryParams((current: any) => {
      if (isFunction(params)) {
        params = params(current);
      }
      if (isEqual(current, params)) {
        return current;
      }
      return params;
    });
  }, []);

  const value: any = {
    ids,
    setIds,
    context,
    menuActions,
    showPanel,
    togglePanel,
    queryParams,
    updateQueryParams,
    defaultFilter,
  };

  return (
    <FilterUIContext.Provider value={value}>
      {children}
    </FilterUIContext.Provider>
  );
};
