import { Dispatch } from "redux";
import { variantAccordingToHttpCode } from "src/app/components/Feedback";
import { feedbacksSlice } from "src/app/components/Feedback/_redux/feedbackSlice";
import * as requestFromServer from "./usersCrud";
import { callTypes, usersSlice } from "./usersSlice";

const { actions } = usersSlice;

export const fetchAgents = (queryParams: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.list } as any));
  return requestFromServer
    .findAgents(queryParams)
    .then((response) => {
      const { totalCount, entities } = response.data;
      dispatch(actions.usersFetched({ totalCount, entities } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find users";

      dispatch(
        actions.catchError({
          payload: {
            error,
            callType: callTypes.list,
          },
        } as any)
      );

      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          payload: {
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          },
        } as any)
      );
    });
};

export const fetchUser = (id: any) => (dispatch: Dispatch) => {
  if (!id) {
    return dispatch(actions.userFetched({ userForEdit: undefined } as any));
  }

  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getUserById(id)
    .then((response) => {
      const user = response.data;
      dispatch(actions.userFetched({ userForEdit: user } as any));
      return user;
    })
    .catch((error) => {
      error.clientMessage = "Can't find user";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const deleteUser = (id: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deleteUser(id)
    .then((response) => {
      dispatch(actions.userDeleted({ id } as any));
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Agent deleted successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete user";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const createUser = (userForCreation: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .createUser(userForCreation)
    .then((response) => {
      //const { user } = response.data;
      dispatch(actions.userCreated());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Agent created successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't create user";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const updateUser = (user: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .updateUser(user)
    .then((response) => {
      dispatch(actions.userUpdated({ user } as any));
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Agent updated successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't update user";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const updateAgentsStatus =
  (ids: any, status: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .updateStatusForAgents(ids, status)
      .then((response) => {
        dispatch(actions.usersStatusUpdated({ ids, status } as any));
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Employees deleted successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't update users status";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const deleteAgents = (ids: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deleteAgents(ids)
    .then((response) => {
      dispatch(actions.usersDeleted({ ids } as any));
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Employees deleted successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete employees";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};
