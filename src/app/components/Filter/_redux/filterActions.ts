import { Dispatch } from "redux";
import * as requestFromServer from "./filterCrud";
import { callTypes, filtersSlice } from "./filterSlice";

const { actions } = filtersSlice;

export const getAllFilter = () => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getAllFilters()
    .then((response) => {
      const filter = response.data;
      dispatch(actions.filtersGetAll(filter));
    })
    .catch((error) => {
      error.clientMessage = "Can't find filter";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};
export const setFilter = (objectFilter: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return dispatch(
    actions.filtersSetAll({ filterForEdit: objectFilter } as any)
  );
};
export const clearFilter = () => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return dispatch(actions.filtersClearAll({ filterForEdit: undefined } as any));
};
export const setDashboardParams = (params: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return dispatch(actions.setDashboardParams(params));
};
