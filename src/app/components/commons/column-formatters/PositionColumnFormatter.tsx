import { Fragment } from "react";

export const PositionColumnFormatter = (position: any): any => (
    <Fragment>
        <span className="text-dark fw-semibold fs-6">
            {position}
        </span>
    </Fragment>
);
