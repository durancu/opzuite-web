// generated with the help of https://app.quicktype.io/
export interface Profile {
  _id: string | undefined;
  __v: number;
  address: Address;
  communication: Communication;
  company: any;
  createdAt: Date;
  deleted: boolean;
  dob: Date;
  email: string;
  emailSettings: EmailSettings;
  employeeInfo: EmployeeInfo;
  firstName: string;
  gender: string;
  language: string;
  lastName: string;
  mobilePhone: string;
  phone: string;
  role: any;
  startedAt: Date;
  supervisor: string;
  timezone: string;
  updatedAt: Date;
  username: string;
  permissions: string[];
  country?: string;
  createdBy: string;
  name: string;
  preferences: Preferences;
  code: string;
  updatedBy: string;
  type: string;
  status: string;
  authorizedIpAddresses: any[];
  confirmToken: string;
  passwordToken: string;
  id: string;
  profilePhoto: string;
  location: any;
}

export interface Address {
  address1: string;
  address2: string;
  city: string;
  county: string;
  state: string;
  country: string;
  zip: string;
  _id: string;
}

export interface Communication {
  // _id: string;
  email: boolean;
  phone: boolean;
  sms: boolean;
}

export interface EmailSettings {
  _id: string;
  emailNotification: boolean;
  sendCopyToPersonalEmail: boolean;
}

export interface EmployeeInfo {
  payFrequency: string;
  payRate: number;
  bonusFrequency: string;
  saleCommissionPlans: any[];
  allowedLocations: any[];
  allowedCountries: any[];
  _id: string;
  code: string;
  overtimeAuthorized: boolean;
  overtimePayRate: number;
  position: string;
  hourlyRate: number;
  category: string;
}

export interface Preferences {
  dashboard: Dashboard;
}

export interface Dashboard {
  layout: Layout;
}

export interface Layout {
  top: Main[];
  main: Main[];
}

export interface Main {
  type: string;
  content: string;
  columnsWidth: number;
}
