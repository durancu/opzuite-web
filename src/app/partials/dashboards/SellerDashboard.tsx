import { Col, Row } from "react-bootstrap";
import { TotalSalesBySellerWidget } from "../widgets";

export function SellerDashboard() {
  return (
    <>
      <Row>
        <Col sm="9">
          <Row>
            <Col>
              <TotalSalesBySellerWidget
                className="card-stretch card-stretch-half gutter-b"
                symbolShape="circle"
                baseColor="success"
                reportCode="1JI24s"
                title="Total Sales by this month"
              />
            </Col>
          </Row>
        </Col>
        <Col sm="3"></Col>
      </Row>
    </>
  );
}
