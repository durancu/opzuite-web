/* eslint-disable no-script-url */

import { Permissions } from "src/app/constants/Permissions";
import { TableActionsMenu } from "../../TableActionsMenu";


export const ActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openEditPermitPage, openDeletePermitDialog, openDetailPermitPage }: any
) => (
  <TableActionsMenu
    readFunction={openDetailPermitPage}
    editFunction={openEditPermitPage}
    deleteFunction={openDeletePermitDialog}
    editPermission={Permissions.APPLICATIONS_UPDATE}
    readPermission={Permissions.APPLICATIONS_READ}
    deletePermission={Permissions.APPLICATIONS_DELETE}
    code={row.code}
  />
);
