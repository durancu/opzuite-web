// routes module, to be imported in src/Routes
import { RouteObject } from "react-router-dom";
import { ServiceDetailPage } from "./pages/ServiceDetailPage";
import { ServiceEdit } from "./pages/service-edit/ServiceEdit";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants";
import { Services } from "./pages/Services";
import { ServiceDeleteDialog } from "./pages/ServiceDeleteDialog";

export const serviceRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.APPLICATIONS_READ}>
        <Services />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.APPLICATIONS_CREATE}>
        <ServiceEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.APPLICATIONS_UPDATE}>
        <ServiceEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.APPLICATIONS_READ}>
        <ServiceDetailPage />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <ServiceDeleteDialog />,
  },
];
