import { Card, CardContent } from "@mui/material";
import { camelCase, startCase } from "lodash";
import { Col, Row } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

interface Props {
  company: any;
}

export const CompanyOverview = (props: Props) => {
  const { company } = props;

  return (
    <Card
      className="mb-4 p-4 overview-section"
      style={{ background: "#f6f6f6" }}
    >
      <CardContent>
        <Row className="mb-0">
          <Col>
            <h3 className="card-title align-items-start flex-column">
              <span className="card-label font-weight-bold">
                <FormattedMessage id="GENERAL.HEADERS.OVERVIEW" />
              </span>
            </h3>
          </Col>
        </Row>
        <Row className="my-0 text-center">
          <Col>
            <Row>
              <Col className="field-value">
                <span>{company && company.name}</span>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col className="field-label">
                <FormattedMessage id="FORM.LABELS.NAME" />
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col className="field-value">
                <span>{company && startCase(camelCase(company.type))}</span>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col className="field-label">
                <FormattedMessage id="FORM.LABELS.TYPE" />
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col className="field-value">
                <span>{(company && company.email) || "-"}</span>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col className="field-label">
                <FormattedMessage id="FORM.LABELS.EMAIL" />
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col className="field-value">
                <span>{company && (company.primaryPhone || "-")}</span>
              </Col>
            </Row>
            <Row className="mt-3">
              <Col className="field-label">
                <FormattedMessage id="FORM.LABELS.PHONE_NUMBER" />
              </Col>
            </Row>
          </Col>
        </Row>
      </CardContent>
    </Card>
  );
};
