import { useState } from "react";
import clsx from "clsx";
import { useFormik } from "formik";
import { FormattedMessage, useIntl } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { authAPI } from "../redux";
import { Alert } from "react-bootstrap";
import { useAppDispatch } from "src/app/hooks";
import { catalogAPI } from "src/app/components/Catalog";
import { useSnackbar } from "notistack";

/*
  Formik+YUP+Typescript:
  https://jaredpalmer.com/formik/docs/tutorial#getfieldprops
  https://medium.com/@maurice.de.beijer/yup-validation-and-typescript-and-formik-6c342578a20e
*/

export const Login = () => {
  const intl = useIntl();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const [toggle, setToggle] = useState(false);
  const [login, { isLoading, isError }] = authAPI.useLoginMutation();

  const loginSchema = Yup.object().shape({
    email: Yup.string()
      .email("Wrong email format")
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
    password: Yup.string()
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: loginSchema,
    onSubmit: async (values) => {
      try {
        await login(values).unwrap();
        await dispatch(authAPI.endpoints.getUser.initiate());
        await dispatch(catalogAPI.endpoints.getAllCatalogs.initiate());
        /*
         * authToken and catalogs will be automagically added in store
         * https://redux-toolkit.js.org/rtk-query/usage/examples#using-extrareducers
         */
        navigate("/");
      } catch (error) {
        enqueueSnackbar("An error occured, please try again", {
          variant: "error",
        });
      }
    },
  });

  return (
    <form
      className="form w-100"
      onSubmit={formik.handleSubmit}
      noValidate
      id="kt_login_signin_form"
    >
      {/* begin::Heading */}
      <div className="text-center mb-11">
        <h1 className="text-dark fw-bolder mb-3">
          <FormattedMessage id="AUTH.LOGIN.TITLE" />
        </h1>
        <div className="text-gray-500 fw-semibold fs-6">
          <FormattedMessage id="AUTH.LOGIN.ENTER_CREDENTIALS" />
        </div>
      </div>
      {/* begin::Heading */}
      {isError && (
        <Alert show={isError} variant="danger">
          <Alert.Heading as="h2">Something went wrong</Alert.Heading>
          <p className="my-4">
            <FormattedMessage id="AUTH.VALIDATION.INVALID_LOGIN" />
          </p>
        </Alert>
      )}
      {/* begin::Form group */}
      <div className="fv-row mb-8">
        <label className="form-label fs-6 fw-bolder text-dark">Email</label>
        <input
          placeholder="Email"
          {...formik.getFieldProps("email")}
          className={clsx(
            "form-control bg-transparent",
            { "is-invalid": formik.touched.email && formik.errors.email },
            {
              "is-valid":
                formik.touched.email && !formik.errors.email && !isError,
            }
          )}
          type="email"
          name="email"
          autoComplete="off"
        />
        {formik.touched.email && formik.errors.email && (
          <div className="fv-plugins-message-container">
            <span role="alert">{formik.errors.email}</span>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Form group */}
      <div className="fv-row mb-3">
        <label className="form-label fw-bolder text-dark fs-6 mb-0">
          Password
        </label>
        <div className="position-relative">
          <input
            type={toggle ? "text" : "password"}
            autoComplete="off"
            {...formik.getFieldProps("password")}
            className={clsx(
              "form-control bg-transparent",
              {
                "is-invalid": formik.touched.password && formik.errors.password,
              },
              {
                "is-valid":
                  formik.touched.password &&
                  !formik.errors.password &&
                  !isError,
              }
            )}
          />

          <button
            type="button"
            className={clsx(
              "btn position-absolute top-0 bottom-0 end-0 d-flex align-items-center p-0 me-2",
              {
                "me-10":
                  (formik.touched.password && !isError) ||
                  formik.errors.password,
              }
            )}
            onClick={() => setToggle(!toggle)}
          >
            <i
              className={clsx("fs-1 bi", toggle ? "bi-eye" : "bi-eye-slash")}
            ></i>
          </button>
        </div>

        {formik.touched.password && formik.errors.password && (
          <div className="fv-plugins-message-container">
            <div className="fv-help-block">
              <span role="alert">{formik.errors.password}</span>
            </div>
          </div>
        )}
      </div>
      {/* end::Form group */}

      {/* begin::Wrapper */}
      <div className="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-8">
        <div />

        {/* begin::Link */}
        <Link to="/auth/forgot-password" className="link-primary">
          {<FormattedMessage id="AUTH.GENERAL.FORGOT_BUTTON" />}
        </Link>
        {/* end::Link */}
      </div>
      {/* end::Wrapper */}

      {/* begin::Action */}
      <div className="d-grid mb-10">
        <button
          type="submit"
          id="kt_sign_in_submit"
          className="btn btn-primary"
          disabled={formik.isSubmitting}
        >
          {!isLoading ? (
            <span className="indicator-label">
              <FormattedMessage id="AUTH.GENERAL.SIGN_IN" />
            </span>
          ) : (
            <span className="indicator-progress" style={{ display: "block" }}>
              Please wait...
              <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
            </span>
          )}
        </button>
      </div>
      {/* end::Action */}
    </form>
  );
};
