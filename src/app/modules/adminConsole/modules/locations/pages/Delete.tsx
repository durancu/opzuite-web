import { useEffect } from "react";
import { Modal, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import * as actions from "../_redux/locationsActions";

export const Delete = () => {
  const { code } = useParams();
  const navigate = useNavigate();

  // Locations Redux state
  const dispatch = useAppDispatch();
  const isLoading = useAppSelector(
    (state: any) => state.locations.actionsLoading
  );

  function goBack() {
    navigate("/manager/locations");
  }

  // if !code we should close modal
  useEffect(() => {
    if (!code) {
      goBack();
    }
  }, [code]);

  // looking for loading/dispatch

  const deleteLocation = () => {
    // server request for deleting location by code
    dispatch(actions.deleteLocation(code)).then(() => {
      // closing delete modal
      goBack();
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete location"
      centered
      backdrop="static"
    >
      {isLoading && <ProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="delete location">Location Delete</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this location?</span>
        )}
        {isLoading && <span>Location is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={goBack}
            className="btn btn-light btn-elevate"
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteLocation}
            className="btn btn-danger btn-elevate"
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
