import { FormikValues } from "formik";

export const FinancerStatusCssClasses = ["success", "info", ""];
export const FinancerStatusTitles = ["Selling", "Sold"];
export const FinancerConditionCssClasses = ["success", "danger", ""];
export const FinancerConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "code", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

export const defaultFilter = {
  filter: { type: "FINANCER" },
  searchText: "",
  sortOrder: "asc",
  sortField: "name",
  pageNumber: 1,
  pageSize: 20,
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "POLICY",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchText: values.searchText,
    sortOrder: "asc",
    sortField: "name",
    pageNumber: 1,
    pageSize: 20,
  };
};
