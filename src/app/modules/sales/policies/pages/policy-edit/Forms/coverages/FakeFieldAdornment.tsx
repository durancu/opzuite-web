import { Fragment } from "react";

import { WarningOutlined } from "@material-ui/icons";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

interface Props {
  hidden?: boolean;
  tooltipText?: string;
  type?: string;
}

export const FakeFieldAdornment = ({ hidden, tooltipText, type }: Props) => {
  return (
    <div className="flex-column ml-0">
      <span hidden={hidden} style={{ maxWidth: "30px" }}>
        {tooltipText && tooltipText.length > 0 && (
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="layout-tooltip" hidden={Boolean(tooltipText)}>
                {tooltipText}
              </Tooltip>
            }
          >
            <Fragment>
              {(!type || type === "warning") && (
                <WarningOutlined
                  color="secondary"
                  style={{ fontSize: 14, marginTop: "-3px" }}
                />
              )}
            </Fragment>
          </OverlayTrigger>
        )}
      </span>
    </div>
  );
};
