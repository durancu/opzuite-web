//material
import { MenuItem } from "@mui/material";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

interface Props {
  labelField: string;
  name: string;
  label: string;
  options: any[];
  disabled?: boolean;
}
export function SelectField(props: Props) {
  const { labelField, name, label, options, disabled } = props;

  return (
    <>
      <Field
        name={name}
        label={label}
        component={TextField}
        select={true}
        disabled={disabled}
      >
        {options.map((item: any) => (
          <MenuItem key={item.id} value={item.id}>
            {item[labelField]}
          </MenuItem>
        ))}
      </Field>
    </>
  );
}
