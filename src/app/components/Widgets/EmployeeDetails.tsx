import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { AddressBlock } from "./AddressBlock";
import { toAbsoluteUrl } from "src/_metronic/helpers";

interface Props {
  employee: any;
  catalogs: any;
}

//TODO: Use Memo
export const languages: any = {
  en: {
    name: "English",
    flag: toAbsoluteUrl("/media/svg/flags/226-united-states.svg"),
  },
  es: {
    lang: "es",
    name: "Spanish",
    flag: toAbsoluteUrl("/media/svg/flags/128-spain.svg"),
  },
};

export const EmployeeDetails = ({ employee }: Props) => {
  const { currentState } = useSelector(
    (state: any) => ({ currentState: state.catalogs }),
    shallowEqual
  );

  const { entities }: any = currentState;

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.DETAILS" />
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>{employee && <AddressBlock address={employee?.address} />}</h4>
            <FormattedMessage id="FORM.LABELS.ADDRESS" />
          </Col>
          <Col>
            <h4>
              {employee &&
                (entities?.roles.find(({ id }: any) => id === employee.role)
                  ?.name ||
                  "-")}
            </h4>
            <FormattedMessage id="FORM.LABELS.ROLE" />
          </Col>
          <Col>
            <h4>{employee && (employee.email || "-")}</h4>
            <FormattedMessage id="FORM.LABELS.EMAIL" />
          </Col>
          <Col>
            <h4>{employee && (employee.phone || "-")}</h4>
            <FormattedMessage id="FORM.LABELS.PHONE_NUMBER" />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
