import { useIntl } from "react-intl";
import { FormSelect } from "../forms";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function PolicyStatusSelectField({
  labelText,
  helperText,
  helperTextId = "FORM.LABELS.HELPER.STATUS",
  labelTextId = "FORM.LABELS.STATUS",
  fieldName = "status",
}: Props) {
  const intl = useIntl();
  const status: any = intl.messages["POLICY_STATUS_CATALOG"];

  return (
    <FormSelect
      name={fieldName}
      options={status}
      labelText={labelText}
      labelTextId={labelTextId}
      helperText={helperText}
      helperTextId={helperTextId}
    />
  );
}
