export const AUTO_ANNUAL_MILEAGE_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "0-7500", name: "0 - 7,500 miles" },
  { id: "7500-10000", name: "7,500 - 10,000 miles" },
  { id: "10000-15000", name: "10,000 - 15,000 miles" },
  { id: "15000+", name: "15,000+ miles" },
];
