import { useGetCatalog } from "../Catalog/hooks";
import { FormAutoComplete } from "../forms";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
  className?: string;
}

export function StatesAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.STATE",
  labelText,
  fieldName = "state",
  className,
}: Props) {
  const { states }: any = useGetCatalog();

  return (
    <FormAutoComplete
      name={fieldName}
      className={className}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={states || []}
    />
  );
}
