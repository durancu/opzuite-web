import { Fragment } from "react";
import { CopyToClipboard } from "../../Widgets";

export const LinkToDetailWithClipboardColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openDetailPageFunction,
    idField,
    textField,
    defaultText = "",
    linkClass = "text-primary cursor-pointer",
    clipboardClass = "me-2",
    showClipboard = true,
  }: any
) => (
  <Fragment>
    {showClipboard && (
      <CopyToClipboard content={cellContent} className={clipboardClass} />
    )}
    <a
      className={linkClass}
      onClick={() => {
        openDetailPageFunction(row[idField]);
      }}
    >
      {row[textField] || defaultText}
    </a>
  </Fragment>
);
