import { Dispatch } from "redux";
import { calculatePolicyMetrics } from "../../../functions/calculatePolicyMetrics";
import { Permissions } from "src/app/constants";
import { isPermitted } from "src/app/utils";
import { callTypes, policyMetricsSlice } from "./policyMetricsSlice";

const { actions } = policyMetricsSlice;

export const calcPolicyMetrics =
  (endorsements: any, user?: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    try {
      const calculatedMetrics = calculatePolicyMetrics(endorsements);
      const metrics = [
        { title: "CHECKSUM SANITY", value: calculatedMetrics.checksumSanity },
        { title: "PREMIUM", value: calculatedMetrics.totalPremium },
        { title: "NON PREMIUM", value: calculatedMetrics.totalNonPremium },
        { title: "TAXES AND FEES", value: calculatedMetrics.totalTaxesAndFees },
        { title: "PAYABLES", value: calculatedMetrics.totalPayables },
        { title: "RECEIVABLES", value: calculatedMetrics.totalReceivables },
        { title: "FINANCED", value: calculatedMetrics.totalFinanced },
        {
          title: "AGENT COMMISSION",
          value: calculatedMetrics.totalAgentCommission,
        },
      ];

      if (isPermitted(Permissions.MY_COMPANY, user.permissions)) {
        metrics.push({
          title: "AGENCY COMMISSION",
          value: calculatedMetrics.totalAgencyCommission,
        });
      }

      dispatch(actions.setPolicyMetrics({ metrics: metrics } as any));
    } catch (error) {
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    }
  };
