export const ETHNICITY_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "HLSO", name: "Hispanic or Latin" },
  { id: "AIAN", name: "American Indian or Alaskan Native" },
  { id: "ASIAN", name: "Asian" },
  { id: "NHOPI", name: "Native Hawaii or Other Pacific Islander" },
  { id: "BLACK", name: "Black or African American" },
  { id: "WHITE", name: "White" },
  { id: "TWO-OR-MORE", name: " Two or more races" },
];
