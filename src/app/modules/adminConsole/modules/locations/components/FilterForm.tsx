import { useAppSelector } from "src/app/hooks";
import { FormSelect } from "src/app/components";

export const FilterForm = () => {
  const entities = useAppSelector((state: any) => state.catalogs?.entities);

  return (
    <FormSelect
      labelText="Country"
      name="country"
      options={entities?.locationAvailableCountries || []}
    />
  );
};
