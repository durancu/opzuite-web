import { camelCase, startCase } from "lodash";
import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

interface Props {
  mga: any;
}

export const MGAOverview = ({ mga }: Props) => {
  return (
    <Card className="mb-4 p-8 overview-section">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body>
        <Row className="text-center">
          <Col>
            <h4>{mga && startCase(camelCase(mga.type))}</h4>
            <span>Type</span>
          </Col>
          <Col>
            <h4>{mga && mga.name}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>{(mga && mga.email) || "-"}</h4>
            <span>Email</span>
          </Col>
          <Col>
            <h4>{mga && (mga.phone || "-")}</h4>
            <span>Phone number </span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
