export const LineOfBusinessColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { entities }: any
) => {
  const lineOfBusiness =
    entities?.linesOfBusiness.find((type: any) => type.id === cellContent)
      ?.name || "-";

  return <>{lineOfBusiness}</>;
};
