import {
  Autocomplete,
  AutocompleteProps,
  TextField,
  createFilterOptions,
} from "@mui/material";
import clsx from "clsx";
import { ChangeEvent, Fragment, useState } from "react";
import { Form } from "react-bootstrap";
import { SELECT_ALL } from "src/app/constants/General";

type Props = Omit<BaseInputProps, "name" | "onChange"> &
  Partial<AutocompleteProps<any, any, any, any>> & {
    onChange: (value: any) => void;
    value: any[];
    options: any[];
    labelText?: string;
    helperText?: string;
    error?: boolean;
    errorText?: any;
  };

const filter = createFilterOptions();
const filterOptions = (options: any, params: any): any => {
  const filtered = filter(options, params);
  return options.length > 1 ? [SELECT_ALL, ...filtered] : [...filtered];
};

export const VehiclesMultiselect = ({
  onChange,
  value,
  options,
  labelText,
  helperText,
  error,
  errorText,
  ...rest
}: Props) => {
  const [all, selectAll] = useState(false);

  function handleChange(_: any, value: any) {
    if (value.indexOf(SELECT_ALL) === -1) {
      onChange(value);
      selectAll(false);
    } else {
      onChange(options);
    }
  }

  function handleSelectAll(evt: ChangeEvent<HTMLInputElement>) {
    selectAll(evt.target.checked);
    if (!evt.target.checked) {
      onChange([]);
    } else {
      onChange(options);
    }
  }

  return (
    <Autocomplete
      {...rest}
      multiple
      disableCloseOnSelect
      options={options || []}
      filterOptions={filterOptions}
      isOptionEqualToValue={(option: any, value) =>
        (Array.isArray(value) ? value : [value]).find(
          (item): any => item === option
        )
      }
      getOptionLabel={(option: any) => {
        if (option) {
          if (option.name) {
            return option.name;
          }
          if (option.vinNumber) {
            return `VIN: ${option.vinNumber}`;
          } else if (option.licensePlate) {
            return `License: ${option.licensePlate}`;
          } else {
            return `${option.make} ${option.model} ${option.year} ${option.color}`;
          }
        } else {
          return "N/A";
        }
      }}
      onChange={handleChange}
      value={value}
      ListboxProps={{
        className:
          "list-group list-group-flush position-absolute z-index-2 w-100 mh-400px overflow-auto rounded border",
      }}
      renderOption={(props, option, state) => {
        const { make, model, year, color, vinNumber, licensePlate } = option;

        return (
          <li
            key={state.index}
            {...props}
            className={clsx(
              "list-group-item list-group-item-action p-3 cursor-pointer fw-normal d-flex gap-3",
              {
                "bg-light-dark": state.selected,
              }
            )}
          >
            {option.name === SELECT_ALL.name ? (
              <Fragment>
                <Form.Check checked={all} readOnly onChange={handleSelectAll} />
                <p className="mb-1 fw-bold">{option.name}</p>
              </Fragment>
            ) : (
              <Fragment>
                <Form.Check checked={state.selected} readOnly />
                <div>
                  <p className="mb-1 fw-bold text-gray-600">
                    {vinNumber && `VIN: ${vinNumber}`} &nbsp;
                    {licensePlate && `License: ${licensePlate}`}
                  </p>
                  <span className="fs-6 text-gray-600">
                    {make} {model} {year} {color}
                  </span>
                </div>
              </Fragment>
            )}
          </li>
        );
      }}
      renderInput={({ inputProps, InputProps, InputLabelProps }) => (
        <Form.Group>
          <Form.Label {...InputLabelProps}>{labelText}</Form.Label>
          <TextField
            fullWidth
            variant="outlined"
            error={error}
            inputProps={{
              ...inputProps,
              className: clsx(inputProps.className, "form-control"),
            }}
            InputProps={{
              ...InputProps,
              className: clsx(
                InputProps.className,
                "form-control border-0 p-2 rounded"
              ),
            }}
          />
          <Form.Text className="text-danger my-1 d-block">
            {error && errorText}
          </Form.Text>
          <Form.Text className="mt-1 d-block">{helperText}</Form.Text>
        </Form.Group>
      )}
    />
  );
};
