import { FormikValues } from "formik";
export const MGAStatusCssClasses = ["success", "info", ""];
export const MGAStatusTitles = ["Selling", "Sold"];
export const MGAConditionCssClasses = ["success", "danger", ""];
export const MGAConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "name", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

const searchColumns = ["email", "name", "phone"];

export const defaultFilter = {
  filter: {
    type: "BROKER",
    name: "",
  },
  sortOrder: "asc",
  sortField: "name",
  pageNumber: 1,
  pageSize: 20,
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "BROKER",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    sortOrder: "asc",
    sortField: "name",
    pageNumber: 1,
    pageSize: 20,
  };
};
