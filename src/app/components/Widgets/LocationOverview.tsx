import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";

interface Props {
  location: any;
}

export const LocationOverview = ({ location }: Props) => {
  const { catalogs } = useSelector(
    (state: any) => ({
      catalogs: state.catalogs.entities,
    }),
    shallowEqual
  );

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body>
        <Row className="text-center">
          <Col>
            <h4>{location && location.business.name}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>
              {location &&
                catalogs?.users?.find(({ id }: any) => id === location.manager)
                  ?.name}
            </h4>
            <span>Manager</span>
          </Col>
          <Col>
            <h4>{location && location.country}</h4>
            <span>Country</span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
