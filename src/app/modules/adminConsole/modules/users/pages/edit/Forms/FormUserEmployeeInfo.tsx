import { Field, useFormikContext } from "formik";
import { Fragment, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import {
  LocationsAutocompleteField,
  SupervisorsAutocompleteField,
} from "src/app/components/FormControls";
import { DatePickerCustom } from "src/app/partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";

import { FormInput } from "src/app/components";
import { Col, Row, Form } from "react-bootstrap";
const FormUserEmployeeInfo = () => {
  const { values, setFieldValue }: any = useFormikContext();

  const { currentState } = useSelector(
    (state: any) => ({
      currentState: state.catalogs,
    }),
    shallowEqual
  );

  const { entities }: any = currentState;

  useEffect(() => {
    if (entities) {
      if (!values.code) {
        if (!values.location && entities.locations) {
          setFieldValue("location", entities.locations[0]?.id);
        }
        if (!values.supervisor && entities.users) {
          setFieldValue("supervisor", entities.users[0]?.id);
        }
      }
    }
  }, [values, entities, setFieldValue]);
  return (
    <Fragment>
      <Row className="mb-4">
        <Col md={3}>
          <LocationsAutocompleteField
            fieldName="location"
            helperTextId="FORM.LABELS.HELPER.LOCATION"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="employeeInfo.position"
            labelTextId="FORM.LABELS.POSITION"
            helperTextId="FORM.LABELS.HELPER.POSITION"
          />
        </Col>
        <Col md={3}>
          <SupervisorsAutocompleteField fieldName="supervisor" />
        </Col>

        <Col md={3}>
          <FormInput
            name="employeeInfo.payRate"
            labelTextId="FORM.LABELS.SALARY.MONTHLY"
            helperTextId="FORM.LABELS.HELPER.SALARY.MONTHLY"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3} hidden>
          <Form.Group>
            <Form.Label>Pay Frequency</Form.Label>
            <Field as={Form.Select} name="employeeInfo.payFrequency" disabled>
              {entities === null ? (
                <option>No available options</option>
              ) : (
                entities.employeeRateFrequencies.map(({ name, id }: any) => (
                  <option key={id} value={id}>
                    {name}
                  </option>
                ))
              )}
            </Field>
          </Form.Group>
        </Col>
        <Col md={3}>
          <DatePickerCustom
            name="employeeInfo.startedAt"
            label="Hiring Date"
            format={DEFAULT_DATEPICKER_FORMAT}
            helperText="Date the employee started working for the company."
          />
        </Col>
        <Col md={3}>
          <DatePickerCustom
            name="employeeInfo.endedAt"
            label="Termination Date"
            format={DEFAULT_DATEPICKER_FORMAT}
            helperText="Date the employee stopped working for the company."
          />
        </Col>
      </Row>
      <Row hidden>
        <Col md={3}>
          <Form.Group>
            <label>Overtime Authorization</label>
            <br />
            <Field
              as={Form.Check}
              type="checkbox"
              name="employeeInfo.overtimeAuthorized"
              label="Is authorized to do overtime hours?"
            />
          </Form.Group>
        </Col>
        <Col md={3}>
          <div hidden={!values.employeeInfo.overtimeAuthorized}>
            <FormInput
              name="employeeInfo.overtimeRate"
              labelText="Pay Rate for Overtime hours"
            />
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

export default FormUserEmployeeInfo;
