import { Fragment } from "react";

export const CityColumnFormatter = (city: any): any => (
    <Fragment>
        <span className="text-dark fw-semibold fs-6">
            {city}
        </span>
    </Fragment>
);
