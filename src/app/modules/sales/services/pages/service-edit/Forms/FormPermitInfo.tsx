import { Fragment, useMemo, useEffect } from "react";
import { Col, Row } from "react-bootstrap";
import { useIntl } from "react-intl";
import { isNotEmpty } from "src/_metronic/helpers";
import { Toolbar } from "src/_metronic/layout/core";
import { FormAutoComplete, RequirePermission } from "src/app/components";
import { PolicyStatusSelectField } from "src/app/components/FormControls";
import { Permissions } from "src/app/constants";
import { useAppDispatch, useGetUserAuth } from "src/app/hooks";
import { DatePickerCustom } from "src/app/partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { isPermitted } from "src/app/utils";
import { saleActions } from "src/app/modules/sales/redux/saleSlice";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { useFormikContext } from "formik";
import { AddCustomerMenu } from "src/app/modules/sales/components/AddCustomerMenu";

const FormPermitInfo = () => {
  const intl = useIntl();
  const dispatch = useAppDispatch();
  const user = useGetUserAuth();
  const { users, customers } = useGetCatalog(false);
  const { values, setFieldValue } = useFormikContext();

  /*
   * optionally skip and create customer before resuming service creation
   * we save the service values in redux to be able to resume the service creation
   */
  const cacheService = () => {
    dispatch(
      saleActions.save({
        type: "savedService",
        values,
      })
    );
  };

  useEffect(() => {
    if (user && user.role.hierarchy > 30) {
      setFieldValue("seller", user.id);
    }
  }, [setFieldValue, user]);

  const filteredUsers = useMemo(() => {
    if (user && isNotEmpty(users)) {
      if (isPermitted(Permissions.MY_COUNTRY, user.permissions)) {
        return users.filter(({ country }: any) => country === user.country);
      } else if (isPermitted(Permissions.MY_LOCATION, user.permissions)) {
        return users.filter(
          ({ location }: any) => location === user.location.name
        );
      }
      return users;
    }
  }, [user, users]);

  return (
    <Fragment>
      <Toolbar>
        <AddCustomerMenu onAdd={cacheService} />
      </Toolbar>

      <Row className="form-group mb-9">
        <Col>
          <FormAutoComplete
            name="customer"
            labelTextId="FORM.LABELS.CUSTOMER"
            labelField="name"
            options={customers}
            helperTextId="FORM.LABELS.HELPER.CUSTOMER"
          />
        </Col>
        <Col>
          <DatePickerCustom
            label={intl.formatMessage({ id: "FORM.LABELS.SOLD_AT" })}
            name="soldAt"
            format={DEFAULT_DATEPICKER_FORMAT}
            maxDate={new Date()}
            required
            helperTextId="FORM.LABELS.HELPER.SALE_DATE"
          />
        </Col>
        <Col>
          <PolicyStatusSelectField />
        </Col>
        <RequirePermission
          permissions={[
            Permissions.MY_LOCATION,
            Permissions.MY_COUNTRY,
            Permissions.MY_COMPANY,
          ]}
        >
          <Col>
            <FormAutoComplete
              name="seller"
              labelTextId="FORM.LABELS.COORDINATOR"
              helperTextId="FORM.LABELS.HELPER.AGENT"
              labelField="name"
              options={filteredUsers}
            />
          </Col>
        </RequirePermission>
      </Row>
    </Fragment>
  );
};

export default FormPermitInfo;
