export const IDENTITY_CARD_TYPE_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "PASSPORT", name: "Passport" },
  { id: "STATE-DRIVER-LICENSE", name: "State driver's license" },
  {
    id: "CERTIFICATE-OF-NATURALIZATION",
    name: "Certificate of Naturalization",
  },
  { id: "CERTIFICATE-OF-CITIZENSHIP", name: "Certificate of Citizenship" },
  { id: "GOVERNMENT-EMPLOYEE-ID", name: "Government employee ID" },
  {
    id: "US-MILITARY-OR-DEPENDENT-ID",
    name: "U.S. military or military dependent ID",
  },
  { id: "MEXICAN-CONSULAR-ID", name: "Mexican Consular ID" },
  { id: "GREEN-CARD", name: "U.S. Permanent Resident Card (Green Card)" },
  { id: "TRUSTED-TRAVELER-ID", name: "Trusted Traveler ID" },
  { id: "ENHANCED-TRIBAL-CARDS", name: "Enhanced Tribal Cards" },
  { id: "OTHER", name: "Other" },
];
