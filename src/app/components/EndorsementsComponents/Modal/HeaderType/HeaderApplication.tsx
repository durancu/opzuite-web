import { ModalEndorsementActionType } from "../../EndorsementTypeAndEnum";
import { useEndorsementUIContext } from "../../EndorsementUIContext";

export const HeaderApplication = () => {
  const { indexEndorsement, cardTitle, endorsementActionType } =
    useEndorsementUIContext();
  return (
    <div
      className="px-6 py-4"
      style={{
        backgroundColor: `${indexEndorsement === 0 ? "#f3f9f3" : "#f0f0f0"}`,
      }}
    >
      <p className="fw-bold">
        {endorsementActionType === ModalEndorsementActionType.ADD
          ? "Add "
          : "Edit "}{" "}
        <strong>{`Service A${indexEndorsement + 1}`} </strong>
        {cardTitle ? " - ".concat(cardTitle) : ""}
      </p>
    </div>
  );
};
