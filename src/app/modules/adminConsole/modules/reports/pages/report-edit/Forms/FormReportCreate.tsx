import { Field, FieldArray, useFormikContext } from "formik";
import { Fragment, useEffect, useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import {
  FormAutoComplete,
  FormInput,
  FormMultiselect,
  FormSelect,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { getHierarchyRoleByUserAuth } from "src/app/components/functions";
import { useAppSelector } from "src/app/hooks";
import { DatePickerCustom } from "src/app/partials/controls";

import { userSelector } from "src/app/modules/auth/redux";

const isDisabledField = (option: any, hierarchy: number) => {
  const { field } = option;

  switch (hierarchy) {
    case 4:
      if (field === "country") return true;
      break;
    case 5:
      if (field === "country" || field === "location") return true;
      break;
    default:
      return false;
  }
  return false;
};

const FormReportCreate = () => {
  const { values, setFieldValue } = useFormikContext<any>();
  // roles: state.catalogs?.entities?.roles,

  const user = useAppSelector(userSelector);
  const catalog = useGetCatalog();

  //Disabled options in autoComplete
  const hierarchy = getHierarchyRoleByUserAuth(
    user?.employeeInfo?.category || '',
    catalog.roles
  );
  const filterValueDisable = values.params?.filters?.filter((option: any) =>
    isDisabledField(option, hierarchy)
  );
  //----
  const [isGroupedByInsurer, setIsGroupedByInsurer] = useState(false);

  useEffect(() => {
    const groupByModel = values.params?.groupByModel;
    if (groupByModel) {
      setIsGroupedByInsurer(["carrier", "broker"].includes(groupByModel));
    }
  }, [values.params?.groupByModel, isGroupedByInsurer]);

  useEffect(() => {
    const filters = values.params?.filters;
    let hasInsurerFilter = false;

    if (filters && filters.length > 0) {
      hasInsurerFilter = filters.find(
        (param: any) => param.field === "broker" || param.field === "carrier"
      );
    }

    if (hasInsurerFilter || isGroupedByInsurer) {
      setFieldValue("params.withCount", true);
      setFieldValue("params.fields", ["items.premium", "items.amount"]);
    } else {
      setFieldValue("params.fields", [
        "totalPremium",
        "totalNonPremium",
        "totalTaxesAndFees",
        "totalPayables",
        "totalReceivables",
        "totalFinanced",
        "totalAgentCommission",
      ]);
    }
  }, [values.params?.filters, isGroupedByInsurer, setFieldValue]);

  const [suggestedName, setSuggestedName] = useState<string>("");

  useEffect(() => {
    if (values.params) {
      let name = "";
      let model = "";
      let group = "";

      if (values.params.model) {
        switch (values.params.model) {
          case "sale":
          default:
            model = "sales";
            break;
        }
        name = "Total " + model;
      }

      if (values.params.groupByModel) {
        switch (values.params.groupByModel) {
          case "seller":
          default:
            group = "Employee";
            break;
        }
        group = " by " + group;
      }

      name = model + group;

      setSuggestedName(name);
    }
  }, [values.params]);

  return (
    <Fragment>
      <Row className="mb-4">
        <Col md={3}>
          <FormAutoComplete
            name="params.model"
            labelText="Source"
            options={catalog?.reportFilterModel || []}
            labelField="name"
            disabled
          />
        </Col>
        <Col md={3}>
          <FormAutoComplete
            disabled={!values.params?.model}
            name="params.groupByModel"
            labelText="Group by"
            options={
              catalog?.reportFilterModel.find(
                ({ id }: any) => id === values.params.model
              )?.groupByFields || []
            }
            labelField="name"
          />
        </Col>
        <Col md={3}>
          <DatePickerCustom label="Date from" name="params.dateFrom" />
        </Col>
        <Col md={3}>
          <DatePickerCustom label="Date to" name="params.dateTo" />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput
            labelText="Name"
            name="name"
            helperText={`Suggested: ${suggestedName}`}
          />
        </Col>
        <Col md={6}>
          <FormInput labelText="Description" name="description" />
        </Col>
        <Col md={3}>
          <Form.Group>
            <Form.Label>Scope</Form.Label>
            <Field as={Form.Select} name="scope">
              <option value="private">Private</option>
              <option value="public">Public</option>
            </Field>
          </Form.Group>
        </Col>
      </Row>
      <Row className="my-8">
        {catalog && (
          <FormMultiselect
            selectionKey="field"
            labelField="field"
            labelText="Filters"
            helperText="Set filters for more specific results"
            value={values.params.filters}
            disabled={!values?.params?.model}
            options={
              catalog?.reportFilterModel
                .find(({ id }: any) => id === values?.params?.model)
                ?.filterFields.map(({ id, name }: any) => ({
                  field: id,
                  operator: "=",
                  value: "",
                  name,
                })) || []
            }
            onChange={(_: any, value: any) => {
              setFieldValue("params.filters", [
                ...filterValueDisable,
                ...value.filter(
                  (option: any) => filterValueDisable.indexOf(option) === -1
                ),
              ]);
            }}
          />
        )}
      </Row>
      <Row className="mb-4">
        {catalog && values?.params?.filters && (
          <FieldArray
            name="params.filters"
            render={() => {
              return values.params.filters?.map(
                (filter: any, index: number) => {
                  const nameField = `params.filters.${index}`;
                  let options: any[] = [];
                  switch (filter.field) {
                    case "type":
                      if (values.params.model === "sale") {
                        options = catalog?.saleTypes;
                      } else if (values.params.model === "customer") {
                        options = catalog?.customerTypes;
                      }
                      break;
                    case "country":
                      options = catalog?.countries;
                      break;
                    case "location":
                      options = catalog?.locations;
                      break;
                    case "carrier":
                      options = catalog?.carriers;
                      break;
                    case "broker":
                      options = catalog?.brokers;
                      break;
                    case "customer":
                      options = catalog?.customers;
                      break;
                    case "seller":
                      options = catalog?.sellers;
                      break;
                    case "status":
                      options = catalog?.saleStatus;
                      break;
                    default:
                      break;
                  }

                  return (
                    <Row key={index} className="mb-4">
                      <Col>
                        <h6>Field</h6>
                        <p>{values.params.filters[index].field}</p>
                      </Col>
                      <Col>
                        <FormInput
                          name={`${nameField}.operator`}
                          labelText="Operator"
                          disabled
                        />
                      </Col>
                      <Col>
                        <FormSelect
                          name={`${nameField}.value`}
                          labelText="Value"
                          options={options || []}
                          disabled={((): boolean => {
                            if (hierarchy === 4 || hierarchy === 5) {
                              if (
                                filter.field === "location" ||
                                filter.field === "country"
                              )
                                return true;
                            }
                            return false;
                          })()}
                        />
                      </Col>
                    </Row>
                  );
                }
              );
            }}
          />
        )}
      </Row>
    </Fragment>
  );
};

export default FormReportCreate;
