import { Fragment, useEffect, useMemo } from "react";
import { shallowEqual, useSelector } from "react-redux";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import { useAppDispatch } from "src/app/hooks";
import { FormAutoComplete } from "src/app/components";
import { Permissions } from "src/app/constants";
import { isPermitted } from "src/app/utils";

export const FilterForm = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  const { entities, user } = useSelector(
    (state: any) => ({
      entities: state.catalogs?.entities,
      user: state.auth.user,
    }),
    shallowEqual
  );

  const locations = useMemo(() => {
    if (entities?.locations) {
      if (isPermitted(Permissions.MY_COMPANY, user.permissions)) {
        return entities.locations;
      } else if (isPermitted(Permissions.MY_COUNTRY, user.permissions)) {
        return entities.locations.filter(
          (location: any) => location.country === user.country
        );
      } else if (isPermitted(Permissions.MY_LOCATION, user.permissions)) {
        return entities.locations.filter(
          (location: any) => location.id === user.location
        );
      }
    }
  }, [user, entities, isPermitted]);

  return (
    <Fragment>
      <FormAutoComplete
        name="location"
        labelTextId="FORM.LABELS.LOCATION"
        options={locations || []}
      />
      <br />
      <FormAutoComplete
        name="periodCode"
        labelTextId="FORM.LABELS.PAY_PERIOD"
        options={entities?.availablePayPeriods || []}
      />
    </Fragment>
  );
};
