/* eslint-disable no-script-url */
import { FormattedMessage } from "react-intl";

export const UpdatedByColumnHeaderFormatter = (
  column: any,
  colIndex: any,
  { sortElement }: any
) => (
  <div className="d-flex gap-2 align-items-center">
    <FormattedMessage id="TABLE.HEADER.UPDATED_BY" />
    {sortElement}
  </div>
);
