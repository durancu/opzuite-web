import * as Yup from "yup";

export const permitBasicInfoValidateSchema = Yup.object().shape({
  type: Yup.string(),
  customer: Yup.string().required("Customer is required."),
  soldAt: Yup.date()
    .required("Service Date is required.")
    .typeError("Service Date is required."),
  status: Yup.string().required("Status is required."),
  chargesPaid: Yup.number(),
  fees: Yup.number(),
  permits: Yup.number(),
  premium: Yup.number(),
  seller: Yup.string().nullable(), //remove hard requirement as it breaks if user role is missing
  tips: Yup.number(),
  downPayment: Yup.number(),
});

const endorsementItemSchema = Yup.object().shape({
  type: Yup.mixed().required("Type is required."),
  amount: Yup.number().required("Amount is required.").default(0),
  status: Yup.string(),
  description: Yup.string(),
  financerCompany: Yup.string(),
  endorsedAt: Yup.date().typeError("Please enter a valid Date"),
  followUpDate: Yup.date().typeError("Please enter a valid Date").nullable(),
  followUpPerson: Yup.string().nullable(),
  accountingClass: Yup.string(),
});

const endorsementSchema = Yup.object().shape({
  type: Yup.mixed().required("Type is required."),
  amount: Yup.number().required("Amount is required.").default(0),
  description: Yup.string(),
  endorsedAt: Yup.date()
    .required("Date is required.")
    .typeError("Please enter a valid Date"),
  followUpDate: Yup.date()
    .required("Date is required.")
    .typeError("Please enter a valid Date"),
  followUpPerson: Yup.string().nullable(),
  seller: Yup.string().nullable(), //remove hard requirement as it breaks if user role is missing
  status: Yup.string(),
  items: Yup.array().of(endorsementItemSchema),
  totalAgencyCommission: Yup.number(),
  totalAgentCommission: Yup.number(),
  totalFinanced: Yup.number(),
  totalFinancedPaid: Yup.number(),
  totalNonPremium: Yup.number(),
  totalPaid: Yup.number(),
  totalPayables: Yup.number(),
  totalPremium: Yup.number(),
  totalReceivables: Yup.number(),
  totalReceived: Yup.number(),
  totalTaxesAndFees: Yup.number(),
});

export const permitDetailsValidateSchema = Yup.object().shape({
  endorsements: Yup.array()
    .of(endorsementSchema)
    .min(1, "At least one application is required."),
});
