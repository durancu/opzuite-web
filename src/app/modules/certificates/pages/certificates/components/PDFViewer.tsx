import { useState } from "react";
import { Button, Modal, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Document, Page, pdfjs } from "react-pdf";
import { PDFDocumentProxy } from "pdfjs-dist";

import "react-pdf/dist/esm/Page/AnnotationLayer.css";
import "react-pdf/dist/esm/Page/TextLayer.css";
interface Props {
  file: string;
  show: boolean;
  isSending?: boolean;
  onHide: () => void;
  onSend?: () => Promise<void>;
  pagesCount: number;
}

pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/build/pdf.worker.min.js`;

function formatPdfName(metadata: Record<string, any>) {
  if (Object.keys(metadata).length) {
    const date = metadata.info?.CreationDate?.substr(2, 8);
    return `ACORD CERTIFICATE OF LIABILITY INSURANCE - ${date}.pdf`;
  }
  return `ACORD CERTIFICATE OF LIABILITY INSURANCE.pdf`;
}

export const PDFViewer = ({
  file,
  show,
  onHide,
  onSend,
  isSending,
  pagesCount,
}: Props) => {
  const [zoom, setZoom] = useState<number>(1.5);
  const [metadata, setMetadata] = useState({});

  async function onLoadSuccess(pdf: PDFDocumentProxy) {
    setMetadata(await pdf.getMetadata());
  }

  return (
    <Modal size="xl" show={show} onHide={onHide} centered backdrop="static">
      <Modal.Header closeButton>
        <Modal.Title>Certificate Preview</Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex justify-content-center align-items-center">
        <Document
          className="mb-5"
          file={file}
          options={{
            cMapUrl: `https://unpkg.com/pdfjs-dist@${pdfjs.version}/cmaps/`,
            cMapPacked: true,
          }}
          onLoadSuccess={onLoadSuccess}
        >
          {/* <Page pageNumber={1} scale={zoom} /> */}

          {Array.from({ length: pagesCount }, (_, i) => i + 1).map((page) => (
            <Page key={page} pageNumber={page} scale={zoom} />
          ))}
        </Document>
      </Modal.Body>

      <Modal.Footer className="d-flex gap-3 justify-content-between">
        <div className="d-flex gap-3 align-items-center">
          <Button
            className="btn-light-primary"
            aria-label="zoom out"
            onClick={() => setZoom((zoom) => zoom - 0.25)}
          >
            <i className="fs-2 bi bi-zoom-out"></i>
          </Button>
          <Button
            className="btn-light-primary"
            aria-label="zoom in"
            onClick={() => setZoom((zoom) => zoom + 0.25)}
          >
            <i className="fs-2 bi bi-zoom-in"></i>
          </Button>
        </div>
        <div className="d-flex gap-3">
          <Button variant="light" onClick={onHide}>
            <FormattedMessage id="BUTTON.CANCEL" />
          </Button>
          <Button
            as="a"
            className="btn-light-primary"
            href={file}
            download={formatPdfName(metadata)}
          >
            Download
          </Button>

          {onSend && (
            <Button
              type="submit"
              className="btn-active-primary"
              disabled={isSending}
              onClick={onSend}
            >
              {isSending ? (
                <div className="d-flex gap-3 align-items-center">
                  <Spinner />
                  <span>Processing</span>
                </div>
              ) : (
                <span>Send Certificate</span>
              )}
            </Button>
          )}
        </div>
      </Modal.Footer>
    </Modal>
  );
};
