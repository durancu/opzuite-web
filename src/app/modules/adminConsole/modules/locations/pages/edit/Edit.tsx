import { Fragment, useEffect } from "react";
import { Card } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { BackdropLoader, WizardForm, WizardFormStep } from "src/app/components";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actions from "../../_redux/locationsActions";
import FormLocationCompanyInfo from "./Forms/FormLocationCompanyInfo";
import { locationInitData } from "./helpers";
import { locationFormSchema } from "./schemas";

export const Edit = () => {
  const navigate = useNavigate();
  const { code }: any = useParams();
  const initLocation = locationInitData();

  const dispatch = useAppDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, locationForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.locations.actionsLoading,
      locationForEdit: state.locations.locationForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchLocation(code));
    dispatch(catalogActions.getAllCatalog());
  }, [code, dispatch]);

  const { isLoading, setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const saveLocation = (values: any) => {
    if (!code) {
      dispatch(actions.createLocation(values)).then(() =>
        backToLocationsList()
      );
    } else {
      dispatch(actions.updateLocation(values)).then(() => navigate(-1));
    }
  };

  const backToLocationsList = () => {
    navigate(-1);
  };

  if (actionsLoading || isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!code ? "New" : "Edit"} Location
      </PageTitle>
      <Card>
        <WizardForm
          initialValues={code ? locationForEdit : initLocation}
          validationSchema={locationFormSchema}
          onSubmit={saveLocation}
          onCancel={backToLocationsList}
        >
          <WizardFormStep labelTextId="">
            <FormLocationCompanyInfo />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
