import { useState } from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import { useChangePasswordMutation } from "../redux";
import { FormInput } from "src/app/components";
import { useSnackbar } from "notistack";
import { logout } from "../../auth/redux";
import { useAppDispatch } from "src/app/hooks";
import { Alert } from "react-bootstrap";

const passwordSchema = Yup.object().shape({
  oldPassword: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Password is required"),
  newPassword: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Password is required"),
  passwordConfirmation: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Password is required")
    .oneOf([Yup.ref("newPassword"), null], "Passwords must match"),
});

const SignInMethod = () => {
  const dispatch = useAppDispatch();
  const [showPasswordForm, setPasswordForm] = useState<boolean>(false);
  const [changePassword, { isLoading }] = useChangePasswordMutation();
  const { enqueueSnackbar } = useSnackbar();

  return (
    <div className="card mb-5 mb-xl-10">
      <div
        className="card-header border-0 cursor-pointer"
        role="button"
        data-bs-toggle="collapse"
        data-bs-target="#kt_account_signin_method"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Sign-in Method</h3>
        </div>
      </div>

      <div id="kt_account_signin_method" className="collapse show">
        <div className="card-body border-top p-9">
          <div className="d-flex flex-wrap align-items-center mb-10">
            <div
              id="kt_signin_password"
              className={" " + (showPasswordForm && "d-none")}
            >
              <div className="fs-6 fw-bolder mb-1">Password</div>
              <div className="fw-bold text-gray-600">************</div>
            </div>

            <div
              id="kt_signin_password_edit"
              className={"flex-row-fluid " + (!showPasswordForm && "d-none")}
            >
              <Alert show variant="warning" className="d-flex gap-4 mb-10">
                <i
                  className="bi bi-exclamation-circle text-warning fw-bolder "
                  style={{ fontSize: "2.5rem" }}
                ></i>
                <div>
                  <Alert.Heading as="h2">Warning</Alert.Heading>
                  <p className="my-4 fw-bolder fs-4">
                    You will need to login again after changing your password
                  </p>
                </div>
              </Alert>
              <Formik
                initialValues={{
                  oldPassword: "",
                  newPassword: "",
                  passwordConfirmation: "",
                }}
                validationSchema={passwordSchema}
                onSubmit={async (values) => {
                  const request = {
                    oldPassword: values.oldPassword,
                    newPassword: values.newPassword,
                  };
                  try {
                    await changePassword(request).unwrap();
                    enqueueSnackbar("Password changed, please login", {
                      variant: "success",
                    });
                    dispatch(logout());
                  } catch (error) {
                    enqueueSnackbar("An error occured", {
                      variant: "error",
                    });
                  }
                }}
              >
                {({ handleSubmit }) => (
                  <form
                    onSubmit={handleSubmit}
                    id="kt_signin_change_password"
                    className="form"
                    noValidate
                  >
                    <div className="row mb-1">
                      <div className="col-lg-4">
                        <FormInput
                          type="password"
                          className="form-control form-control-lg form-control-solid"
                          placeholder=""
                          labelText="Current Password"
                          name="oldPassword"
                          required
                          helperText="Password must be at least 8 character and contain symbols"
                        />
                      </div>

                      <div className="col-lg-4">
                        <FormInput
                          type="password"
                          className="form-control form-control-lg form-control-solid"
                          placeholder=""
                          labelText="New Password"
                          name="newPassword"
                          required
                        />
                      </div>

                      <div className="col-lg-4">
                        <FormInput
                          type="password"
                          className="form-control form-control-lg form-control-solid"
                          placeholder=""
                          labelText="Confirm Password"
                          name="passwordConfirmation"
                          required
                        />
                      </div>
                    </div>

                    <div className="d-flex gap-3 my-4">
                      <button
                        onClick={() => setPasswordForm(false)}
                        id="kt_password_cancel"
                        type="button"
                        className="btn btn-light btn-active-light-primary px-6"
                      >
                        Cancel
                      </button>
                      <button
                        id="kt_password_submit"
                        type="submit"
                        className="btn btn-primary  px-6"
                      >
                        {!isLoading ? (
                          "Change Password"
                        ) : (
                          <span className="indicator-progress d-block">
                            Please wait...
                            <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                          </span>
                        )}
                      </button>
                    </div>
                  </form>
                )}
              </Formik>
            </div>

            <div
              id="kt_signin_password_button"
              className={"ms-auto " + (showPasswordForm && "d-none")}
            >
              <button
                onClick={() => setPasswordForm(true)}
                className="btn btn-light btn-active-light-primary"
              >
                Change Password
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { SignInMethod };
