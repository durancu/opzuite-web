import { Card } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import { FormattedMessage } from "react-intl";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { NoRecordsFoundMessage, PleaseWaitMessage } from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

interface Props {
  items: any[];
}

export const CoverageTable = ({ items = [] }: Props) => {
  const entities = useGetCatalog();

  const columns = [
    {
      dataField: "lineOfBusiness",
      text: "Line Of Business",
      sort: true,
      headerFormatter: headerFormatters.LineOfBusinessColumnHeaderFormatter,
      formatter: columnFormatters.LineOfBusinessColumnFormatter,
      headerClasses: "fw-bold",
      formatExtraData: {
        entities,
      },
    },
    {
      dataField: "productItem",
      text: "Coverages",
      sort: true,
      headerFormatter: headerFormatters.CoveragesColumnHeaderFormatter,
      formatter: columnFormatters.PolicyDetailsCoveragesColumnFormatter,
      headerClasses: "fw-bold",
    },
    {
      dataField: "broker",
      text: "MGA",
      sort: true,
      headerFormatter: headerFormatters.MGAColumnHeaderFormatter,
      formatter: columnFormatters.CoverageInsurerColumnFormatter,
      headerClasses: "fw-bold",
      formatExtraData: {
        entities,
      },
    },
    {
      dataField: "carrier",
      text: "Carrier",
      sort: true,
      headerFormatter: headerFormatters.CarrierColumnHeaderFormatter,
      formatter: columnFormatters.CoverageInsurerColumnFormatter,
      headerClasses: "fw-bold",
      formatExtraData: {
        entities,
      },
    },
    {
      dataField: "premium",
      text: "Premium",
      sort: true,
      formatter: columnFormatters.AmountColumnFormatter,
      headerClasses: "fw-bold",
    },
  ];

  return (
    <Card className="mb-4 p-8 coverages-table">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.POLICIES" />
      </Card.Title>
      <BootstrapTable
        wrapperClasses="table-responsive"
        classes="table table-head-custom table-vertical-center"
        bootstrap4
        bordered={false}
        remote
        keyField="code"
        data={items || []}
        columns={columns}
      />
      <PleaseWaitMessage entities={items} />
      <NoRecordsFoundMessage entities={items} />
    </Card>
  );
};
