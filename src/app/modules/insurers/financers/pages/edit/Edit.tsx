import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import {
  WizardForm,
  WizardFormStep,
  BackdropLoader,
  Error,
} from "src/app/components";
import {
  financerBusinessValidationSchema,
  financerContactInfoValidationSchema,
} from "./schemas";
import { BusinessInfo, ContactInfo } from "src/app/modules/insurers/components";
import { breadcrumbs } from "../../breadcrumbs";
import { insurersAPI } from "../../../redux/insurerSlice";
import { defaultFinancer } from "src/app/modules/insurers/helpers";
import { useSnackbar } from "notistack";
import { FormikValues } from "formik";

const Edit = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: financer,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [createFinancer, { isLoading: isCreating }] =
    insurersAPI.useCreateInsurerMutation();

  const [updateFinancer, { isLoading: isUpdating }] =
    insurersAPI.useUpdateInsurerMutation();

  const backToFinancersList = () => {
    navigate("/financers");
  };

  async function handleCreate(values: FormikValues) {
    try {
      const response = await createFinancer(values).unwrap();
      enqueueSnackbar("Financer created successfully", {
        variant: "success",
      });
      navigate(`/financers/${response.code}/detail`);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleUpdate(values: FormikValues) {
    try {
      await updateFinancer(values).unwrap();
      enqueueSnackbar("Financer updated successfully", {
        variant: "success",
      });
      backToFinancersList();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading || isCreating || isUpdating) {
    return <BackdropLoader />;
  }

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!code ? "New " : "Edit"} Financer
      </PageTitle>
      <Card>
        <WizardForm
          initialValues={code ? financer : defaultFinancer}
          onSubmit={code ? handleUpdate : handleCreate}
          onCancel={backToFinancersList}
        >
          <WizardFormStep
            labelTextId="WIZARD.FINANCER.BUSINESS.INFO"
            validationSchema={financerBusinessValidationSchema}
          >
            <BusinessInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.FINANCER.BASIC.INFO"
            validationSchema={financerContactInfoValidationSchema}
          >
            <ContactInfo />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};

export default Edit;
