import { useEffect, useState } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useGetUserAuth } from "src/app/hooks";
import * as actionCatalog from "../redux/catalogActions";

interface Props {
  labelField: string;
  labelHelperFields: any[];
  valueField: string;
  joint?: string;
}

export const DEFAULT_JOINT = " | ";

export function useGetCustomersCatalog(loadData: boolean, props: Props) {
  const dispatch = useAppDispatch();
  const { entities } = useSelector(
    (state: any) => ({ entities: state.catalogs?.entities }),
    shallowEqual
  );

  useEffect(() => {
    if (loadData) {
      dispatch(actionCatalog.getAllCatalog());
    }
  }, [dispatch, loadData]);

  const user = useGetUserAuth();

  const [customersOptions, setCustomersOptions] = useState([]);

  useEffect(() => {
    if (entities?.customers && entities?.customers.length > 0) {
      setCustomersOptions(entities.customers);
    }
  }, [entities?.customers, user]);

  return (
    customersOptions?.map((customer: any) => {
      const extractedProperties: any[] = [];
      props.labelHelperFields.forEach((field: string) => {
        if (Object.prototype.hasOwnProperty.call(customer, field)) {
          extractedProperties.push(customer[field]);
        }
      });
      return {
        id: customer[props.valueField],
        name: customer[props.labelField],
        description: extractedProperties.join(props.joint || DEFAULT_JOINT),
      };
    }) || []
  );
}
