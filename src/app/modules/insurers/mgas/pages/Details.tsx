import { useEffect, Fragment } from "react";
import { useSnackbar } from "notistack";
import { useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import {
  MGADetails,
  MGAOverview,
  Policies,
  PageDetailTopButtons,
} from "src/app/components/Widgets";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { BackdropLoader, Error } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { insurersAPI } from "../../redux/insurerSlice";
import { breadcrumbs } from "../breadcrumbs";

const Details = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: mga,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(`${code}?layout=FULL`, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [deleteMGA, { isLoading: isDeleting }] =
    insurersAPI.useDeleteInsurerMutation();

  const goToEditMga = () => {
    navigate(`/mgas/${code}/edit`);
  };

  async function handleDelete() {
    try {
      await deleteMGA(code).unwrap();
      enqueueSnackbar("MGA deleted", {
        variant: "success",
      });
      navigate("/mgas");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.BROKER" })}
      </PageTitle>
      <Toolbar>
        <PageDetailTopButtons
          editFunction={goToEditMga}
          deleteFunction={handleDelete}
          editPermission={Permissions.MGAS_UPDATE}
          deletePermission={Permissions.MGAS_DELETE}
          componentLabel="MGA"
        />
      </Toolbar>

      <MGAOverview mga={mga} />
      <MGADetails mga={mga} />

      <Policies
        filter={{ broker: mga?.id }}
        subTitle="Policies with coverage provided by this broker."
        context="mgas"
      />
    </Fragment>
  );
};

export default Details;
