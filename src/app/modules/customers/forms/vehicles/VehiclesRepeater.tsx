import { Fragment } from "react";
import { FieldArray, useFormikContext } from "formik";
import { Button, Col, Card, Accordion } from "react-bootstrap";
import { ItemDeleteModal } from "src/app/components/DeleteDialog";
import { VehicleFields } from "./VehicleFields";
import { vehicle } from "./helpers";
import objectPath from "object-path";
import { MissingFieldsAlert } from "src/app/components";

export const VehiclesRepeater = () => {
  const { values, errors, setFieldValue } = useFormikContext<any>();

  function addVehicle() {
    setFieldValue(`vehicles`, [vehicle, ...values.vehicles]);
  }

  return (
    <Fragment>
      <section>
        <header className="mb-6 d-flex justify-content-between align-items-center">
          <h2 className="fw-bold text-primary">Vehicles</h2>
          <Button
            variant="primary"
            onClick={() => addVehicle()}
            className="d-flex gap-1 align-items-center"
          >
            <i className="fs-3 bi bi-plus-circle-dotted"></i>
            <span className="">Add Vehicle</span>
          </Button>
        </header>

        {values && !values.vehicles.length && (
          <Col className="h-300px rounded d-flex flex-column justify-content-center align-items-center bg-light-primary">
            <p className="fw-bold fs-3 mb-5">No available vehicles</p>
            <Button
              variant="primary"
              onClick={() => addVehicle()}
              className="d-flex gap-1 align-items-center"
            >
              <i className="fs-3 bi bi-plus-circle-dotted"></i>
              <span className="">Add Vehicle</span>
            </Button>
          </Col>
        )}
      </section>

      <FieldArray
        name="vehicles"
        render={(helpers) => (
          <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
            {values.vehicles.length > 0 &&
              values.vehicles.map((vehicle: any, index: number) => {
                return (
                  <Accordion.Item
                    key={index}
                    eventKey={index.toString()}
                    className="mb-6 border"
                  >
                    <Card>
                      <Accordion.Button className="px-6 py-4 bg-light-primary">
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <h4 className="fw-bold">
                            {`Vehicle ${index + 1}`}
                            {values.vehicles[index].vinNumber &&
                              ` - ${values.vehicles[index].vinNumber}`}
                          </h4>

                          <div className="d-flex justify-content-between align-items-center gap-3">
                            <MissingFieldsAlert
                              show={objectPath.get(errors, `vehicles.${index}`)}
                            />
                            <ItemDeleteModal
                              title="Delete Vehicle"
                              itemName={`Vehicle ${vehicle.vinNumber}`}
                              toolTipLabel="Delete"
                              deleteAction={() => helpers.remove(index)}
                            />
                          </div>
                        </div>
                      </Accordion.Button>
                      <Accordion.Body>
                        <VehicleFields
                          index={index}
                          customerType={values?.type}
                        />
                      </Accordion.Body>
                    </Card>
                  </Accordion.Item>
                );
              })}
          </Accordion>
        )}
      />
    </Fragment>
  );
};
