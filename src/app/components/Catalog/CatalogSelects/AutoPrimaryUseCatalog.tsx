export const AUTO_PRIMARY_USE_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "COMMUTING", name: "Commuting" },
  { id: "PLEASURE", name: "Pleasure" },
  { id: "BUSINESS", name: "Business" },
];
