import { PayloadAction, combineReducers } from "@reduxjs/toolkit";

import { apiInterceptorsSlice } from "src/app/components/ApiInterceptor/_redux/apiInterceptorSlice";
import { catalogsSlice } from "src/app/components/Catalog/redux/catalogSlice";
import { feedbacksSlice } from "src/app/components/Feedback/_redux/feedbackSlice";
import { filtersSlice } from "src/app/components/Filter/_redux/filterSlice";
import { policyMetricsSlice } from "src/app/components/Widgets/_redux/PolicyMetrics/policyMetricsSlice";
import {
  locationsSlice,
  payrollsSlice,
  reportsSlice,
  usersSlice,
} from "src/app/modules/adminConsole";
import { authSlice, logout } from "src/app/modules/auth/redux";
import { certificateSlice } from "src/app/modules/certificates/redux";
import { customersSlice } from "src/app/modules/customers/redux/customersSlice";
import { dashboardsSlice } from "src/app/modules/dashboard/_redux/dashboards/dashboardsSlice";
import salesReducer from "src/app/modules/sales/redux/saleSlice";
import insurersReducer from "src/app/modules/insurers/redux/insurerSlice";
import { API } from "./api";

const appReducers = combineReducers({
  auth: authSlice.reducer,
  users: usersSlice.reducer,
  sales: salesReducer,
  insurers: insurersReducer,
  customers: customersSlice.reducer,
  catalogs: catalogsSlice.reducer,
  reports: reportsSlice.reducer,
  dashboards: dashboardsSlice.reducer,
  payrolls: payrollsSlice.reducer,
  locations: locationsSlice.reducer,
  filters: filtersSlice.reducer,
  feedback: feedbacksSlice.reducer,
  apiInterceptor: apiInterceptorsSlice.reducer,
  policyMetric: policyMetricsSlice.reducer,
  certificates: certificateSlice.reducer,
  [API.reducerPath]: API.reducer,
});

export const rootReducer = (state: any, action: PayloadAction) => {
  if (logout.match(action)) {
    // clear all state on logout
    state = undefined;
  }
  return appReducers(state, action);
};
