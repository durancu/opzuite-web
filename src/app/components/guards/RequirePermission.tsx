import { Fragment, ReactNode } from "react";
import { Navigate } from "react-router-dom";
import { useAppSelector } from "src/app/hooks";
import { userSelector } from "src/app/modules/auth/redux";
import { isPermitted } from "src/app/utils";

type Props = {
  children: ReactNode;
  permissions: string | string[];
  isRoute?: boolean;
};

export const RequirePermission = ({
  children,
  permissions,
  isRoute,
}: Props) => {
  const user = useAppSelector(userSelector);

  // if not user and not guarding a route
  if (!user && !isRoute) return null;
  if (!user && isRoute) return <Navigate to="/forbidden" replace />;

  /* since permissions can either be single or an array we want to
   * make sure we always send an array to the checker
   */
  const normalizedPermissions = Array.isArray(permissions)
    ? permissions
    : [permissions];

  // if user is permitted then display component
  return isPermitted(normalizedPermissions, user?.permissions || []) ? (
    <Fragment>{children}</Fragment>
  ) : isRoute ? (
    <Navigate to="/forbidden" replace />
  ) : null;
};
