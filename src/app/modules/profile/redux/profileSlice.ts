import { API } from "src/redux/api";
import { Profile } from "../types";

export const profileAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getProfile: build.query<any, void>({
      query: () => "/profile",
    }),
    updateProfile: build.mutation<any, Partial<Profile>>({
      query: (data) => ({
        url: "/profile",
        method: "put",
        body: data,
      }),
    }),
    changePassword: build.mutation<
      any,
      Partial<{ oldPassword: string; newPassword: string }>
    >({
      query: (data) => ({
        url: "/profile/password",
        method: "put",
        body: data,
      }),
    }),
    updatePhoto: build.mutation<any, FormData>({
      query: (data) => ({
        url: "/profile/photo",
        method: "post",
        body: data,
      }),
    }),
    deletePhoto: build.mutation<any, void>({
      query: () => ({
        url: "/profile/photo",
        method: "delete",
      }),
    }),
  }),
  overrideExisting: false,
});

export const {
  useGetProfileQuery,
  useUpdateProfileMutation,
  useChangePasswordMutation,
  useUpdatePhotoMutation,
  useDeletePhotoMutation,
} = profileAPI;
