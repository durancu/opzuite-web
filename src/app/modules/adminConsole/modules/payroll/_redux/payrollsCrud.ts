import axios from "axios";

export const PAYROLLS_URL = "api/v2/payrolls";

// GET => GET: add a new payroll to the server
export function getPayrollAvailableDates(locationId: any) {
  return axios.get(`${PAYROLLS_URL}/available-dates?location=${locationId}`);
}

// Prepare =>  POST: initialize a new payroll
export function initializePayroll(payroll: any) {
  return axios.post(`${PAYROLLS_URL}/init`, payroll);
}

// CREATE =>  POST: add a new payroll to the server
export function createPayroll(payroll: any) {
  return axios.post(PAYROLLS_URL, payroll);
}

// CREATE =>  POST: add a new payroll to the server
export function generateMonthPayrolls() {
  return axios.post(`${PAYROLLS_URL}/generate`);
}

// EXECUTE =>  PUT: execute a payroll exist to the server
export function executePayroll(code: any) {
  return axios.patch(`${PAYROLLS_URL}/${code}/execute`);
}

// READ
export function getAllPayrolls() {
  return axios.get(PAYROLLS_URL);
}

export function getPayrollByCode(code: any) {
  return axios.get(`${PAYROLLS_URL}/${code}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findPayrolls(queryParams: any) {
  return axios.post(`${PAYROLLS_URL}/search`, { queryParams });
}

export function updatePayroll(payroll: any) {
  return axios.put(`${PAYROLLS_URL}/${payroll.code}`, payroll);
}

// DELETE => delete the payroll from the server
export function deletePayroll(code: any) {
  return axios.delete(`${PAYROLLS_URL}/${code}`);
}

// DELETE Payrolls by ids
export function deletePayrolls(ids: any) {
  return axios.post(`${PAYROLLS_URL}/delete`, { ids });
}
