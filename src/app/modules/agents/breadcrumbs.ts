import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Agents",
    path: "agents",
    isSeparator: false,
    isActive: true,
  },
];
