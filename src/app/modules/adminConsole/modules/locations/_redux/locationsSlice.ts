import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialLocationsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  locationForEdit: undefined,
  lastError: null,
};
export const callTypes = {
  list: "list",
  action: "action",
};

export const locationAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllLocations: build.query<any, QueryParams>({
      query: (queryParams) => ({
        url: "/locations/search",
        method: "post",
        body: { queryParams },
      }),
    }),
  }),
  overrideExisting: false,
});

export const locationsSlice = createSlice({
  name: "locations",
  initialState: initialLocationsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getLocationByCode
    locationFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.locationForEdit = action.payload.locationForEdit;
      state.error = null;
    },
    // clear
    locationClear: (state: any) => {
      state.actionsLoading = false;
      state.locationForEdit = null;
      state.error = null;
    },
    // findLocations
    locationsFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createLocation
    locationCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // updateLocation
    locationUpdated: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
      /*       state.entities = state.entities.map((entity:any) => {
        if (entity.id === action.payload.location.id) {
          return action.payload.location;
        }
        return entity;
      }); */
    },
    // deleteLocation
    locationDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => el.id !== action.payload.id
      );
    },
    // deleteLocations
    locationsDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => !action.payload.ids.includes(el.id)
      );
    },
  },
});
