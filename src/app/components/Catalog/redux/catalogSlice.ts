import { createSlice, createSelector } from "@reduxjs/toolkit";
import { API } from "src/redux/api";
import { RootState } from "src/redux/store";

interface CatalogState {
  listLoading: boolean;
  actionsLoading: boolean;
  totalCount: number;
  entities: CatalogInterface | any;
  catalogForEdit: Record<any, any>;
  lastError: unknown;
}

const initialCustomersState: CatalogState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: {},
  catalogForEdit: {},
  lastError: null,
};

export const callTypes = {
  list: "list",
  action: "action",
};

export const catalogAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllCatalogs: build.query<CatalogInterface, void>({
      query: () => ({
        url: "catalogs/search",
        method: "POST",
      }),
    }),
  }),
  overrideExisting: false,
});

export const catalogsSlice = createSlice({
  name: "catalogs",
  initialState: initialCustomersState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getCustomerById
    catalogFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.catalogForEdit = action.payload.catalogForEdit;
      state.error = null;
    },
    // findCustomers
    catalogsFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // findCustomers
    catalogsGetAll: (state: any, action: any) => {
      const entities = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
    },
    // createCustomer
    catalogCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // updateCustomer
    catalogUpdated: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map((entity: any) => {
        if (entity.id === action.payload.catalog.id) {
          return action.payload.catalog;
        }
        return entity;
      });
    },
    // deleteCustomer
    catalogDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => el.id !== action.payload.id
      );
    },
    // deleteCustomers
    catalogsDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => !action.payload.ids.includes(el.id)
      );
    },
    // catalogsUpdateState
    catalogsStatusUpdated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map((entity: any) => {
        if (ids.findIndex((id: any) => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
    getVehiclesModelFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      const { vehicleModels } = action.payload;
      state.entities = { ...state.entities, vehicleModels };
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      catalogAPI.endpoints.getAllCatalogs.matchFulfilled,
      (state, { payload }) => {
        return {
          ...state,
          entities: payload,
        };
      }
    );
  },
});

export const catalogSelector = createSelector(
  (state: RootState) => state.catalogs,
  (catalogs: CatalogState) => catalogs.entities as CatalogInterface
);
