export const getHierarchyRoleByUserAuth = (
  roleName: string,
  roles: any[] = []
): number => roles.find(({ key }: any) => key === roleName)?.hierarchy;
