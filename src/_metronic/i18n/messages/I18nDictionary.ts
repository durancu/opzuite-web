import enCatalogs from "./catalogs/en.json";
import esCatalogs from "./catalogs/es.json";
import enComponents from "./components/en.json";
import esComponents from "./components/es.json";
import enContents from "./contents/en.json";
import esContents from "./contents/es.json";
import enForms from "./forms/en.json";
import esForms from "./forms/es.json";
import enGeneral from "./general/en.json";
import esGeneral from "./general/es.json";
import enMenus from "./menus/en.json";
import esMenus from "./menus/es.json";
import enModulesAuth from "./modules/auth/en.json";
import esModulesAuth from "./modules/auth/es.json";
import enResponses from "./responses/en.json";
import esResponses from "./responses/es.json";
import enTables from "./tables/en.json";
import esTables from "./tables/es.json";
import enWizard from "./wizard/en.json";
import esWizard from "./wizard/es.json";

const allContents = {
  en: enContents,
  es: esContents,
};

const allComponents = {
  en: enComponents,
  es: esComponents,
};

const allForms = {
  en: enForms,
  es: esForms,
};

const allGeneral = {
  en: enGeneral,
  es: esGeneral,
};

const allMenus = {
  en: enMenus,
  es: esMenus,
};

const allResponses = {
  en: enResponses,
  es: esResponses,
};

const allTables = {
  en: enTables,
  es: esTables,
};

const allCatalogs = {
  en: enCatalogs,
  es: esCatalogs,
};

const allModulesAuth = {
  en: enModulesAuth,
  es: esModulesAuth,
};

const allModulesWizard = {
  en: enWizard,
  es: esWizard,
};

export const I18nDictionary: any = {
  en: {
    ...allContents.en,
    ...allComponents.en,
    ...allForms.en,
    ...allGeneral.en,
    ...allMenus.en,
    ...allResponses.en,
    ...allTables.en,
    ...allCatalogs.en,
    ...allModulesAuth.en,
    ...allModulesWizard.en,
  },
  es: {
    ...allContents.es,
    ...allComponents.es,
    ...allForms.es,
    ...allGeneral.es,
    ...allMenus.es,
    ...allResponses.es,
    ...allTables.es,
    ...allCatalogs.es,
    ...allModulesAuth.es,
    ...allModulesWizard.es,
  },
};
