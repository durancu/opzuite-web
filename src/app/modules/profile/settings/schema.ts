import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const profileSchema = Yup.object().shape({
  firstName: Yup.string().required("First name is required"),
  lastName: Yup.string().required("Last name is required"),
  company: Yup.object().shape({
    name: Yup.string().required("Company Name is required."),
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      country: Yup.string(),
      zip: Yup.string(),
    }),
    email: Yup.string()
      .trim()
      .email("Must be a valid email address")
      .required("Email address is required."),
    fax: Yup.string(),
    industry: Yup.string(),
    primaryPhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number.")
      .required("Phone number is required."),
    primaryPhoneExtension: Yup.string(),
    website: Yup.string(),
    usDOT: Yup.string(),
    mcNumber: Yup.string(),
    txDMV: Yup.string(),
  }),
  country: Yup.string().nullable(),
  language: Yup.string().required("Language is required"),
  timezone: Yup.string().required("Time zone is required"),
});
