import { Card, Col, Row } from "react-bootstrap";

interface Props {
  children: any;
  title?: string;
}

export const CardStructure = ({ children, title }: Props) => (
  <Card className="mb-5">
    <Card.Body>
      {title && (
        <Row className="mb-3">
          <Col>
            <div className="form form-inline">
              <div className="form-control border-0 pl-0">
                <h4 className="card-title align-items-start flex-column">
                  <span className="card-label font-weight-bold">{title}</span>
                </h4>
              </div>
            </div>
          </Col>
        </Row>
      )}
      {children}
    </Card.Body>
  </Card>
);
