import { useEffect } from "react";
import { Form, Formik } from "formik";
import { Modal, ProgressBar, Button } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch } from "src/app/hooks";
import * as Yup from "yup";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as actions from "../_redux/locationsActions";
import { FormInput } from "src/app/components";

export const ChangeIP = () => {
  const { code } = useParams();
  const navigate = useNavigate();

  // Locations Redux state
  const dispatch = useAppDispatch();
  const { actionsLoading, locationForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.locations.actionsLoading,
      locationForEdit: state.locations.locationForEdit,
    }),
    shallowEqual
  );

  function goBack() {
    navigate("/manager/locations");
  }

  // if !id we should close modal
  useEffect(() => {
    if (!code) {
      goBack();
    }
    dispatch(actions.fetchLocation(code));
  }, [code]);

  // looking for loading/dispatch
  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const updateIp = (location: any) => {
    // server request for deleting location by id
    dispatch(actions.updateLocation(location)).then(() => {
      // closing delete modal
      goBack();
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="location change IP"
      centered
      backdrop="static"
    >
      <Formik
        validateOnBlur={false}
        validateOnChange={false}
        enableReinitialize={true}
        initialValues={locationForEdit || {}}
        validationSchema={Yup.object().shape({
          ipAddress: Yup.string().required("New Ip Address is required."),
        })}
        onSubmit={(values) => updateIp(values)}
      >
        <Form>
          {actionsLoading && <ProgressBar variant="query" />}
          <Modal.Header closeButton>
            <Modal.Title id="location change IP">
              <FormattedMessage id="GENERAL.MODAL.TITLE.CHANGE_IP_ADDRESS" />
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormInput
              name="ipAddress"
              labelText="New Ip Address"
              type="text"
              required
            />
          </Modal.Body>
          <Modal.Footer>
            <Button
              type="button"
              variant="light"
              onClick={goBack}
              disabled={actionsLoading}
            >
              <FormattedMessage id="BUTTON.CANCEL" />
            </Button>
            <Button type="submit" variant="primary" disabled={actionsLoading}>
              <span className="mr-3">
                <FormattedMessage id="BUTTON.SAVE_AND_CLOSE" />
              </span>
            </Button>
          </Modal.Footer>
        </Form>
      </Formik>
    </Modal>
  );
};
