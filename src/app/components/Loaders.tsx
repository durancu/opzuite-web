import { Modal } from "react-bootstrap";
import ScaleLoader from "react-spinners/ScaleLoader";

export const BackdropLoader = () => {
  return (
    <Modal
      show
      centered
      backdrop="static"
      aria-labelledby="description"
      contentClassName="bg-transparent shadow-none"
    >
      <div className=" d-flex flex-column justify-content-center align-items-center bg-transparent">
        <ScaleLoader
          color="#fff"
          height={45}
          width={7}
          aria-labelledby="description"
        />
        <h4 id="description" className="my-4 text-light">
          Loading
        </h4>
      </div>
    </Modal>
  );
};
