import { isEmpty } from "lodash";

import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { CopyToClipboardLink } from "../../Widgets";
export const PolicyNumberColumnFormatter = (policyNumber: any) => (
  <div>
    {!isEmpty(policyNumber) && (
      <CopyToClipboardLink
        content={policyNumber}
        fieldId={"FORM.LABELS.POLICY_NUMBER"}
      >
        <span className="text-dark fw-semibold fs-6">
          {policyNumber}
        </span>
      </CopyToClipboardLink>
    )}
    {isEmpty(policyNumber) && (
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="quick-actions-tooltip">
            <span>
              Policy Number is missing
            </span>
          </Tooltip>
        }
      >
        <i className="fa fa-exclamation-circle text-warning px-10"></i>
      </OverlayTrigger>
    )}
  </div>
);
