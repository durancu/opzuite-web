import { RoleHierarchyEdges } from "src/app/constants/Roles";


//ADMIN ACCESS BY ROLE FUNCTIONS
export const hasLocationManagerRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.AGENT;
};

export const hasRegionManagerRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.LOCATION;
};

export const hasCountryManagerRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.REGION;
};

export const hasOfficerManagerRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.COUNTRY;
};

export const hasOwnerRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.OFFICER;
};

export const hasAdminRoleAccess = (role: any) => {
  return role.hierarchy < RoleHierarchyEdges.OWNER;
};

export const hasSuperAdminRoleAccess = (role: any) => {
  return role.hierarchy === RoleHierarchyEdges.SUPERADMIN;
};

//ROLE RANGES BASED FUNCTIONS

export const inLocationManagerRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.LOCATION &&
    role.hierarchy < RoleHierarchyEdges.AGENT
  );
};

export const inRegionManagerRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.REGION &&
    role.hierarchy < RoleHierarchyEdges.LOCATION
  );
};

export const inCountryManagerRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.COUNTRY &&
    role.hierarchy < RoleHierarchyEdges.REGION
  );
};

export const inOfficerManagerRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.OFFICER &&
    role.hierarchy < RoleHierarchyEdges.COUNTRY
  );
};

export const inOwnerRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.OWNER &&
    role.hierarchy < RoleHierarchyEdges.OFFICER
  );
};

export const inAdminRolesRange = (role: any) => {
  return (
    role.hierarchy > RoleHierarchyEdges.ADMIN &&
    role.hierarchy < RoleHierarchyEdges.OWNER
  );
};

export const inSuperAdminRolesRange = (role: any) => {
  return role.hierarchy === RoleHierarchyEdges.SUPERADMIN;
};
