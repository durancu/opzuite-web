/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const PayrollActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openEditPayrollPage, openDetailPayrollPage }: any
) => (
  <TableActionsMenu
    readFunction={openDetailPayrollPage}
    editFunction={openEditPayrollPage}
    editPermission={Permissions.PAYROLLS_UPDATE}
    readPermission={Permissions.PAYROLLS_READ}
    code={row.code}
  />
);
