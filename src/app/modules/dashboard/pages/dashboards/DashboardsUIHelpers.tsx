export const DashboardStatusCssClasses = ["success", "info", ""];
export const DashboardStatusTitles = ["Selling", "Sold"];
export const DashboardConditionCssClasses = ["success", "danger", ""];
export const DashboardConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];
export const initialFilter = {
  filter: {},
  sortOrder: "desc", // asc||desc
  sortField: "totalPremium",
  pageNumber: 1,
  pageSize: 20,
};
