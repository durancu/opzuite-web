export const UserStatus = (status: any): any => {
    switch (status) {
        case "UNCONFIRMED":
            return {
                icon: "fas fa-envelope",
                color: "warning"
            };
        case "CONFIRMED":
            return {
                icon: "fas fa-check",
                color: "success"
            };
        case "EXTERNAL_PROVIDER":
            return {
                icon: "fas fa-cloudsmith",
                color: "info"
            };
        case "ARCHIVED":
            return {
                icon: "fas fa-archive",
                color: "info"
            };
        case "RESET_REQUIRED":
            return {
                icon: "fas fa-arrows-turn-to-dots",
                color: "warning"
            };
        case "FORCE_CHANGE_PASSWORD":
            return {
                icon: "fas fa-asterisk",
                color: "warning"
            };
        case "UNKNOWN":
        default:
            return {
                icon: "fas fa-question",
                color: "danger"
            };
    }
};