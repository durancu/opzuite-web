import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { RootState } from "./store";
/*
 * Base API service to be registered in store
 * Each section will extend it accordingly
 * @see https://redux-toolkit.js.org/rtk-query/usage/code-splitting
 */

export const API = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: `${process.env.REACT_APP_API_URL}/api/v2`,
    prepareHeaders: (headers, { getState }) => {
      // add authorization headers
      const token = (getState() as RootState).auth.accessToken;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: () => ({}),
});
