import { FormikValues } from "formik";
export const ReportStatusCssClasses = ["success", "info", ""];
export const ReportStatusTitles = ["Selling", "Sold"];
export const ReportConditionCssClasses = ["success", "danger", ""];
export const ReportConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

export const defaultFilter = {
  filter: {},
  sortOrder: "desc", // asc||desc
  sortField: "totalPremium",
  pageNumber: 1,
  pageSize: 20,
};

export const initialValues = {
  type: "",
  model: "sale",
  groupByModel: "",
  dateFrom: null,
  dateTo: null,
  searchText: "",
  filters: [],
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {};

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchText: values.searchText,
    sortOrder: "asc",
    sortField: "name",
    pageNumber: 1,
    pageSize: 20,
  };
};
