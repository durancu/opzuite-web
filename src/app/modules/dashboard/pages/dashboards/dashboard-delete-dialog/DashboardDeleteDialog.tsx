import { useEffect, useMemo } from "react";
import { Modal, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch } from "src/app/hooks";
import * as actions from "../../../_redux/dashboards/dashboardsActions";
import { useDashboardsUIContext } from "../DashboardsUIContext";

export function DashboardDeleteDialog() {
  const navigate = useNavigate();
  const { code } = useParams();
  // Dashboard UI Context
  const dashboardUIContext: any = useDashboardsUIContext();
  const dashboardUIProps = useMemo(() => {
    return {
      setIds: dashboardUIContext.setIds,
      queryParams: dashboardUIContext.queryParams,
    };
  }, [dashboardUIContext]);

  // Dashboard Redux state
  const dispatch = useAppDispatch();
  const { isLoading } = useSelector(
    (state: any) => ({ isLoading: state.dashboards.actionsLoading }),
    shallowEqual
  );

  function goBack() {
    navigate(-1);
  }

  // if !code we should close modal
  useEffect(() => {
    if (!code) {
      goBack();
    }
  }, [code]);

  const deleteDashboard = () => {
    // server request for deleting dashboard by code
    dispatch(actions.deleteDashboard(code)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchDashboards(dashboardUIProps.queryParams));
      // clear selections list
      dashboardUIProps.setIds([]);
      // closing delete modal
      goBack();
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="example-modal-sizes-title-lg"
      centered
      backdrop="static"
    >
      {isLoading && <ProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Dashboard Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this dashboard?</span>
        )}
        {isLoading && <span>Dashboard is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={goBack}
            className="btn btn-light btn-elevate"
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteDashboard}
            className="btn btn-danger btn-elevate"
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
