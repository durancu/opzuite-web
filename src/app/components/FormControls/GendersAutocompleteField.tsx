import { useIntl } from "react-intl";
import { FormAutoComplete } from "../forms";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function GendersAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.GENDER",
  labelText,
  fieldName = "gender",
}: Props) {
  const intl = useIntl();
  const genders: any = intl.messages["GENDER_CATALOG"];

  return (
    <FormAutoComplete
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={genders || []}
    />
  );
}
