import { capitalize, lowerCase } from "lodash";

import { Card, Col, Row } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { formatDate } from "../functions";

interface Props {
  sale: any;
}

export const PermitOverview = ({ sale }: Props) => {
  return (
    <Card className="mb-4 p-8 overview-section">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>{sale && sale.customer?.name}</h4>
            <FormattedMessage id="FORM.LABELS.CUSTOMER" />
          </Col>

          <Col>
            <h4>{sale && formatDate(sale.soldAt)}</h4>
            <FormattedMessage id="FORM.LABELS.DATE" />
          </Col>

          <Col>
            <h4>{sale && capitalize(lowerCase(sale.status))}</h4>

            <FormattedMessage id="FORM.LABELS.STATUS" />
          </Col>

          <Col>
            <h4>{sale && sale?.seller?.name}</h4>
            <FormattedMessage id="FORM.LABELS.COORDINATOR" />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
