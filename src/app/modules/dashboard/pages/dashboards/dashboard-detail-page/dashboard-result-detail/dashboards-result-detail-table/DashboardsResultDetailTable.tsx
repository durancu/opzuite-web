import { Typography } from "@mui/material";
import { useEffect, useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useParams } from "react-router-dom";
import {
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
} from "src/app/components";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import * as filterActions from "src/app/components/Filter/_redux/filterActions";
import * as actions from "../../../../../_redux/dashboards/dashboardsActions";
import { useDashboardsResultDetailUIContext } from "../DashboardsResultDetailUIContext";
import * as columnFormatters from "./column-formatters";

export function DashboardsResultDetailTable() {
  const { code }: any = useParams();
  // Dashboards UI Context
  const { queryParams, setQueryParams }: any =
    useDashboardsResultDetailUIContext();
  const dispatch = useAppDispatch();

  // Getting curret state of dashboards list from store (Redux)
  const { filterForEdit, actionsLoading }: any = useSelector(
    (state: any) => ({
      currentState: state.dashboards,
      actionsLoading: state.dashboards.actionsLoading,
      catalogs: state.catalogs?.entities,
      filterForEdit: state.filters.filterForEdit,
    }),
    shallowEqual
  );

  const [filterIsNotSet, setFilterIsNotSet] = useState<boolean>(true);

  const [metrics, setMetrics] = useState<any[]>([]);
  const [appliedParams, setAppliedParams] = useState<any>({});

  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  useEffect(() => {
    if (filterForEdit && !filterIsNotSet) {
      setFilterIsNotSet(true);
    }
  }, [filterForEdit, filterIsNotSet]);

  useEffect(() => {
    dispatch(actions.resultDashboard(queryParams, code)).then(
      (response: any) => {
        setMetrics(response.metrics);
        setAppliedParams(response.appliedParams);
        filterIsNotSet &&
          dispatch(filterActions.setFilter(response.appliedParams));
      }
    );

    return () => {
      dispatch(filterActions.clearFilter());
    };
  }, [filterIsNotSet, queryParams, dispatch, code]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const ModelNameHeaderFormatter = () => {
    return <>{appliedParams?.groupByModel}</>;
  };

  // Table columns

  const columns: any = [
    {
      dataField: "_id.id",
      text: "Model",
      formatter: columnFormatters.ModelNameColumnFormatter,
      footer: "TOTAL",
      formatExtraData: {
        model: appliedParams?.groupByModel,
      },
      headerFormatter: ModelNameHeaderFormatter,
    },
    {
      dataField: "totalPremium",
      text: "Premium",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalPremium"
      ), */
    },
    {
      dataField: "totalTaxesAndFees",
      text: "Taxes And Fees",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field !== "totalTaxesAndFees"
      ), */
    },
    {
      dataField: "totalPayables",
      text: "Payables",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalPayables"
      ), */
    },
    {
      dataField: "totalReceivables",
      text: "Receivables",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalReceivables"
      ), */
    },
    {
      dataField: "totalFinanced",
      text: "Financed",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalFinanced"
      ), */
    },
    /* 
    {
      
      dataField: "totalAgencyCommission",
      text: "Agency Comm.",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalAgencyCommission"
      ), 
    }, */
    {
      dataField: "totalAgentCommission",
      text: "Agent Comm.",
      headerAlign: "right",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "right",
      footerAlign: "right",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      /* hidden: !dashboardForEdit?.params?.fields?.find(
        ({ field }: any) => field === "totalAgentCommission"
      ), */
    },
  ];
  return (
    <>
      {metrics.length ? (
        <>
          <BootstrapTable
            wrapperClasses="table-responsive"
            classes="table table-head-custom table-vertical-center overflow-hidden"
            bootstrap4
            bordered={false}
            remote
            keyField="_id.id"
            data={metrics}
            columns={columns}
            onTableChange={getHandlerTableChange(setQueryParams)}
          />
          <PleaseWaitMessage entities={metrics} />
          <NoRecordsFoundMessage entities={metrics} />
        </>
      ) : (
        <>
          <Typography variant="h6">No metrics for this dashboard</Typography>
          <Typography variant="subtitle1">
            Try to modify parameters and/or filters to get data.
          </Typography>
        </>
      )}
    </>
  );
}
