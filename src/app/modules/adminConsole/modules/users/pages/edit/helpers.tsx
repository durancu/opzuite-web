import { customAlphabet } from "nanoid";

export const randomUsername = () => {
  const nanoid = customAlphabet("1234567890abcdefghijklmnopqrstuvwxyz", 10);
  return nanoid();
};

export const initUser = {
  address: {
    address1: "",
    address2: "",
    city: "",
    state: "TX",
    country: "USA",
    zip: "",
  },
  country: "USA",
  dob: null,
  communication: {
    email: false,
    sms: false,
    phone: false,
  },
  email: "",
  emailSettings: {
    emailNotification: true,
    sendCopyToPersonalEmail: true,
    activityRelatesEmail: {
      youHaveNewNotifications: false,
      youAreSentADirectMessage: false,
      locationTargetReached: false,
      newTeamMember: false,
      employeeTargetReached: false,
    },
  },
  firstName: "",
  gender: "",
  language: "es",
  lastName: "",
  location: "",
  password: "",
  confirmPassword: "",
  mobilePhone: null,
  phone: null,
  role: null,
  timezone: "US/Central",
  username: randomUsername(),
  website: "",
  company: "",
  supervisor: "",
  employeeInfo: {
    endedAt: null,
    startedAt: new Date(),
    position: "",
    payFrequency: "MONTHLY",
    saleCommissionPlans: [],
    bonusFrequency: "MONTHLY",
    payRate: 0,
    hourlyRate: 0,
    overtimeAuthorized: false,
    overtimePayRate: 1.5,
  },
};
