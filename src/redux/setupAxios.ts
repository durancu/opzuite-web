export default function setupAxios(axios: any, store: any) {
  const { REACT_APP_API_URL } = process.env;
  axios.defaults.baseURL = REACT_APP_API_URL;

  axios.interceptors.request.use(
    (config: { headers: { Authorization: string } }) => {
      const {
        auth: { accessToken },
      } = store.getState();

      if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
      }

      return config;
    },
    (err: any) => Promise.reject(err)
  );
}
