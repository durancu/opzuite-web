// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import Carriers from "./pages/Carriers";
import DeleteDialog from "../components/DeleteDialog";
import Edit from "./pages/edit/Edit";
import Details from "./pages/Details";

export const carrierRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.CARRIERS_READ}>
        <Carriers />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.CARRIERS_CREATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.CARRIERS_UPDATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.CARRIERS_READ}>
        <Details />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <DeleteDialog label="carrier" path="/carriers" />,
  },
];
