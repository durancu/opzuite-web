import { Fragment } from "react";
import { Modal, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FieldArray, useFormikContext } from "formik";
import { FormattedMessage } from "react-intl";
import { FormInput } from "src/app/components";

interface Props {
  show: boolean;
  onHide: () => void;
}
export const AddEmailModal = ({ show, onHide }: Props) => {
  const { values, errors, setFieldValue }: any = useFormikContext();

  return (
    <Modal size="xl" show={show} onHide={onHide} centered backdrop="static">
      <Modal.Header className="bg-light">
        <Modal.Title>Manage Emails</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FieldArray
          name="ccEmails"
          render={(fieldHelpers) => (
            <Fragment>
              {values.ccEmails.length ? (
                values.ccEmails.map((holder: any, index: any) => (
                  <div
                    key={index}
                    className="d-flex gap-3 mb-4 align-items-start justify-content-center"
                  >
                    <div className="flex-grow-1">
                      <FormInput
                        labelText={`Email #${index + 1}`}
                        name={`ccEmails.${index}`}
                      />
                    </div>
                    <div className="mt-8">
                      <OverlayTrigger
                        placement="top"
                        overlay={
                          <Tooltip className="text-capitalize">Delete</Tooltip>
                        }
                      >
                        <Button
                          variant="danger"
                          onClick={() => fieldHelpers.remove(index)}
                        >
                          <i className="fs-4 bi bi-trash3"></i>
                        </Button>
                      </OverlayTrigger>
                    </div>
                  </div>
                ))
              ) : (
                <h2 className="text-center py-5">No available Emails</h2>
              )}
            </Fragment>
          )}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="light"
          onClick={onHide}
          disabled={errors.ccEmails?.length ? true : false}
        >
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>
        <Button
          className="btn-light-primary"
          onClick={() => {
            setFieldValue("ccEmails", ["", ...values.ccEmails]);
          }}
        >
          Add Email
        </Button>
        <Button
          variant="primary"
          onClick={onHide}
          disabled={errors.ccEmails?.length ? true : false}
        >
          <FormattedMessage id="BUTTON.SAVE" />
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
