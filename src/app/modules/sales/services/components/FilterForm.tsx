import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import { DatePickerCustom } from "src/app/partials/controls";
import { FormAutoComplete } from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const FilterForm = () => {
  const catalog = useGetCatalog();

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            labelText="Customer"
            name="customer"
            options={catalog?.customers || []}
            labelField="name"
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            labelText="Location"
            name="location"
            options={catalog?.locations || []}
            labelField="name"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            labelText="Seller"
            name="seller"
            options={catalog?.employees || []}
            labelField="name"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col sm="6">
          <DatePickerCustom label="From" name="saleDateFrom" />
        </Col>
        <Col sm="6">
          <DatePickerCustom label="To" name="saleDateTo" />
        </Col>
      </Row>
    </Fragment>
  );
};
