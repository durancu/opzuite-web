export const USAStateColumnFormatter = (
  cellContent: any,
  row: any,
  index: any,
  { states }: any
) => (
  <>{states && states.find((state: any) => state.id === cellContent)?.name}</>
);
