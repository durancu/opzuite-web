import { FC, Suspense } from "react";
import TopBarProgress from "react-topbar-progress-indicator";
import { getCSSVariableValue } from "src/_metronic/assets/ts/_utils";
import { WithChildren } from "src/_metronic/helpers";

export const SuspensedView: FC<WithChildren> = ({ children }) => {
  const baseColor = getCSSVariableValue("--bs-primary");
  TopBarProgress.config({
    barColors: {
      "0": baseColor,
    },
    barThickness: 1,
    shadowBlur: 5,
  });
  return <Suspense fallback={<TopBarProgress />}>{children}</Suspense>;
};
