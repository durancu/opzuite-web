export interface SaleItemVersion {
  customer: string; // Copy of policy->customer
  policy: SaleItem;

  action: string; //enum: UPDATE, REPLACE, DELETED
  deleted: boolean;
  updatedAt: Date;
  updatedBy: string;
}

export interface SaleItem {
  id?: string;
  code?: string;
  amount?: number;
  broker?: string;
  carrier?: string;
  details?: any;
  deleted?: boolean;
  description?: string;
  history?: SaleItemVersion[];
  premium?: number;
  deductible?: number;
  compensation?: number;
  limit?: number;
  product?: string;
  name?: string;
  agencyCommission?: number;
  type?: string;
  number?: string;
  nonCommissionableReferenceId?: any;
  includeInCommissions?: boolean;
  //effectiveAt?: Date;
  //expiresAt?: Date;
  //cancelledAt?: Date;
  //renewalFrequency?: string;
  autoRenew?: boolean;
  status?: string;
  lineOfBusiness?: string;
  createdBy?: string;
  updatedBy?: string;
}
