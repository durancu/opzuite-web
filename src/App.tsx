/**
 * Entry application component used to compose providers and render Routes.
 * */
import { Suspense } from "react";
import { Outlet } from "react-router-dom";
import { I18nProvider } from "src/_metronic/i18n/i18nProvider";
import { MasterInit } from "src/_metronic/layout/MasterInit";
import { LayoutProvider, LayoutSplashScreen } from "src/_metronic/layout/core";
import { InterceptorResponse } from "./app/components/ApiInterceptor";
import { BackdropUIProvider } from "./app/components/Backdrop";
import { NotifyComponent, SnackBarsStack } from "./app/components/Feedback";

const App = () => {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <I18nProvider>
        <LayoutProvider>
          <MasterInit />
          <SnackBarsStack>
            <NotifyComponent />
            <BackdropUIProvider>
              <InterceptorResponse />
              <Outlet />
            </BackdropUIProvider>
          </SnackBarsStack>
        </LayoutProvider>
      </I18nProvider>
    </Suspense>
  );
};

export default App;
