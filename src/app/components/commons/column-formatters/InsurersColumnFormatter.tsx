import { nanoid } from "@reduxjs/toolkit";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

export function InsurersColumnFormatter(cellContent: any, row: any) {
  return (
    <div>
      {!!row.items &&
        row.items.map((policy: any) => {
          const sameCarrierAndBroker: boolean =
            policy.brokerName &&
            policy?.carrierName &&
            policy?.carrierName === policy.brokerName;

          const providerLabel: any = sameCarrierAndBroker ? (
            <>
              <FormattedMessage id="FORM.LABELS.MGA" />/
              <FormattedMessage id="FORM.LABELS.CARRIER" />
            </>
          ) : (
            <FormattedMessage id="FORM.LABELS.CARRIER" />
          );
          const showConditions: any =
            !policy.deleted &&
            (policy?.carrierName?.length > 0 || policy.brokerName?.length > 0);

          return (
            showConditions && (
              <div className="d-flex align-items-center mb-2" key={nanoid()}>
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="quick-actions-tooltip">
                      <strong>{policy.product?.name}</strong>
                      <br />
                      {policy.product?.description}
                    </Tooltip>
                  }
                >
                  <div className="symbol symbol-30px me-3 ">
                    <span className="symbol-label bg-light badge-product-icon">
                      <i
                        className={`fa fa-${policy.product?.iconClass} ki-duotone ki-scroll fs-2x text-dark`}
                      />
                    </span>
                  </div>
                </OverlayTrigger>

                <div className="d-flex justify-content-start flex-column">
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip id="quick-actions-tooltip">
                        <span className="fw-bolder">
                          {providerLabel}:&nbsp;{" "}
                        </span>
                        <span className="fw-boldest">{policy.carrierName}</span>
                      </Tooltip>
                    }
                  >
                    <Link
                      hidden={!policy?.carrierName}
                      className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
                      to={`/carriers/${policy?.carrierCode}/detail`}
                    >
                      {policy?.carrierName}
                    </Link>
                  </OverlayTrigger>

                  {!sameCarrierAndBroker && (
                    <OverlayTrigger
                      placement="top"
                      overlay={
                        <Tooltip id="quick-actions-tooltip">
                          <span className="fw-bolder">
                            <FormattedMessage id="FORM.LABELS.MGA" />
                            :&nbsp;
                          </span>
                          <span className="fw-boldest">
                            {policy?.brokerName}
                          </span>
                        </Tooltip>
                      }
                    >
                      <Link
                        className="text-muted fw-semibold text-muted text-hover-primary d-block fs-7"
                        to={`/mgas/${policy?.brokerCode}/detail`}
                        hidden={policy.brokerName?.length === 0}
                      >
                        {policy?.brokerName}
                      </Link>
                    </OverlayTrigger>
                  )}
                </div>
              </div>
            )
          );
        })}
    </div>
  );
}
