import { formatDate } from "../../../components/functions";

type FieldType =
  | "text"
  | "select"
  | "number"
  | "date"
  | "multi-select"
  | "checkbox";

interface Options {
  id: string;
  name: string;
}

interface Props {
  label: string;
  value: string | number | any[];
  type: FieldType;
  description?: string;
  options?: Options[];
  labelClassName?: string;
  valueClassName?: string;
}

export const FakeField = ({
  label,
  value,
  description,
  type,
  options,
  labelClassName,
  valueClassName,
}: Props) => {
  function selectValue(value: any): string {
    return options?.find(({ id }: Options) => id === value)?.name || value;
  }

  function valueFormatter(value: string | number | any[]): any {
    switch (type) {
      case "date":
        return formatDate(new Date(value.toString()));
      case "number":
        return value;
      case "text":
        return value;
      case "select":
        return selectValue(value);
      case "multi-select":
        return value;
      case "checkbox":
        return value ? "Yes" : "No";
      default:
        return "-";
    }
  }

  return (
    <div>
      <p className={`${labelClassName}`}>{label}</p>
      <p
        className={`mt-3 mb-6 fw-bold text-nowrap field-label ${valueClassName}`}
      >
        {value ? valueFormatter(value) : "-"}
      </p>
      <small className="mt-5 text-secondary form-text">{description}</small>
    </div>
  );
};
