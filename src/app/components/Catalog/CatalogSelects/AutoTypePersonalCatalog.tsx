export const AUTO_TYPE_PERSONAL_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "SEDAN", name: "Sedan" },
  { id: "COUPE", name: "Coupe" },
  { id: "SPORT", name: "Sports Car " },
  { id: "WAGON", name: "Station Wagon" },
  { id: "HATCHBACK", name: "Hatchback" },
  { id: "CONVERTIBLE", name: "Convertible" },
  { id: "SUV", name: "SUV" },
  { id: "MINIVAN", name: "Minivan" },
  { id: "MID-PICKUP-TRUCK", name: "Mid- size Pickup Truck" },
  { id: "FULL-PICKUP-TRUCK", name: "Full - size Pickup Truck" },
  { id: "OTHER", name: "Other" },
];
