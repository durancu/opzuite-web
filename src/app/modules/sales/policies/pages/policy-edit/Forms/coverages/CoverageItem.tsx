import { useFormikContext } from "formik";
import { isEmpty } from "lodash";
import { Badge, Card, Col, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { FakeField } from "../../../../../../../partials/controls";
import { FakeFieldAdornment } from "./FakeFieldAdornment";

interface Props {
  item: any;
  isProviderSelected: boolean;
}

export const CoverageItem = ({ item, isProviderSelected }: Props) => {
  const { linesOfBusiness, brokers, carriers, employees, saleStatus } =
    useGetCatalog();
  const { values } = useFormikContext<any>();

  return (
    <Card.Body className="px-6">
      <Row className="mb-4">
        <Col>
          <FakeField
            type="select"
            label="Line of Business"
            value={item.lineOfBusiness}
            options={linesOfBusiness}
          />
        </Col>
        <Col>
          <FakeField
            type="text"
            label="Coverage"
            value={item.name}
            labelClassName="font-size-sm"
          />
        </Col>
        <Col>
          <FakeField
            type="select"
            label="MGA"
            value={item.broker}
            options={brokers}
            labelClassName={!isProviderSelected ? "text-danger" : ""}
            valueClassName={!isProviderSelected ? "text-danger" : ""}
          />
        </Col>
        <Col>
          <FakeField
            type="select"
            label="Carrier"
            value={item.carrier}
            options={carriers}
            labelClassName={
              !isProviderSelected ||
              (!isEmpty(item.broker) && isEmpty(item.carrier))
                ? "text-danger"
                : ""
            }
            valueClassName={
              !isProviderSelected ||
              (!isEmpty(item.broker) && isEmpty(item.carrier))
                ? "text-danger"
                : ""
            }
          />
        </Col>
        <Col>
          <FakeField
            type="select"
            label="Seller"
            value={item.seller || values.seller}
            options={employees}
          />
        </Col>
        <Col>
          <FakeField
            type="select"
            label="Status"
            value={item.status}
            options={saleStatus}
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col md={2} hidden={!values.isChargeItemized} className="d-flex">
          <div className="flex-column">
            <FakeField
              type="number"
              label="Premium"
              value={item.premium}
              labelClassName={`${
                item.isNonCommissionablePremium ? "text-danger" : ""
              }`}
              valueClassName=""
            />
          </div>
          <div className="flex-column ml-3 pt-0">
            <FakeFieldAdornment
              hidden={!item.isNonCommissionablePremium}
              type="warning"
              tooltipText="Premium not included in end-month sales commissions"
            />
          </div>
        </Col>
        <Col md={2} hidden={!values.isChargeItemized}>
          <FakeField type="number" label="Down Payment" value={item.amount} />
        </Col>
        <Col md={2}>
          <FakeField type="number" label="Limit" value={item.limit} />
        </Col>
        <Col md={2}>
          <FakeField type="number" label="Deductible" value={item.deductible} />
        </Col>
        <Col md={2}>
          <FakeField
            type="number"
            label="Compensation"
            value={item.compensation}
          />
        </Col>
      </Row>

      {/*
      {/* General, Automobile Umbrella/Excess and Workers/Employers Sepcific fields 
      {[
        "GENERAL_LIABILITY",
        "AUTOMOBILE_LIABILITY",
        "EXCESS_UMBRELLA_LIABILITY",
        "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
      ].includes(item?.product) && (
        <Fragment>
          {/* Workers and Employers 
          {item?.product === "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
            <Fragment>
              <Row className="mb-8">
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.hasExcludedPeople}
                    label="Any Proprietor/Partner/Executive/Officer/Member Excluded?"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.perStatute}
                    label="Per Statute"
                  />
                </Col>

                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.other}
                    label="Other"
                  />
                </Col>

                {item?.details?.hasExcludedPeople && (
                  <Col md={12} className="my-4">
                    <FakeField
                      type="text"
                      label="Description of People Excluded"
                      value={item?.details?.excludedPeopleDescription}
                    />
                  </Col>
                )}
              </Row>
              <Row className="mb-8">
                <Col>
                  <FakeField
                    type="number"
                    label="Employer's Liability Each Accident Limit"
                    value={item?.details?.employerLiabilityEachAccidentLimit}
                  />
                </Col>
                <Col>
                  <FakeField
                    type="number"
                    label="Employer's Liability Each Employee Limit"
                    value={item?.details?.employerLiabilityEachEmployeeLimit}
                  />
                </Col>
                <Col>
                  <FakeField
                    type="number"
                    label="Employer's Liability Disease Policy Limit"
                    value={item?.details?.employerLiabilityDiseasePolicyLimit}
                  />
                </Col>
              </Row>
            </Fragment>
          )}
          {/* Comercial Auto specific 
          {item?.product === "AUTOMOBILE_LIABILITY" && (
            <Fragment>
              <Row className="mb-8">
                <Col>
                  <FakeField
                    type="select"
                    value={item?.details?.autoCoverageScope}
                    label="Auto Coverage"
                    options={[
                      { name: "Any Auto", id: "ANY" },
                      { name: "Scheduled Auto", id: "SCHEDULED" },
                    ]}
                  />
                </Col>
              </Row>

              <Row className="mb-8 d-flex gap-4 align-items-start">
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeOwnedAutos}
                    label="Include Owned Autos"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeHiredAutos}
                    label="Include Hired Autos"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeNonOwnedAutos}
                    label="Include Non-Owned Autos"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeOtherCoveredAutoA}
                    label="Include Other Covered Auto A"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeOtherCoveredAutoB}
                    label="Include Other Covered Auto B"
                  />
                </Col>
              </Row>

              <Row className="mb-8">
                {item?.details?.includeOtherCoveredAutoA && (
                  <Col>
                    <FakeField
                      type="text"
                      label="Other Covered Auto A Description"
                      value={item?.details?.otherCoveredAutoADescription}
                    />
                  </Col>
                )}
                {item?.details?.includeOtherCoveredAutoB && (
                  <Col>
                    <FakeField
                      type="text"
                      label="Other Covered Auto B Description"
                      value={item?.details?.otherCoveredAutoBDescription}
                    />
                  </Col>
                )}
              </Row>

              <Row className="mb-8">
                <Col>
                  <FakeField
                    type="number"
                    label="Combined Single Limit (ea Accident)"
                    value={item?.details?.combinedSingleLimit}
                  />
                </Col>
                <Col>
                  <FakeField
                    type="number"
                    label="Bodily Injury (Per Person)"
                    value={item?.details?.bodilyInjuryPerPersonLimit}
                  />
                </Col>

                <Col>
                  <FakeField
                    type="number"
                    label="Bodily Injury (Per Accident)"
                    value={item?.details?.bodilyInjuryPerAccidentLimit}
                  />
                </Col>

                <Col>
                  <FakeField
                    type="number"
                    label="Property Damage (Per Accident)"
                    value={item?.details?.propertyDamagePerAccidentLimit}
                  />
                </Col>
              </Row>
            </Fragment>
          )}

          {/* Umbrella/Excess specific 
          {item?.product === "EXCESS_UMBRELLA_LIABILITY" && (
            <Row className="mb-8">
              <Col>
                <FakeField
                  type="select"
                  value={item?.details?.coverageFormUsed}
                  label="Coverage Form Used"
                  options={[
                    { name: "Umbrella Liability", id: "UMBRELLA" },
                    { name: "Excess Liability", id: "EXCESS" },
                  ]}
                />
              </Col>

              <Col>
                <FakeField
                  type="number"
                  label="Retention Amount"
                  value={item?.details?.deductibleOrRetentionAmount}
                />
              </Col>
              <Col className="d-flex justify-content-center align-items-center">
                <FakeField
                  type="checkbox"
                  value={item?.details?.hasDeductible}
                  label="Has Deductible"
                />
              </Col>
              <Col className="d-flex justify-content-center align-items-center">
                <FakeField
                  type="checkbox"
                  value={item?.details?.hasRetention}
                  label="Has Retention"
                />
              </Col>
            </Row>
          )}

          {/* Shared rows, for excess and general only  
          {![
            "AUTOMOBILE_LIABILITY",
            "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
          ].includes(item?.product) && (
            <Row className="mb-8">
              <Col>
                <FakeField
                  type="select"
                  value={item?.details?.policyBasedForm}
                  label="Policy Based Form"
                  options={[
                    { name: "Claims-based form", id: "CLAIMS" },
                    { name: "Occurrence-based form", id: "OCCURENCE" },
                  ]}
                />
              </Col>

              <Col className="mb-4">
                <FakeField
                  type="number"
                  label="Each Occurrence Limit"
                  value={item?.details?.eachOccurrenceLimit}
                />
              </Col>
              <Col className="mb-4">
                <FakeField
                  type="number"
                  label="General Aggregate Limit "
                  value={item?.details?.generalAggregateLimit}
                />
              </Col>
            </Row>
          )}

          {/* Checkboxes  
          <Row className="mb-8 d-flex gap-4 align-items-center">
            {item?.product !==
              "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
              <Col>
                <FakeField
                  type="checkbox"
                  value={item?.details?.additionalInsured}
                  label="Additional Insured"
                />
              </Col>
            )}
            <Col>
              <FakeField
                type="checkbox"
                value={item?.details?.waiverOfSubrogation}
                label="Waiver of Subrogation?"
              />
            </Col>
          </Row>

          {/* General Specific 
          {item?.product === "GENERAL_LIABILITY" && (
            <Fragment>
              <Row className="mb-8 d-flex gap-4 align-items-start">
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeOtherCoverageA}
                    label="Include Other Coverage (A)?"
                  />
                </Col>
                <Col>
                  <FakeField
                    type="checkbox"
                    value={item?.details?.includeOtherCoverageB}
                    label="Include Other Coverage (B)?"
                  />
                </Col>
              </Row>

              <Row className="mb-8">
                {item?.details?.includeOtherCoverageA && (
                  <Col>
                    <FakeField
                      type="text"
                      label="Other Coverage (A) Description"
                      value={item?.details.otherCoverageADescription}
                    />
                  </Col>
                )}
                {item?.details?.includeOtherCoverageB && (
                  <Col>
                    <FakeField
                      type="text"
                      label="Other Coverage (B) Description"
                      value={item?.details.otherCoverageBDescription}
                    />
                  </Col>
                )}
              </Row>
              <Row className="mb-8">
                <Col md={4}>
                  <FakeField
                    type="select"
                    value={item?.details?.generalAggregateLimitAppliesPer}
                    label="General Aggregate Limit Applies Per"
                    options={[
                      { name: "Policy", id: "POLICY" },
                      { name: "Project", id: "PROJECT" },
                      { name: "Location", id: "LOCATION" },
                      { name: "Other", id: "OTHER" },
                    ]}
                  />
                </Col>

                {item?.details?.generalAggregateLimitAppliesPer === "OTHER" && (
                  <Col md={4}>
                    <FakeField
                      type="text"
                      label="Enter Other"
                      value={
                        item?.details?.generalAggregateLimitAppliesPerOther
                      }
                    />
                  </Col>
                )}
                <Col md={4} className="mb-4">
                  <FakeField
                    type="number"
                    label="Damage to Rented Premises Limit"
                    value={item?.details?.damageToRentedPremisesLimit}
                  />
                </Col>
                <Col md={4} className="mb-4">
                  <FakeField
                    type="number"
                    label="Medical Expenses Limit"
                    value={item?.details?.medicalExpensesLimit}
                  />
                </Col>
                <Col md={4} className="mb-4">
                  <FakeField
                    type="number"
                    label="Personal and Advertising Injury Limit"
                    value={item?.details?.personalAndAdvertisingInjuryLimit}
                  />
                </Col>
                <Col md={4} className="mb-4">
                  <FakeField
                    type="number"
                    label="Products/Completed Operations Aggregate Limit"
                    value={
                      item?.details?.productCompletedOperationsAggregateLimit
                    }
                  />
                </Col>
              </Row>
            </Fragment>
          )}

          {/* Other  limits except workers and employers
          {item?.product !== "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
            <Row className="mb-8">
              <Col>
                <FakeField
                  type="text"
                  label="Other Limit 1 Name"
                  value={item?.details?.other1LimitName}
                />
              </Col>
              <Col>
                <FakeField
                  type="number"
                  label="Other Limit 1 Amount"
                  value={item?.details?.other1LimitAmount}
                />
              </Col>

              {/* Shared limits, for excess and general only 
              {item?.product !== "AUTOMOBILE_LIABILITY" && (
                <Fragment>
                  <Col>
                    <FakeField
                      type="text"
                      label="Other Limit 2 Name"
                      value={item?.details?.other2LimitName}
                    />
                  </Col>
                  <Col>
                    <FakeField
                      type="number"
                      label="Other Limit 2 Amount"
                      value={item?.details?.other2LimitAmount}
                    />
                  </Col>
                </Fragment>
              )}
            </Row>
          )}
        </Fragment>
      )}
*/}
      <Row className="mb-4">
        {![
          "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
          "OCCUPATIONAL_ACCIDENT",
        ].includes(item?.product) && (
          <Col>
            <p>Vehicles</p>
            <div className="d-flex gap-4 flex-wrap">
              {item.vehicles && item.vehicles.length
                ? item.vehicles.map((vehicle: any, index: number) => (
                    <Badge
                      key={index}
                      pill
                      bg="secondary"
                      className="p-3 fw-bold"
                    >
                      {vehicle.vinNumber && `VIN: ${vehicle.vinNumber}`}
                    </Badge>
                  ))
                : "-"}
            </div>
          </Col>
        )}

        {((![
          "AUTOMOBILE_LIABILITY",
          "PHYSICAL_DAMAGE",
          "TRAILER_INTERCHANGE_PHYSICAL_DAMAGE",
        ].includes(item?.product) &&
          !["BOBTAIL_LIABILITY", "NON_OWNED_TRAILER_LIABILITY"].includes(
            item?.lineOfBusiness
          )) ||
          item?.details?.autoCoverageScope === "SCHEDULED") && (
          <Col>
            <p>Cohorts/Drivers</p>
            <div className="d-flex gap-4 flex-wrap">
              {item.cohorts && item.cohorts.length
                ? item.cohorts.map((cohort: any, index: number) => (
                    <Badge
                      key={index}
                      pill
                      bg="secondary"
                      className="p-3 fw-bold"
                    >
                      {cohort.firstName} {cohort.lastName}
                    </Badge>
                  ))
                : "-"}
            </div>
          </Col>
        )}
      </Row>
    </Card.Body>
  );
};
