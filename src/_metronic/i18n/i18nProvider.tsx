import { FC } from "react";
import { useLang } from "./Metronici18n";
import { IntlProvider } from "react-intl";
import "@formatjs/intl-relativetimeformat/polyfill";
import "@formatjs/intl-relativetimeformat/locale-data/en";
import "@formatjs/intl-relativetimeformat/locale-data/es";

import { WithChildren } from "../helpers";

import { I18nDictionary } from "./messages/I18nDictionary";

const I18nProvider: FC<WithChildren> = ({ children }) => {
  const locale = useLang();
  const messages = I18nDictionary[locale];

  return (
    <IntlProvider locale={locale} messages={messages}>
      {children}
    </IntlProvider>
  );
};

export { I18nProvider };
