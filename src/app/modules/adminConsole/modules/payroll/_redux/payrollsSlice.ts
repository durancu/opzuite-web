import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialPayrollsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  payrollForEdit: undefined,
  payrollAvailableDates: undefined,
  lastError: null,
};
export const callTypes = {
  list: "list",
  action: "action",
};

export const payrollAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getPayroll: build.query<any, QueryParams>({
      query: (queryParams) => ({
        url: "/payrolls/search",
        method: "post",
        body: { queryParams },
      }),
    }),
  }),
  overrideExisting: false,
});

export const payrollsSlice = createSlice({
  name: "payrolls",
  initialState: initialPayrollsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getPayrollByCode
    payrollFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.payrollForEdit = action.payload.payrollForEdit;
      state.error = null;
    },
    // findPayrolls
    payrollsFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // prepareAvailableDatePayroll
    payrollAvailableDates: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      state.payrollAvailableDates = action.payload.payroll;
    },
    // initializedPayroll
    payrollInitialize: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      state.payrollForEdit = action.payload.payroll;
    },
    // initializedPayroll
    payrollInitialized: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      state.payrollForEdit = action.payload.payroll;
    },
    payrollUpdated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      state.payrollCreate = action.payload.payroll;
    },
    // createPayroll
    payrollCreated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      state.payrollForEdit = action.payload.payroll;
    },
    // generate Payrolls
    payrollsCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // executePayroll
    payrollExecuted: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // deletePayroll
    payrollDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => el.code !== action.payload.code
      );
    },
    // deletePayrolls
    payrollsDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => !action.payload.ids.includes(el.id)
      );
    },
  },
});
