import { TextField } from "@mui/material";
import { useFormikContext } from "formik";
import objectPath from "object-path";
import { useState } from "react";

interface Props {
  name: string;
  label: string;
  type?: string;
  required?: boolean;
  isNegative?: boolean;
  disabled?: boolean;
  otherOnblurActions?: any;
  startAdornment?: any;
  endAdornment?: any;
  style?: any;
  multiline?: boolean;
  minRows?: number;
  variant?: string;
  inputComponent?: any;
}

export const OnBlurField = (props: Props) => {
  const {
    otherOnblurActions,
    name,
    label,
    type,
    required,
    isNegative,
    disabled,
    startAdornment,
    endAdornment,
    style,
    multiline,
    minRows,
    variant,
    inputComponent,
  } = props;

  const { values, handleChange, errors, validateField } =
    useFormikContext<any>();
  const [value, setValue] = useState<any>(objectPath.get(values, name));

  const thisHandleChange = ({ target }: any) => {
    setValue(target.value);
  };

  const onBlurActions = (e: any) => {
    handleChange(e);
    validateField(name);
    otherOnblurActions && otherOnblurActions();
  };

  return (
    <TextField
      name={name}
      label={label}
      type={type}
      autoComplete={type === "password" ? "on" : "off"}
      required={required}
      disabled={disabled}
      onBlur={onBlurActions}
      onChange={thisHandleChange}
      value={isNegative && type === "number" ? -1 * Math.abs(value) : value}
      error={objectPath.get(errors, name) ? true : false}
      helperText={objectPath.get(errors, name)}
      {...style}
      InputProps={{
        endAdornment: endAdornment,
        startAdornment: startAdornment,
        inputComponent,
      }}
      minRows={minRows}
      multiline={multiline}
      variant={variant}
    />
  );
};
