// routes module, to be imported in adminconsole
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Locations } from "./pages/Locations";
import { ChangeIP } from "./pages/ChangeIP";
import { Details } from "./pages/Details";
import { Edit } from "./pages/edit/Edit";
import { Delete } from "./pages/Delete";

export const locationRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.LOCATIONS_READ} isRoute>
        <Locations />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.LOCATIONS_CREATE} isRoute>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.LOCATIONS_UPDATE} isRoute>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/change-ip",
    element: (
      <RequirePermission permissions={Permissions.LOCATIONS_UPDATE} isRoute>
        <ChangeIP />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.LOCATIONS_READ} isRoute>
        <Details />
      </RequirePermission>
    ),
  },

  {
    path: ":code/delete",
    element: <Delete />,
  },
];
