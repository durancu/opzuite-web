import { useEffect } from "react";
import { Form, Row } from "react-bootstrap";
import { Field, useFormikContext } from "formik";
import { FormattedMessage } from "react-intl";
import { timezonesOptions } from "src/app/components/functions";
import { FormAutoComplete, WizardFormSection } from "src/app/components";

const timezones = timezonesOptions();

const FormCustomerSetting = () => {
  const { values, setFieldValue }: any = useFormikContext();

  useEffect(() => {
    // identify and set user timezone if exists. Defaults to US/Central
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    if (timezone) {
      setFieldValue("contact.timezone", timezone);
    }
  }, [setFieldValue]);

  return (
    <WizardFormSection label="Settings">
      <Row className="mb-4">
        <Form.Group className="col">
          <Form.Label>
            <FormattedMessage id="FORM.LABELS.LANGUAGE" />
          </Form.Label>
          <Field as={Form.Select} name="contact.language">
            <option value="en">English</option>
            <option value="es">Spanish</option>
          </Field>
        </Form.Group>
        <Form.Group className="col">
          <FormAutoComplete
            name="contact.timezone"
            labelTextId="FORM.LABELS.TIMEZONE"
            options={timezones || []}
          />
        </Form.Group>
      </Row>
      <Row className="mb-4">
        <Form.Group className="col">
          <Form.Label>Email notifications</Form.Label>
          <Field
            as={Form.Check}
            type="switch"
            checked={values?.communication?.email}
            name="communication.email"
            label="Do you want to receive notifications by email?"
          />
        </Form.Group>
        <Form.Group className="col">
          <Form.Label>SMS notifications</Form.Label>
          <br />
          <Field
            as={Form.Check}
            type="switch"
            checked={values?.communication?.sms}
            name="communication.sms"
            label=" Do you want to receive notifications by sms?"
          />
        </Form.Group>
        <Form.Group className="col">
          <Form.Label>Phone notifications</Form.Label>
          <br />
          <Field
            as={Form.Check}
            type="switch"
            checked={values?.communication?.phone}
            name="communication.phone"
            label="Do you want to receive notifications by phone?"
          />
        </Form.Group>
      </Row>
    </WizardFormSection>
  );
};

export default FormCustomerSetting;
