import { Modal, Button, Col, Row } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { ENDORSEMENT_ITEM_TYPE } from "src/app/components/EndorsementsComponents";
import { FormInput, FormTextArea, FormAutoComplete } from "src/app/components";
import { DatePickerCustom } from "src/app/partials/controls";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { useFormikContext } from "formik";
import { HeaderSelector } from "./HeaderSelector";
import objectPath from "object-path";

type MODAL_ACTION = string | "EDIT" | "ADD" | "";

interface Props {
  type: ENDORSEMENT_ITEM_TYPE;
  action: MODAL_ACTION;
  index: any;
  show: boolean;
  onHide: () => void;
  endorsementIndex: number;
}

export const EndorsementItemModal = ({
  type,
  index,
  action,
  show,
  onHide,
  endorsementIndex,
}: Props) => {
  const { values, errors, setFieldValue } = useFormikContext<any>();
  const item = values?.endorsements?.[endorsementIndex].items?.[index] || {};

  const {
    financers,
    remitToOptions,
    endorsementItemTypes,
    endorsementStatus,
    employees,
  } = useGetCatalog();

  const handleClose = () => {
    if (action === "ADD") {
      setFieldValue(
        `endorsements.${endorsementIndex}.items`,
        values.endorsements[endorsementIndex].items.slice(0, index)
      );
    }
    onHide();
  };

  function formatName(name: string) {
    return index !== undefined
      ? `endorsements.${endorsementIndex}.items.${index}.${name}`
      : name;
  }

  return (
    <Modal size="xl" show={show} centered>
      <Modal.Header className="p-0">
        <Modal.Title className="w-100">
          <HeaderSelector
            type={type}
            action={action}
            index={index}
            endorsementIndex={endorsementIndex}
          />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/*Shared fields/defaults */}
        <Row className="mb-4">
          <Col md={4}>
            <FormAutoComplete
              name={formatName("type")}
              labelText="Type"
              options={endorsementItemTypes[item.accountingClass] || []}
              disabled={
                endorsementIndex === 0 &&
                index === 0 &&
                objectPath.get(
                  values,
                  `endorsements.${endorsementIndex}.type`
                ) === "INITIAL_BASE_PREMIUM" &&
                objectPath.get(
                  values,
                  `endorsements.${endorsementIndex}.items.${index}.type`
                ) === "DOWN_PAYMENT"
              }
            />
          </Col>
          <Col md={4}>
            <DatePickerCustom name={formatName("endorsedAt")} label="Date" />
          </Col>
          <Col md={4}>
            <FormInput
              type="number"
              labelText="Amount"
              name={formatName("amount")}
              disabled={
                endorsementIndex === 0 &&
                index === 0 &&
                objectPath.get(values, `endorsements.${0}.type`) ===
                  "INITIAL_BASE_PREMIUM" &&
                objectPath.get(values, `endorsements.${0}.items.${0}.type`) ===
                  "DOWN_PAYMENT"
              }
            />
          </Col>
        </Row>

        {/* shared by agent comission and fee/tax */}
        {["AGENT_COMMISSION", "FEE_TAX"].includes(type) && (
          <Row className="mb-4">
            <Col>
              <FormAutoComplete
                name={formatName("status")}
                labelText="Status"
                options={endorsementStatus}
              />
            </Col>
            <Col>
              <FormAutoComplete
                name={formatName("seller")}
                labelText="Seller"
                options={employees}
              />
            </Col>
          </Row>
        )}

        {type === "FINANCING_DIRECT_BILL" && (
          <Row className="mb-4">
            <Col>
              <FormAutoComplete
                name={formatName("financerCompany")}
                labelText="Finance Company (PFA)"
                options={financers}
              />
            </Col>
          </Row>
        )}

        {/* Shared between payable and fee/tax */}
        {["PAYABLE", "FEE_TAX"].includes(type) && (
          <Row className="mb-4">
            <FormAutoComplete
              labelText="Remit To"
              name={formatName("otherDetails.remitTo")}
              value={item?.otherDetails?.remitTo}
              options={remitToOptions}
            />
          </Row>
        )}

        {/* Shared description */}
        <Row className="mb-4">
          {!["RECEIVABLE", "PAYABLE"].includes(type) && (
            <Col md={12} className="mb-4">
              <FormTextArea
                labelText="Description"
                name={formatName("description")}
              />
            </Col>
          )}

          {/* those that need other details */}
          {["FINANCING_DIRECT_BILL", "AGENT_COMMISSION"].includes(type) && (
            <Col md={12} className="mb-4">
              <FormTextArea
                labelText="Other descriptions"
                name={formatName("otherDetails.description")}
              />
            </Col>
          )}
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" onClick={handleClose} className="fw-bolder">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>
        <Button
          variant="primary"
          className="px-6 py-3 d-flex gap-2"
          onClick={onHide}
          disabled={objectPath.get(
            errors,
            `endorsements.${endorsementIndex}.items.${index}`
          )}
        >
          <span className="mr-3">
            <FormattedMessage id="BUTTON.SAVE" />
          </span>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
