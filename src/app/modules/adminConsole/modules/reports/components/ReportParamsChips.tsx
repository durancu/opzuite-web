import { Chip } from "@mui/material";
import { Col, Row } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { paramsChipListGenerator } from "./listingChipGenerator";

export const ReportParamChips = () => {
  const { catalog, filterParams } = useSelector(
    (state: any) => ({
      catalog: state.catalogs?.entities,
      filterParams: state.reports.appliedParams,
    }),
    shallowEqual
  );

  return (
    <Row
      className="mt-3 d-flex align-items-center gap-4"
      hidden={!(filterParams?.groupByModel || filterParams?.source)}
    >
      <Col>
        <span className="">Params: </span>
        {paramsChipListGenerator(filterParams || {}, catalog).map(
          ({ label }: any) => (
            <Chip label={label} key={label} size="small" />
          )
        )}
      </Col>
    </Row>
  );
};
