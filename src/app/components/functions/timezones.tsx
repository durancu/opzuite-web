import { tz } from "moment-timezone";

export const timezonesOptions = () => {
  return tz.names().map((timezone: string) => ({
    name: timezone,
    id: timezone,
  }));
};

export const defaultTimezoneByCountry = (country: string) => {
  switch (country) {
    case "MEX":
      return "America/Mexico_City";
    case "COL":
      return "America/Bogota";
    case "USA":
    default:
      return "US/Central";
  }
};
