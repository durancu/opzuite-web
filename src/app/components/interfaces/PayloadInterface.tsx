export interface PayloadInterface {
  payload:
    | undefined
    | {
        callType: string;
      };
  type: string;
}
