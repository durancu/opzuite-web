import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import { useThemeMode } from "src/_metronic/partials";
import { toAbsoluteUrl } from "src/_metronic/helpers";

const BODY_CLASSES = ["bgi-size-cover", "bgi-position-center", "bgi-no-repeat"];

export const ErrorsLayout = () => {
  const { mode } = useThemeMode();
  useEffect(() => {
    BODY_CLASSES.forEach((c) => document.body.classList.add(c));
    document.body.style.backgroundImage =
      mode === "dark"
        ? `url(${toAbsoluteUrl("/media/auth/bg7-dark.jpg")})`
        : `url(${toAbsoluteUrl("/media/auth/bg7.jpg")})`;

    return () => {
      BODY_CLASSES.forEach((c) => document.body.classList.remove(c));
      document.body.style.backgroundImage = "none";
    };
  }, [mode]);

  return (
    <div className="d-flex text-center align-items-center justify-content-center">
      <div className="card card-flush  w-lg-650px py-5 mt-20">
        <div className="card-body p-lg-20">
          <Outlet />
        </div>
      </div>
    </div>
  );
};
