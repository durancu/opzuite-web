export const NameColumnFormatter = (cellContent: any, row: any) => (
  <>{`${row?._id?.firstName} ${row?._id?.lastName}`}</>
);
