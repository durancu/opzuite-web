import { formatAmountWithCommas } from "src/app/components/functions";
import { getCSSVariableValue } from "src/_metronic/assets/ts/_utils";
interface ChartDataInterface {
  series?: { name: string; data: any[] };
  categories?: string[];
}

interface Colors {
  gray900: string;
  gray500: string;
  gray600: string;
  gray200: string;
  gray300: string;
  baseColor: string;
  colorClass: string;
}

function getChartColors(name: string): Colors {
  let baseColor = "success";

  if (name.includes("Permit")) {
    baseColor = "primary";
  } else if (name.includes("Policy")) {
    baseColor = "success";
  } else if (name.includes("Carrier")) {
    baseColor = "danger";
  } else if (name.includes("Broker")) {
    baseColor = "warning";
  } else if (name.includes("Location")) {
    baseColor = "info";
  }

  return {
    gray900: getCSSVariableValue("--bs-gray-900"),
    gray500: getCSSVariableValue("--bs-gray-500"),
    gray600: getCSSVariableValue("--bs-gray-600"),
    gray200: getCSSVariableValue("--bs-gray-200"),
    gray300: getCSSVariableValue("--bs-gray-300"),
    baseColor: getCSSVariableValue(`--bs-${baseColor}`),
    colorClass: baseColor,
  };
}

function getChartIcon(name: string) {
  let icon = "bi bi-grid-1x2";

  if (name.includes("Permit")) {
    icon = "bi bi-app-indicator";
  } else if (name.includes("Policy")) {
    icon = "bi bi-shield-shaded";
  } else if (name.includes("Carrier")) {
    icon = "bi bi-umbrella-fill";
  } else if (name.includes("Broker")) {
    icon = "fa-regular fa-handshake";
  } else if (name.includes("Location")) {
    icon = "bi bi-geo-alt";
  }

  return icon;
}

export function getChartConfig(name = "", dataset: ChartDataInterface) {
  const colors = getChartColors(name);
  const options = {
    chart: {
      type: "bar",
      height: "100%",
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: true,
      },
      sparkline: {
        enabled: false,
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "55%",
        borderRadius: 5,
      },
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    fill: {
      type: "solid",
      opacity: 1,
    },
    stroke: {
      curve: "smooth",
      show: false,
      width: 2,
      colors: [colors.baseColor],
    },
    xaxis: {
      categories: dataset.categories,
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: true,
        style: {
          colors: colors.gray900,
          fontSize: "0.8rem",
          fontFamily: "inherit",
        },
      },
      crosshairs: {
        show: true,
        position: "front",
        stroke: {
          color: colors.gray300,
          width: 1,
          dashArray: 3,
        },
      },
      tooltip: {
        enabled: false,
      },
    },
    yaxis: {
      min: 0,
      //tickAmount: 1,
      labels: {
        show: true,
        style: {
          colors: colors.gray900,
          fontSize: "0.9rem",
          fontFamily: "inherit",
        },
        formatter: function (val: number) {
          return formatAmountWithCommas(parseInt(val.toFixed(0)));
        },
      },
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      hover: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0,
        },
      },
    },
    tooltip: {
      style: {
        fontSize: "0.9rem",
        fontFamily: "inherit",
      },
      y: {
        formatter: function (val: any) {
          return "$" + Math.round(val);
        },
      },
    },
    colors: [colors.baseColor],
    markers: {
      colors: [colors.baseColor],
      strokeColor: [colors.baseColor],
      strokeWidth: 3,
    },
  };

  return {
    options,
    icon: getChartIcon(name),
    colorClass: colors.colorClass,
  };
}
