import { kebabCase } from "lodash";
import { METRIC_COLORS } from "../constants/General";

// helper functions
const includesPermission = (
  userPermission: string,
  permissions: string[]
): boolean => {
  if (permissions.includes(userPermission)) return true;

  return permissions.some((permissionToCheck: any): boolean => {
    return permissionToCheck && permissionToCheck.includes(userPermission);
  });
};

// check if permissions exist
export const isPermitted = (
  permissions: string | string[] = [],
  userPermissions: string[] = []
): boolean => {
  /* since permissions can either be single or an array we want to
   * make sure we always send an array to the checker
   */
  const normalizedPermissions = Array.isArray(permissions)
    ? permissions
    : [permissions];

  // catch all permission errors
  if (!permissions || !permissions.length || !userPermissions.length)
    return false;

  return userPermissions.some((permission) => {
    return includesPermission(permission, normalizedPermissions);
  });
};

export function getMetricColorByTitle(title: string) {
  const normalizedTitle = kebabCase(title);
  return METRIC_COLORS[normalizedTitle] ?? METRIC_COLORS["non-premium"];
}
