/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const PermitActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openEditPermitPage, openDeletePermitDialog, openDetailPermitPage }: any
) => (
  <TableActionsMenu
    readFunction={openDetailPermitPage}
    editFunction={openEditPermitPage}
    deleteFunction={openDeletePermitDialog}
    editPermission={Permissions.APPLICATIONS_UPDATE}
    readPermission={Permissions.APPLICATIONS_READ}
    deletePermission={Permissions.APPLICATIONS_DELETE}
    code={row.code}
  />
);
