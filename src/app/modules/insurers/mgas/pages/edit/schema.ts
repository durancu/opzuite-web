
import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const mgaBusinessValidationSchema = Yup.object().shape({
  business: Yup.object().shape({
    name: Yup.string().required("Business Name is required."),
    primaryPhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    //.required("Phone Number is required."),
    email: Yup.string().trim().email("Must be a valid email address."),
    //.required("Email is required."),
    fax: Yup.string().trim(),
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      zip: Yup.string(),
      country: Yup.string(),
    }),
    website: Yup.string().trim(),
    naicCode: Yup.string()
      .min(5, "NAIC Code must be 5 digits")
      .max(5, "NAIC Code must be 5 digits"),
    startedAt: Yup.mixed().nullable(),
  }),
});

export const mgaContactInfoValidationSchema = Yup.object().shape({
  contact: Yup.object().shape({
    firstName: Yup.string(),
    lastName: Yup.string(),
    email: Yup.string().trim().email("Must be a valid email address."),
    phone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    mobilePhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
  }),
});

export const mgaCommissionsValidationSchema = Yup.object().shape({
  commissions: Yup.array().of(
    Yup.object().shape({
      coverage: Yup.string().required("Coverages is no set."),
      percent: Yup.number().required("This field is required."),
    })
  ),
});
