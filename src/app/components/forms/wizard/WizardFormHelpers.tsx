import { Fragment, ReactNode } from "react";
import { ObjectSchema } from "yup";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import clsx from "clsx";

type Base = {
  label?: string;
  labelText?: string;
  labelTextId?: string;
  children: ReactNode;
  className?: string;
  action?: ReactNode;
};

type WFStep = Base & {
  validationSchema?: ObjectSchema<any>;
};

export const WizardFormStep = ({ children }: WFStep) => {
  return <Fragment>{children}</Fragment>;
};

export const WizardFormSection = ({
  label,
  labelText,
  labelTextId,
  children,
  className,
  action,
}: Base) => {
  return (
    <Card className={clsx("mb-4", className)}>
      <Card.Header className="p-0 d-flex justify-content-between">
        <Card.Title as="h3" className="fw-bold text-primary mb-10">
          {labelTextId ? (
            <FormattedMessage id={labelTextId} />
          ) : (
            label || labelText
          )}
        </Card.Title>
        <div>{action}</div>
      </Card.Header>

      <Card.Body className="p-0">{children}</Card.Body>
    </Card>
  );
};
