import { Fragment } from "react";
import { Outlet } from "react-router-dom";
import { ProfileHeader } from "./ProfileHeader";

export const ProfileLayout = () => {
  return (
    <Fragment>
      <ProfileHeader />
      <Outlet />
    </Fragment>
  );
};
