import { DatePicker, MuiPickersContext } from "@material-ui/pickers";
import React, { useContext, useState } from "react";

export function DateRangeCustom({
  pickerComponent = DatePicker,
  value,
  onChange,
  labelFunc,
  format,
  emptyLabel,
  autoOk,
  onClose,
  ...props
}: any) {
  const PickerComponent = pickerComponent || DatePicker;
  const [begin, setBegin] = useState(value[0]);
  const [end, setEnd] = useState(value[1]);
  const [hover, setHover] = useState(undefined);
  const picker: any = React.useRef();
  const utils: any = useContext(MuiPickersContext);

  const min = Math.min(begin, end || hover);
  const max = Math.max(begin, end || hover);

  function renderDay(
    day: any,
    selectedDate: any,
    dayInCurrentMonth: any,
    dayComponent: any
  ) {
    return React.cloneElement(dayComponent, {
      onClick: (e) => {
        e.stopPropagation();
        if (!begin) setBegin(day);
        else if (!end) {
          setEnd(day);
          if (autoOk) {
            onChange([begin, day].sort());
            picker.current.close();
          }
        } else {
          setBegin(day);
          setEnd(undefined);
        }
      },
      onMouseEnter: () => setHover(day),
    });
  }

  const formatDate = (date: any) =>
    utils.format(date, format || utils.dateFormat);

  return (
    <>
      <div hidden={true}>
        {min}
        {max}
      </div>
      <PickerComponent
        {...props}
        value={begin}
        renderDay={renderDay}
        onClose={() => {
          onChange([begin, end].sort());
          if (onClose) onClose();
        }}
        onClear={() => {
          setBegin(undefined);
          setEnd(undefined);
          setHover(undefined);
          onChange([]);
        }}
        forwardedRef={picker}
        labelFunc={(date: any, invalid: any) =>
          labelFunc
            ? labelFunc([begin, end].sort(), invalid)
            : date && begin && end
            ? `${formatDate(begin)} - ${formatDate(end)}`
            : emptyLabel || ""
        }
      />
    </>
  );
}
