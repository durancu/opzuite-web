import { Dispatch } from "redux";
import { variantAccordingToHttpCode } from "src/app/components/Feedback";
import { feedbacksSlice } from "src/app/components/Feedback/_redux/feedbackSlice";
import * as requestFromServer from "./reportCrud";
import { callTypes, reportsSlice } from "./reportsSlice";

const { actions } = reportsSlice;

export const fetchReports = (queryParams: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.list } as any));
  return requestFromServer
    .findReports(queryParams)
    .then((response) => {
      const { totalCount, entities } = response.data;
      dispatch(actions.reportsFetched({ totalCount, entities } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find reports";
      dispatch(actions.catchError({ error, callType: callTypes.list } as any));
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const fetchReport = (code: any) => (dispatch: Dispatch) => {
  if (!code) {
    return dispatch(actions.reportFetched({ reportForEdit: undefined } as any));
  }

  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getReportById(code)
    .then((response) => {
      const report = response.data;
      dispatch(actions.reportFetched({ reportForEdit: report } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find report";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const fetchReportChartData = (code: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getReportChartData(code)
    .then((response) => {
      const chart = response.data;
      dispatch(actions.reportCharDataFetched({ chart: chart } as any));
      return chart;
    })
    .catch((error) => {
      error.clientMessage = "Can't find report";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const deleteReport = (code: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deleteReport(code)
    .then((response) => {
      dispatch(actions.reportDeleted());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Report deleted successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete report";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const createReport =
  (reportForCreation: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .createReport(reportForCreation)
      .then((response) => {
        const report = response.data;
        dispatch(actions.reportCreated());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Report created successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
        return report;
      })
      .catch((error) => {
        error.clientMessage = "Can't create report";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };
export const resultReport =
  (queryParams: any, code: string) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .reportResults(queryParams, code)
      .then((response) => {
        const report = response.data?.report;
        dispatch(actions.reportResult({ report } as any));
        return response.data;
      })
      .catch((error) => {
        error.clientMessage = "Can't load report";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const updateReport = (report: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .updateReport(report)
    .then((response) => {
      //const report = response.data;
      dispatch(actions.reportUpdated());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Report updated successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't update report";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const updateReportsStatus =
  (ids: any, status: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .updateStatusForReports(ids, status)
      .then(() => {
        dispatch(actions.reportsStatusUpdated({ ids, status } as any));
      })
      .catch((error) => {
        error.clientMessage = "Can't update reports status";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: error.clientMessage,
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(
                error?.response?.data?.statusCode || 404
              ),
            },
          } as any)
        );
      });
  };

export const deleteReports = (ids: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deleteReports(ids)
    .then((response) => {
      dispatch(actions.reportsDeleted());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Reports deleted successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete reports";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};
