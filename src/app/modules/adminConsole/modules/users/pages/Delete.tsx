import { useEffect } from "react";
import { Modal, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import * as actions from "../redux/usersActions";

export const Delete = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector((state) => state.users.actionsLoading);

  function goBack() {
    navigate("/manager/users");
  }

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      goBack();
    }
  }, [id]);

  const deleteUser = () => {
    // server request for deleting user by id
    dispatch(actions.deleteUser(id)).then(() => {
      // closing delete modal
      goBack();
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete user"
      centered
      backdrop="static"
    >
      {isLoading && <ProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="delete user">User Delete</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this user?</span>
        )}
        {isLoading && <span>User is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={goBack}
            className="btn btn-light btn-elevate"
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteUser}
            className="btn btn-danger btn-elevate"
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
