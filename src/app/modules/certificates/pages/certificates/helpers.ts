import { nanoid } from "nanoid";
import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const initialValues = {
  name: "",
  holder: "",
  ccFaxNumbers: [],
  ccEmails: [],
  coverPage: "",
  masterTemplate: "64257798ae573e6eee8afa4a", // Accord 25 certificate id
  cohortsIncluded: [],
  vehiclesIncluded: [],
  policiesIncluded: [],
  equipment: [],
  properties: [],
  email: {
    subject: "",
    message: "",
    template: "",
    sendToMe: true,
    attachFilesForms: false,
    sendCopyToInsured: false,
  },
  description: {
    template: "",
    message: "",
    addToAcord101: false,
  },
};

export const holderValues = {
  code: nanoid(6),
  type: "CERTIFICATE_HOLDER",
  business: {
    name: "",
    email: "",
    fax: "",
    address: {
      address1: "",
      address2: "",
      city: "",
      state: "",
      zip: "",
      country: "USA",
    },
  },
  contact: {
    address: {
      address1: "",
      address2: "",
      city: "",
      state: "",
      country: "USA",
      zip: "",
    },
    email: "",
    firstName: "",
    language: "en",
    lastName: "",
    mobilePhone: null,
    phone: null,
    timezone: "US/Central",
    website: "",
  },
};

export const holdersQuery = {
  filter: {
    type: "CERTIFICATE_HOLDER",
  },
  sortOrder: "asc",
  sortField: "business.name",
  pageNumber: 1,
  pageSize: 20,
};

export const holderSchema = Yup.object().shape({
  business: Yup.object().shape({
    id: Yup.string(),
    name: Yup.string().required("Holder name is required"),
    fax: Yup.string().required("Holder fax is required"),
    email: Yup.string()
      .email("Please enter a valid email")
      .required("Holder email is required"),
    address: Yup.object().shape({
      address1: Yup.string().required("Holder address1 is required"),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      country: Yup.string(),
      zip: Yup.string(),
    }),
  }),
});

export const schema = Yup.object().shape({
  name: Yup.string(),
  holder: Yup.string()
    .required("Please select a holder")
    .typeError("Please select a holder"),
  ccFaxNumbers: Yup.array().of(
    Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Please enter a valid number")
      .required("Fax is required")
  ),
  ccEmails: Yup.array().of(
    Yup.string()
      .email("Please enter a valid email")
      .required("Email is required")
  ),
  coverPage: Yup.string(),
  masterTemplate: Yup.string().required("Please select a certificate"),
  cohortsIncluded: Yup.array(),
  vehiclesIncluded: Yup.array(),
  policiesIncluded: Yup.array(),
  equipment: Yup.array(),
  properties: Yup.array(),
  email: Yup.object({
    subject: Yup.string(),
    message: Yup.string(),
    template: Yup.string(),
    sendToMe: Yup.bool(),
    attachFilesForms: Yup.bool(),
    sendCopyToInsured: Yup.bool(),
  }).required(),
  description: Yup.object().shape({
    template: Yup.string(),
    message: Yup.string(),
    addToAcord101: Yup.bool(),
  }),
});
