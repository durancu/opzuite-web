import { Field, Formik, FormikValues } from "formik";
import { useSnackbar } from "notistack";
import { Fragment, useEffect, useState } from "react";
import { Alert, Button, Card, Col, Form, Row, Spinner } from "react-bootstrap";
import { useNavigate, useSearchParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import {
  BackdropLoader,
  CohortsMultiselect,
  FormInput,
  FormMultiselect,
  FormSelect,
  RichTextEditor,
  VehiclesMultiselect,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { formatPhoneNumber } from "src/app/components/functions";
import { breadcrumbs } from "../../breadcrumbs";
import { certificatesAPI } from "../../redux";
import { AddEmailModal, AddFaxModal, Holders, PDFViewer } from "./components";
import { initialValues, schema } from "./helpers";
import { CustomerEditModal } from "src/app/modules/customers/pages/customer-edit/CustomerEditModal";

export const Certificates = () => {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [searchParams] = useSearchParams();
  const customerId = searchParams.get("customer");
  const catalog = useGetCatalog();

  const [isOpen, toggleOpen] = useState<Record<string, any>>({
    preview: false,
    faxNumbers: false,
    emails: false,
    customerEdit: false,
  });

  const {
    data = {},
    isLoading,
    refetch,
    isError,
  } = certificatesAPI.useInitQuery(customerId, {
    skip: !customerId,
  });

  const [generate, { data: file, isLoading: isCreating }] =
    certificatesAPI.useGenerateMutation();

  const [send, { isLoading: isSending }] = certificatesAPI.useSendMutation();
  // only show page if there is a customerID
  useEffect(() => {
    if (!customerId) {
      navigate(-1);
    }
  }, [customerId]);

  // modal helper
  function toggleModal(modal: string) {
    toggleOpen((current) => {
      return { ...current, [modal]: !current[modal] };
    });
  }

  async function generateCertificate(values: FormikValues) {
    try {
      await generate(values).unwrap();
      enqueueSnackbar("Certificate generated successfully", {
        variant: "success",
      });
      toggleModal("preview");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function sendCertificate(values: FormikValues) {
    const request = {
      ...values,
      customer: values.customer.id,
    };

    try {
      await send(request).unwrap();
      enqueueSnackbar("Certificate sent successfully", {
        variant: "success",
      });

      navigate("/certificates");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  // prevent screen flash for no customerId
  if (!customerId || isLoading) return <BackdropLoader />;

  if (isError) {
    return (
      <Fragment>
        <PageTitle breadcrumbs={breadcrumbs}>Certificates</PageTitle>
        <Alert show={isError} variant="danger">
          <Alert.Heading as="h2">Something went wrong</Alert.Heading>
          <p className="my-4 fs-6">
            We {`couldn't`} initialize the certificate. Please try again
          </p>
          <Button onClick={() => refetch()} variant="danger">
            Try again
          </Button>
        </Alert>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Certificates</PageTitle>
      <Formik
        initialValues={{
          ...initialValues,
          ...data,
        }}
        validationSchema={schema}
        onSubmit={(values) => {
          return values.operation === "send"
            ? sendCertificate(values)
            : generateCertificate(values);
        }}
      >
        {({ values, errors, handleSubmit, setFieldValue, submitCount }) => (
          <form onSubmit={handleSubmit}>
            <Row className="mb-4">
              <Col md={6}>
                <Card className="h-100">
                  <Card.Header>
                    <Card.Title className="fw-bolder">General</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <div className="mb-4">
                      <FormSelect
                        labelText="Master Certificate"
                        name="masterTemplate"
                        options={catalog?.certificateMasterTemplates || []}
                      />
                    </div>

                    <Holders />

                    <div className="mb-4">
                      <div className="d-flex gap-3 mb-4 align-items-start justify-content-center">
                        <div className="flex-grow-1">
                          <FormMultiselect
                            freeSolo
                            labelText="Emails"
                            options={[]}
                            value={values.ccEmails}
                            error={errors.ccEmails ? true : false}
                            errorText={
                              errors.ccEmails && errors.ccEmails.toString()
                            }
                            onChange={(_, values) => {
                              setFieldValue("ccEmails", values);
                            }}
                          />
                        </div>
                        <Button
                          className="btn btn-light-primary text-nowrap mt-8"
                          onClick={() => toggleModal("emails")}
                        >
                          Manage
                        </Button>
                      </div>
                    </div>

                    <div className="mb-4">
                      <div className="d-flex gap-3 mb-4 align-items-start justify-content-center">
                        <div className="flex-grow-1">
                          <FormMultiselect
                            freeSolo
                            labelText="Fax"
                            options={[]}
                            value={values.ccFaxNumbers}
                            error={errors.ccFaxNumbers ? true : false}
                            errorText={
                              errors.ccFaxNumbers &&
                              errors.ccFaxNumbers.toString()
                            }
                            onChange={(_, values) => {
                              setFieldValue("ccFaxNumbers", values);
                            }}
                          />
                        </div>
                        <Button
                          className="btn btn-light-primary text-nowrap mt-8"
                          onClick={() => toggleModal("faxNumbers")}
                        >
                          Manage
                        </Button>
                      </div>
                    </div>

                    <div className="mb-4">
                      <Form.Label className="mb-4">Send Copy</Form.Label>
                      <Field
                        as={Form.Check}
                        name="email.sendToMe"
                        checked={values.email.sendToMe}
                        label="Send CC to Me"
                        className="mb-2"
                      />

                      <Field
                        as={Form.Check}
                        name="email.sendCopyToInsured"
                        checked={values.email.sendCopyToInsured}
                        label="Send CC to Insured"
                      />
                    </div>
                  </Card.Body>
                </Card>
              </Col>
              <Col md={6}>
                <Card className="h-100">
                  <Card.Header className="d-flex justify-content-between">
                    <Card.Title className="fw-bolder">Customer</Card.Title>
                    <div className="d-flex align-items-center gap-3">
                      <Button
                        className="btn btn-primary"
                        onClick={() => toggleModal("customerEdit")}
                      >
                        Edit Insured Info
                      </Button>
                    </div>
                  </Card.Header>
                  <Card.Body>
                    <div className="mb-12 fw-medium">
                      <div className="d-flex mb-4 flex-wrap gap-10 align-items-center">
                        <p className="d-flex gap-3 align-items-center">
                          <i className="bi bi-person-circle fs-3"></i>
                          <span>{data.customer.name}</span>
                        </p>
                        <p className="d-flex gap-3 align-items-center">
                          <i className="bi bi-envelope fs-3"></i>
                          <span>{data.customer.email}</span>
                        </p>
                        <p className="d-flex gap-3 align-items-center">
                          <i className="bi bi-telephone fs-3"></i>
                          <span>{formatPhoneNumber(data.customer.phone)}</span>
                        </p>
                      </div>
                      <div className="d-flex gap-2 align-items-center flex-wrap">
                        <i className="bi bi-geo-alt fs-3"></i>
                        {data.customer.contact &&
                          data.customer.contact.address && (
                            <div className="me-5">
                              <span>
                                {[
                                  "address1",
                                  "address2",
                                  "city",
                                  "state",
                                  "zip",
                                  "country",
                                ]
                                  .map(
                                    (property) =>
                                      data.customer.contact.address[property]
                                  )
                                  .filter(
                                    (value) =>
                                      typeof value === "string" &&
                                      value.trim() !== ""
                                  )
                                  .join(", ") || "N/A"}
                              </span>
                            </div>
                          )}
                      </div>
                    </div>

                    <div className="mb-4">
                      <VehiclesMultiselect
                        labelText="Include Vehicles"
                        options={data.customer.vehicles || []}
                        value={values.vehiclesIncluded}
                        error={errors.vehiclesIncluded ? true : false}
                        errorText={errors.vehiclesIncluded}
                        onChange={(value) => {
                          setFieldValue("vehiclesIncluded", value);
                        }}
                      />
                    </div>
                    <div className="mb-4">
                      <CohortsMultiselect
                        labelText="Include Drivers"
                        options={data.customer.cohorts || []}
                        value={values.cohortsIncluded}
                        error={errors.cohortsIncluded ? true : false}
                        errorText={errors.cohortsIncluded}
                        onChange={(value) => {
                          setFieldValue("cohortsIncluded", value);
                        }}
                      />
                    </div>
                    {/* <div className="mb-4">
                        	<Form.Label>Include Equipment</Form.Label>
                        	<Field
                          as={Form.Select}>
                          	<option value="">Select an option</option>
                        	</Field>
                      	</div>
                      	<div className="mb-4">
                        	<Form.Label>Include Properties</Form.Label>
                        	<Field
                          as={Form.Select}>
                          	<option value="">Select an option</option>
                        	</Field>
                      	</div> */}
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            <Row className="mb-4">
              <Col md={6}>
                <Card className="h-100">
                  <Card.Header>
                    <Card.Title className="fw-bolder">Additional</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    <div className="mb-4">
                      <FormInput
                        name="email.subject"
                        labelText="Email Subject"
                      />
                      <div className="d-flex gap-3 my-4 align-items-start justify-content-center">
                        <div className="flex-grow-1">
                          <FormSelect
                            name="email.template"
                            options={catalog?.certificateCoverTemplates || []}
                          />
                        </div>
                        <Button
                          className="btn btn-light-primary text-nowrap"
                          onClick={() => {
                            const template =
                              catalog?.certificateCoverTemplates.find(
                                (item: any) => item.id === values.email.template
                              );

                            if (template) {
                              setFieldValue("email.subject", template.name);
                              setFieldValue("email.message", template.content);
                            } else {
                              setFieldValue("email", {
                                ...values.email,
                                template: "",
                                subject: "",
                                message: "",
                              });
                            }
                          }}
                        >
                          Apply
                        </Button>
                      </div>
                    </div>

                    <div className="mb-4">
                      <Form.Label>Message</Form.Label>
                      <RichTextEditor name="email.message" />
                      <Field
                        as={Form.Check}
                        name="email.attachFilesForms"
                        checked={values.email.attachFilesForms}
                        label="Attach files/ Forms"
                      />
                    </div>
                  </Card.Body>
                </Card>
              </Col>

              <Col md={6}>
                <Card className="h-100">
                  <Card.Header>
                    <Card.Title className="fw-bolder">Description</Card.Title>
                  </Card.Header>
                  <Card.Body>
                    {/* <div className="mb-4">
                          <div className="d-flex flex-nowrap  gap-2 my-4">
                            <Field as={Form.Select}>
                              <option value="">Select template</option>
                            </Field>
                            <Button className="btn btn-light-primary text-nowrap">
                              Add New
                            </Button>
                          </div>
                        </div> */}

                    <div className="mb-4">
                      <Form.Label>Message</Form.Label>
                      <RichTextEditor name="description.message" />
                      <Field
                        as={Form.Check}
                        name="description.addToAcord101"
                        checked={values.description.addToAcord101}
                        label="Show Description in attached Acord 101"
                      />
                    </div>
                  </Card.Body>
                </Card>
              </Col>
            </Row>

            {/* <Row>
                  <p className="fw-bolder p-8">
                    Mass Action (Send Active Certificate to all recent Holders)
                    <Badge bg="warning" className="ms-2">
                      View
                    </Badge>
                  </p>
                </Row> */}

            <Row className="mb-4">
              <div className="gap-4 d-flex justify-content-end align-items-center p-8">
                <Button variant="light" onClick={() => navigate(-1)}>
                  Cancel
                </Button>
                <Button
                  type="submit"
                  className="btn-light-primary"
                  disabled={isCreating || isSending}
                  onClick={() => setFieldValue("operation", "preview")}
                >
                  {isCreating ? (
                    <div className="d-flex gap-3 align-items-center">
                      <Spinner />
                      <span>Processing</span>
                    </div>
                  ) : (
                    <span>Preview</span>
                  )}
                </Button>
                <Button
                  type="submit"
                  className="btn-active-primary"
                  disabled={isCreating || isSending || submitCount < 1}
                  onClick={() => setFieldValue("operation", "send")}
                >
                  {isSending ? (
                    <div className="d-flex gap-3 align-items-center">
                      <Spinner />
                      <span>Processing</span>
                    </div>
                  ) : (
                    <span>Send Certificate</span>
                  )}
                </Button>
              </div>
            </Row>

            <AddFaxModal
              show={isOpen.faxNumbers}
              onHide={() => toggleModal("faxNumbers")}
            />
            <AddEmailModal
              show={isOpen.emails}
              onHide={() => toggleModal("emails")}
            />

            <PDFViewer
              file={file}
              show={isOpen.preview}
              isSending={isSending}
              onHide={() => toggleModal("preview")}
              onSend={() => sendCertificate(values)}
              pagesCount={
                values.vehiclesIncluded || values.cohortsIncluded ? 2 : 1
              }
            />

            <CustomerEditModal
              customer={data.customer}
              show={isOpen.customerEdit}
              onHide={() => {
                toggleModal("customerEdit");
                refetch();
              }}
            />
          </form>
        )}
      </Formik>
    </Fragment>
  );
};
