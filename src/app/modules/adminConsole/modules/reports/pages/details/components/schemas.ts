import * as Yup from "yup";

export const reportResultDetailValidationSchema = Yup.object().shape({
  groupByField: Yup.array(),
  fields: Yup.array(),
  filters: Yup.array().of(
    Yup.object().shape({
      field: Yup.string().required("Field is required."),
      operator: Yup.string().required("Operator is required."),
      value: Yup.string().required("Value is required."),
    })
  ),
  model: Yup.string().required("Model is required"),
  groupByModel: Yup.string().required("Group By is required"),
  dateFrom: Yup.mixed(),
  dateTo: Yup.mixed(),
});
