import { shallowEqual, useSelector } from "react-redux";
import { FormAutoComplete } from "../forms";
interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function SupervisorsAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.SUPERVISORS",
  labelText,
  fieldName = "supervisor",
}: Props) {
  const { catalogs } = useSelector(
    (reduxState: any) => ({ catalogs: reduxState.catalogs }),
    shallowEqual
  );
  const { entities }: any = catalogs;

  return (
    <FormAutoComplete
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={entities?.users || []}
    />
  );
}
