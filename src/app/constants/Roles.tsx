export const RoleHierarchyEdges = {
  SUPERADMIN: 1,
  ADMIN: 20,
  OWNER: 30,
  OFFICER: 50,
  SENIOR: 70,
  COUNTRY: 100,
  REGION: 200,
  LOCATION: 300,
  AGENT: 400,
  TRAINEE: 500,
};
