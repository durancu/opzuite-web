import { Permissions } from "src/app/constants/Permissions";
import { TableActionsMenu } from "../../TableActionsMenu";

export const UserActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openEditUserPage,
    openDeleteUserDialog,
    openDetailUserPage,
    openChangeUserPassword,
  }: // user,
    // roles,
    any
) => {
  return (
    <TableActionsMenu
      readFunction={openDetailUserPage}
      editFunction={openEditUserPage}
      deleteFunction={openDeleteUserDialog}
      changePasswordFunction={openChangeUserPassword}
      editPermission={Permissions.USERS_UPDATE}
      readPermission={Permissions.USERS_READ}
      deletePermission={Permissions.USERS_DELETE}
      changePasswordPermission={Permissions.USERS}
      code={row.id}
    />
  );
};
