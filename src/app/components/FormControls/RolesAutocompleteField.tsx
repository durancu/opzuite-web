import { shallowEqual, useSelector } from "react-redux";
import { useGetUserAuth } from "src/app/hooks";
import { FormAutoComplete } from "../forms";
interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function RolesAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.ROLE",
  labelText,
  fieldName = "role",
}: Props) {
  const { catalogs } = useSelector(
    (reduxState: any) => ({ catalogs: reduxState.catalogs }),
    shallowEqual
  );
  const { entities }: any = catalogs;
  const user = useGetUserAuth();

  return (
    <FormAutoComplete
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={entities?.roles.filter(
        (role: any) => role.hierarchy > user?.role.hierarchy
      )}
    />
  );
}
