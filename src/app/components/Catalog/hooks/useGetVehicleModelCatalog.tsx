import { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "../redux/catalogActions";

interface UseGetVehicleModel {
  vehicleModels: any[];
  actionsLoading: boolean;
  error: any;
}

export function useGetVehicleModelCatalog(year?: number): UseGetVehicleModel {
  const dispatch = useAppDispatch();
  const { vehicleModels, actionsLoading, error } = useSelector(
    (state: any) => ({
      vehicleModels: state.catalogs?.entities?.vehicleModels,
      actionsLoading: state.catalogs?.actionsLoading,
      error: state.catalogs?.error,
    }),
    shallowEqual
  );

  useEffect(() => {
    if (year) {
      dispatch(catalogActions.getVehicleModelCatalog(year));
    }
  }, [dispatch, year]);

  return { vehicleModels, actionsLoading, error };
}
