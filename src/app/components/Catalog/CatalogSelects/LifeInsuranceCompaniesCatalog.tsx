export const LIFE_INSURANCE_COMPANIES_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "NY-LIFE-GRP", name: "New York Life Grp" },
  { id: "NORTHWESTERN-MUTUAL", name: "Northwestern Mutual" },
  { id: "MET", name: "Metropolitan Group(MET)" },
  { id: "PRU", name: "Prudential of America(PRU)" },
  { id: "LINCOLN-NATIONAL", name: "Lincoln National" },
  { id: "MASS-MUTUAL", name: "MassMutual" },
  { id: "STATE-FARM", name: "State Farm" },
  { id: "AEG", name: "Aegon(AEG)" },
  { id: "JOHN-HANCOCK", name: "John Hancock" },
  { id: "MINNESOTA-MUTUAL", name: "Minnesota Mutual Grp" },
  { id: "OTHER", name: "Other" },
];
