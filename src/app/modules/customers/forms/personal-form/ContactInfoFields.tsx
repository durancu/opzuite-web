import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import {
  FormInput,
  FormAutoComplete,
  PhoneNumberInput,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const ContactInfoFields = () => {
  const catalog = useGetCatalog();

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormInput
            name="contact.firstName"
            labelTextId="FORM.LABELS.FIRST_NAME"
          />
        </Col>

        <Col>
          <FormInput
            name="contact.lastName"
            labelTextId="FORM.LABELS.LAST_NAME"
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormInput
            type="email"
            name="contact.email"
            labelTextId="FORM.LABELS.EMAIL"
          />
        </Col>
        <Col>
          <PhoneNumberInput
            name="contact.phone"
            labelTextId="FORM.LABELS.WORK_NUMBER"
          />
        </Col>
        <Col>
          <PhoneNumberInput
            name="contact.mobilePhone"
            labelTextId="FORM.LABELS.MOBILE_NUMBER"
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormInput
            name="contact.address.address1"
            labelTextId="FORM.LABELS.ADDRESS_LINE_1"
          />
        </Col>
        <Col>
          <FormInput
            name="contact.address.address2"
            labelTextId="FORM.LABELS.ADDRESS_LINE_2"
          />
        </Col>
        <Col>
          <FormInput
            name="contact.address.city"
            labelTextId="FORM.LABELS.CITY"
          />
        </Col>
      </Row>

      <Row>
        <Col>
          <FormAutoComplete
            name="contact.address.state"
            labelTextId="FORM.LABELS.STATE"
            options={catalog?.states || []}
          />
        </Col>
        <Col>
          <FormInput
            name="contact.address.zip"
            labelTextId="FORM.LABELS.ZIP_CODE"
          />
        </Col>
        <Col>
          <FormAutoComplete
            name="contact.address.country"
            labelTextId="FORM.LABELS.COUNTRY"
            helperTextId="FORM.LABELS.HELPER.COUNTRIES"
            options={catalog?.countries || []}
            disabled={true}
          />
        </Col>
      </Row>
    </Fragment>
  );
};
