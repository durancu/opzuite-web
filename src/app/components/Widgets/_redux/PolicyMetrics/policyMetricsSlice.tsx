import { createSlice } from "@reduxjs/toolkit";

const initialPolicyMetricsState = {
  actionsLoading: false,
  metrics: [],
};
export const callTypes = {
  list: "list",
  action: "action",
};

export const policyMetricsSlice = createSlice({
  name: "policyMetric",
  initialState: initialPolicyMetricsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    setPolicyMetrics: (state: any, action: any) => {
      state.actionsLoading = false;
      state.metrics = action.payload.metrics;
      state.error = null;
    },
  },
});
