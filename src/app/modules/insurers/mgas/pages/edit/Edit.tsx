import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import {
  WizardForm,
  WizardFormStep,
  BackdropLoader,
  Error,
} from "src/app/components";

import {
  mgaBusinessValidationSchema,
  mgaCommissionsValidationSchema,
  mgaContactInfoValidationSchema,
} from "./schema";
import {
  BusinessInfo,
  ContactInfo,
  Commissions,
} from "src/app/modules/insurers/components";
import { FormikValues } from "formik";
import { insurersAPI } from "src/app/modules/insurers/redux/insurerSlice";
import { useSnackbar } from "notistack";
import { defaultMGA } from "src/app/modules/insurers/helpers";
import { breadcrumbs } from "../../breadcrumbs";

const Edit = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: mga,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [createMGA, { isLoading: isCreating }] =
    insurersAPI.useCreateInsurerMutation();

  const [updateMGA, { isLoading: isUpdating }] =
    insurersAPI.useUpdateInsurerMutation();

  const backToMGAsList = () => {
    navigate("/mgas");
  };

  async function handleCreate(values: FormikValues) {
    try {
      const response = await createMGA(values).unwrap();
      enqueueSnackbar("MGA created successfully", {
        variant: "success",
      });
      navigate(`/mgas/${response.code}/detail`);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleUpdate(values: FormikValues) {
    try {
      await updateMGA(values).unwrap();
      enqueueSnackbar("MGA updated successfully", {
        variant: "success",
      });
      backToMGAsList();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading || isCreating || isUpdating) {
    return <BackdropLoader />;
  }

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {code ? "Edit" : "New"} MGA
      </PageTitle>
      <Card>
        <WizardForm
          initialValues={code ? mga : defaultMGA}
          onSubmit={code ? handleUpdate : handleCreate}
          onCancel={backToMGAsList}
        >
          <WizardFormStep
            labelTextId="WIZARD.BUSINESS_INFO"
            validationSchema={mgaBusinessValidationSchema}
          >
            <BusinessInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.CONTACT_INFO"
            validationSchema={mgaContactInfoValidationSchema}
          >
            <ContactInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.COMMISSIONS"
            validationSchema={mgaCommissionsValidationSchema}
          >
            <Commissions />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};

export default Edit;
