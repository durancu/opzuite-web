// routes module, to be imported in src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Agents } from "./pages/Agents";
import { AgentsDetailPage } from "./pages/AgentsDetailPage";

export const agentRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.USERS_READ}>
        <Agents />
      </RequirePermission>
    ),
  },
  {
    path: ":id/detail",
    element: (
      <RequirePermission permissions={Permissions.USERS_READ}>
        <AgentsDetailPage />
      </RequirePermission>
    ),
  },
];
