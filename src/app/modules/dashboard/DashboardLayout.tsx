import { Outlet, useNavigate } from "react-router-dom";
import { DashboardsUIProvider } from "./pages/dashboards/DashboardsUIContext";

export const DashboardLayout = () => {
  const navigate = useNavigate();
  const dashboardsUIEvents = {
    openFetchDashboardsDialog: () => {
      navigate(`fetch`);
    },
    newDashboardButtonClick: () => {
      navigate("new");
    },
    openEditDashboardPage: (code: string) => {
      navigate(`${code}/edit`);
    },
    openDeleteDashboardDialog: (code: any) => {
      navigate(`${code}/delete`);
    },
    openDetailDashboardPage: (code: any) => {
      navigate(`${code}/detail`);
    },
    openDeleteDashboardsDialog: () => {
      navigate(`deleteDashboards`);
    },
  };

  return (
    <DashboardsUIProvider dashboardsUIEvents={dashboardsUIEvents}>
      <Outlet />
    </DashboardsUIProvider>
  );
};
