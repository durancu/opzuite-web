import axios from "axios";

export const FILTERS_URL = "api/v2/companies/filter";

// READ
export function getAllFilters() {
  return axios.post(FILTERS_URL, {});
}
