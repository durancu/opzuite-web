import { roundAmount } from ".";
import { Endorsement } from "../interfaces/Endorsement";
import { EndorsementItem } from "../interfaces/EndorsementItem";
import { Sale } from "../interfaces/Sale";

const PERMIT_COMMISSION_PERCENT = 0.2;
const FEE_COMMISSION_PERCENT = 0.3;

export const calculatePermitMetrics = (
  originalSale: Partial<Sale>
  /* providers: Partial<Insurer>[] */
): Partial<Sale> => {
  const sale = { ...originalSale };
  let checksumSanity = 0;
  let totalAgencyCommission = 0;
  let totalAgentCommission = 0;
  let totalFinanced = 0;
  let totalFinancedPaid = 0;
  let totalPayables = 0;
  let totalPaid = 0;
  let totalReceivables = 0;
  let totalReceived = 0;
  let totalPremium = 0;
  let totalNonPremium = 0;
  let totalTaxesAndFees = 0;
  const totalCoveragesPremium = 0;
  const totalCoveragesDownPayment = 0;
  const totalPermits = 0;

  if (sale?.endorsements) {
    sale?.endorsements.map((element: Endorsement) => {
      const endorsement = { ...element };
      endorsement.totalAgencyCommission = 0;
      endorsement.totalAgentCommission = 0;
      endorsement.totalFinanced = 0;
      endorsement.totalFinancedPaid = 0;
      endorsement.totalPayables = 0;
      endorsement.totalPaid = 0;
      endorsement.totalReceivables = 0;
      endorsement.totalReceived = 0;
      endorsement.totalNonPremium = 0;
      endorsement.totalTaxesAndFees = 0;

      endorsement.totalPremium = endorsement.amount || 0;

      if (endorsement.items) {
        endorsement.items.map((element: EndorsementItem) => {
          const item = { ...element };
          item.amount = item.amount || 0;
          item.amountPaid = item.amountPaid || 0;

          switch (item.accountingClass) {
            case "PAYABLE":
              endorsement.totalPayables += item.amount;
              endorsement.totalPaid += item.amountPaid;
              break;
            case "RECEIVABLE":
              endorsement.totalReceivables += item.amount;
              endorsement.totalReceived += item.amountPaid;
              break;
            case "FINANCING_DIRECT_BILL":
              endorsement.totalFinanced += item.amount;
              endorsement.totalFinancedPaid += item.amountPaid;
              break;
            case "FEE_TAX":
              endorsement.totalTaxesAndFees += item.amount;
              endorsement.totalNonPremium += item.amount;
              break;
            case "AGENT_COMMISSION":
              endorsement.totalAgentCommission += item.amount;
              break;
            case "AGENCY_COMMISSION":
              endorsement.totalAgencyCommission += item.amount;
              break;
            default:
            //Do nothing
          }
          return item;
        });

        totalAgencyCommission += endorsement.totalAgencyCommission;
        totalAgentCommission += endorsement.totalAgentCommission;
        totalFinanced += endorsement.totalFinanced;
        totalFinancedPaid += endorsement.totalFinancedPaid;
        totalPayables += endorsement.totalPayables;
        totalPaid += endorsement.totalPaid;
        totalReceivables += endorsement.totalReceivables;
        totalReceived += endorsement.totalReceived;
        totalPremium += endorsement.totalPremium;
        totalNonPremium += endorsement.totalNonPremium;
        totalTaxesAndFees += endorsement.totalTaxesAndFees;
      }
      return endorsement;
    });
  }

  checksumSanity =
    totalPremium +
    totalNonPremium -
    totalFinanced -
    totalReceivables +
    totalPayables;

  sale.checksumSanity = roundAmount(checksumSanity);
  sale.totalFinanced = roundAmount(totalFinanced);
  sale.totalFinancedPaid = roundAmount(totalFinancedPaid);
  sale.totalPayables = roundAmount(totalPayables);
  sale.totalPaid = roundAmount(totalPaid);
  sale.totalReceivables = roundAmount(totalReceivables);
  sale.totalReceived = roundAmount(totalReceived);
  sale.totalPremium = roundAmount(totalPremium);
  sale.totalNonPremium = roundAmount(totalNonPremium);
  sale.totalTaxesAndFees = roundAmount(totalTaxesAndFees);
  sale.totalCoveragesPremium = roundAmount(totalCoveragesPremium);
  sale.totalCoveragesDownPayment = roundAmount(totalCoveragesDownPayment);
  sale.totalPermits = roundAmount(totalPermits);

  sale.totalAgencyCommission = roundAmount(
    totalAgencyCommission +
      (1 - PERMIT_COMMISSION_PERCENT) * sale.totalPremium +
      (1 - FEE_COMMISSION_PERCENT) * sale.totalTaxesAndFees
  );
  sale.totalAgentCommission = roundAmount(
    totalAgentCommission +
      PERMIT_COMMISSION_PERCENT * sale.totalPremium +
      FEE_COMMISSION_PERCENT * sale.totalTaxesAndFees
  );

  return sale;
};
