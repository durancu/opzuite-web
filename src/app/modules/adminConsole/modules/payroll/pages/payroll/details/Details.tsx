import { Fragment, useEffect } from "react";
import { Button, Card, Col, ProgressBar, Row } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as actions from "src/app/components/Catalog/redux/catalogActions";
import {
  formatAmount,
  formatDate,
  formatNegativeAmount,
  roundAmount,
} from "src/app/components/functions";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actionspayroll from "../../../_redux/payrollsActions";
import { PayrollEmployeeTable } from "./components/PayrollEmployeeTable";

export const PayrollDetailPage = () => {
  const { code } = useParams();
  const navigate = useNavigate();

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(actions.getAllCatalog());
  }, [dispatch]);

  const { currentState, actionsLoading, payrollForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.payrolls.actionsLoading,
      payrollForEdit: state.payrolls.payrollForEdit,
      currentState: state.catalogs,
    }),
    shallowEqual
  );

  const { entities }: any = currentState;

  useEffect(() => {
    dispatch(actionspayroll.fetchPayroll(code));
  }, [code, dispatch]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const goToEditPayroll = () => {
    navigate(`/manager/payrolls/${payrollForEdit.code}/edit`);
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Payrolls</PageTitle>

      <div className="d-flex justify-content-between align-items-center py-4">
        <h3 className="fw-bolder">Payroll Details</h3>
        <Button
          variant="primary"
          onClick={goToEditPayroll}
          className="d-flex align-items-center gap-2"
        >
          <span className="indicator-label">Edit</span>
          <i className="fs-4 bi bi-pencil-square"></i>
        </Button>
      </div>

      {actionsLoading && <ProgressBar />}

      <Card className="mb-4 p-8">
        <Card.Title as="h3" className="text-primary">
          <FormattedMessage id="GENERAL.OVERVIEW" />
        </Card.Title>

        <Row className="my-4">
          <Col md={3}>
            <p>Location</p>
            <h6>
              {payrollForEdit &&
                (entities?.locations.find(
                  ({ id }: any) => id === payrollForEdit?.location
                )?.name ||
                  "-")}
            </h6>
          </Col>
          <Col md={6}>
            <p>Date range</p>
            <h6>
              <span className="mr-6" style={{ marginRight: "20px" }}>
                <small>From:</small>{" "}
                {payrollForEdit &&
                  formatDate(payrollForEdit?.payPeriodStartedAt)}
              </span>
              <span>
                <small>To:</small>{" "}
                {payrollForEdit && formatDate(payrollForEdit?.payPeriodEndedAt)}
              </span>
            </h6>
          </Col>
        </Row>
        <Row className="my-4">
          <Col>
            <p>Total Commissions</p>
            <h5>
              {payrollForEdit &&
                formatAmount(
                  roundAmount(payrollForEdit.totalSaleBonus) +
                  roundAmount(payrollForEdit.totalExtraBonus) +
                  roundAmount(payrollForEdit.totalPermits * 0.2) +
                  roundAmount(payrollForEdit.totalFees * 0.2)
                )}
            </h5>
          </Col>
          <Col>
            <p>Total Reimbursements</p>
            <h5>
              {payrollForEdit &&
                formatAmount(payrollForEdit.totalReimbursement)}
            </h5>
          </Col>
          <Col>
            <p>Total Discounts</p>
            <h5>
              {payrollForEdit &&
                formatNegativeAmount(payrollForEdit.totalDiscount)}
            </h5>
          </Col>
          <Col>
            <p>Total Salary Paid</p>
            <h5>
              <strong>
                {payrollForEdit && formatAmount(payrollForEdit.totalNetSalary)}
              </strong>
            </h5>
          </Col>
        </Row>
      </Card>

      <Card className="mb-4 p-8">
        <Card.Title as="h3" className="mb-4 text-primary">
          Paystubs Summary
        </Card.Title>

        <PayrollEmployeeTable
          employeeList={
            payrollForEdit !== undefined ? payrollForEdit.payStubs : []
          }
        />
      </Card>
    </Fragment>
  );
};
