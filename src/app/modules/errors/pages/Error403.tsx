import { Link } from "react-router-dom";

export const Error403 = () => {
  return (
    <>
      {/* begin::Title */}
      <h1 className="fw-bolder fs-2hx text-gray-900 mb-4 text-danger">
        Forbidden
      </h1>
      {/* end::Title */}

      {/* begin::Text */}
      <div className="fw-semibold fs-4 mb-7">
        You are not allowed to access this content.
      </div>
      {/* end::Text */}

      {/* begin::Link */}
      <div className="mb-0">
        <Link to="/" className="btn btn-primary p-4">
          Return Home
        </Link>
      </div>
      {/* end::Link */}
    </>
  );
};
