export interface Commission {
  id?: string;
  code?: string;
  coverage?: string;
  percent?: number;
}
