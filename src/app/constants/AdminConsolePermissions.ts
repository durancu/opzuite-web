// admin console permission, used for menus and routing
import { Permissions } from "./Permissions";

export const ADMIN_CONSOLE_PERMISSIONS = [
  Permissions.USERS,
  Permissions.USERS_CREATE,
  Permissions.USERS_UPDATE,
  Permissions.USERS_DELETE,
  Permissions.COMPANIES,
  Permissions.COMPANIES_CREATE,
  Permissions.COMPANIES_UPDATE,
  Permissions.COMPANIES_DELETE,
  Permissions.CARRIERS,
  Permissions.CARRIERS_CREATE,
  Permissions.CARRIERS_UPDATE,
  Permissions.CARRIERS_DELETE,
  Permissions.MGAS,
  Permissions.MGAS_CREATE,
  Permissions.MGAS_UPDATE,
  Permissions.MGAS_DELETE,
  Permissions.LOCATIONS,
  Permissions.LOCATIONS_CREATE,
  Permissions.LOCATIONS_UPDATE,
  Permissions.LOCATIONS_DELETE,
  Permissions.PAYROLLS,
  Permissions.PAYROLLS_CREATE,
  Permissions.PAYROLLS_UPDATE,
  Permissions.PAYROLLS_DELETE,
];
