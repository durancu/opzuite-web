export const ModelNameColumnFormatter = (row: any) => {
  return <>{`${row?._id?.label}`}</>;
};
