import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import { useFormikContext } from "formik";
import { FormInput, PhoneNumberInput } from "src/app/components";
import { StatesAutocompleteField } from "src/app/components/FormControls/StatesAutocompleteField";

const BusinessInfo = () => {
  const { values } = useFormikContext<any>();
  return (
    <Fragment>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput name="business.name" labelTextId="FORM.LABELS.NAME" />
        </Col>
        <Col md={3}>
          <FormInput
            labelTextId="FORM.LABELS.EMAIL"
            type="email"
            name="business.email"
          />
        </Col>
        <Col md={3}>
          <PhoneNumberInput name="business.primaryPhone" />
        </Col>
        <Col md={3}>
          <PhoneNumberInput name="business.fax" labelTextId="FORM.LABELS.FAX" />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput
            name="business.address.address1"
            labelTextId="FORM.LABELS.ADDRESS_LINE_1"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="business.address.address2"
            labelTextId="FORM.LABELS.ADDRESS_LINE_2"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="business.address.city"
            labelTextId="FORM.LABELS.CITY"
          />
        </Col>
        <Col md={3}>
          <StatesAutocompleteField fieldName="business.address.state" />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput
            name="business.address.zip"
            labelTextId="FORM.LABELS.ZIP_CODE"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="business.address.country"
            labelTextId="FORM.LABELS.COUNTRY"
            disabled
          />
        </Col>
        <Col md={3}>
          <FormInput
            type="url"
            name="business.website"
            labelTextId="FORM.LABELS.WEBSITE"
          />
        </Col>

        {values?.type !== "FINANCER" && (
          <Col md={3}>
            <FormInput
              type="number"
              name="business.naicCode"
              labelText="NAIC Code"
              helperText="National Association of Insurance Commissioners (5–digit) insurance carrier ID number"
            />
          </Col>
        )}
      </Row>
    </Fragment>
  );
};

export default BusinessInfo;
