import { formatAmount } from "src/app/components/functions";

export const AmountColumnFormatter = (cellContent: any) => (
  <>{formatAmount(cellContent)}</>
);
