import { truncate } from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export const ApplicationsColumnFormatter = (cellContent: any, row: any) =>
  typeof row.products !== "undefined" &&
  row.products.map(
    (product: any, index: number) =>
      product.id !== "" && (
        <span className="p-0 mr-2 mb-2 text-nowrap text-truncate" key={index}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="quick-actions-tooltip">
                <strong>{product.name}</strong>
                <br />
                <small>{product.description}</small>
              </Tooltip>
            }
          >
            <span
              hidden={!product}
              className="text-truncate py-1 px-2 inline badge-application symbol-label font-size-small font-weight-bold rounded"
            >
              <small className="font-weight-bolder">
                {truncate(product.name, {
                  length: 12,
                })}
              </small>
            </span>
          </OverlayTrigger>
        </span>
      )
  );
