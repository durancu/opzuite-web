import { Col, Row, Card } from "react-bootstrap";
import { toAbsoluteUrl } from "src/_metronic/helpers";
import { AddressBlock } from "./AddressBlock";

interface Props {
  financer: any;
}

//TODO: Use Memo
export const languages: any = {
  en: {
    name: "English",
    flag: toAbsoluteUrl("/media/svg/flags/226-united-states.svg"),
  },
  es: {
    lang: "es",
    name: "Spanish",
    flag: toAbsoluteUrl("/media/svg/flags/128-spain.svg"),
  },
};

export const FinancerDetails = ({ financer }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        Business Details
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>
              {financer && (
                <AddressBlock address={financer.business?.address} />
              )}
            </h4>
            <span>Address </span>
          </Col>
          <Col>
            {financer && <h4>{financer.business.website || "-"}</h4>}
            <span>Website</span>
          </Col>

          <Col>
            {financer && (
              <h4>
                {financer.contact.firstName} {financer.contact.lastName}
              </h4>
            )}
            <span>Contact Name</span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
