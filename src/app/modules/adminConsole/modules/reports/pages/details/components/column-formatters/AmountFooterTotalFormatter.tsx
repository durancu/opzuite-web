import { formatAmount } from "src/app/components/functions";

export const AmountFooterTotalFormatter = (
  column: any,
  colIndex: any,
  { text }: any
) => <>{formatAmount(text)}</>;
