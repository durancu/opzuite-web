import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import {
  FormAutoComplete,
  FormInput,
  PhoneNumberInput,
  WizardFormSection,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const BusinessContactInfo = () => {
  // fetch catalogs
  const catalog = useGetCatalog();

  return (
    <Fragment>
      <WizardFormSection label="Contact Info">
        <Row className="mb-4">
          <Col md={3}>
            <FormInput
              name="business.name"
              labelTextId="FORM.LABELS.COMPANY_NAME"
            />
          </Col>
          <Col md={3}>
            <FormAutoComplete
              name="business.industry"
              labelTextId="FORM.LABELS.INDUSTRY"
              options={catalog?.industries || []}
            />
          </Col>
          <Col md={3}>
            <FormInput
              type="url"
              name="business.website"
              labelTextId="FORM.LABELS.WEBSITE"
            />
          </Col>
          <Col md={3}>
            <FormInput
              type="email"
              name="business.email"
              labelTextId="FORM.LABELS.EMAIL"
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col md={3}>
            <PhoneNumberInput name="business.primaryPhone" />
          </Col>
          <Col md={3}>
            <PhoneNumberInput
              name="business.fax"
              labelTextId="FORM.LABELS.FAX"
            />
          </Col>
          <Col md={3}>
            <FormInput
              name="business.address.address1"
              labelTextId="FORM.LABELS.ADDRESS_LINE_1"
            />
          </Col>
          <Col md={3}>
            <FormInput
              name="business.address.address2"
              labelTextId="FORM.LABELS.ADDRESS_LINE_2"
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col md={3}>
            <FormInput
              name="business.address.city"
              labelTextId="FORM.LABELS.CITY"
            />
          </Col>
          <Col md={3}>
            <FormAutoComplete
              name="business.address.state"
              labelTextId="FORM.LABELS.STATE"
              options={catalog?.states || []}
            />
          </Col>
          <Col md={3}>
            <FormInput
              name="business.address.zip"
              labelTextId="FORM.LABELS.ZIP_CODE"
            />
          </Col>
          <Col md={3}>
            <FormAutoComplete
              name="business.address.country"
              labelTextId="FORM.LABELS.COUNTRY"
              helperTextId="FORM.LABELS.HELPER.COUNTRIES"
              options={catalog?.countries || []}
              disabled={true}
            />
          </Col>
        </Row>
      </WizardFormSection>
      <WizardFormSection label="Commercial Details">
        <Row className="mb-4">
          <Col md={3}>
            <FormInput name="business.usDOT" labelText="US DOT" />
          </Col>
          <Col md={3}>
            <FormInput name="business.mcNumber" labelText="MC Number" />
          </Col>
          <Col md={3}>
            <FormInput name="business.txDMV" labelText="TX DMV" />
          </Col>
        </Row>
      </WizardFormSection>
    </Fragment>
  );
};
