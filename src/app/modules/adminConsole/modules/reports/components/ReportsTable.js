import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { useGetUserAuth } from "src/app/hooks";
import { reportAPI } from "../_redux/reportsSlice";
import { prepareFilter, sizePerPageList } from "./helpers";

export const ReportsTable = () => {
  const user = useGetUserAuth();
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = reportAPI.useGetAllReportsQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.TitleColumnHeaderFormatter,
      formatter: columnFormatters.ReportNameWithLinkColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
        idField: "code",
        textField: "name",
        defaultText: "N/A",
      },
    },
    {
      dataField: "createdByName",
      text: "Created By",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.AuthorColumnHeaderFormatter,
      formatter: columnFormatters.CreatedByAndLocationColumnFormatter,
    },
    {
      dataField: "updatedAt",
      text: "Last Update",
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.LastUpdateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "scope",
      text: "Scope",
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.AccessColumnHeaderFormatter,
      formatter: columnFormatters.ReportScopeColumnFormatter,
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "right",
      align: "right",
      formatter: columnFormatters.ReportActionsColumnFormatter,
      formatExtraData: {
        openEditReportPage: menuActions.edit,
        openDeleteReportDialog: menuActions.delete,
        openDetailReportPage: menuActions.details,
        user: user,
      },
      classes: "text-right pr-0",
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <FilterPanel prepareFilter={prepareFilter} />
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "soldAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};
