/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const CertificateActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openEditCertificatePage,
    openDeleteCertificateDialog,
    openDetailCertificatePage,
    previewCertificatePage,
  }: any
) => (
  <TableActionsMenu
    previewFunction={previewCertificatePage}
    openCertificates={openDetailCertificatePage}
    editFunction={openEditCertificatePage}
    deleteFunction={openDeleteCertificateDialog}
    editPermission={Permissions.POLICIES_CERTIFICATES_UPDATE}
    readPermission={Permissions.POLICIES_CERTIFICATES_READ}
    deletePermission={Permissions.POLICIES_CERTIFICATES_DELETE}
    code={row.code}
    customerId={row.customerId}
  />
);
