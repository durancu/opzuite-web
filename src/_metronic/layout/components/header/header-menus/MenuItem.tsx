import { FC } from "react";
import { NavLink } from "react-router-dom";
import clsx from "clsx";
import { KTSVG } from "../../../../helpers";

type Props = {
  to: string;
  title: string;
  icon?: string;
  fontIcon?: string;
  hasArrow?: boolean;
  hasBullet?: boolean;
  end?: boolean; // match route exactly or use as parent route
};

const MenuItem: FC<Props> = ({
  to,
  end,
  title,
  icon,
  fontIcon,
  hasArrow = false,
  hasBullet = false,
}) => {
  return (
    <div className="menu-item me-lg-1">
      <NavLink
        to={to}
        end={end}
        className={({ isActive }) =>
          clsx("menu-link py-3", {
            "active menu-here": isActive,
          })
        }
      >
        {hasBullet && (
          <span className="menu-bullet">
            <span className="bullet bullet-dot"></span>
          </span>
        )}

        {icon && (
          <span className="menu-icon">
            <KTSVG path={icon} className="svg-icon-2" />
          </span>
        )}

        {fontIcon && (
          <span className="menu-icon">
            <i className={clsx("fs-3", fontIcon)}></i>
          </span>
        )}

        <span className="menu-title">{title}</span>

        {hasArrow && <span className="menu-arrow"></span>}
      </NavLink>
    </div>
  );
};

export { MenuItem };
