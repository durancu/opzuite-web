interface Vehicle {
  vinNumber: string;
  licensePlate: string;
  hasAutoInsurance: boolean;
  autoInsuranceCompany: string;
  notes: string;
  make: string;
  model: string;
  year: string;
  type: string;
  trim: string;
  color: string;
  condition: string;
  ownershipStatus: string; //Owned, Financed, Leased
  primaryUse: string; //Commuting, Pleasure, Business
  drivingDaysPerWeek: string;
  drivingMilesPerWeek: string;
  annualMileage: string;
}

type CustomerType = "BUSINESS" | "INDIVIDUAL";

interface Cohort {
  address: {
    address1: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    zip: string;
  };
  dob: any;
  driverLicense: string;
  email: string;
  firstName: string;
  language: string;
  lastName: string;
  mobilePhone: string | null;
  phone: string | null;
  ssn: string;
  timezone: string;
  website: string;
  driverLicenseState: string;
  educationLevel: string;
  employer: string;
  employmentStatus: string;
  ethnicity: string;
  gender: string;
  identityCardId: string;
  identityCardType: string;
  isInsured: boolean;
  immigrantStatus: string;
  hasHealthInsurance: boolean;
  healthInsuranceCompany: string;
  hasAutoInsurance: boolean;
  autoInsuranceCompany: string;
  hasLifeInsurance: boolean;
  lifeInsuranceCompany: string;
  hasHomeInsurance: false;
  homeInsuranceCompany: string;
  maritalStatus: string;
  notes: string;
  relationship: string;
  yearlyGrossIncome: string;
}

interface Customer {
  _id: string;
  business: Business;
  communication: Communication;
  company: string;
  contact: Contact;
  createdBy: string;
  email: string;
  name: string;
  phone: string;
  type: string;
  cohorts: Cohort[];
  contacts: any[];
  vehicles: Vehicle[];
  deleted: boolean;
  code: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
  updatedBy: string;
  state: string;
  id: string;
}

interface Business {
  address: Address;
  code: string;
  contacts: any[];
  email: string;
  fax: string;
  industry: string;
  logo: string;
  mcNumber: string;
  naicCode: string;
  name: string;
  otherPhones: any[];
  primaryPhone: string;
  primaryPhoneExtension: string;
  secondaryPhone: string;
  secondaryPhoneExtension: string;
  sector: string;
  txDMV: string;
  usDOT: string;
  vehicles: any[];
  website: string;
  _id: string;
}

interface Address {
  address1: string;
  address2: string;
  city: string;
  country: string;
  county: string;
  state: string;
  zip: string;
  _id: string;
}

interface Contact {
  address: Address;
  autoInsuranceCompany: string;
  code: string;
  dob: null;
  driverLicense: string;
  driverLicenseState: string;
  educationLevel: string;
  email: string;
  employer: string;
  employmentStatus: string;
  ethnicity: string;
  firstName: string;
  gender: string;
  hasAutoInsurance: boolean;
  hasHealthInsurance: boolean;
  hasHomeInsurance: boolean;
  hasLifeInsurance: boolean;
  healthInsuranceCompany: string;
  homeInsuranceCompany: string;
  identityCardId: string;
  identityCardType: string;
  immigrantStatus: string;
  language: string;
  lastName: string;
  lifeInsuranceCompany: string;
  maritalStatus: string;
  mobilePhone: null | string;
  notes: string;
  phone: null | string;
  relationship: string;
  ssn: string;
  timezone: string;
  vehicles: any[];
  website: string;
  yearlyGrossIncome: number;
  _id: string;
}

interface Communication {
  email: boolean;
  phone: boolean;
  sms: boolean;
  _id: string;
}
