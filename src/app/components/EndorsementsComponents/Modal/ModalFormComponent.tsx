import { Modal, Button } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useEndorsementUIContext } from "../EndorsementUIContext";
import { HeaderSelector } from "./HeaderSelector";

interface Props {
  children: any;
}

export function ModalFormComponent({ children }: Props) {
  const {
    setModalIsOpen,
    setDefaultValues,
    handleClickButtonSave,
    modalIsOpen,
  } = useEndorsementUIContext();

  const handleClose = () => {
    setDefaultValues();
    setModalIsOpen(false);
  };

  const saveEndorsement = () => {
    handleClickButtonSave();
  };

  return (
    <Modal size="xl" show={modalIsOpen} centered>
      <Modal.Header className="p-0">
        <Modal.Title className="w-100">
          <HeaderSelector />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
      <Modal.Footer>
        <Button variant="light" onClick={handleClose} className="fw-bolder">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>
        <Button
          variant="primary"
          className="px-6 py-3 d-flex gap-2"
          onClick={() => {
            saveEndorsement();
          }}
        >
          <span className="mr-3">
            <FormattedMessage id="BUTTON.SAVE" />
          </span>
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
