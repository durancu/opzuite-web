/* eslint-disable no-script-url */
import { Info } from "@material-ui/icons";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

export const CodeColumnHeaderFormatter = () => (
  <>
    {/* <FormattedMessage id="TABLE.HEADER.CODE" /> */}
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="quick-actions-tooltip">
          <FormattedMessage id="FORM.LABELS.CODE" />
        </Tooltip>
      }
    >
      <Info />
    </OverlayTrigger>
  </>
);
