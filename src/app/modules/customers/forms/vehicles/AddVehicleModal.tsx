import { useState, Fragment } from "react";
import { Modal, Button, Col, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Formik, FormikValues } from "formik";
import { vehicleSchema } from "../../pages/customer-edit/schemas";
import { vehicle } from "./helpers";
import { VehicleFields } from "./VehicleFields";
import { customersAPI } from "../../redux";
import { useSnackbar } from "notistack";

interface Props {
  customerId: string;
}

export const AddVehicleModal = ({ customerId }: Props) => {
  const { enqueueSnackbar } = useSnackbar();
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const { data: customer, refetch } =
    customersAPI.useGetCustomerByIdQuery(customerId);
  const [update, { isLoading: isUpdating }] =
    customersAPI.useUpdateCustomerMutation();

  async function processVehicle(vehicle: FormikValues) {
    // we need to return the full customer object modifying just vehicles in this case
    const request = {
      ...customer,
      vehicles: [vehicle, ...customer.vehicles],
    };

    try {
      await update(request).unwrap();
      enqueueSnackbar("Vehicle added successfully", {
        variant: "success",
      });
      refetch();
      setIsOpen(false);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  return (
    <Fragment>
      <Col className="pl-0">
        <Button
          size="sm"
          variant="primary"
          onClick={() => setIsOpen(true)}
          className="d-flex gap-2"
        >
          <span className="">New Co-insured Vehicle</span>
          <i className="fs-3 bi bi-car-front"></i>
        </Button>
      </Col>

      <Modal
        show={isOpen}
        size="xl"
        centered
        contentClassName="m-6"
        backdrop="static"
      >
        <Modal.Header>
          <Modal.Title className="text-primary">
            Add Customer Vehicle
          </Modal.Title>
        </Modal.Header>

        <Formik
          initialValues={vehicle}
          validationSchema={vehicleSchema}
          onSubmit={processVehicle}
        >
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <VehicleFields customerType={customer?.type} />
              </Modal.Body>

              <Modal.Footer className="d-flex gap-3 justify-content-end">
                <Button variant="secondary" onClick={() => setIsOpen(false)}>
                  <FormattedMessage id="BUTTON.CANCEL" />
                </Button>
                <Button type="submit" variant="primary" disabled={isUpdating}>
                  {isUpdating ? (
                    <div className="d-flex gap-3 align-items-center">
                      <Spinner />
                      <span>Processing</span>
                    </div>
                  ) : (
                    <FormattedMessage id="BUTTON.SAVE" />
                  )}
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    </Fragment>
  );
};
