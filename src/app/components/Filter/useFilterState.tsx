import { useAppSelector } from "src/app/hooks";

export const useFilterState = (): any => {
  const filterState = useAppSelector(
    (state: any) => state.filters.filterForEdit
  );
  return filterState;
};
