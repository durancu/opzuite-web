/* eslint-disable no-restricted-imports */
import { Fragment, useState } from "react";
import { Modal, ProgressBar, Button } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { GenericFunction } from "../interfaces/FunctionInterfaces";

interface Props {
  label: string;
  actionDelete: GenericFunction;
  isLoading?: boolean;
  hidden?: boolean;
}

export function DeleteDialog(props: Props) {
  const { actionDelete, label, isLoading, hidden }: Props = props;
  const [isShow, setIsShow] = useState<boolean>(false);

  return (
    <Fragment>
      <Button variant="danger" onClick={() => setIsShow(true)} hidden={hidden}>
        Delete <i className="fs-4 bi bi-trash3"></i>
      </Button>

      <Modal
        show={isShow}
        aria-labelledby="delete dialog"
        centered
        backdrop="static"
      >
        {isLoading && <ProgressBar variant="query" />}
        <Modal.Header closeButton>
          <Modal.Title id="delete dialog">
            {label.charAt(0).toUpperCase() + label.slice(1)} Delete
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {!isLoading && (
            <span>Are you sure to permanently delete this {label}?</span>
          )}
          {isLoading && (
            <span>
              {label.charAt(0).toUpperCase() + label.slice(1)} is deleting...
            </span>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button
            type="button"
            variant="light"
            onClick={() => {
              setIsShow(false);
            }}
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </Button>
          <Button
            type="button"
            variant="danger"
            onClick={() => {
              actionDelete();
            }}
          >
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
