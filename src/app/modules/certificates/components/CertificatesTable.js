import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { useNavigate } from "react-router-dom";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  Pagination,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { sizePerPageList } from "src/app/constants/tables";
import { certificatesAPI } from "../redux";
import { prepareFilter } from "./helpers";

export const CertificatesTable = () => {
  const navigate = useNavigate();
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();
  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = certificatesAPI.useSearchCertificatesQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "customerName",
      text: "Customer",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CustomerColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedCustomerNameColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "masterTemplateName",
      text: "Acord",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CertificateNameColumnHeaderFormatter,
      formatter: columnFormatters.CertificateNameColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: (code) => navigate(code),
        idField: "code",
        textField: "name",
        defaultText: "N/A",
      },
      align: "left",
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "holderName",
      text: "Customer",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CustomerColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedCustomerNameColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },

    {
      dataField: "createdBy",
      text: "Issued By",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CreatedByColumnHeaderFormatter,
      formatter: columnFormatters.CreatedByColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "issuedAt",
      text: "Issued At",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.DateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
      formatter: columnFormatters.CertificateActionsColumnFormatter,
      formatExtraData: {
        openDeleteCertificateDialog: menuActions.delete,
        openDetailCertificatePage: menuActions.certificates,
        editCertificatePage: menuActions.edit,
        previewCertificatePage: menuActions.preview,
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <FilterPanel prepareFilter={prepareFilter} />
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table-responsive"
                bordered={false}
                remote
                keyField="code"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "createdAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error
                show={isError}
                action={refetch}
                description="We couldn't fetch certificates. Please try again"
              />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};
