import { Button } from "@mui/material";
import { Add, Delete, Edit } from "@material-ui/icons";
import { FormattedMessage } from "react-intl";

interface Props {
  onClickFunction: any;
  labelLangId: any;
  variant?: any;
  color?: any;
  size?: any;
  actionType?: any;
  className?: any;
  icon?: any;
}

export function CallToActionButton({
  onClickFunction,
  variant,
  color,
  size,
  labelLangId,
  actionType,
  className,
}: Props) {
  return (
    <Button
      variant={variant}
      color={color}
      size={size}
      onClick={onClickFunction}
      className={className}
    >
      {actionType === "add" && <Add />}
      {actionType === "remove" && <Delete />}
      {actionType === "edit" && <Edit />}
      <span className="ml-2">
        <FormattedMessage id={labelLangId} />
      </span>
    </Button>
  );
}
