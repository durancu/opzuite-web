import { nanoid } from "@reduxjs/toolkit";
import { truncate } from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

export function InsurersColumnFormatter(cellContent: any, row: any) {
  return (
    <div>
      {!!row.items &&
        row.items.map((policy: any) => {
          const sameCarrierAndBroker: boolean =
            policy.brokerName &&
            policy?.carrierName &&
            policy?.carrierName === policy.brokerName;
          const providerLabel: any = sameCarrierAndBroker ? (
            <>
              <FormattedMessage id="FORM.LABELS.MGA" />/
              <FormattedMessage id="FORM.LABELS.CARRIER" />
            </>
          ) : (
            <FormattedMessage id="FORM.LABELS.MGA" />
          );
          const showConditions: any =
            !policy.deleted &&
            (policy?.carrierName?.length > 0 || policy.brokerName.length > 0);

          return (
            showConditions && (
              <div className="my-2" key={nanoid()}>
                <div className="p-0 mx-1 my-1">
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip id="quick-actions-tooltip">
                        <strong>{policy.product?.name}</strong>
                        <br />
                        <small>{policy.product?.description}</small>
                      </Tooltip>
                    }
                  >
                    <span className="pt-2 pb-1 px-2 inline badge-product-icon rounded-left">
                      <i className={`fa fa-${policy.product?.iconClass}`} />
                    </span>
                  </OverlayTrigger>
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip id="quick-actions-tooltip">
                        <span className="fw-bolder">{providerLabel}:&nbsp; </span>
                        <span className="fw-boldest">{policy.brokerName}</span>
                      </Tooltip>
                    }
                  >
                    <span
                      hidden={policy.brokerName?.length === 0}
                      className="text-truncate py-1 px-2 inline badge-broker symbol-label fs-5"
                    >
                      <small>
                        {truncate(policy.brokerName, {
                          length: 30,
                        })}
                      </small>
                    </span>
                  </OverlayTrigger>

                  {!sameCarrierAndBroker && (
                    <>
                      <span
                        hidden={policy.brokerName?.length === 0}
                        className="text-truncate px-1 py-1 inline badge-product-arrow symbol-label"
                      >
                        <i className="fas fa-link" />
                      </span>

                      <OverlayTrigger
                        placement="top"
                        overlay={
                          <Tooltip id="quick-actions-tooltip">
                            <span className="fw-bolder">
                              <FormattedMessage id="FORM.LABELS.CARRIER" />:&nbsp;
                            </span>
                            <span className="fw-boldest">
                              {policy?.carrierName}
                            </span>
                          </Tooltip>
                        }
                      >
                        <span
                          hidden={!policy?.carrierName}
                          className="text-truncate py-1 px-2 inline badge-carrier symbol-label fs-5 rounded-right"
                        >
                          <small>
                            {truncate(policy?.carrierName, {
                              length: 30,
                            })}
                          </small>
                        </span>
                      </OverlayTrigger>
                    </>
                  )}
                </div>
              </div>
            )
          );
        })}
    </div>
  );
}
