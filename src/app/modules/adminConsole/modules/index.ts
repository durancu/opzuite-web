// only exporting state slices to ease the integration into root reducer at /src/redux
export { locationsSlice } from "./locations";
export { payrollsSlice } from "./payroll";
export { reportsSlice } from "./reports";
export { usersSlice } from "./users";

