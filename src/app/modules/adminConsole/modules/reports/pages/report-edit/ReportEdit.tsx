import { Fragment, useEffect } from "react";
import { Card, ProgressBar } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { WizardForm, WizardFormStep } from "src/app/components";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actions from "../../_redux/reportsActions";
import FormReportCreate from "./Forms/FormReportCreate";
import { reportCreateValidationSchema } from "./ReportEditFormValidation";
import { useInitializerFilterData } from "./useInitizalizerFilterData";

export const ReportEdit = () => {
  const navigate = useNavigate();
  const { code }: any = useParams();
  const reportInitData = useInitializerFilterData();
  const dispatch = useAppDispatch();

  const { actionsLoading, reportForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.reports.actionsLoading,
      reportForEdit: state.reports.reportForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchReport(code));
    dispatch(catalogActions.getAllCatalog());
  }, [code, dispatch]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const goToReportDetail = (report: any) => {
    navigate(`/manager/reports/${report.code}/detail`);
  };

  const saveReport = (values: any) => {
    if (!code) {
      dispatch(actions.createReport(values)).then((report: any) =>
        goToReportDetail(report)
      );
    } else {
      dispatch(actions.updateReport(values)).then(() => backToReportsList());
    }
  };

  const backToReportsList = () => {
    navigate(-1);
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!code ? "New" : "Edit"} Report
      </PageTitle>
      <Card>
        {actionsLoading && <ProgressBar />}
        <Card.Body>
          <WizardForm
            initialValues={code ? reportForEdit : reportInitData}
            onSubmit={saveReport}
            onCancel={backToReportsList}
          >
            <WizardFormStep validationSchema={reportCreateValidationSchema}>
              <FormReportCreate />
            </WizardFormStep>
          </WizardForm>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
