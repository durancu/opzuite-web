import { useState } from "react";

import { useFormikContext } from "formik";
import { nanoid } from "nanoid";
import { Button } from "react-bootstrap";
import { useIntl } from "react-intl";
import { useGetUserAuth } from "src/app/hooks";
import { AutoCompleteNoFormik } from "src/app/partials/controls";

interface Props {
  onAdd: (index: number) => void;
  isProviderSelected: boolean;
  binder: string;
}

export const AddCoverage = ({
  onAdd,
  binder = "",
  isProviderSelected,
}: Props) => {
  const intl = useIntl();
  const user = useGetUserAuth();
  const coverages: any = intl.messages["COVERAGES_TYPES"];
  const linesOfBusiness: any = intl.messages["LINES_OF_BUSINESS"];

  const { values, setFieldValue } = useFormikContext<any>();
  const [lineOfBusinessSelect, setLineOfBusinessSelect] =
    useState<string>("COMMERCIAL_AUTO");
  const [coverageSelect, setCoverageSelect] = useState<string>(
    "AUTOMOBILE_LIABILITY"
  );

  // const availableLinesOfBusiness = useMemo(() => {
  //   return linesOfBusiness?.filter((business: any) => {
  //     let exists = false;
  //     values.items.forEach((item: any) => {
  //       if (!item.deleted && business.id === item.lineOfBusiness) {
  //         exists = true;
  //       }
  //     });
  //     return !exists && business;
  //   });
  // }, [linesOfBusiness, values.items]);

  const addCoverage = (
    coverages: any[],
    lineOfBusinessSelect: string,
    coverageSelect: string,
    user: any,
    binder: string
  ) => {
    const lineOfBusiness = linesOfBusiness.find(
      ({ id }: any) => id === lineOfBusinessSelect
    );
    const coverage = coverages.find(({ id }: any) => id === coverageSelect);

    const newItems = [
      ...values.items,
      {
        agencyCommission: 0,
        amount: 0,
        broker: binder,
        carrier: "",
        createdBy: user.id,
        deleted: false,
        description: coverage.description,
        effectiveAt: null,
        expiresAt: null,
        history: [],
        id: coverage.id,
        isNonCommissionablePremium: false,
        lineOfBusiness: lineOfBusiness.id,
        name: coverage.name,
        newItem: true,
        number: "",
        premium: 0,
        limit: 0,
        deductible: 0,
        compensation: 0,
        product: coverage.id,
        renewalFrequency: "VARIABLE",
        renewalReference: [],
        status: "PENDING",
        seller: values.seller,
        updatedBy: user.id,
        referralId: nanoid(6),
        vehicles: [],
        cohorts: [],
        details: {
          autoCoverageScope: "ANY",
          includeOwnedAutos: true,
          includeHiredAutos: true,
          includeNonOwnedAutos: true,
          includeOtherCoveredAutoA: false,
          otherCoveredAutoADescription: "",
          includeOtherCoveredAutoB: false,
          otherCoveredAutoBDescription: "",
          combinedSingleLimit: 0,
          bodilyInjuryPerPersonLimit: 0,
          bodilyInjuryPerAccidentLimit: 0,
          propertyDamagePerAccidentLimit: 0,
          coverageFormUsed: "UMBRELLA",
          policyBasedForm: "CLAIMS",
          includeOtherCoverageA: false,
          otherCoverageADescription: "",
          includeOtherCoverageB: false,
          otherCoverageBDescription: "",
          generalAggregateLimitAppliesPer: "PROJECT",
          generalAggregateLimitAppliesPerOther: null,
          additionalInsured: true,
          waiverOfSubrogation: true,
          hasDeductible: false,
          hasRetention: false,
          deductibleOrRetentionAmount: 0,
          eachOccurrenceLimit: 0,
          damageToRentedPremisesLimit: 0,
          medicalExpensesLimit: 0,
          generalAggregateLimit: 0,
          productCompletedOperationsAggregateLimit: 0,
          other1LimitName: "",
          other1LimitAmount: 0,
          other2LimitName: "",
          other2LimitAmount: 0,
          hasExcludedPeople: false,
          excludedPeopleDescription: "",
          perStatute: false,
          other: false,
          employerLiabilityEachAccidentLimit: 0,
          employerLiabilityEachEmployeeLimit: 0,
          employerLiabilityDiseasePolicyLimit: 0,
        },
      },
    ];

    setFieldValue("items", newItems);

    // setLineOfBusinessSelect("COMMERCIAL_AUTO");
    // setCoverageSelect("AUTOMOBILE_LIABILITY");
    onAdd(newItems.length - 1);
  };

  const addCoverageHandler = () => {
    addCoverage(coverages, lineOfBusinessSelect, coverageSelect, user, binder);
  };

  return (
    <div className="d-flex gap-3 align-items-start justify-content-center">
      <div className="flex-grow-1">
        <AutoCompleteNoFormik
          name="lineOfBusinessSelect"
          label="Line Of Business"
          labelField="name"
          options={linesOfBusiness || []}
          value={lineOfBusinessSelect}
          handleChange={({ target }: any) => {
            setLineOfBusinessSelect(target.value);
          }}
          helperText="Choose a the line of business."
        />
      </div>
      <div className="flex-grow-1">
        <AutoCompleteNoFormik
          name="coverageSelect"
          label="Coverage"
          labelField="name"
          options={coverages}
          value={coverageSelect}
          handleChange={({ target }: any) => {
            setCoverageSelect(target.value);
          }}
          helperText='Choose a new coverage type, then click "New Coverage" to
          enter details.'
        />
      </div>
      <Button
        variant="primary"
        onClick={addCoverageHandler}
        disabled={
          !coverageSelect || !isProviderSelected || !lineOfBusinessSelect
        }
        className="d-flex gap-1 align-items-center  text-nowrap mt-8"
      >
        <i className="fs-3 bi bi-plus-circle-dotted"></i>
        <span className="">New Coverage</span>
      </Button>
    </div>
  );
};
