export const valuePropertyByPath = (obj: any, path: string) =>
  path
    .split(".")
    .reduce((o: any, key: string) => (o && o[key] ? o[key] : ""), obj);
