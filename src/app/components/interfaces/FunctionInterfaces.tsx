export interface GenericFunction {
  (e?: any): void;
}
