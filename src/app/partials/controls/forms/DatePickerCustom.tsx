import { useField } from "formik";
import { useIntl } from "react-intl";
import { DEFAULT_DATEPICKER_FORMAT } from "./DateRange/dateFactory";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import {
  LocalizationProvider,
  DatePickerProps,
  DatePicker,
} from "@mui/x-date-pickers";
import { Form } from "react-bootstrap";
import clsx from "clsx";

interface Props extends Partial<DatePickerProps<any, any>> {
  name: string;
  label?: string;
  maxDate?: Date;
  labelTextId?: string;
  helperTextId?: string;
  helperText?: string;
  format?: string;
  hidden?: boolean;
  required?: boolean;
  className?: string;
}

export const DatePickerCustom = ({
  name,
  maxDate,
  minDate,
  label,
  hidden,
  format,
  required,
  labelTextId,
  helperText = "",
  helperTextId,
  className,
  ...rest
}: Props) => {
  const [field, meta, helpers] = useField(name);
  const intl = useIntl();

  const handleChange = (value: any) => {
    helpers.setValue(value);
  };

  const handleError = (reason: any, value: any) => {
    switch (reason) {
      case "invalidDate":
        helpers.setError("Invalid date format");
        break;

      case "disablePast":
        helpers.setError("Values in the past are not allowed");
        break;

      case "maxDate":
        helpers.setError(`Date should not be after ${value}`);
        break;

      case "minDate":
        helpers.setError(`Date should not be before ${value}`);
        break;

      default:
        helpers.setError(undefined);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        {...rest}
        value={field.value || null}
        maxDate={maxDate}
        minDate={minDate}
        onError={handleError}
        onChange={handleChange}
        inputFormat={format || DEFAULT_DATEPICKER_FORMAT}
        label={labelTextId ? intl.formatMessage({ id: labelTextId }) : label}
        renderInput={({ inputRef, inputProps, InputProps, label }) => (
          <Form.Group hidden={hidden} className="position-relative">
            <Form.Label htmlFor={name} className={clsx({ required })}>
              {label}
            </Form.Label>
            <div className="position-relative">
              <Form.Control
                ref={inputRef}
                {...inputProps}
                required={required}
                className={className}
                isInvalid={meta.error ? true : false}
              />
              <span
                className={clsx(
                  "position-absolute top-50 end-0",
                  meta.error ? "me-12" : "me-4"
                )}
              >
                {InputProps?.endAdornment}
              </span>
            </div>
            <Form.Text className="text-danger my-1 d-block">
              {meta.error}
            </Form.Text>
            <Form.Text id="helperText" className="my-1 d-block">
              {helperTextId
                ? intl.formatMessage({ id: helperTextId })
                : helperText}
            </Form.Text>
          </Form.Group>
        )}
      />
    </LocalizationProvider>
  );
};
