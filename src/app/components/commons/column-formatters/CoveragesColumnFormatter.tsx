import { nanoid } from "@reduxjs/toolkit";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export const CoveragesColumnFormatter = (row: any) => {
  return (
    <div className="d-inline-flex" style={{ maxWidth: "200px" }}>
      {typeof row.items !== "undefined" &&
        row.items.map(
          ({ product }: any) =>
            product.id !== "" && (
              <div className="p-0 mr-2 my-1" key={nanoid()}>
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="quick-actions-tooltip">
                      <strong>{product.name}</strong>
                      <br />
                      <small>{product.description}</small>
                    </Tooltip>
                  }
                >
                  <i
                    className={`fa fa-${product.iconClass}`}
                    style={{ color: "#666" }}
                  />
                </OverlayTrigger>
              </div>
            )
        )}
    </div>
  );
};
