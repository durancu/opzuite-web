import { FormHelperText } from "@mui/material";
import { useIntl } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { AutoComplete } from "../../partials/controls";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function IndustriesAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.INDUSTRY",
  labelText,
  fieldName = "industry",
}: Props) {
  const intl = useIntl();
  const { catalogs } = useSelector(
    (reduxState: any) => ({ catalogs: reduxState.catalogs }),
    shallowEqual
  );
  const { entities }: any = catalogs;

  return (
    <>
      <AutoComplete
        name={fieldName}
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
        options={entities?.industry}
        labelField="name"
      />
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
