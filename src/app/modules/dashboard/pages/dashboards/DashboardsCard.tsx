import { useMemo } from "react";
import { DashboardsTable } from "./dashboard-table/DashboardsTable";
import { DashboardsFilter } from "./dashboards-filter/DashboardsFilter";
import { DashboardsGrouping } from "./dashboards-grouping/DashboardsGrouping";
import { useDashboardsUIContext } from "./DashboardsUIContext";
import { Card } from "react-bootstrap";

export function DashboardsCard() {
  const dashboardsUIContext: any = useDashboardsUIContext();
  const dashboardsUIProps: any = useMemo(() => {
    return {
      ids: dashboardsUIContext.ids,
      queryParams: dashboardsUIContext.queryParams,
      setQueryParams: dashboardsUIContext.setQueryParams,
      newDashboardButtonClick: dashboardsUIContext.newDashboardButtonClick,
      openDeleteDashboardsDialog:
        dashboardsUIContext.openDeleteDashboardsDialog,
      openEditDashboardPage: dashboardsUIContext.openEditDashboardPage,
      openDetailDashboardPage: dashboardsUIContext.openDetailDashboardPage,
      openUpdateDashboardsStatusDialog:
        dashboardsUIContext.openUpdateDashboardsStatusDialog,
      openFetchDashboardsDialog: dashboardsUIContext.openFetchDashboardsDialog,
    };
  }, [dashboardsUIContext]);

  return (
    <Card>
      <Card.Header title="">
        <button onClick={dashboardsUIProps.newDashboardButtonClick}>
          New Dashboard
        </button>
      </Card.Header>
      <Card.Body>
        <DashboardsFilter />
        {dashboardsUIProps.ids.length > 0 && <DashboardsGrouping />}
        <DashboardsTable />
      </Card.Body>
    </Card>
  );
}
