export type CoverageActionTypes = "UPDATE" | "REPLACE" | "DELETE" | "";

export enum CoverageActions {
  update = "UPDATE",
  replace = "REPLACE",
  delete = "DELETE",
}
