import { Dispatch } from "redux";
import { variantAccordingToHttpCode } from "src/app/components/Feedback";
import { feedbacksSlice } from "src/app/components/Feedback/_redux/feedbackSlice";
import * as requestFromServer from "./locationsCrud";
import { callTypes, locationsSlice } from "./locationsSlice";

const { actions } = locationsSlice;

export const fetchLocations = (queryParams: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.list } as any));
  return requestFromServer
    .findLocations(queryParams)
    .then((response) => {
      const { totalCount, entities } = response.data;
      dispatch(actions.locationsFetched({ totalCount, entities } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find locations";
      dispatch(actions.catchError({ error, callType: callTypes.list } as any));
    });
};

export const fetchLocation = (code: any) => (dispatch: Dispatch) => {
  if (!code) {
    return dispatch(
      actions.locationFetched({ locationForEdit: undefined } as any)
    );
  }

  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getLocationByCode(code)
    .then((response) => {
      const location = response.data;
      dispatch(actions.locationFetched({ locationForEdit: location } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find location";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const deleteLocation = (code: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deleteLocation(code)
    .then((response) => {
      dispatch(actions.locationDeleted({ code } as any));
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Location deleted successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete location";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const createLocation =
  (locationForCreation: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .createLocation(locationForCreation)
      .then((response) => {
        dispatch(actions.locationCreated());
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Location created successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't create location";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };

export const clearLocation = () => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  dispatch(actions.locationClear());
};

export const updateLocation = (location: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .updateLocation(location)
    .then((response) => {
      dispatch(actions.locationUpdated());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Location updated successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't update location";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const deleteLocations =
  (ids: string[]) =>
  (dispatch: Dispatch): Promise<void> => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .deleteLocations(ids)
      .then((response) => {
        dispatch(actions.locationsDeleted({ ids } as any));
        dispatch(
          feedbacksSlice.actions.enqueueFeedbackMessage({
            message: "Location deleted successfully.",
            options: {
              key: new Date().getTime() + Math.random(),
              variant: variantAccordingToHttpCode(response.status),
            },
          } as any)
        );
      })
      .catch((error) => {
        error.clientMessage = "Can't delete locations";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };
