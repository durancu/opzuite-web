import { useEffect, useState } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch, useGetUserAuth } from "src/app/hooks";
import { isPermitted } from "src/app/utils";
import * as actionCatalog from "../redux/catalogActions";

interface Props {
  labelField: string;
  labelHelperFields: any[];
  valueField: string;
  joint?: string;
}

export const DEFAULT_JOINT = " | ";

export function useGetEmployeeCatalog(loadData: boolean, props: Props) {
  const dispatch = useAppDispatch();
  const { entities } = useSelector(
    (state: any) => ({ entities: state.catalogs?.entities }),
    shallowEqual
  );

  useEffect(() => {
    if (loadData) {
      dispatch(actionCatalog.getAllCatalog());
    }
  }, [dispatch, loadData]);

  const user = useGetUserAuth();

  const [employeesOptions, setEmployeesOptions] = useState([]);

  useEffect(() => {
    if (entities?.employees && entities?.employees.length > 0) {
      if (isPermitted(Permissions.MY_COUNTRY, user?.permissions || [])) {
        setEmployeesOptions(
          entities?.employees.filter(
            ({ country }: any) => country === user?.country
          ) || []
        );
      } else if (isPermitted(Permissions.MY_LOCATION, user?.permissions || [])) {
        setEmployeesOptions(
          entities?.employees.filter(
            ({ location }: any) => location === user?.location
          )
        );
      } else {
        setEmployeesOptions(entities.employees);
      }
    }
  }, [entities?.employees, user]);

  return (
    employeesOptions?.map((employee: any) => {
      const extractedProperties: any[] = [];
      props.labelHelperFields.forEach((field: string) => {
        if (Object.prototype.hasOwnProperty.call(employee, field)) {
          extractedProperties.push(employee[field]);
        }
      });
      return {
        id: employee[props.valueField],
        name: employee[props.labelField],
        description: extractedProperties.join(props.joint || DEFAULT_JOINT),
      };
    }) || []
  );
}
