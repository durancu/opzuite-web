import { Fragment, useState } from "react";
import { Form, Modal } from "react-bootstrap";
import { toAbsoluteUrl } from "src/_metronic/helpers";

const flag = "HIDE_RELEASE_NOTES";
export const VersionInfo = () => {
  const [open, toggleOpen] = useState(() => {
    return localStorage.getItem(flag) === "true" ? false : true;
  });

  function handleClose() {
    toggleOpen(false);
  }

  return (
    <Fragment>
      <button
        title="Version info"
        className="btn btn-icon btn-active-light-primary btn-custom px-3"
        onClick={() => toggleOpen(!open)}
      >
        <i className="fs-3 bi bi-info-circle"></i>
      </button>

      <Modal size="xl" show={open} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Version Info</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <h1 className="d-flex align-items-center gap-2">
            <img
              alt="Logo"
              src={toAbsoluteUrl("/media/logos/logo-icon.png")}
              className="h-40px"
            />
            <span className="h1 fw-bold">Inzuite v1.4</span>
          </h1>

          <p className="lead my-8">May 2023</p>

          <h2 className="fw-bold mb-4">Feature Updates</h2>

          <ul className="fs-5">
            <li>
              Inzuite had a redesign!. The new theme adds dark themes and a lot
              of perfomance impovements
            </li>
            <li>
              We added a new &nbsp;
              {/* <Link to="/certificates" className="text-underline"> */}
              certificates module
              {/* </Link> */}
              . You can generate several new certificates for each customer.
            </li>
            <li>
              You can now access and manage your profile by clicking on the
              profile icon in the top right menu. There you can update your
              profile picture, address and so much more
            </li>
            <li>
              Improved table filters design. You can now quickly clear the
              filters with button on the far end of the filter bar and
              optionally add advanced filter options when available
            </li>

            <li>
              We added more quick links to table columns. Looking at policies
              and want to learn more about the customer? Just click on their
              name, {"you,re"} welcome
            </li>

            <li>
              We noticed some forms get very long so we added collapsible
              sections so that you can better manage them. This feature is
              available in customer verhicles, customer cohorts, policy
              coverages and service coverages
            </li>
          </ul>
        </Modal.Body>

        <Modal.Footer className="">
          <Form.Check
            label="Do not show this message again"
            defaultChecked={localStorage.getItem(flag) === "true"}
            onChange={(evt) => {
              localStorage.setItem(flag, evt.target.checked.toString());
            }}
          />
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};
