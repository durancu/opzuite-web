import {
  LOGIN_URL,
  ME_URL,
  REGISTER_URL,
  REQUEST_PASSWORD_URL,
} from "./authCrud";
import userTableMock from "./userTableMock";

export default function mockAuth(mock: any) {
  mock.onPost(LOGIN_URL).reply(({ data }: any) => {
    const { email, password } = JSON.parse(data);

    if (email && password) {
      const user = userTableMock.find(
        (x) =>
          x.email.toLowerCase() === email.toLowerCase() &&
          x.password === password
      );

      if (user) {
        return [200, { ...user, password: undefined }];
      }
    }

    return [400];
  });

  mock.onPost(REGISTER_URL).reply(({ data }: any) => {
    const { email, fullname, username, password } = JSON.parse(data);

    if (email && fullname && username && password) {
      const user: any = {
        id: generateUserId(),
        email,
        fullname,
        username,
        password,
        role: {
          _id: "610b689d8c7d19bc90035379",
          permissions: [
            "users",
            "policies",
            "applications",
            "customers",
            "carriers",
            "mgas",
            "financers",
            "locations",
            "payrolls",
            "my-company",
            "reports",
            "dashboards",
          ],
          name: "CEO",
          description: "Company Executive Officer",
          key: "ceo",
          company: "60a818c03aa4b8fa845a1475",
          code: "hv0wBp",
          __v: 0,
          hierarchy: 30,
        },
        accessToken: "access-token-" + Math.random(),
        refreshToken: "access-token-" + Math.random(),
        pic: process.env.MEDIA_URL + "/media/users/default.jpg",
      };

      userTableMock.push(user);

      return [200, { ...user, password: undefined }];
    }

    return [400];
  });

  mock.onPost(REQUEST_PASSWORD_URL).reply(({ data }: any) => {
    const { email } = JSON.parse(data);

    if (email) {
      const user: any = userTableMock.find(
        (x) => x.email.toLowerCase() === email.toLowerCase()
      );

      if (user) {
        user.password = undefined;

        return [200, { ...user, password: undefined }];
      }
    }

    return [400];
  });

  mock.onGet(ME_URL).reply(({ headers: { Authorization } }: any) => {
    const accessToken =
      Authorization &&
      Authorization.startsWith("Bearer ") &&
      Authorization.slice("Bearer ".length);

    if (accessToken) {
      const user = userTableMock.find((x) => x.accessToken === accessToken);

      if (user) {
        return [200, { ...user, password: undefined }];
      }
    }

    return [401];
  });

  function generateUserId() {
    const ids = userTableMock.map((el) => el.id);
    const maxId = Math.max(...ids);
    return maxId + 1;
  }
}
