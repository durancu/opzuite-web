import { Fragment } from "react";

export const CertificateNameColumnFormatter = (
  certificateName: any,
) => (
  <Fragment>
    <span
      className="text-dark fw-semibold fs-6"
    >
      {certificateName}
    </span>
  </Fragment>
);
