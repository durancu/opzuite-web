type ListItem = {
  id: string;
  name: string;
  description?: string;
};

type BaseInputProps = {
  name: string;
  value?: any;
  disabled?: boolean;
  required?: boolean;
  hidden?: boolean;
  labelText?: string;
  labelTextId?: string;
  helperText?: string;
  helperTextId?: string;
  onChange?: (evt: any) => void;
  className?: string;
};
