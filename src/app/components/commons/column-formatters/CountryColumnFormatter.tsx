import { Fragment } from "react";

export const CountryColumnFormatter = (country: any): any => (
    <Fragment>
        <span className="text-dark fw-semibold fs-6">
            {country}
        </span>
    </Fragment>
);
