/* eslint-disable @typescript-eslint/no-unused-vars */
import { createContext, useContext, useState } from "react";
import { BackdropLoader } from "../Loaders";

interface Props {
  children: any;
}

const BackdropUIContext = createContext({
  step: 0,
  isLoading: false,
  setIsLoading: (e: boolean) => e,
});

export const useBackdropUIContext = () => {
  return useContext(BackdropUIContext);
};

export const BackdropIUConsumer = BackdropUIContext.Consumer;

export const BackdropUIProvider = ({ children }: Props) => {
  const [isLoading, setIsLoading] = useState(false);

  const backdropProps: any = {
    isLoading,
    setIsLoading,
  };

  return (
    <BackdropUIContext.Provider value={backdropProps}>
      {isLoading && <BackdropLoader />}
      {children}
    </BackdropUIContext.Provider>
  );
};
