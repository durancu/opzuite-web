import { isEqual, isFunction } from "lodash";
import { createContext, useCallback, useContext, useState } from "react";
import { initialFilter } from "./DashboardsResultDetailUIHelpers";

const DashboardsResultDetailUIContext = createContext(null);

export function useDashboardsResultDetailUIContext() {
  return useContext(DashboardsResultDetailUIContext);
}

export const DashboardsResultDetailUIConsumer =
  DashboardsResultDetailUIContext.Consumer;

export function DashboardsResultDetailUIProvider({ children }: any) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams: any) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value: any = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
  };

  return (
    <DashboardsResultDetailUIContext.Provider value={value}>
      {children}
    </DashboardsResultDetailUIContext.Provider>
  );
}
