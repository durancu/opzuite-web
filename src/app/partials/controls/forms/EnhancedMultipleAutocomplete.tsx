/* eslint-disable no-use-before-define */
import { Checkbox, FormHelperText, TextField } from "@mui/material";
import { CheckBox, CheckBoxOutlineBlankOutlined } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { ErrorMessage, useFormikContext } from "formik";
import objectPath from "object-path";
import React, { useState } from "react";

const icon = <CheckBoxOutlineBlankOutlined fontSize="small" />;
const checkedIcon = <CheckBox fontSize="small" />;

interface AutocompleteOption {
  id: string;
  name: string;
  description?: string;
}

interface Props {
  name: string;
  label: string;
  labelField: string;
  valueField: string;
  showDescription?: boolean;
  labelFieldDescription?: string;
  options: any[];
  variant?: string;
  disabled?: boolean;
  required?: boolean;
  otherOnChangeActions?: any;
}

export function EnhancedMultipleAutocomplete({
  labelFieldDescription = "description",
  ...props
}: Props) {
  const { handleChange, values, errors } = useFormikContext<any>();

  const [localValue, setLocalValue] = useState(
    objectPath.get(values, props.name)
  );

  React.useEffect(() => {
    setLocalValue(objectPath.get(values, props.name));
  }, [props.name, values]);

  const handleChangeComponent = (value: any) => {
    const newE = {
      target: {
        value,
        name: props.name || "name",
        description: value?.description || "",
      },
    };
    handleChange(newE);
    setLocalValue(value);
    props.otherOnChangeActions && props.otherOnChangeActions();
  };

  return (
    <>
      <Autocomplete
        multiple
        disableCloseOnSelect
        disabled={props.disabled}
        options={props.options || ([] as AutocompleteOption[])}
        getOptionSelected={(option, value) =>
          (Array.isArray(value) ? value : [value]).find(
            (item) => item[props.valueField] === option[props.valueField]
          )
        }
        getOptionLabel={(option: any) => {
          return option[props.labelField];
        }}
        onChange={(_, value) => {
          handleChangeComponent(value);
        }}
        value={localValue}
        renderOption={(option, { selected }) => (
          <React.Fragment>
            <span>{selected}</span>
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              style={{ marginRight: 8 }}
              checked={selected}
            />
            <div>
              {option[props.labelField]}
              {props.showDescription && (
                <FormHelperText>{option[labelFieldDescription]}</FormHelperText>
              )}
            </div>
          </React.Fragment>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label={props.label}
            required={props.required}
            error={objectPath.get(errors, props.name) ? true : false}
          />
        )}
      />
      <FormHelperText className="MuiFormHelperText-root Mui-error">
        <ErrorMessage name={props.name} />
      </FormHelperText>
    </>
  );
}
