import { useEffect, useMemo } from "react";
import ApexCharts from "apexcharts";
import SVG from "react-inlinesvg";
import { getCSSVariableValue } from "src/_metronic/assets/ts/_utils";
import { toAbsoluteUrl } from "src/_metronic/helpers";

interface Props {
  className: string;
  symbolShape: string;
  baseColor: string;
}

export const PoliciesAmountByWeekDayWidget = ({
  className,
  symbolShape,
  baseColor,
}: Props) => {
  const layoutProps = useMemo(() => {
    return {
      colorsGrayGray500: getCSSVariableValue("--bs-gray-500"),
      colorsGrayGray600: getCSSVariableValue("--bs-gray-600"),
      colorsGrayGray200: getCSSVariableValue("--bs-gray-200"),
      colorsGrayGray300: getCSSVariableValue("--bs-gray-300"),
      colorsThemeBaseSuccess: getCSSVariableValue(`--bs-${baseColor}`),
      colorsThemeLightSuccess: getCSSVariableValue(`--bs-${baseColor}-light`),
    };
  }, [baseColor]);

  useEffect(() => {
    const element = document.getElementById("policies_amount_by_seller_chart");

    if (!element) {
      return;
    }

    const options = getChartOption(layoutProps);
    const chart = new ApexCharts(element, options);
    chart.render();
    return function cleanUp() {
      chart.destroy();
    };
  }, [layoutProps]);

  return (
    <div className={`card card-custom ${className}`}>
      <div className="card-body p-0">
        <div className="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
          <span
            className={`symbol ${symbolShape} symbol-50 symbol-light-${baseColor} mr-2`}
          >
            <span className="symbol-label">
              <span className={`svg-icon svg-icon-xl svg-icon-${baseColor}`}>
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Layout/Layout-4-blocks.svg"
                  )}
                ></SVG>
              </span>
            </span>
          </span>
          <div className="d-flex flex-column text-right">
            <span className="text-dark-75 font-weight-bolder font-size-h3">
              340,030
            </span>
            <span className="text-muted font-weight-bold mt-2">
              Weekly Income
            </span>
          </div>
        </div>
        <div
          id="policies_amount_by_seller_chart"
          className="card-rounded-bottom"
          style={{ height: "600px" }}
        ></div>
      </div>
    </div>
  );
};

function getChartOption(layoutProps: any) {
  const options = {
    series: [
      {
        name: "Policies Sales",
        data: [44200, 33245, 53192, 34520, 31224, 34429],
      },
    ],
    chart: {
      type: "bar",
      height: 200,
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: false,
      },
      sparkline: {
        enabled: false,
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "40%",
        //endingShape: 'rounded'
      },
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    fill: {
      type: "solid",
      opacity: 1,
    },
    stroke: {
      curve: "smooth",
      show: true,
      width: 2,
      colors: [layoutProps.colorsThemeBaseSuccess],
    },
    xaxis: {
      categories: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: true,
        style: {
          colors: layoutProps.colorsGrayGray500,
          fontSize: "12px",
          fontFamily: layoutProps.fontFamily,
        },
      },
      crosshairs: {
        show: false,
        position: "front",
        stroke: {
          color: layoutProps.colorsGrayGray300,
          width: 1,
          dashArray: 3,
        },
      },
      tooltip: {
        enabled: true,
        formatter: undefined,
        offsetY: 0,
        style: {
          fontSize: "12px",
          fontFamily: layoutProps.fontFamily,
        },
      },
    },
    yaxis: {
      min: 0,
      max: 70000,
      labels: {
        show: true,
        style: {
          colors: layoutProps.colorsGrayGray500,
          fontSize: "12px",
          fontFamily: layoutProps.fontFamily,
        },
      },
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      hover: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0,
        },
      },
    },
    tooltip: {
      style: {
        fontSize: "12px",
        fontFamily: layoutProps.fontFamily,
      },
      y: {
        formatter: function (val: string) {
          return "$" + val + " thousands";
        },
      },
    },
    colors: [layoutProps.colorsThemeLightSuccess],
    markers: {
      colors: [layoutProps.colorsThemeLightSuccess],
      strokeColor: [layoutProps.colorsThemeBaseSuccess],
      strokeWidth: 3,
    },
  };
  return options;
}
