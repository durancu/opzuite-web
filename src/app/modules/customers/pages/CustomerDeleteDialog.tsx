import { useEffect } from "react";
import { Modal, Button, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router";
import { useSnackbar } from "notistack";
import { customersAPI } from "../redux";

type routeParams = {
  code: string;
};

export const CustomerDeleteDialog = () => {
  const { code } = useParams<routeParams>();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [deleteCustomer, { isLoading }] =
    customersAPI.useDeleteCustomerMutation();

  function goBack() {
    navigate("/customers");
  }

  // if !id we should close modal
  useEffect(() => {
    if (!code) goBack();
  }, [code]);

  async function handleDeleteCustomer() {
    try {
      await deleteCustomer(code).unwrap();
      enqueueSnackbar("Customer deleted", {
        variant: "success",
      });
      navigate("/customers");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete insured"
      centered
      backdrop="static"
    >
      <Modal.Header closeButton>
        <Modal.Title id="delete insured">Delete Insured </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <span>Are you sure to permanently delete this customer?</span>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={goBack} className="btn btn-light btn-elevate">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>

        <Button
          onClick={handleDeleteCustomer}
          className="btn btn-danger btn-elevate"
          disabled={isLoading}
        >
          {isLoading ? (
            <div className="d-flex gap-3 align-items-center">
              <Spinner />
              <span>Processing</span>
            </div>
          ) : (
            <FormattedMessage id="BUTTON.DELETE" />
          )}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
