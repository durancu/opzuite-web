import { CircularProgress, TextField } from "@mui/material";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { FunctionComponent } from "react";

interface Props {
  name: string;
  label: string;
  value: any;
  labelField?: string;
  options: any[];
  variant?: string;
  startAdornment?: any;
  endAdornment?: any;
  disabled?: boolean;
  handleChange: any;
  loading: boolean;
}

export const AutoCompleteAsynchronous: FunctionComponent<Props> = (props) => {
  const {
    name,
    label,
    labelField,
    value,
    options,
    handleChange,
    loading,
    ...restProps
  } = props;

  return (
    <Autocomplete
      {...restProps}
      onChange={(_, newValue) => {
        handleChange({
          target: {
            name: name,
            value: newValue,
          },
        });
      }}
      value={value}
      getOptionSelected={() => true}
      getOptionLabel={(option: any) => {
        return labelField ? option[labelField] : option;
      }}
      options={options}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
              </>
            ),
          }}
        />
      )}
    />
  );
};
