import { FormikValues } from "formik";
export const PayrollStatusCssClasses = ["success", "info", ""];
export const PayrollStatusTitles = ["Selling", "Sold"];
export const PayrollConditionCssClasses = ["success", "danger", ""];
export const PayrollConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

export const defaultFilter = {
  filter: {},
  sortOrder: "desc", // asc||desc
  sortField: "payPeriodStartedAt",
  pageNumber: 1,
  pageSize: 20,
};

export const initialValues = {
  location: "",
  searchText: "",
  periodCode: "",
  country: "USA",
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {};

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchText: values.searchText,
    sortOrder: "desc",
    sortField: "payPeriodStartedAt",
    pageNumber: 1,
    pageSize: 20,
  };
};
