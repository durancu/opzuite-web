import { FormattedNumber } from "react-intl";

export const AmountColumnFormatter = (amount: any) => (

  <span className="text-dark fw-semibold fs-6">
    <FormattedNumber value={amount} style={`currency`} currency="USD" />
  </span>


);
