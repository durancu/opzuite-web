import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

import { RequirePermission } from "../guards";

export const TableActionsMenu = React.memo(({ ...props }: any) => {
  const {
    openCertificates,
    readFunction,
    editFunction,
    renewFunction,
    deleteFunction,
    changePasswordFunction,
    changeIpLocationFunction,
    readPermission,
    hideReadCondition,
    editPermission,
    certificatePermission,
    hideRenewCondition,
    hideEditCondition,
    renewPermission,
    deletePermission,
    hideDeleteCondition,
    changePasswordPermission,
    previewFunction,
    code,
    type,
    customerId
  } = props;

  return (
    <div className="d-flex justify-content-end">
      {openCertificates && (
        <RequirePermission permissions={certificatePermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-details">
                <FormattedMessage id="MENU.ACTIONS.CERTIFICATE" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="edit-certificate"
              onClick={() => openCertificates(customerId)}
            >
              <i className="fs-3 bi bi-file-earmark-text"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {previewFunction && (
        //<RequirePermission permissions={readPermission}>
        <OverlayTrigger
          placement="top"
          overlay={
            <Tooltip id="action-tooltip-details">
              <FormattedMessage id="MENU.ACTIONS.PREVIEW" />
            </Tooltip>
          }
        >
          <button
            className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
            aria-label="preview-certificate"
            onClick={() => previewFunction(code)}
          >
            <i className="fs-3 bi bi-binoculars"></i>
          </button>
        </OverlayTrigger>
        //</RequirePermission>
      )}
      {readFunction && (
        <RequirePermission permissions={readPermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-details">
                <FormattedMessage id="MENU.ACTIONS.DETAILS" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="details"
              hidden={hideReadCondition}
              onClick={() => {
                readFunction(code);
              }}
            >
              <i className="fs-3 bi bi-eye"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {editFunction && (
        <RequirePermission permissions={editPermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-edit">
                <FormattedMessage id="MENU.ACTIONS.EDIT" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="edit"
              hidden={hideEditCondition}
              onClick={() => {
                editFunction(code, type);
              }}
            >
              <i className="fs-3 bi bi-pencil-square"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {renewFunction && (
        <RequirePermission permissions={renewPermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-renew">
                <FormattedMessage id="MENU.ACTIONS.RENEW" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="renew"
              hidden={hideRenewCondition}
              onClick={() => {
                renewFunction(code, type);
              }}
            >
              <i className="fs-3 bi bi-arrow-repeat"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {changePasswordFunction && (
        <RequirePermission permissions={changePasswordPermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="qaction-tooltip-change-pass">
                <FormattedMessage id="MENU.ACTIONS.CHANGE-PASS" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="change-pass"
              onClick={() => {
                changePasswordFunction(code);
              }}
            >
              <i className="fs-3 bi bi-key"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {changeIpLocationFunction && (
        <RequirePermission permissions={editPermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-change-ip">
                <FormattedMessage id="MENU.ACTIONS.CHANGE-IP" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-primary me-1"
              aria-label="change-ip"
              onClick={() => {
                changeIpLocationFunction(code);
              }}
            >
              <i className="fs-3 bi bi-router"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
      {deleteFunction && (
        <RequirePermission permissions={deletePermission}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id="action-tooltip-delete">
                <FormattedMessage id="MENU.ACTIONS.DELETE" />
              </Tooltip>
            }
          >
            <button
              className="btn btn-sm p-1 btn-icon btn-bg-light btn-active-danger me-1"
              aria-label="delete"
              onClick={() => {
                deleteFunction(code);
              }}
              hidden={hideDeleteCondition}
            >
              <i className="fs-3 bi bi-trash3"></i>
            </button>
          </OverlayTrigger>
        </RequirePermission>
      )}
    </div>
  );
});

TableActionsMenu.displayName = "TableActionsMenu";
