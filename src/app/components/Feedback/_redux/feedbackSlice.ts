import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface FeedbackState {
  notifications: any[];
}

const initialState = {
  notifications: [],
} as FeedbackState;

export const callTypes = {
  list: "list",
  action: "action",
};

export const feedbacksSlice = createSlice({
  name: "feedbacks",
  initialState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getCustomerById
    feedbackSetMessage: (state: any, action: PayloadAction<any>) => {
      const { message, id, httpCode } = action.payload;
      state.message = message + "test";
      state.id = id;
      state.httpCode = httpCode;
      state.date = new Date();
    },
    // findCustomers
    feedbackCleanMessage: (state: any) => {
      state.message = null;
      state.id = null;
      state.httpCode = null;
      state.date = null;
    },
    enqueueFeedbackMessage: (state: any, action: any) => {
      state.notifications = [
        ...state.notifications,
        { key: action.payload.options.key, ...action.payload },
      ];
    },
    removeFeedbackMessage: (state: any, action: any) => {
      state.notifications = state.notifications.filter(
        (notification: any) => notification.options.key !== action.payload.key
      );
    },
  },
});
