import { Fragment } from "react";
import { Form } from "react-bootstrap";
import { ContactInfoFields } from "./personal-form/ContactInfoFields";
import { InsuranceCoveragesFields } from "./personal-form/InsuranceCoveragesFields";
import { OtherDetailsFields } from "./personal-form/OtherDetailsFields";
import { WizardFormSection, FormTextArea } from "src/app/components";
import { useFormikContext } from "formik";
import { Button } from "react-bootstrap";

const FormCustomerPersonalInfo = () => {
  const { values, setFieldValue }: Record<string, any> = useFormikContext();

  function copyInfo() {
    setFieldValue("contact.address", values.business.address);
    setFieldValue("contact.email", values.business.email);
    setFieldValue("contact.phone", values.business.primaryPhone);
    setFieldValue("contact.mobilePhone", values.business.fax);
  }

  return (
    <Fragment>
      <WizardFormSection
        label="Contact Info"
        action={<Button onClick={copyInfo}>Copy from Business Info</Button>}
      >
        <ContactInfoFields />
      </WizardFormSection>
      <WizardFormSection label="Other Details">
        <OtherDetailsFields />
      </WizardFormSection>
      <WizardFormSection label="Insurance Coverages">
        <InsuranceCoveragesFields />
      </WizardFormSection>
      <Form.Group>
        <FormTextArea labelTextId="FORM.LABELS.NOTES" name="contact.notes" />
      </Form.Group>
    </Fragment>
  );
};

export default FormCustomerPersonalInfo;
