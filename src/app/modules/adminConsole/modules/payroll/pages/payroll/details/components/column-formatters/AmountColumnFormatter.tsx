import { formatAmount } from "../../../../../../../../../components/functions";

export const AmountColumnFormatter = (cellContent: any) => (
  <>{formatAmount(cellContent)}</>
);
