import { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useBackdropUIContext } from "../Backdrop";
import * as policyMetricActions from "./_redux/PolicyMetrics/policyMetricsActions";
import { FormattedMessage } from "react-intl";
import { MetricCard } from "./MetricCard";
import { Card } from "react-bootstrap";

interface Props {
  policyDetail: any;
}

export const PolicyMetricsAccordion = ({ policyDetail }: Props) => {
  const dispatch = useAppDispatch();

  const { user, metrics, actionsLoading } = useSelector(
    (state: any) => ({
      metrics: state.policyMetric?.metrics,
      actionsLoading: state.policyMetric?.actionsLoading,
      user: state.auth.user,
    }),
    shallowEqual
  );
  const { setIsLoading } = useBackdropUIContext();

  useEffect(() => {
    if (policyDetail?.endorsements && user) {
      dispatch(
        policyMetricActions.calcPolicyMetrics(policyDetail.endorsements, user)
      );
    }
  }, [policyDetail?.endorsements, user, dispatch]);

  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  return (
    <Card>
      <Card.Title as="h3" className="text-primary mb-8">
        <FormattedMessage id="GENERAL.METRICS" />
      </Card.Title>
      <MetricCard metrics={metrics} />
    </Card>
  );
};

PolicyMetricsAccordion.displayName = "PolicyMetricsAccordion";
