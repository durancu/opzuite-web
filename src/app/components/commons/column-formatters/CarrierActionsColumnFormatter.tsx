import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const CarrierActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openEditCarrierPage, openDeleteCarrierDialog, openDetailCarrierPage }: any
) => (
  <TableActionsMenu
    readFunction={openDetailCarrierPage}
    editFunction={openEditCarrierPage}
    deleteFunction={openDeleteCarrierDialog}
    editPermission={Permissions.CARRIERS_UPDATE}
    readPermission={Permissions.CARRIERS_READ}
    deletePermission={Permissions.CARRIERS_DELETE}
    code={row.code}
  />
);
