import { Link } from "@mui/material";
import { Alert as AlertMaterial, Color } from "@material-ui/lab";
import React from "react";
import { Col, Row } from "react-bootstrap";
import { GenericFunction } from "../../interfaces/FunctionInterfaces";
interface Props {
  action: GenericFunction;
  variant: Color;
  labelComponent: string;
  show: boolean;
}

export const Alert = (props: Props) => {
  const { action, variant, labelComponent, show } = props;

  const preventDefault = (event: React.SyntheticEvent) => {
    event.preventDefault();
    action();
  };

  return (
    <Row hidden={!show}>
      <Col />
      <Col>
        <AlertMaterial className="mb-5" severity={variant}>
          This is an unsaved copy of a previously started {labelComponent}.{" "}
          <Link variant="subtitle1" href="" onClick={preventDefault}>
            Click here
          </Link>{" "}
          if you want to discard these values and reset the form.
        </AlertMaterial>
      </Col>
    </Row>
  );
};
