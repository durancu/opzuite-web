import { nanoid } from "nanoid";
import { useEffect, useState } from "react";

interface Props {
  messageObject: any;
}

export const GenerateMessageList = ({ messageObject }: Props) => {
  const [messageList, setMessageList] = useState<any[]>([]);

  useEffect(() => {
    let messages: any;

    if (messageObject !== null) {
      messages = Object.assign(
        {},
        ...(function _flatten(o): any {
          return [].concat(
            ...Object.keys(o).map((k) =>
              typeof o[k] === "object" &&
              o[k] !== undefined &&
              o[k] !== null &&
              Object.keys(o[k]).length > 0
                ? _flatten(o[k])
                : { [k]: o[k] }
            )
          );
        })(messageObject)
      );

      setMessageList(Object.values(messages));
    }
  }, [messageObject]);

  return (
    <>
      <ul>
        {messageList.map(
          (message) => message && <li key={nanoid()}>{message}</li>
        )}
      </ul>
    </>
  );
};
