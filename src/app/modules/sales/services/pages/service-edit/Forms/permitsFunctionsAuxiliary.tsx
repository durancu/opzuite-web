export const permitTypesByStateList = (
  stateType: string,
  entities: any,
  seller: string
): any[] => {
  const permits = entities.permitTypesByState.find(
    ({ state }: any) => state === stateType
  ).permits;

  const endorsements = permits.map((permit: any) => {
    return {
      ...entities.permitTypes.find(({ type }: any) => type === permit),
    };
  });

  const endorsementsPrepare = endorsements.map((endorsement: any) => ({
    ...endorsement,
    status: "ENTERED_IN_SYSTEM",
    amount: 0,
    seller,
    endorsedAt: null,
    followUpDate: null,
    totalAgencyCommission: 0,
    totalAgentCommission: 0,
    totalFinanced: 0,
    totalFinancedPaid: 0,
    totalNonPremium: 0,
    totalPaid: 0,
    totalPayables: 0,
    totalPremium: 0,
    totalReceivables: 0,
    totalReceived: 0,
    totalTaxesAndFees: 0,
    items: [
      {
        amount: endorsement.amount,
        description: "",
        endorsedAt: new Date(),
        followUpDate: null,
        followUpPerson: null,
        seller,
        status: "",
        otherDetails: {},
        payments: [],
        type: "PAID_IN_FULL",
        accountingClass: "PAYABLE",
      },
    ],
  }));

  return endorsementsPrepare;
};

export const generateEndorsementOptions = (
  permitTypes: any[],
  seller: string
): any[] => {
  return permitTypes.map((element: any) => ({
    ...element,
    seller: seller,
    status: "ENTERED_IN_SYSTEM",
    amount: 0,
    endorsedAt: new Date(),
    followUpDate: null,
    totalAgencyCommission: 0,
    totalAgentCommission: 0,
    totalFinanced: 0,
    totalFinancedPaid: 0,
    totalNonPremium: 0,
    totalPaid: 0,
    totalPayables: 0,
    totalPremium: 0,
    totalReceivables: 0,
    totalReceived: 0,
    totalTaxesAndFees: 0,
    items: [
      {
        amount: element.amount,
        description: "",
        endorsedAt: new Date(),
        followUpDate: null,
        followUpPerson: null,
        seller: seller,
        status: "",
        otherDetails: {},
        payments: [],
        type: "PAID_IN_FULL",
        accountingClass: "PAYABLE",
      },
    ],
  }));
};
