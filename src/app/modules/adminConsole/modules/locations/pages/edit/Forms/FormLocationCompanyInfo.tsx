import { useFormikContext } from "formik";
import { useEffect, useState, Fragment } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { WizardHiddenButtons } from "src/app/components/Wizard/WizardHiddenButtons";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { Row, Col } from "react-bootstrap";
import {
  FormInput,
  PhoneNumberInput,
  FormAutoComplete,
} from "src/app/components";
import { DatePickerCustom } from "src/app/partials/controls";

const FormCustomerBusinessInfo = () => {
  const { values } = useFormikContext<any>();
  const { catalog } = useSelector(
    (state: any) => ({
      catalog: state.catalogs?.entities,
    }),
    shallowEqual
  );
  const [isUSALocation, setIsUSALocation]: any = useState(true);

  useEffect(() => {
    setIsUSALocation(values?.business?.address?.country === "USA");
  }, [values?.business?.address?.country]);

  return (
    <Fragment>
      <Row className="mb-4">
        <Col md={3}>
          <FormAutoComplete
            name="business.address.country"
            labelText="Country"
            labelField="name"
            options={catalog?.locationAvailableCountries}
          />
        </Col>
        <Col md={3}>
          <FormAutoComplete
            name="manager"
            labelText="Manager"
            labelField="name"
            options={catalog?.users}
          />
        </Col>
        <Col md={3}>
          <FormInput labelText="Public IP Address" name="ipAddress" />
        </Col>
        <Col md={3}>
          <FormAutoComplete
            name="payFrequency"
            labelText="Payrolls Frequency"
            options={catalog?.locationPayFrequencies}
            labelField="name"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput name="business.name" labelText="Name" />
        </Col>
        <Col md={3}>
          <FormInput labelText="Alias" name="alias" />
        </Col>
        <Col md={3}>
          <DatePickerCustom
            name="business.startedAt"
            label="Location Start Date"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
        <Col md={3}>
          <FormInput
            type="email"
            name="business.email"
            labelTextId="FORM.LABELS.EMAIL"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <PhoneNumberInput
            labelText="Primary Phone"
            name="business.primaryPhone"
          />
        </Col>
        <Col md={3}>
          <PhoneNumberInput
            labelText="Secondary Phone"
            name="business.secondaryPhone"
          />
        </Col>
        <Col md={3}>
          <PhoneNumberInput name="business.fax" labelText="Fax Number" />
        </Col>

        {!isUSALocation && (
          <Col md={3}>
            <FormInput
              name="business.address.state"
              labelText="State/Province"
            />
          </Col>
        )}
        {isUSALocation && (
          <Col md={3}>
            <FormAutoComplete
              name="business.address.state"
              labelText="State/Province"
              options={catalog?.states}
              labelField="name"
            />
          </Col>
        )}
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput
            name="business.address.address1"
            labelText="Address Line 1"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="business.address.address2"
            labelText="Address Line 2"
          />
        </Col>
        <Col md={3}>
          <FormInput name="business.address.city" labelText="City" />
        </Col>

        <Col md={3}>
          <FormInput name="business.address.zip" labelText="Zip Code" />
        </Col>
      </Row>
      <WizardHiddenButtons />
    </Fragment>
  );
};

export default FormCustomerBusinessInfo;
