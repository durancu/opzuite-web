import axios from "axios";

export const LOCATIONS_URL = "api/v2/locations";

// CREATE =>  POST: add a new location to the server
export function createLocation(location: any) {
  return axios.post(LOCATIONS_URL, location);
}

// READ
export function getAllLocations() {
  return axios.get(LOCATIONS_URL);
}

export function getLocationByCode(locationId: any) {
  return axios.get(`${LOCATIONS_URL}/${locationId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findLocations(queryParams: any) {
  return axios.post(`${LOCATIONS_URL}/search`, { queryParams });
}

// UPDATE => PUT: update the location on the server
export function updateLocation(location: any) {
  return axios.put(`${LOCATIONS_URL}/${location.code}`, location);
}

// UPDATE Status
export function updateStatusForLocations(ids: any, status: any) {
  return axios.post(`${LOCATIONS_URL}/updateStatusForLocations`, {
    ids,
    status,
  });
}

// DELETE => delete the location from the server
export function deleteLocation(locationId: any) {
  return axios.delete(`${LOCATIONS_URL}/${locationId}`);
}

// DELETE Locations by ids
export function deleteLocations(ids: any) {
  return axios.post(`${LOCATIONS_URL}/delete`, { ids });
}
