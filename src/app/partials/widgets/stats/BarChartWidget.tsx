/* eslint-disable no-script-url */
import { Skeleton } from "@material-ui/lab";
import ApexCharts from "apexcharts";
import { useEffect, useMemo } from "react";
import { Card } from "react-bootstrap";
import SVG from "react-inlinesvg";
import { shallowEqual, useSelector } from "react-redux";
import { getCSSVariableValue } from "src/_metronic/assets/ts/_utils";
import { toAbsoluteUrl } from "src/_metronic/helpers";
import { useAppDispatch } from "src/app/hooks";
import * as reportActions from "src/app/modules/adminConsole/modules/reports/_redux/reportsActions";
import { useBackdropUIContext } from "../../../components/Backdrop";
import * as catalogActions from "../../../components/Catalog/redux/catalogActions";

interface ChartDataInterface {
  series?: { name: string; data: any[] };
  categories?: string[];
}

export function BarChartWidget({
  className,
  symbolShape,
  baseColor,
  reportCode,
  title,
}: any) {
  const dispatch = useAppDispatch();

  const { actionsLoading, reportChartData } = useSelector(
    (state: any) => ({
      actionsLoading: state.reports.actionsLoading,
      reportChartData: state.reports.reportChartData,
    }),
    shallowEqual
  );

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  useEffect(() => {
    dispatch(reportActions.fetchReportChartData(reportCode));
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch, reportCode]);

  const layoutProps = useMemo(() => {
    return {
      colorsGrayGray500: getCSSVariableValue("--bs-gray-500"),
      colorsGrayGray600: getCSSVariableValue("--bs-gray-600"),
      colorsGrayGray200: getCSSVariableValue("--bs-gray-200"),
      colorsGrayGray300: getCSSVariableValue("--bs-gray-300"),
      colorsThemeBaseSuccess: getCSSVariableValue(`--bs-${baseColor}`),
      colorsThemeLightSuccess: getCSSVariableValue(`--bs-${baseColor}-light`),
    };
  }, [baseColor]);

  useEffect(() => {
    if (reportChartData) {
      const element = document.getElementById(
        "policies_premium_by_seller_chart"
      );

      if (!element) {
        return;
      }

      const options = getChartOption(layoutProps, reportChartData);
      const chart = new ApexCharts(element, options);
      chart.render();
      return function cleanUp() {
        chart.destroy();
      };
    }
  }, [layoutProps, reportChartData]);

  const calculateTotalMetrics = (data: any[] | undefined) => {
    if (data) {
      return data
        .reduce((accumulator: any, value: any) => accumulator + (value || 0), 0)
        .toFixed(0);
    } else {
      console.log("no data");
    }
    return 0;
  };

  return actionsLoading ? (
    <Skeleton variant="rect" width={"100%"} height={600} />
  ) : (
    <Card
      className={`card card-custom ${className}`}
      style={{ height: "600px" }}
    >
      <Card.Body className="p-3">
        <div className="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
          <span
            className={`symbol ${symbolShape} symbol-50 symbol-light-${baseColor} mr-2`}
          >
            <span className="symbol-label">
              <span className={`svg-icon svg-icon-xl svg-icon-${baseColor}`}>
                <SVG
                  src={toAbsoluteUrl(
                    "/media/svg/icons/Layout/Layout-4-blocks.svg"
                  )}
                ></SVG>
              </span>
            </span>
          </span>
          <div className="d-flex flex-column text-right">
            <span className="text-dark-75 font-weight-bolder font-size-h3">
              {reportChartData &&
                calculateTotalMetrics(reportChartData.series?.data)}
            </span>
            <span className="text-muted font-weight-bold mt-2">{title}</span>
          </div>
        </div>
        <div
          id="policies_premium_by_seller_chart"
          className="card-rounded-bottom"
          style={{ height: "500px" }}
        ></div>
      </Card.Body>
    </Card>
  );
}

function getChartOption(layoutProps: any, chartData: ChartDataInterface) {
  const options = {
    series: [
      {
        name: "Total Premium",
        data: chartData.series?.data,
      },
    ],
    chart: {
      type: "bar",
      height: "80%",
      toolbar: {
        show: true,
      },
      zoom: {
        enabled: true,
      },
      sparkline: {
        enabled: false,
      },
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: "55%",
        borderRadius: 5,
      },
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    fill: {
      type: "solid",
      opacity: 1,
    },
    stroke: {
      curve: "smooth",
      show: false,
      width: 2,
      colors: [layoutProps.colorsThemeBaseSuccess],
    },
    xaxis: {
      categories: chartData.categories,
      axisBorder: {
        show: false,
      },
      axisTicks: {
        show: false,
      },
      labels: {
        show: true,
        style: {
          colors: layoutProps.colorsGrayGray900,
          fontSize: "11px",
          fontFamily: layoutProps.fontFamily,
        },
      },
      crosshairs: {
        show: true,
        position: "front",
        stroke: {
          color: layoutProps.colorsGrayGray300,
          width: 1,
          dashArray: 3,
        },
      },
      tooltip: {
        enabled: true,
        offsetY: 0,
        style: {
          fontSize: "12px",
          fontFamily: layoutProps.fontFamily,
        },
      },
    },
    yaxis: {
      min: 0,
      //tickAmount: 1,
      labels: {
        show: true,
        style: {
          colors: layoutProps.colorsGrayGray900,
          fontSize: "12px",
          fontFamily: layoutProps.fontFamily,
        },
        formatter: function (val: number) {
          return val.toFixed(0);
        },
      },
    },
    states: {
      normal: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      hover: {
        filter: {
          type: "none",
          value: 0,
        },
      },
      active: {
        allowMultipleDataPointsSelection: false,
        filter: {
          type: "none",
          value: 0,
        },
      },
    },
    tooltip: {
      style: {
        fontSize: "12px",
        fontFamily: layoutProps.fontFamily,
      },
      y: {
        formatter: function (val: any) {
          return "$" + Math.round(val);
        },
      },
    },
    colors: [layoutProps.colorsThemeLightSuccess],
    markers: {
      colors: [layoutProps.colorsThemeLightSuccess],
      strokeColor: [layoutProps.colorsThemeBaseSuccess],
      strokeWidth: 3,
    },
  };
  return options;
}
