import { Fragment, useState } from "react";
import { FieldArray, useFormikContext } from "formik";
import { Accordion, Button, Card, Col, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  ButtonModalEndorsementAction,
  EndorsementType,
  EndorsementUIProvider,
  ItemDeleteModal,
  ModalEndorsementActionType,
} from "src/app/components/EndorsementsComponents";
import { useGetUserAuth } from "src/app/hooks";
import { FakeField } from "src/app/partials/controls";
import RepeaterPolicyEndorsementItems from "./RepeaterPolicyEndorsementItems";
import { EndorsementModal } from "./components";
import { initEndorsement } from "./helpers";
import objectPath from "object-path";
import clsx from "clsx";
import { MissingFieldsAlert } from "src/app/components";

const RepeaterPolicyEndorsements = () => {
  const user = useGetUserAuth();
  const { values, errors, setFieldValue } = useFormikContext<any>();
  const { employees, endorsementStatus, endorsementTypes } = useGetCatalog();

  const [localState, setLocalState] = useState<Record<string, any>>({
    type: "", // endorsementType
    action: "", // endorsement action, Add, edit ...
    index: null, // actual endorsement
    modal: false, // show or hide modal
  });

  function updateLocalState(key: string, value: boolean) {
    setLocalState((current) => {
      return { ...current, [key]: value };
    });
  }

  const addNewEndosement = () => {
    const newEndorsement = initEndorsement(user?.id || "");
    const updatedEndorsements = [...values.endorsements, newEndorsement];

    setFieldValue("endorsements", updatedEndorsements);
    setLocalState((current) => {
      return {
        ...current,
        type: EndorsementType.CONTAINER,
        action: ModalEndorsementActionType.ADD,
        index: updatedEndorsements.length - 1, // you don't want to edit the wrong one xd
        modal: true,
      };
    });
  };

  return (
    <EndorsementUIProvider>
      <Fragment>
        <EndorsementModal
          action={localState.action}
          index={localState.index}
          show={localState.modal}
          onHide={() => updateLocalState("modal", false)}
        />

        <FieldArray
          name="endorsements"
          render={(endorsements) => (
            <Accordion defaultActiveKey={["0", "1"]} flush alwaysOpen>
              {values.endorsements?.map((element: any, index: number) => {
                return (
                  <Accordion.Item key={index} eventKey={index.toString()}>
                    <h3 className="mb-4 text-primary" hidden={index !== 0}>
                      Origin Sale Details
                    </h3>

                    <h3
                      className="mb-4 card-title align-items-start flex-column"
                      hidden={index !== 1}
                    >
                      <span className="card-label fw-bold text-primary">
                        Endorsements
                      </span>
                    </h3>

                    <Card key={index} className="mb-8 border p-0">
                      <Accordion.Button className="px-6 py-4 bg-light-primary">
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <h4 className="fw-bold">
                            {index === 0 && `Coverages Summary`}
                            <strong>
                              {index > 0 && `Endorsement E${index}`}
                            </strong>
                            {`- ${
                              endorsementTypes.find(
                                (e: any) => e.id === element.type
                              )?.name || ""
                            }`}
                          </h4>

                          <div
                            className={clsx(
                              "d-flex justify-content-between align-items-center gap-3",
                              {
                                "d-none":
                                  index === 0 &&
                                  objectPath.get(
                                    values,
                                    `endorsements.${0}.type`
                                  ) === "INITIAL_BASE_PREMIUM",
                              }
                            )}
                          >
                            <MissingFieldsAlert
                              show={objectPath.get(
                                errors,
                                `endorsements.${index}`
                              )}
                            />
                            <ButtonModalEndorsementAction
                              className="mr-2"
                              actionName={ModalEndorsementActionType.EDIT}
                              tooltipText="Edit Endorsement"
                              anotherActions={() => {
                                setLocalState((current) => {
                                  return {
                                    ...current,
                                    type: EndorsementType.CONTAINER,
                                    action: ModalEndorsementActionType.EDIT,
                                    index: index, // you don't want to edit the wrong one xd
                                    modal: true,
                                  };
                                });
                              }}
                            />
                            <ItemDeleteModal
                              action={() => endorsements.remove(index)}
                              itemTitle={`Endorsement E${index} - ${
                                endorsementTypes.find(
                                  (e: any) => e.id === element.type
                                )?.name || ""
                              }`}
                            />
                          </div>
                        </div>
                      </Accordion.Button>
                      <Accordion.Body className="px-6">
                        <Row className="mb-12">
                          <Col md={3}>
                            <FakeField
                              label="Type"
                              type="select"
                              value={element?.type}
                              options={
                                index === 0
                                  ? endorsementTypes
                                  : endorsementTypes.filter(
                                      (endorsementType: any) =>
                                        endorsementType.id !==
                                        "INITIAL_BASE_PREMIUM"
                                    )
                              }
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              label="Date"
                              type="date"
                              value={element?.endorsedAt}
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              label="Amount"
                              type="number"
                              value={element?.amount}
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              label="Description"
                              type="text"
                              value={element?.description}
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              label="Status"
                              type="select"
                              value={element?.status}
                              options={endorsementStatus}
                            />
                          </Col>

                          <Col md={3}>
                            <FakeField
                              label="Seller"
                              type="select"
                              value={element?.seller}
                              options={employees}
                            />
                          </Col>
                        </Row>
                        <RepeaterPolicyEndorsementItems
                          endorsementIndex={index}
                        />
                      </Accordion.Body>
                    </Card>
                  </Accordion.Item>
                );
              })}

              <Button
                variant="primary"
                size="sm"
                onClick={() => addNewEndosement()}
                disabled={values?.items?.length === 0}
                className="d-flex gap-1 align-items-center mb-4"
              >
                <i className="fs-3 bi bi-plus-circle-dotted"></i>
                <span>New Endorsement</span>
              </Button>
            </Accordion>
          )}
        />
      </Fragment>
    </EndorsementUIProvider>
  );
};

export default RepeaterPolicyEndorsements;
