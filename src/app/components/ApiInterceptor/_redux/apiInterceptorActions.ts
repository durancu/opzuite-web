import { Dispatch } from "redux";
import { apiInterceptorsSlice, callTypes } from "./apiInterceptorSlice";

const { actions } = apiInterceptorsSlice;

export const addSuccessResponseInterceptor =
  (response: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    try {
      dispatch(actions.addResponse({ response } as any));
    } catch (error) {
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    }
  };

export const addErrorResponseInterceptor =
  (response: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    try {
      dispatch(actions.addError({ response } as any));
    } catch (error) {
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    }
  };

export const addRequestInterceptor = (request: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  try {
    dispatch(actions.addRequest({ request } as any));
  } catch (error) {
    dispatch(actions.catchError({ error, callType: callTypes.action } as any));
  }
};
