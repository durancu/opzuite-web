import { formatAmount } from "../../../../../../../../components/functions";

export const AmountFooterTotalFormatter = (
  column: any,
  colIndex: any,
  { text }: any
) => <>{formatAmount(text)}</>;
