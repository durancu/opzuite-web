import { FormikValues } from "formik";

export const PoliciestatusCssClasses = ["success", "info", ""];
export const defaultSorted = [{ dataField: "code", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
];
const searchColumns = [
  "seller.name",
  "customer.name",
  "customer.business.usDOT",
  "location.business.name",
  "soldAt",
  "code",
];

export const defaultFilter = {
  filter: { type: "POLICY" },
  searchColumns,
  searchText: "",
  monthsToExpiration: 3,
  sortOrder: "desc",
  sortField: "soldAt",
  pageNumber: 1,
  pageSize: 20,
};

export const defaultRenewalFilter = {
  ...defaultFilter,
  sortOrder: "asc",
  sortField: "expiresAt",
};

export const initialValues = {
  broker: "",
  carrier: "",
  lineOfBusiness: "",
  status: "",
  customer: "",
  location: "",
  country: "",
  saleDateFrom: null,
  saleDateTo: null,
  expirationDateFrom: null,
  expirationDateTo: null,
  effectiveDateFrom: null,
  effectiveDateTo: null,
  searchText: "",
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "POLICY",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    monthsToExpiration: 3,
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 20,
  };
};
