import FormReportCreate from "./Forms/FormReportCreate";
import { reportCreateValidationSchema } from "./ReportEditFormValidation";
import { Form, Formik } from "formik";

export const ReportEditForm = ({ report, saveReport }: any) => {
  return (
    <Formik
      initialValues={report ? report : {}}
      validationSchema={reportCreateValidationSchema}
      onSubmit={async (values, helpers) => {
        await saveReport(values, helpers);
      }}
    >
      {() => (
        <Form autoComplete="off">
          <FormReportCreate />
          <button type="submit" className="btn btn-dark">
            Submit
          </button>
        </Form>
      )}
    </Formik>
  );
};
