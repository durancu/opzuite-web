import { useEffect } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { useAppSelector } from "src/app/hooks";
import { toAbsoluteUrl } from "src/_metronic/helpers";

export const AuthLayout = () => {
  const navigate = useNavigate();
  const isAuthorized = useAppSelector((state) => state.auth.user != null);

  useEffect(() => {
    // if logged in, go back
    if (isAuthorized) {
      navigate("/");
    }
    const root = document.getElementById("root");
    if (root) {
      root.style.height = "100%";
    }
    return () => {
      if (root) {
        root.style.height = "auto";
      }
    };
  }, [isAuthorized]);

  return (
    <div className="d-flex flex-column flex-lg-row flex-column-fluid h-100">
      {/* begin::Body */}
      <div className="d-flex flex-column flex-lg-row-fluid w-lg-50 p-10 order-2 order-lg-1">
        {/* begin::Form */}
        <div className="d-flex flex-center flex-column flex-lg-row-fluid">
          {/* begin::Wrapper */}
          <div className="w-lg-500px p-10">
            <Outlet />
          </div>
          {/* end::Wrapper */}
        </div>
        {/* end::Form */}

        {/* begin::Footer */}
        <div className="d-flex flex-center flex-wrap px-5">
          {/* begin::Links */}
          <div className="d-flex fw-semibold text-primary fs-base">
            <a href="#" className="px-5" target="_blank">
              Privacy
            </a>

            <a href="#" className="px-5" target="_blank">
              Terms
            </a>

            <a href="#" className="px-5" target="_blank">
              Contact Us
            </a>
          </div>
          {/* end::Links */}
        </div>
        {/* end::Footer */}
      </div>
      {/* end::Body */}

      {/* begin::Aside */}
      <div
        className="d-flex flex-lg-row-fluid w-lg-50 bgi-size-cover bgi-position-center order-1 order-lg-2"
        style={{
          backgroundImage: `url(${toAbsoluteUrl("/media/bg/back-login.jpg")})`,
        }}
      >
        {/* begin::Content */}
        <div className="d-flex flex-column flex-center py-15 px-5 px-md-15 w-100">
          {/* begin::Logo */}
          <Link to="/" className="mb-12">
            <img
              alt="Logo"
              src={toAbsoluteUrl("/media/logos/logo-light.png")}
              className="h-80px"
            />
          </Link>
          {/* end::Logo */}

          {/* begin::Image */}
          <img
            className="mx-auto w-275px w-md-50 w-xl-600px mb-10 mb-lg-20"
            src={toAbsoluteUrl("/media/misc/auth-screens.png")}
            alt=""
          />
          {/* end::Image */}

          {/* begin::Title */}
          <h1 className="text-white fs-2qx fw-bolder text-center mb-7">
            Welcome to InZuite
          </h1>
          <h2 className="font-size-h4 mb-5 text-white">
            Customer Relationship Manager
          </h2>

          {/* end::Title */}

          {/* begin::Text */}
          <p className="text-white fs-base text-center">
            The ultimate CRM for Small Businesses wanting to step ahead their
            customer relationships.
          </p>

          {/* end::Text */}
        </div>
        {/* end::Content */}
      </div>
      {/* end::Aside */}
    </div>
  );
};
