import { Button } from "react-bootstrap";
import { DeleteDialog } from "../DeleteDialog";
import { RequirePermission } from "../guards";

export const PageDetailTopButtons = (props: any) => {
  const {
    editFunction,
    //renewFunction,
    deleteFunction,
    componentLabel,
    editPermission,
    renewPermission,
    deletePermission,
  } = props;

  const allPermissions = [
    editPermission,
    deletePermission,
    renewPermission,
  ].filter(
    (permission: any) => permission && typeof permission !== "undefined"
  );

  return (
    <RequirePermission permissions={allPermissions}>
      <div className="d-flex gap-3">
        <RequirePermission permissions={editPermission}>
          <Button
            variant="primary"
            onClick={editFunction}
            className="d-flex align-items-center gap-1"
          >
            <span className="indicator-label">Edit</span>
            <i className="fs-4 bi bi-pencil-square"></i>
          </Button>
        </RequirePermission>
        <RequirePermission permissions={deletePermission}>
          <DeleteDialog label={componentLabel} actionDelete={deleteFunction} />
        </RequirePermission>
      </div>
    </RequirePermission>
  );
};
