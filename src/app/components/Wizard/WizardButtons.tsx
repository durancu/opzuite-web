import { Button } from "@mui/material";
import { Save } from "@material-ui/icons";
import { FunctionComponent } from "react";
import { FormattedMessage } from "react-intl";
import { useWizardUIContext } from "./WizardUIContext";

export const WizardButtons: FunctionComponent = () => {
  const {
    handleClickButtonNext,
    handleClickButtonBack,
    activeStep,
    steps,
    handleClickButtonSave,
    isLoading,
  } = useWizardUIContext();
  return (
    <div>
      <Button
        disabled={isLoading}
        style={activeStep === 0 ? { display: "none" } : { minWidth: "100px" }}
        onClick={handleClickButtonBack}
        className="btn btn-secondary"
      >
        <i className="fa fa-chevron-left" style={{ marginRight: "15px" }}></i>
        Back
      </Button>
      <Button
        disabled={isLoading}
        style={
          activeStep === steps.length - 1
            ? { display: "none", minWidth: "100px" }
            : { minWidth: "150px" }
        }
        //disabled={isBlockStep}
        onClick={handleClickButtonNext}
      >
        Next
        <i className="fa fa-chevron-right" style={{ marginLeft: "15px" }}></i>
      </Button>
      {activeStep === steps.length - 1 && (
        <>
          <Button
            variant="contained"
            color="primary"
            disabled={isLoading}
            onClick={handleClickButtonSave}
            style={{ minWidth: "120px" }}
          >
            <span className="mr-3">
              <FormattedMessage id="BUTTON.SAVE_AND_REVIEW" />
            </span>

            <Save />
          </Button>
        </>
      )}
    </div>
  );
};
