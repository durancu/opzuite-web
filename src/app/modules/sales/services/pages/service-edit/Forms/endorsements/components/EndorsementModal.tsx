import { Modal, Button, Row, Col } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useFormikContext } from "formik";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import objectPath from "object-path";
import { FormInput, FormTextArea, FormAutoComplete } from "src/app/components";
import { DatePickerCustom } from "src/app/partials/controls";
import { ModalEndorsementActionType } from "src/app/components/EndorsementsComponents";

type MODAL_ACTION = string | "EDIT" | "ADD" | "";

interface Props {
  index: any;
  action: MODAL_ACTION;
  show: boolean;
  onHide: () => void;
}

export const EndorsementModal = ({ show, onHide, index, action }: Props) => {
  const { values, errors, setFieldValue } = useFormikContext<any>();

  const { permitTypes, permitEndorsementStatus, employees } = useGetCatalog();
  const item = values?.endorsements?.[index] || {};

  const handleClose = () => {
    if (action === "ADD") {
      setFieldValue("endorsements", values.endorsements.slice(0, index));
    }
    onHide();
  };

  function formatName(name: string) {
    return index !== undefined ? `endorsements.${index}.${name}` : name;
  }

  return (
    <Modal size="xl" show={show} centered>
      <Modal.Header className="bg-light-primary">
        <Modal.Title className="w-100">
          {action === ModalEndorsementActionType.ADD ? "Add " : "Edit "}
          Service A{index + 1} - &nbsp;
          {permitTypes.find((option: any) => option.type === item.type)?.name ||
            ""}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row className="mb-4">
          <Col>
            <FormInput
              labelText="Name"
              name={formatName("name")}
              disabled={
                objectPath.get(values, `endorsements.${index}.type`) !==
                "CUSTOM"
              }
            />
          </Col>
          <Col>
            <DatePickerCustom
              name={formatName("endorsedAt")}
              label="Requested on"
            />
          </Col>

          <Col>
            <DatePickerCustom
              name={formatName("followUpDate")}
              label="Completed on"
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col>
            <FormInput
              labelText="Amount"
              name={formatName("amount")}
              type="number"
            />
          </Col>
          <Col>
            <FormAutoComplete
              name={formatName("status")}
              labelText="Status"
              options={permitEndorsementStatus}
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col>
            <FormAutoComplete
              name={formatName("seller")}
              labelText="Seller"
              options={employees}
            />
          </Col>
          <Col>
            <FormAutoComplete
              name={formatName("followUpPerson")}
              labelText="Follow Up Person"
              options={employees}
            />
          </Col>
        </Row>
        <Row className="mb-4">
          <Col>
            <FormTextArea
              labelText="Description"
              name={formatName("description")}
              disabled={
                objectPath.get(values, `endorsements.${index}.type`) !==
                "CUSTOM"
              }
            />
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" onClick={handleClose} className="fw-bolder">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>
        <Button
          variant="primary"
          className="px-6 py-3 d-flex gap-2"
          onClick={onHide}
          disabled={objectPath.get(errors, `endorsements.${index}`)}
        >
          <span className="mr-3">
            <FormattedMessage id="BUTTON.SAVE" />
          </span>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
