import { Fragment } from "react";
import { formatPhoneNumber } from "../../functions";

export const PhoneColumnFormatter = (phone: any) => (
  <Fragment>
    <span className="text-dark fw-semibold fs-6">
      {formatPhoneNumber(phone)}
    </span>
  </Fragment>
);
