import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

export const customersAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllCustomers: build.query<Customer[], QueryParams>({
      query: (queryParams) => ({
        url: "/customers/search",
        method: "post",
        body: { queryParams },
      }),
    }),
    getCustomerByCode: build.query<any, any>({
      query: (code) => `/customers/${code}?layout=FULL`,
    }),
    getCustomerById: build.query<any, any>({
      query: (id) => ({
        url: `/customers/${id}`,
        headers: {
          "id-field": "id",
        },
      }),
    }),
    createCustomer: build.mutation<Customer, any>({
      query: (data) => ({
        url: "/customers",
        method: "post",
        body: data,
      }),
    }),
    updateCustomer: build.mutation<any, any>({
      query: (data) => ({
        url: `/customers/${data.code}`,
        method: "put",
        body: data,
      }),
    }),
    deleteCustomer: build.mutation<any, any>({
      query: (code) => ({
        url: `/customers/${code}`,
        method: "delete",
      }),
    }),
  }),
  overrideExisting: false,
});

export const customersSlice = createSlice({
  name: "customers",
  initialState: {},
  reducers: {},
});
