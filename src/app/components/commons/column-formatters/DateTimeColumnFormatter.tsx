import { FormattedDate, FormattedTime } from "react-intl";

export const DateTimeColumnFormatter = (datetime: any) => (
  <div>
    <span className="text-dark fw-semibold mb-1 fs-6">
      <FormattedDate value={datetime} />
    </span>
    <span className="text-muted fw-semibold d-block fs-7">
      <FormattedTime value={datetime} />
    </span>
  </div>
);
