import { toUpper } from "lodash";
import { ReactElement } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { ReportScope } from "../../functions/ReportScope";

export const ReportScopeColumnFormatter = (status: any): ReactElement => {

  return (
    <OverlayTrigger
      overlay={
        <Tooltip id="column-tooltip">
          <FormattedMessage
            id={`TABLE.HEADER.REPORT_SCOPE.TOOLTIP.${toUpper(status)}`}
          />{" "}
        </Tooltip>
      }
    >

      <span className={`badge badge-light-${ReportScope(status)?.color}`}>
        <FormattedMessage
          id={`TABLE.HEADER.REPORT_SCOPE.LABEL.${toUpper(status)}`}
        />{" "}
      </span>

    </OverlayTrigger>
  );
};
