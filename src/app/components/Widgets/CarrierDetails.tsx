import { Card, Col, Row } from "react-bootstrap";
import { toAbsoluteUrl } from "src/_metronic/helpers";
import { AddressBlock } from "./AddressBlock";

interface Props {
  carrier: any;
}

//TODO: Use Memo
export const languages: any = {
  en: {
    name: "English",
    flag: toAbsoluteUrl("/media/svg/flags/226-united-states.svg"),
  },
  es: {
    lang: "es",
    name: "Spanish",
    flag: toAbsoluteUrl("/media/svg/flags/128-spain.svg"),
  },
};

export const CarrierDetails = ({ carrier }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        Business Details
      </Card.Title>
      <Card.Body>
        <Row className="text-center">
          <Col>
            <h4>
              {carrier && <AddressBlock address={carrier.business?.address} />}
            </h4>
            <span>Address</span>
          </Col>
          <Col>
            {carrier && <h4>{carrier.business.website || "-"}</h4>}
            <span>Website</span>
          </Col>
          <Col>
            {carrier && (
              <h4>
                {carrier.contact.firstName || "-"} &nbsp;
                {carrier.contact.lastName || "-"}
              </h4>
            )}
            <span>Contact Name</span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
