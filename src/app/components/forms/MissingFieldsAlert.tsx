import { Alert } from "react-bootstrap";

interface Props {
  show: boolean;
  description?: string;
}

export const MissingFieldsAlert = ({ show = false, description }: Props) => {
  return (
    <Alert show={show} variant="danger" className="py-2 px-3 m-0">
      <Alert.Heading className="fs-6 fw-medium m-0">
        {description ||
          "You missed a few fields please check and fill them before proceeding"}
      </Alert.Heading>
    </Alert>
  );
};
