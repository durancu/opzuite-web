import { join } from "lodash";
import { Fragment } from "react";

interface Props {
  address: any;
}

export const AddressBlock = ({ address }: Props) => {
  return (
    <Fragment>
      {join(
        [
          address.address1,
          address.address2,
          address.city,
          address.state,
          address.zip,
          address.country,
        ].filter(Boolean),
        ", "
      )}
    </Fragment>
  );
};
