import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import { FormAutoComplete } from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const FilterForm = () => {
  const catalog = useGetCatalog();

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            labelText="Customer Type"
            name="type"
            options={catalog?.customerTypes || []}
          />
        </Col>
      </Row>
    </Fragment>
  );
};
