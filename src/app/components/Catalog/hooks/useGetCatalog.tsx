import { useAppSelector } from "src/app/hooks";
import { catalogAPI } from "../redux/catalogSlice";
import { catalogSelector } from "../redux/catalogSlice";
import { isEmpty } from "lodash";

export function useGetCatalog(skipFetch = true): CatalogInterface {
  const catalog = useAppSelector(catalogSelector);
  // always refetch if empty or fetch is not skipped
  catalogAPI.useGetAllCatalogsQuery(undefined, {
    skip: skipFetch ?? !isEmpty(catalog),
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
    refetchOnReconnect: true,
  });

  return catalog;
}
