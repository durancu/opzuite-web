/* eslint-disable no-script-url */
import { hasLocationManagerRoleAccess } from "../../functions/rolesHierarchyHelpers";
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

interface User {
  id: string;
  role: string;
}

const userCanEdit = (user: User, customer: any): boolean =>
  hasLocationManagerRoleAccess(user.role) || user.id === customer.createdBy;

export const CustomerActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openCertificates,
    openEditCustomerPage,
    openDeleteCustomerDialog,
    openDetailCustomerPage,
    user,
  }: any
) => (
  <TableActionsMenu
    openCertificates={openCertificates}
    readFunction={openDetailCustomerPage}
    editFunction={openEditCustomerPage}
    deleteFunction={openDeleteCustomerDialog}
    editPermission={userCanEdit(user, row) && Permissions.CUSTOMERS_UPDATE}
    readPermission={Permissions.CUSTOMERS_READ}
    deletePermission={Permissions.CUSTOMERS_DELETE}
    certificatePermission={Permissions.POLICIES_CERTIFICATES}
    code={row.code}
    type={row.type}
    customerId={row.id}
  />
);
