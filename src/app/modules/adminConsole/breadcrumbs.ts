import { PageLink } from "src/_metronic/layout/core";
export const breadcrumbs: Array<PageLink> = [
  {
    title: "Admin Console",
    path: "manager",
    isSeparator: false,
    isActive: true,
  },
];
