/* eslint-disable no-use-before-define */
import { FormHelperText, TextField } from "@mui/material";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useFormikContext } from "formik";
import objectPath from "object-path";
import { useEffect, useState } from "react";

interface AutocompleteOption {
  id: string;
  name: string;
  description?: any;
}

interface Props {
  name: string;
  label: string;
  labelField: string;
  options: AutocompleteOption[];
  variant?: string;
  disabled?: boolean;
  required?: boolean;
  helperText?: string;
}

export function EnhancedAutocomplete(props: Props) {
  const [localValue, setLocalValue] = useState<any>({
    id: "",
    [props.labelField]: "",
    description: "",
  });
  const [error, setError] = useState<string>();

  const { handleChange, values, errors } = useFormikContext<any>();

  useEffect(() => {
    const item = objectPath.get(values, props.name);
    const valueOption = props.options?.find(
      (element: any) => element.id === item
    );
    setLocalValue(valueOption);
  }, [props.name, props.options, values]);

  const handleChangeComponent = (value: any) => {
    const newE = { target: { value: value?.id || "", name: props.name || "" } };
    handleChange(newE);
  };

  useEffect(() => {
    setError(objectPath.get(errors, props.name));
  }, [errors, props.name]);

  return (
    <>
      <Autocomplete
        disabled={props.disabled}
        options={props.options || ([] as AutocompleteOption[])}
        getOptionSelected={(item, current) => (item === current ? true : true)}
        getOptionLabel={(option: any) => {
          return option[props.labelField] || "N/A";
        }}
        onChange={(_, value) => {
          setLocalValue(value);
          handleChangeComponent(value);
        }}
        value={localValue || { id: "", [props.labelField]: "" }}
        renderOption={(option) => (
          <div>
            {option.name}
            {
              <FormHelperText hidden={!option.description}>
                {Array.isArray(option.description)
                  ? Array.from(option.description).join(" | ")
                  : option.description}
              </FormHelperText>
            }
          </div>
        )}
        renderInput={(params) => (
          <TextField
            {...params}
            label={props.label}
            required={props.required}
            error={!!error}
            helperText={error}
          />
        )}
      />
    </>
  );
}
