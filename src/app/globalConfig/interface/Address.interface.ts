export interface Address {
  readonly address1: string;
  readonly address2: string;
  readonly state: string;
  readonly city: string;
  readonly country: string;
  readonly zip: string;
}
