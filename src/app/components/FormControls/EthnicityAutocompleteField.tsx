import { useIntl } from "react-intl";
import { FormAutoComplete } from "../forms";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function EthnicityAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.ETHNICITY",
  labelText,
  fieldName = "gender",
}: Props) {
  const intl = useIntl();
  const ethnicity: any = intl.messages["ETHNICITY_CATALOG"];

  return (
    <FormAutoComplete
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={ethnicity || []}
    />
  );
}
