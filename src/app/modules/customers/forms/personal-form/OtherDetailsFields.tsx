import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import * as CATALOG from "src/app/components/Catalog/CatalogSelects";
import {
  DatePickerField,
  EthnicityAutocompleteField,
  GendersAutocompleteField,
  StatesAutocompleteField,
} from "src/app/components/FormControls";

import { FormInput, FormSelect, SSNInput } from "src/app/components";

export const OtherDetailsFields = () => {
  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <DatePickerField
            fieldName="contact.dob"
            labelTextId="FORM.LABELS.DOB"
          />
        </Col>
        <Col>
          <FormInput
            name="contact.driverLicense"
            labelTextId="FORM.LABELS.DRIVER_LICENSE"
          />
        </Col>
        <Col>
          <StatesAutocompleteField
            fieldName="contact.driverLicenseState"
            labelTextId="FORM.LABELS.DRIVER_LICENSE.STATE"
          />
        </Col>
        <Col>
          <SSNInput name="contact.ssn" labelTextId="FORM.LABELS.SSN" />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <GendersAutocompleteField fieldName="contact.gender" />
        </Col>
        <Col>
          <EthnicityAutocompleteField fieldName="contact.ethnicity" />
        </Col>
        <Col>
          <FormSelect
            name="contact.maritalStatus"
            labelTextId="FORM.LABELS.MARITAL_STATUS"
            options={CATALOG.MARITAL_STATUS_CATALOG}
          />
        </Col>
        <Col>
          <FormSelect
            name="contact.educationLevel"
            labelTextId="FORM.LABELS.EDUCATION_LEVEL"
            options={CATALOG.EDUCATION_LEVEL_CATALOG}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormSelect
            name="contact.immigrantStatus"
            labelTextId="FORM.LABELS.IMMIGRANT_STATUS"
            options={CATALOG.IMMIGRANT_STATUS_CATALOG}
          />
        </Col>
        <Col>
          <FormSelect
            name="contact.identityCardType"
            labelTextId="FORM.LABELS.IDENTITY_CARD_TYPE"
            options={CATALOG.IDENTITY_CARD_TYPE_CATALOG}
          />
        </Col>
        <Col>
          <FormInput
            name="contact.identityCardId"
            labelTextId="FORM.LABELS.IDENTITY_CARD.ID"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormSelect
            name="contact.employmentStatus"
            labelTextId="FORM.LABELS.EMPLOYEMENT_STATUS"
            options={CATALOG.EMPLOYMENT_STATUS_CATALOG}
          />
        </Col>
        <Col>
          <FormInput
            name="contact.employer"
            labelTextId="FORM.LABELS.EMPLOYEE"
          />
        </Col>
        <Col>
          <FormInput
            name="contact.yearlyGrossIncome"
            labelTextId="FORM.LABELS.YEARLY_GROSS_INCOME"
            type="number"
          />
        </Col>
      </Row>
    </Fragment>
  );
};
