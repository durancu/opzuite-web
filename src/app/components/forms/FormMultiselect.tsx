import { ChangeEvent, Fragment, useState } from "react";

import {
  Autocomplete,
  AutocompleteProps,
  TextField,
  createFilterOptions,
} from "@mui/material";
import clsx from "clsx";
import { startCase } from "lodash";
import { Form } from "react-bootstrap";
import { useIntl } from "react-intl";
import { SELECT_ALL } from "src/app/constants/General";

type Props = Omit<BaseInputProps, "name" | "onChange"> &
  Partial<AutocompleteProps<any, any, any, any>> & {
    labelField?: string;
    selectionKey?: string;
    options: ListItem[] | any;
    onChange: (evt: any, value: any, reason?: any) => void;
    error?: boolean;
    errorText?: string;
  };

const filter = createFilterOptions();
const filterOptions = (options: any, params: any): any => {
  const filtered = filter(options, params);
  return options.length > 1 ? [SELECT_ALL, ...filtered] : [...filtered];
};

export const FormMultiselect = ({
  value,
  onChange,
  labelText,
  labelTextId,
  labelField = "name",
  selectionKey = "type",
  helperText,
  helperTextId,
  hidden,
  options = [],
  disabled,
  error,
  errorText,
  ...rest
}: Props) => {
  const intl = useIntl();
  const [all, selectAll] = useState(false);

  function handleChange(_: any, value: any) {
    if (value.indexOf(SELECT_ALL) === -1) {
      onChange(_, value);
      selectAll(false);
    } else {
      onChange(_, options);
    }
  }

  function handleSelectAll(evt: ChangeEvent<HTMLInputElement>) {
    selectAll(evt.target.checked);
    if (!evt.target.checked) {
      onChange(null, []);
    } else {
      onChange(null, options);
    }
  }

  return (
    <Autocomplete
      {...rest}
      multiple
      disablePortal
      disableCloseOnSelect
      value={value}
      disabled={disabled}
      hidden={hidden}
      options={options}
      filterOptions={filterOptions}
      onChange={handleChange}
      getOptionLabel={(option: any) => startCase(option[labelField]) || option}
      ListboxProps={{
        className:
          "list-group list-group-flush position-absolute mt-1 z-index-2 w-100 mh-400px overflow-auto rounded border",
      }}
      isOptionEqualToValue={(option, value) => {
        if (option[selectionKey]) {
          return option[selectionKey] === value[selectionKey];
        }
        return option === value;
      }}
      renderOption={(props, option, { index, selected }) => (
        <li
          {...props}
          key={index}
          className={clsx(
            "list-group-item p-4 cursor-pointer fw-normal d-flex gap-4"
          )}
        >
          {option.name === SELECT_ALL.name ? (
            <Fragment>
              <Form.Check checked={all} readOnly onChange={handleSelectAll} />
              <p className="mb-1 fw-bold">{option.name}</p>
            </Fragment>
          ) : (
            <Fragment>
              <Form.Check checked={selected} readOnly />
              <div>
                <p className="mb-1 fw-bold">{option[labelField] || option}</p>
                <span className="fs-6" hidden={!option.description}>
                  {Array.isArray(option.description)
                    ? Array.from(option.description).join(" | ")
                    : option.description}
                </span>
              </div>
            </Fragment>
          )}
        </li>
      )}
      renderInput={({ inputProps, InputProps, InputLabelProps }) => (
        <Form.Group>
          <Form.Label {...InputLabelProps}>
            {labelTextId ? intl.formatMessage({ id: labelTextId }) : labelText}
          </Form.Label>
          <TextField
            fullWidth
            variant="outlined"
            error={error}
            inputProps={{
              ...inputProps,
              className: "form-control",
            }}
            InputProps={{
              ...InputProps,
              className: "form-control border-0 p-0 mh-45px px-4 rounded",
            }}
          />
          <Form.Text className="text-danger my-1 d-block">
            {error && errorText}
          </Form.Text>
          <Form.Text className="mt-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    />
  );
};
