import { Fragment } from "react";
import { FieldArray, useFormikContext } from "formik";
import { Button, Card, Col, Accordion } from "react-bootstrap";
import { ItemDeleteModal } from "src/app/components/DeleteDialog";
import { cohort } from "./helpers";
import { CohortFields } from "./CohortFields";
import { MissingFieldsAlert } from "src/app/components";
import objectPath from "object-path";

export const CohortsRepeater = () => {
  const { values, errors, setFieldValue } = useFormikContext<any>();

  function addCohort() {
    setFieldValue("cohorts", [cohort, ...values.cohorts]);
  }

  return (
    <Fragment>
      <section>
        <header className="mb-6 d-flex justify-content-between align-items-center">
          <h2 className="fw-bold text-primary">Cohorts</h2>
          <Button
            variant="primary"
            onClick={() => addCohort()}
            className="d-flex gap-1 align-items-center"
          >
            <i className="fs-3 bi bi-plus-circle-dotted"></i>
            <span className="">Add Cohorts</span>
          </Button>
        </header>

        {values && !values.cohorts.length && (
          <Col className="h-300px rounded d-flex flex-column justify-content-center align-items-center bg-light-primary">
            <p className="fw-bold fs-3 mb-5">No available cohorts</p>
            <Button
              variant="primary"
              onClick={() => addCohort()}
              className="d-flex gap-1 align-items-center"
            >
              <i className="fs-3 bi bi-plus-circle-dotted"></i>
              <span className="">Add Cohorts</span>
            </Button>
          </Col>
        )}
      </section>

      <FieldArray
        name="cohorts"
        render={(helpers) => (
          <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
            {values.cohorts.length > 0 &&
              values.cohorts.map((cohort: any, index: number) => {
                return (
                  <Accordion.Item
                    key={index}
                    eventKey={index.toString()}
                    className="mb-6 border"
                  >
                    <Card>
                      <Accordion.Button className="px-6 py-4 bg-light-primary">
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <h4 className="fw-bold">
                            {`Cohort ${index + 1}`}
                            {values.cohorts[index].firstName &&
                              `: ${values.cohorts[index].firstName} ${values.cohorts[index].lastName}`}
                          </h4>

                          <div className="d-flex justify-content-between align-items-center gap-3">
                            <MissingFieldsAlert
                              show={objectPath.get(errors, `cohorts.${index}`)}
                            />
                            <ItemDeleteModal
                              title="Delete Cohort"
                              itemName={`Cohort ${cohort.firstName}`}
                              toolTipLabel="Delete"
                              deleteAction={() => helpers.remove(index)}
                            />
                          </div>
                        </div>
                      </Accordion.Button>
                      <Accordion.Body>
                        <CohortFields index={index} />
                      </Accordion.Body>
                    </Card>
                  </Accordion.Item>
                );
              })}
          </Accordion>
        )}
      />
    </Fragment>
  );
};
