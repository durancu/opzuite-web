import moment from "moment";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export const IsRenewedColumnFormatter = (cellContent: any, row: any) => {
  const recentlyRenewed =
    row.isRenewal && moment().diff(moment(row.soldAt), "days") < 15;
  const needsRenew = moment(row.expiresAt).diff(moment(), "days") < 30;

  let className = "";
  let tooltipMessage = "";

  if (recentlyRenewed) {
    className = "fas fa-redo-alt text-success";
    tooltipMessage = "Just Renewed";
  } else if (needsRenew) {
    className = "fas fa-redo-alt text-danger";
    tooltipMessage = "Needs To Renew";
  }

  return recentlyRenewed || needsRenew ? (
    <OverlayTrigger
      overlay={<Tooltip id="column-tooltip">{tooltipMessage}</Tooltip>}
    >
      <div style={{ width: "100%" }}>
        <i
          style={{ paddingLeft: "20px", fontSize: "13px" }}
          className={className}
        ></i>
      </div>
    </OverlayTrigger>
  ) : (
    <></>
  );
};
