interface Props {
  communication: any;
}

export const CommunicationBlock = (props: Props) => {
  const { communication } = props;

  const favoriteCommunicationChannels = (): any => {
    const channels = [];

    if (communication.email) channels.push("Email");
    if (communication.sms) channels.push("SMS");
    if (communication.phone) channels.push("Phone calls");
    return channels.join(", ");
  };

  return <span>{favoriteCommunicationChannels()}</span>;
};
