export const MARITAL_STATUS_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "SINGLE", name: "Single" },
  { id: "MARRIED", name: "Married" },
  { id: "DIVORCEE", name: "Divorcee" },
  { id: "WIDOW", name: "Widow" },
  { id: "OTHER", name: "Other" },
];
