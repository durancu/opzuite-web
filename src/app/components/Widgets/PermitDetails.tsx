import { Card, CardContent } from "@mui/material";
import { Col, Row } from "react-bootstrap";
import { formatDate } from "../functions";

interface Props {
  sale: any;
}

export const PermitDetails = (props: Props) => {
  const { sale } = props;

  return (
    <Card className="mb-4 p-4 detail-section">
      <CardContent>
        <Row className="mb-5">
          <Col>
            <h4 className="card-title align-items-start flex-column">
              <span className="card-label font-weight-bold">Details</span>
            </h4>
          </Col>
        </Row>
        <Row style={{ width: "100%" }}>
          <Col>
            <Row style={{ width: "100%" }} className="mb-3 text-center">
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && sale.seller.name}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>CSR/Manager</span>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && (sale.location || "-")}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>Location</span>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && formatDate(sale.effectiveAt)}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>Effective Since</span>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && formatDate(sale.expiresAt)}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>Expiration Date</span>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && sale.renewalFrequency}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>Renewal Frequency</span>
                  </Col>
                </Row>
              </Col>
              <Col>
                <Row className="mb-3">
                  <Col className="field-value">
                    <span>{sale && sale.autoRenew ? "Yes" : "No"}</span>
                  </Col>
                </Row>
                <Row className="mb-0">
                  <Col className="field-label">
                    <span>Auto-renew</span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </CardContent>
    </Card>
  );
};
