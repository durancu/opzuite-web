// routes module, to be imported i src/Routes
import { Navigate } from "react-router-dom";
import { RouteObject } from "react-router-dom";
import { Registration } from "./pages/Registration";
import { ForgotPassword } from "./pages/ForgotPassword";
import { Login } from "./pages/Login";
import ResetPassword from "./pages/ResetPassword";
import ConfirmEmailAddress from "./pages/ConfirmEmailAddress";

export const AuthRoutes: RouteObject[] = [
  { index: true, element: <Navigate to="login" /> },
  { path: "login", element: <Login /> },
  { path: "registration", element: <Registration /> },
  { path: "forgot-password", element: <ForgotPassword /> },
  { path: "reset-password/:passwordToken", element: <ResetPassword /> },
  { path: "confirm-email/:confirmToken", element: <ConfirmEmailAddress /> },
];
