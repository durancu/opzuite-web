FROM node:16.17.0-alpine

# set the working direction
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./

COPY package-lock.json ./
#COPY package-lock.json ./

RUN npm install npm -g

RUN npm install --force

RUN chown -R node /app/node_modules/

# add app
COPY . ./

# start app
CMD ["npm", "start"]