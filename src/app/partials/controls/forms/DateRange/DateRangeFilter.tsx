import moment from "moment";
import { ChangeEvent, useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { DatePicker } from "src/app/components";
import { DateRangeEnum, dateRangeByName } from "./dateFactory";

export interface DateRangeValues {
  dateFrom: any;
  dateTo: any;
}

interface Props {
  values: DateRangeValues;
  onChange: (values: DateRangeValues) => void;
}

const defaultDateRange = dateRangeByName(DateRangeEnum.THIS_FISCAL_MONTH);

export const DateRangeFilter = ({
  values = defaultDateRange,
  onChange,
}: Props) => {
  const intl = useIntl();

  const [dateRangeName, setDateRangeName] = useState(
    DateRangeEnum.THIS_FISCAL_MONTH
  );

  const handleDateRangeDropdownChange = ({
    target,
  }: ChangeEvent<HTMLSelectElement>) => {
    setDateRangeName(target.value);
    onChange(dateRangeByName(target.value));
  };

  //Load data of form
  const handleChangeDate = (name: string, value: Date) => {
    setDateRangeName("CUSTOM");
    onChange({
      ...values,
      [name]: moment(value).toDate(),
    });
  };

  return (
    <Row>
      <Col sm="4">
        <Form.Group>
          <Form.Label>
            {intl.formatMessage({ id: "FORM.LABELS.DATE_PERIOD" })}
          </Form.Label>
          <Form.Select
            name="dateRangeName"
            onChange={handleDateRangeDropdownChange}
            value={dateRangeName}
            className="my-1 mr-2"
          >
            {/* <option value="">
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.ALL" />
            </option> */}
            <option value={DateRangeEnum.CUSTOM}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.CUSTOM" />
            </option>
            <option value={DateRangeEnum.THIS_FISCAL_MONTH}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.THIS_FISCAL_MONTH" />
            </option>
            <option value={DateRangeEnum.LAST_FISCAL_MONTH}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.LAST_FISCAL_MONTH" />
            </option>
            <option value={DateRangeEnum.TODAY}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.TODAY" />
            </option>
            <option value={DateRangeEnum.YESTERDAY}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.YESTERDAY" />
            </option>
            <option value={DateRangeEnum.WEEK_TO_DATE}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.WEEK_TO_DATE" />
            </option>
            <option value={DateRangeEnum.LAST_WEEK}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.LAST_WEEK" />
            </option>
            <option value={DateRangeEnum.MONTH_TO_DATE}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.MONTH_TO_DATE" />
            </option>
            <option value={DateRangeEnum.LAST_MONTH}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.LAST_MONTH" />
            </option>
            <option value={DateRangeEnum.YEAR_TO_DATE}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.YEAR_TO_DATE" />
            </option>
            <option value={DateRangeEnum.LAST_YEAR}>
              <FormattedMessage id="FORM.SELECT.DATE_RANGE.LAST_YEAR" />
            </option>
          </Form.Select>
        </Form.Group>
      </Col>
      <Col sm="4" hidden={dateRangeName === ""}>
        <DatePicker
          name="dateFrom"
          label={`${intl.formatMessage({ id: "FORM.LABELS.DATE_FROM" })}`}
          value={values.dateFrom}
          onChange={(value) => handleChangeDate("dateFrom", value)}
          inputFormat="MM/dd/yyyy"
        />
      </Col>
      <Col sm="4" hidden={!dateRangeName}>
        <DatePicker
          name="dateTo"
          label={`${intl.formatMessage({ id: "FORM.LABELS.DATE_TO" })}`}
          value={values.dateTo}
          onChange={(value) => handleChangeDate("dateTo", value)}
          inputFormat="MM/dd/yyyy"
        />
      </Col>
    </Row>
  );
};
