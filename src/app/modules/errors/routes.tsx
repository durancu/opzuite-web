// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { Error500 } from "./pages/Error500";
import { Error404 } from "./pages/Error404";
import { Error403 } from "./pages/Error403";
import { UnauthorizedIP } from "./pages/UnauthorizedIP";

export const ErrorRoutes: RouteObject[] = [
  { index: true, element: <Error404 /> },
  { path: "403", element: <Error403 /> },
  { path: "404", element: <Error404 /> },
  { path: "500", element: <Error500 /> },
  { path: "ip", element: <UnauthorizedIP /> },
];
