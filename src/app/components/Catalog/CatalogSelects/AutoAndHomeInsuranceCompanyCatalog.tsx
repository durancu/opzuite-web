export const AUTO_AND_HOME_INSURANCE_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "STATE-FARM-GROUP", name: "State Farm Group" },
  { id: "BRK.A", name: "Berkshire Hathaway (BRK.A)" },
  { id: "PGR", name: "Progressive Insurance Group (PGR)" },
  { id: "ALL", name: "Allstate Insurance Group (ALL)" },
  { id: "LIBERTY-MUTUAL", name: "Liberty Mutual" },
  { id: "TRV", name: "Travelers Group (TRV)" },
  { id: "USAA-GROUP", name: "USAA Group" },
  { id: "CB", name: "Chubb (CB)" },
  { id: "FARMERS-INSURANCE-GROUP", name: "Farmers Insurance Group" },
  { id: "NATIONWIDE", name: "Nationwide" },
  { id: "OTHER", name: "Other" },
];
