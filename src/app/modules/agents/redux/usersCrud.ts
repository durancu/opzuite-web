import axios from "axios";

export const USERS_URL = "api/v2/users";
export const MY_COMPANY_URL = "api/v2/companies/my-company";

// CREATE =>  POST: add a new user to the server
export function createUser(user: any) {
  return axios.post(USERS_URL, user);
}

// READ
export function getAllAgents() {
  return axios.get(USERS_URL);
}

export function getUserById(userId: any) {
  return axios.get(`${USERS_URL}/${userId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findAgents(queryParams: any) {
  return axios.post(`${USERS_URL}/search`, { queryParams });
}

// UPDATE => PUT: update the procuct on the server
export function updateUser(user: any) {
  return axios.put(`${USERS_URL}/${user.id}`, user);
}

// UPDATE Status
export function updateStatusForAgents(ids: any, status: any) {
  return axios.post(`${USERS_URL}/updateStatusForAgents`, {
    ids,
    status,
  });
}

// DELETE => delete the user from the server
export function deleteUser(userId: any) {
  return axios.delete(`${USERS_URL}/${userId}`);
}

// DELETE Agents by ids
export function deleteAgents(ids: any) {
  return axios.post(`${USERS_URL}/deleteAgents`, { ids });
}
