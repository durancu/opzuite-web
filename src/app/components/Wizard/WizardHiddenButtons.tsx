import { useFormikContext } from "formik";
import { useWizardUIContext } from "./WizardUIContext";
interface Props {
  validateBack?: any;
  validateNext?: any;
}

export const WizardHiddenButtons = ({
  validateNext = (): boolean => true,
  validateBack = (): boolean => true,
}: Props) => {
  const { validateForm, setTouched } = useFormikContext<any>();
  const { btnBackRef, btnNextRef, btnSaveRef, handleNext, handleBack } =
    useWizardUIContext();
  return (
    <>
      <button
        type="button"
        style={{ display: "none" }}
        onClick={() => {
          validateForm().then((validation: any) => {
            setTouched(validation);
            validateBack() && handleBack();
          });
        }}
        ref={btnBackRef}
      ></button>
      <button
        type="button"
        style={{ display: "none" }}
        onClick={() => {
          validateForm().then((validation: any) => {
            setTouched(validation);
            validateNext() && handleNext();
          });
        }}
        ref={btnNextRef}
      ></button>
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnSaveRef}
      ></button>
    </>
  );
};
