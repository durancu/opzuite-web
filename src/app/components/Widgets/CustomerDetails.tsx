import { Card, Col, OverlayTrigger, Row, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { toAbsoluteUrl } from "src/_metronic/helpers";

interface Props {
  customer: Customer;
}

//TODO: Use Memo
export const languages: any = {
  en: {
    name: "English",
    flag: toAbsoluteUrl("/media/svg/flags/226-united-states.svg"),
  },
  es: {
    lang: "es",
    name: "Spanish",
    flag: toAbsoluteUrl("/media/svg/flags/128-spain.svg"),
  },
};

export const CustomerDetails = (props: Props) => {
  const { customer } = props;

  const favoriteCommunicationChannels = (): any => {
    const channels = [];
    if (customer) {
      if (customer.communication.email) channels.push("Email");
      if (customer.communication.sms) channels.push("SMS");
      if (customer.communication.phone) channels.push("Phone calls");
      return channels.join(", ");
    }
  };

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        Preferences
      </Card.Title>
      <Card.Body>
        <Row className="text-center">
          <Col>
            {customer && (
              <OverlayTrigger
                placement="bottom"
                overlay={
                  <Tooltip id="language-panel-tooltip">
                    {languages[customer.contact.language]?.name}
                  </Tooltip>
                }
              >
                <img
                  className="h-25px w-25px rounded mb-4 d-block mx-auto"
                  src={languages[customer.contact.language]?.flag}
                  alt={languages[customer.contact.language]?.name}
                />
              </OverlayTrigger>
            )}
            <FormattedMessage id="FORM.LABELS.LANGUAGE" />
          </Col>
          <Col>
            <h4>{customer && favoriteCommunicationChannels()}</h4>
            <FormattedMessage id="FORM.LABELS.COMMUNICATION" />
          </Col>
          <Col>
            <h4>{customer && (customer.contact.timezone || "-")}</h4>

            <FormattedMessage id="FORM.LABELS.TIMEZONE" />
          </Col>
          <Col>
            <h4>{customer && (customer.contact.address.state || "-")}</h4>
            <FormattedMessage id="FORM.LABELS.STATE" />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
