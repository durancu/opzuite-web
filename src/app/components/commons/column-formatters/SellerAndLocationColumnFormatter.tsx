import { Link } from "react-router-dom";

export function SellerAndLocationColumnFormatter(sellerName: any, policy: any) {
  return (
    <>
      <Link
        hidden={!policy?.sellerName}
        className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
        to={`/agents/${policy?.seller}/detail`}
      >
        {policy?.sellerName}
      </Link>


      <Link className="text-muted fw-semibold text-muted text-hover-primary d-block fs-7"
        to={`/manager/locations/${policy?.locationCode}/detail`}
      >
        {policy?.locationName}
      </Link>
    </>
  );
}
