import {
  Autocomplete,
  AutocompleteProps,
  TextField,
  createFilterOptions,
} from "@mui/material";
import { useField } from "formik";
import { Form } from "react-bootstrap";
import { useIntl } from "react-intl";
import clsx from "clsx";

interface Props
  extends BaseInputProps,
    Omit<AutocompleteProps<any, any, any, any>, "renderInput"> {
  labelField?: string;
  options: ListItem[] | any;
  onChange?: (value: any) => void;
}

const filter = createFilterOptions();
const filterOptions = (options: any, params: any): any => {
  return filter(options, params);
};

export const FormAutoComplete = ({
  name,
  value,
  labelText,
  labelTextId,
  labelField = "name",
  helperText,
  helperTextId,
  hidden,
  options = [],
  disabled,
  required,
  className,
  onChange,
  ...rest
}: Props) => {
  const intl = useIntl();

  const [field, meta, helpers] = useField(name);

  function handleChange(_: any, value: any, reason: string) {
    if (onChange) {
      onChange(value);
    }

    if (reason === "clear") {
      helpers.setValue("");
    } else if (value.id) {
      helpers.setValue(value.id);
    } else if (value.code) {
      // used in certificates holder
      helpers.setValue(value.code);
    } else {
      helpers.setValue(value);
    }
  }

  function getOptionById(id: string) {
    const option = options?.find((item: any) => item.id === id);
    return option;
  }

  return (
    <Autocomplete
      {...rest}
      disablePortal
      value={value ? getOptionById(value) : getOptionById(field.value)}
      disabled={disabled}
      hidden={hidden}
      options={options}
      filterOptions={filterOptions}
      getOptionLabel={(option: any) => option[labelField] || option.toString()}
      onChange={handleChange}
      isOptionEqualToValue={(option, value) => option === value || true}
      ListboxProps={{
        className:
          "list-group list-group-flush position-absolute z-index-2 w-100 mh-400px overflow-auto rounded border",
      }}
      renderOption={(props, option, state) => (
        <li
          {...props}
          key={state.index}
          className={clsx(
            "list-group-item list-group-item-action p-3 cursor-pointer fw-normal",
            {
              "bg-light-dark": state.selected,
            }
          )}
        >
          <span className="mb-1 fs-5 d-block text-gray-600">
            {option.name || option.toString()}
          </span>
          <span
            className="fs-7 d-block text-gray-600"
            hidden={!option.description}
          >
            {Array.isArray(option.description)
              ? Array.from(option.description).join(" | ")
              : option.description}
          </span>
        </li>
      )}
      renderInput={({ inputProps, InputProps, InputLabelProps }) => (
        <Form.Group>
          <Form.Label {...InputLabelProps}>
            {labelTextId ? intl.formatMessage({ id: labelTextId }) : labelText}
          </Form.Label>
          <TextField
            fullWidth
            variant="outlined"
            error={meta.error ? true : false}
            inputProps={{
              ...inputProps,
              className: clsx("form-control fs-4 px-4", className),
            }}
            InputProps={{
              ...InputProps,
              className: "form-control border-0 p-0 mh-45px rounded",
            }}
            required={required}
          />
          <Form.Text className="text-danger my-1 d-block">
            {meta.error}
          </Form.Text>
          <Form.Text className="mt-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    />
  );
};
