import { shallowEqual, useSelector } from "react-redux";
import { FormAutoComplete } from "../forms";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
  disabled?: boolean;
}

export function CountriesAutocompleteField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.COUNTRY",
  labelText,
  fieldName = "country",
  disabled = false,
}: Props) {
  const { catalogs } = useSelector(
    (reduxState: any) => ({ catalogs: reduxState.catalogs }),
    shallowEqual
  );

  const { entities }: any = catalogs;

  return (
    <FormAutoComplete
      disabled={disabled}
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={entities?.countries || []}
    />
  );
}
