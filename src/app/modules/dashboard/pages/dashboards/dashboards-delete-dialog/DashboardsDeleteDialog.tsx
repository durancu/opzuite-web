import { useEffect, useMemo } from "react";
import { Modal, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useAppDispatch } from "src/app/hooks";

import * as actions from "../../../_redux/dashboards/dashboardsActions";
import { useDashboardsUIContext } from "../DashboardsUIContext";

export function DashboardsDeleteDialog() {
  const navigate = useNavigate();
  // Dashboards UI Context
  const dashboardsUIContext: any = useDashboardsUIContext();
  const dashboardsUIProps = useMemo(() => {
    return {
      ids: dashboardsUIContext.ids,
      setIds: dashboardsUIContext.setIds,
      queryParams: dashboardsUIContext.queryParams,
    };
  }, [dashboardsUIContext]);

  // Dashboards Redux state
  const dispatch = useAppDispatch();
  const { isLoading } = useSelector(
    (state: any) => ({ isLoading: state.dashboards.actionsLoading }),
    shallowEqual
  );

  function goBack() {
    navigate(-1);
  }

  // if there weren't selected dashboards we should close modal
  useEffect(() => {
    if (!dashboardsUIProps.ids || dashboardsUIProps.ids.length === 0) {
      goBack();
    }
  }, [dashboardsUIProps.ids]);

  const deleteDashboards = () => {
    // server request for deleting dashboard by seleted ids
    dispatch(actions.deleteDashboards(dashboardsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchDashboards(dashboardsUIProps.queryParams)).then(
        () => {
          // clear selections list
          dashboardsUIProps.setIds([]);
          // closing delete modal
          goBack();
        }
      );
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="example-modal-sizes-title-lg"
      centered
      backdrop="static"
    >
      {isLoading && <ProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Dashboards Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected dashboards?</span>
        )}
        {isLoading && <span>Dashboards are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={goBack}
            className="btn btn-light btn-elevate"
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteDashboards}
            className="btn btn-primary btn-elevate"
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
