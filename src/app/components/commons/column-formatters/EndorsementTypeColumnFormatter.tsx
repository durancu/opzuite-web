export const EndorsementTypeColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { entities }: any
) => {
  return (
    entities?.endorsementTypes.find(({ id }: any) => id === cellContent)
      ?.name || "-"
  );
};
