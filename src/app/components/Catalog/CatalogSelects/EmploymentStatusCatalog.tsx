export const EMPLOYMENT_STATUS_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "PERMANENT-FULL-TIME", name: "Permanent Full-Time" },
  { id: "PERMANENT-PART-TIME", name: "Permanent Part-Time" },
  { id: "SELF-EMPLOYED", name: "Self-Employed" },
  { id: "CONTRACTOR", name: "Contractor" },
  { id: "LIMITED-TERM", name: "Limited Term Employee" },
  { id: "SEASONAL", name: "Seasonal" },
  { id: "VOLUNTEER", name: "Volunteer" },
  { id: "RETIREE", name: "Retiree" },
  { id: "OTHER", name: "Other" },
];
