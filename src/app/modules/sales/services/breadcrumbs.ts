import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Services",
    path: "services",
    isSeparator: false,
    isActive: true,
  },
];
