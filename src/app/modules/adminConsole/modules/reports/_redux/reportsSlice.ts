import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialReportsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  reportForEdit: undefined,
  reportResponse: undefined,
  lastError: null,
  reportChartData: undefined,
  appliedParams: undefined,
};
export const callTypes = {
  list: "list",
  action: "action",
};

export const reportAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllReports: build.query<any, QueryParams>({
      query: (queryParams) => ({
        url: "/reports/search",
        method: "post",
        body: { queryParams },
      }),
    }),
    getReportResults: build.query<any, any>({
      query: ({ queryParams, code }) => ({
        url: `/reports/${code}/results`,
        method: "post",
        body: { queryParams },
      }),
    }),
  }),
  overrideExisting: false,
});

export const reportsSlice = createSlice({
  name: "reports",
  initialState: initialReportsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getReportById
    reportFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.reportForEdit = action.payload.reportForEdit;
      state.error = null;
    },
    reportCharDataFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.reportChartData = action.payload.reportChartData;
      state.error = null;
    },
    // getReportById
    reportResult: (state: any, action: any) => {
      state.actionsLoading = false;
      state.reportForEdit = action.payload.report;
      state.error = null;
    },
    // findReports
    reportsFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
      state.actionsLoading = false;
    },
    // createReport
    reportCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },

    // updateReport
    reportUpdated: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // deleteReport
    reportDeleted: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // deleteReports
    reportsDeleted: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // reportsUpdateState
    reportsStatusUpdated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map((entity: any) => {
        if (ids.findIndex((id: any) => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
  },

  extraReducers: (builder) => {
    builder.addMatcher(
      reportAPI.endpoints.getReportResults.matchFulfilled,
      (state, { payload }) => {
        return {
          ...state,
          reportForEdit: payload.report,
          appliedParams: payload.appliedParams,
        };
      }
    );
  },
});
