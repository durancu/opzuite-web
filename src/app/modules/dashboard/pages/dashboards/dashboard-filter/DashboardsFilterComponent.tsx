import { Formik } from "formik";
import { isEqual } from "lodash";
import { useMemo } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useFilterUIContext } from "../../../../../components/Filter";
import * as filterActions from "../../../../../components/Filter/_redux/filterActions";
import { useDashboardsUIContext } from "../DashboardsUIContext";
import { FilterForm } from "./form/FilterForm";

const prepareFilter = (queryParams: any, values: any) => {
  const { type, customer, product, insurer } = values;
  const newQueryParams = { ...queryParams };
  const filter: any = {};
  // Filter by status
  if (type && type !== "") {
    filter.type = type;
  }
  // Filter by all fields

  if (customer) {
    filter["customer"] = customer;
  }
  if (product) {
    filter["product"] = product;
  }
  if (insurer) {
    filter["insurer"] = insurer;
  }

  newQueryParams.filter = filter;
  return newQueryParams;
};

export function DashboardsFilterComponent() {
  // Dashboards UI Context
  const dashboardsUIContext: any = useDashboardsUIContext();
  const { togglePanel } = useFilterUIContext();
  const dispatch = useAppDispatch();

  const { filterForEdit } = useSelector(
    (state: any) => ({
      filterForEdit: state.filters.filterForEdit,
    }),
    shallowEqual
  );

  const dashboardsUIProps: any = useMemo(() => {
    return {
      setQueryParams: dashboardsUIContext.setQueryParams,
      queryParams: dashboardsUIContext.queryParams,
    };
  }, [dashboardsUIContext]);

  const applyFilter = (values: any) => {
    const newQueryParams = prepareFilter(dashboardsUIProps.queryParams, values);

    if (!isEqual(newQueryParams, dashboardsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      dashboardsUIProps.setQueryParams(newQueryParams);
    }
  };

  const initialValues = {
    insurer: "",
    product: "",
    status: "",
    type: "",
  };

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={filterForEdit || initialValues}
      onSubmit={(values) => {
        applyFilter(values);
        dispatch(filterActions.setFilter(values));
        togglePanel(false);
      }}
    >
      {() => {
        return <FilterForm />;
      }}
    </Formik>
  );
}
