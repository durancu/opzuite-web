import { useSnackbar } from "notistack";
import { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as actions from "../_redux/feedbackActions";

let displayed: any[] = [];

export const NotifyComponent = () => {
  const dispatch = useAppDispatch();

  const { notifications } = useSelector(
    (state: any) => ({
      notifications: state.feedback.notifications,
    }),
    shallowEqual
  );

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const storeDisplayed = (id: any) => {
    displayed = [...displayed, id];
  };

  const removeDisplayed = (id: any) => {
    displayed = [...displayed.filter((key) => id !== key)];
  };

  useEffect(() => {
    notifications.forEach(
      ({ key, message, options = {}, dismissed = false }: any) => {
        if (dismissed) {
          // dismiss snackbar using notistack
          closeSnackbar(key);
          return;
        }

        // do nothing if snackbar is already displayed
        if (displayed.includes(key)) return;

        // display snackbar using notistack
        enqueueSnackbar(message, {
          key,
          ...options,
          onClose: (event, reason, myKey) => {
            if (options.onClose) {
              options.onClose(event, reason, myKey);
            }
          },
          anchorOrigin: {
            vertical: "bottom",
            horizontal: "right",
          },
          onExited: (event, myKey) => {
            // remove this snackbar from redux store
            dispatch(actions.feedbackRemoveMessage(myKey));
            removeDisplayed(myKey);
          },
        });

        // keep track of snackbars that we've displayed
        storeDisplayed(key);
      }
    );
  }, [notifications, closeSnackbar, enqueueSnackbar, dispatch]);

  return null;
};
