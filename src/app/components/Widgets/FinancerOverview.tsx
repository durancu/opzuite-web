import { camelCase, startCase } from "lodash";
import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

interface Props {
  financer: any;
}

export const FinancerOverview = ({ financer }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>{financer && startCase(camelCase(financer.type))}</h4>
            <span>Type</span>
          </Col>
          <Col>
            <h4>{financer && financer.name}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>{(financer && financer.email) || "-"}</h4>
            <span>Email</span>
          </Col>
          <Col>
            <h4>{financer && (financer.phone || "-")}</h4>
            <span>Phone number </span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
