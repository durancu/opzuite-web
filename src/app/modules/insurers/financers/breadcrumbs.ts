import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Financers",
    path: "financers",
    isSeparator: false,
    isActive: true,
  },
];
