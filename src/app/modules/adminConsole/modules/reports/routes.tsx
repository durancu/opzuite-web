// routes module, to be imported in admin-console/routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Reports } from "./pages/Reports";
import { ReportDeleteDialog } from "./pages/ReportDeleteDialog";
import { Details } from "./pages/details/Details";
import { ReportEdit } from "./pages/report-edit/ReportEdit";

export const reportRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.REPORTS_READ} isRoute>
        <Reports />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.REPORTS_CREATE} isRoute>
        <ReportEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.REPORTS_UPDATE} isRoute>
        <ReportEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.REPORTS_READ} isRoute>
        <Details />
      </RequirePermission>
    ),
  },

  {
    path: ":code/delete",
    element: <ReportDeleteDialog />,
  },
];
