import { split } from "lodash";
import { Fragment, useEffect } from "react";
import { Card } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { WizardForm, WizardFormStep } from "src/app/components";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actions from "../../_redux/payrollsActions";
import FormCreatePayroll from "./Forms/FormCreatePayroll";
import FormPreparePayroll from "./Forms/FormPreparePayroll";
import { payrollInitData } from "./helpers";
import {
  payrollSalaryMetricsValidationSchema,
  payrollSetupValidationSchema,
} from "./schemas";

export const Edit = () => {
  const navigate = useNavigate();
  const { code }: any = useParams();
  const location = useLocation();

  const dispatch = useAppDispatch();

  const { actionsLoading, payrollForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.payrolls.actionsLoading,
      payrollForEdit: state.payrolls.payrollForEdit,
    }),
    shallowEqual
  );
  const isEditPayroll = split(location.pathname, "/").includes("edit") || false;
  const initPayroll = payrollInitData();

  useEffect(() => {
    dispatch(actions.fetchPayroll(code));
  }, [code, dispatch]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  const savePayroll = (values: any) => {
    if (!code) {
      dispatch(actions.createPayroll(values)).then((payroll: any) =>
        goToPayrollDetail(payroll)
      );
    } else {
      dispatch(actions.updatePayroll(values)).then((payroll: any) =>
        goToPayrollDetail(payroll)
      );
    }
  };

  const backToPayrollsList = () => {
    navigate(-1);
  };

  const goToPayrollDetail = (payroll: any) => {
    navigate(`/manager/payrolls/${payroll.code}/detail`);
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!code ? "New" : "Edit"} Payroll
      </PageTitle>

      <Card>
        <WizardForm
          initialValues={payrollForEdit || initPayroll}
          onSubmit={savePayroll}
          onCancel={backToPayrollsList}
        >
          {!isEditPayroll && (
            <WizardFormStep
              labelTextId="WIZARD.SALARY.SETUP"
              validationSchema={payrollSetupValidationSchema}
            >
              <FormPreparePayroll />
            </WizardFormStep>
          )}
          <WizardFormStep
            labelTextId="WIZARD.SALARY.METRICS"
            validationSchema={payrollSalaryMetricsValidationSchema}
          >
            <FormCreatePayroll />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
