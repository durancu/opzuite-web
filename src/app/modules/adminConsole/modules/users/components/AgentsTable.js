import PropTypes from "prop-types";
import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { useGetUserAuth } from "src/app/hooks";
import { userAPI } from "../redux/usersSlice";
import { FilterForm } from "./FilterForm";
import { defaultSorted, prepareFilter, sizePerPageList } from "./helpers";

export const AgentsTable = ({ showFilter = true }) => {
  const user = useGetUserAuth();
  const { roles } = useGetCatalog();
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = userAPI.useGetAllUsersQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      headerAlign: "left",
      align: "left",
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      headerFormatter: headerFormatters.NameColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedAgentWithLinkColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
        idField: "id",
        textField: "name",
        defaultText: "N/A",
      },
    },
    {
      dataField: "position",
      text: "Position",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      formatter: columnFormatters.PositionColumnFormatter,
      headerFormatter: headerFormatters.PositionColumnHeaderFormatter,
    },
    {
      dataField: "location",
      text: "Location",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      formatter: columnFormatters.LocationNameColumnFormatter,
      headerFormatter: headerFormatters.LocationColumnHeaderFormatter,
    },
    {
      dataField: "roleName",
      text: "Role",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      headerFormatter: headerFormatters.RoleColumnHeaderFormatter,
      formatter: columnFormatters.RoleColumnFormatter,
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      classes: "text-left ps-6",
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      headerFormatter: headerFormatters.StatusColumnHeaderFormatter,
      formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "updatedAt",
      text: "Last Update",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold text-muted",
      headerFormatter: headerFormatters.LastUpdateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "left",
      align: "left",
      formatter: columnFormatters.UserActionsColumnFormatter,
      formatExtraData: {
        openDetailUserPage: menuActions.details,
        openEditUserPage: menuActions.edit,
        openDeleteUserDialog: menuActions.delete,
        openChangeUserPassword: menuActions.changePassword,
        user: {
          location: user?.location,
          country: user?.country,
          role: user?.employeeInfo.category,
        },
        roles,
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      {showFilter && (
        <FilterPanel
          initialValues={{
            roles: "",
            searchText: "",
          }}
          prepareFilter={prepareFilter}
          filterForm={<FilterForm />}
        />
      )}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-left overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={data.entities || []}
                columns={columns}
                defaultSorted={defaultSorted}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

AgentsTable.propTypes = {
  showFilter: PropTypes.bool,
};
