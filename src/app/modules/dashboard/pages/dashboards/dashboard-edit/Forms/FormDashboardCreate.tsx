import {
  Checkbox,
  FormControl,
  FormHelperText,
  MenuItem,
  TextField as TextFieldNoFormik,
} from "@mui/material";
import { CheckBox, CheckBoxOutlineBlankOutlined } from "@material-ui/icons";
import { Autocomplete as NoFormikAutocomplete } from "@material-ui/lab";
import { nanoid } from "@reduxjs/toolkit";
import { Field, FieldArray, useFormikContext } from "formik";
import { TextField } from "formik-material-ui";
import { kebabCase, startCase } from "lodash";
import React, { useEffect, useMemo } from "react";
import { Card, Col, InputGroup, Row } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "../../../../../../components/Catalog/redux/catalogActions";
import {
  AutoComplete,
  DatePickerCustom,
  SelectField,
} from "../../../../../../partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "../../../../../../partials/controls/forms/DateRange/dateFactory";

const setOptions = (option: [] = []) => option;

const icon = <CheckBoxOutlineBlankOutlined fontSize="small" />;
const checkedIcon = <CheckBox fontSize="small" />;

const FormDashboardCreate = () => {
  const { values, setFieldValue } = useFormikContext<any>();

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  const { currentState } = useSelector(
    (state: any) => ({ currentState: state.catalogs?.entities }),
    shallowEqual
  );

  const dashboardFilterModel: any = useMemo(
    () => setOptions(currentState?.dashboardFilterModel),
    [currentState?.dashboardFilterModel]
  );
  const saleStatus: any = useMemo(
    () => setOptions(currentState?.saleStatus),
    [currentState?.saleStatus]
  );
  const countries: any = useMemo(
    () => setOptions(currentState?.countries),
    [currentState?.countries]
  );
  const customers: any = useMemo(
    () => setOptions(currentState?.customers),
    [currentState?.customers]
  );
  const locations: any = useMemo(
    () => setOptions(currentState?.locations),
    [currentState?.locations]
  );
  const saleTypes: any = useMemo(
    () => setOptions(currentState?.saleTypes),
    [currentState?.saleTypes]
  );
  const customerTypes: any = useMemo(
    () => setOptions(currentState?.customerTypes),
    [currentState?.customerTypes]
  );

  useEffect(() => {
    switch (values.params?.model) {
      case "sale":
        setFieldValue("params.filters", []);
        setFieldValue("params.fields", [
          "totalPremium",
          "totalNonPremium",
          "totalTaxesAndFees",
          "totalPayables",
          "totalReceivables",
          "totalFinanced",
          "totalAgentCommission",
        ]);

        break;
      case "customer":
        setFieldValue("params.filters", []);
        setFieldValue("params.fields", ["count"]);
        break;
    }
  }, [values.params.model, setFieldValue]);

  return (
    <>
      {/* BASIC INFO */}
      <Row className="mb-6">
        <Col md="3">
          <FormControl className="form-control">
            <AutoComplete
              name="params.model"
              label="Source"
              options={dashboardFilterModel || []}
              labelField="name"
              disabled
            />
          </FormControl>
        </Col>
        <Col md="3">
          <FormControl className="form-control">
            <AutoComplete
              disabled={!values.params?.model}
              name="params.groupByModel"
              label="Group by"
              options={
                dashboardFilterModel.find(
                  ({ id }: any) => id === values.params.model
                )?.groupByFields || []
              }
              labelField="name"
            />
          </FormControl>
        </Col>
        <Col md="3">
          <FormControl className="form-control">
            <DatePickerCustom
              label="Date from"
              name="params.dateFrom"
              format={DEFAULT_DATEPICKER_FORMAT}
            />
          </FormControl>
        </Col>
        <Col md="3">
          <FormControl className="form-control">
            <DatePickerCustom
              label="Date to"
              name="params.dateTo"
              format={DEFAULT_DATEPICKER_FORMAT}
            />
          </FormControl>
        </Col>
      </Row>
      <Row className="mb-6 pt-4">
        <Col md="3">
          <FormControl className="form-control">
            <Field component={TextField} label="Name" name="name" />
          </FormControl>
        </Col>
        <Col md="6">
          <FormControl className="form-control">
            <Field
              component={TextField}
              label="Description"
              name="description"
            />
          </FormControl>
        </Col>
        <Col md="3">
          <FormControl className="form-control">
            <Field
              component={TextField}
              label="Scope"
              name="scope"
              select={true}
            >
              <MenuItem value="public">Public</MenuItem>
              <MenuItem value="private">Private</MenuItem>
            </Field>
          </FormControl>
        </Col>
      </Row>
      <Row className="mt-20">
        <Col>
          <Card>
            <Card.Body>
              <Card.Subtitle>Filters</Card.Subtitle>
              <Row className="mt-6 mb-20">
                <Col>
                  <FormControl className="form-control">
                    <NoFormikAutocomplete
                      disabled={!values.params.model}
                      multiple
                      disableCloseOnSelect
                      options={
                        dashboardFilterModel
                          .find(({ id }: any) => id === values.params.model)
                          ?.filterFields.map(({ id, name }: any) => ({
                            field: id,
                            operator: "=",
                            value: "",
                            name,
                          })) || []
                      }
                      getOptionSelected={(option, value) =>
                        (Array.isArray(value) ? value : [value]).find(
                          (item) => item["field"] === option["field"]
                        )
                      }
                      getOptionLabel={(option: any) => {
                        return startCase(kebabCase(option["field"]));
                      }}
                      onChange={(_, value) => {
                        setFieldValue("params.filters", value);
                      }}
                      value={values.params.filters}
                      renderOption={(option, { selected }) => (
                        <React.Fragment>
                          <span>{selected}</span>
                          <Checkbox
                            icon={icon}
                            checkedIcon={checkedIcon}
                            style={{ marginRight: 8 }}
                            checked={selected}
                          />
                          <div>
                            {startCase(kebabCase(option["field"]))}
                            <FormHelperText>
                              {option.description}
                            </FormHelperText>
                          </div>
                        </React.Fragment>
                      )}
                      renderInput={(params) => (
                        <TextFieldNoFormik
                          {...params}
                          label="Set filters for more specific results"
                        />
                      )}
                    />
                  </FormControl>
                </Col>
              </Row>
              <FieldArray
                name="params.filters"
                render={() => {
                  return values.params.filters?.map(
                    (filter: any, index: number) => {
                      const nameField = `params.filters.${index}`;
                      let options: any[] = [];
                      switch (filter.field) {
                        case "type":
                          if (values.params.model === "sale") {
                            options = saleTypes;
                          } else if (values.params.model === "customer") {
                            options = customerTypes;
                          }
                          break;
                        case "country":
                          options = countries;
                          break;
                        case "location":
                          options = locations;
                          break;
                        case "customer":
                          options = customers;
                          break;
                        case "status":
                          options = saleStatus;
                          break;
                        default:
                          break;
                      }

                      return (
                        <Row key={nanoid()} className="mb-6">
                          <Col>
                            <InputGroup>
                              <FormControl className="form-control">
                                <TextFieldNoFormik
                                  disabled
                                  label="Field"
                                  value={values.params.filters[index].field}
                                />
                              </FormControl>
                              <FormControl className="form-control ml-3 mr-3">
                                <Field
                                  disabled
                                  name={`${nameField}.operator`}
                                  component={TextField}
                                  label="Operator"
                                />
                              </FormControl>
                              <FormControl className="form-control">
                                <SelectField
                                  name={`${nameField}.value`}
                                  label="Value"
                                  labelField="name"
                                  options={options}
                                />
                              </FormControl>
                            </InputGroup>
                          </Col>
                        </Row>
                      );
                    }
                  );
                }}
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </>
  );
};

export default FormDashboardCreate;
