import { Fragment, useMemo } from "react";
import { Col, Row } from "react-bootstrap";
import {
  FormAutoComplete,
  FormInput,
  RequirePermission,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { Permissions } from "src/app/constants/Permissions";
import { useAppSelector } from "src/app/hooks";
import { DatePickerCustom } from "src/app/partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { isPermitted } from "src/app/utils";
import { userSelector } from "../../../auth/redux";

export const FilterForm = () => {
  const catalog = useGetCatalog();

  const user = useAppSelector(userSelector);

  const filteredLocations = useMemo(() => {
    if (catalog?.locations?.length) {
      if (isPermitted(Permissions.MY_COUNTRY, user?.permissions || [])) {
        return catalog.locations.filter(
          (location: any) => location.country === user?.country
        );
      }
    }
  }, [user, catalog]);

  return (
    <Fragment>
      <RequirePermission permissions={Permissions.MY_COUNTRY}>
        <Row className="mb-4">
          <Col>
            <FormAutoComplete
              name="location"
              labelText="Location"
              options={filteredLocations || []}
            />
          </Col>
        </Row>
      </RequirePermission>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="lineOfBusiness"
            labelTextId="FORM.FILTERS.LINE_OF_BUSINESS"
            options={catalog?.linesOfBusiness || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="status"
            labelTextId="FORM.FILTERS.STATUS"
            options={catalog?.saleStatus || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="carrier"
            labelTextId="FORM.FILTERS.CARRIER"
            options={catalog?.carriers || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="broker"
            labelTextId="FORM.FILTERS.MGA"
            options={catalog?.brokers || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="customer"
            labelTextId="FORM.FILTERS.INSURED"
            options={catalog?.customers || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormAutoComplete
            name="seller"
            labelTextId="FORM.FILTERS.SELLER"
            options={catalog?.employees || []}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormInput name="usDOT" labelTextId="FORM.FILTERS.US_DOT" />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.SOLD_FROM"
            name="saleDateFrom"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.SOLD_TO"
            name="saleDateTo"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.POLICY_EFECTIVE_FROM"
            name="effectiveDateFrom"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.POLICY_EFECTIVE_TO"
            name="effectiveDateTo"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.POLICY_EXPIRE_FROM"
            name="expirationDateFrom"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
        <Col sm="6">
          <DatePickerCustom
            labelTextId="FORM.FILTERS.POLICY_EXPIRE_TO"
            name="expirationDateTo"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </Col>
      </Row>
    </Fragment>
  );
};
