import { Fragment, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useParams, useNavigate } from "react-router-dom";
import * as actionsCatalog from "src/app/components/Catalog/redux/catalogActions";
import {
  Applications,
  EmployeeDetails,
  EmployeeOverview,
  Policies,
  PageDetailTopButtons,
} from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import * as actions from "../redux/usersActions";
import { PageTitle } from "src/_metronic/layout/core";
import { breadcrumbs } from "../../../breadcrumbs";
import { useIntl } from "react-intl";
import { BackdropLoader } from "src/app/components";

export const Details = () => {
  const { id }: any = useParams();
  const navigate = useNavigate();
  const intl = useIntl();
  const dispatch = useAppDispatch();
  const { actionsLoading, userForEdit, catalogs } = useSelector(
    (state: any) => ({
      actionsLoading: state.users.actionsLoading,
      userForEdit: state.users.userForEdit,
      catalogs: state.catalogs.entities,
      user: state.auth?.user,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchUser(id));
    dispatch(actionsCatalog.getAllCatalog());
  }, [id, dispatch]);

  const editUser = () => {
    navigate(`/manager/users/${id}/edit`);
  };

  const deleteUser = () =>
    dispatch(actions.deleteUser(id)).then(() => {
      navigate("/manager/users");
    });

  if (actionsLoading) return <BackdropLoader />;
  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.EMPLOYEE" })}
      </PageTitle>
      <div>
        <div className="d-flex justify-content-between align-items-center py-4">
          <h3 className="fw-bolder">Employee Details</h3>

          <PageDetailTopButtons
            editFunction={editUser}
            deleteFunction={deleteUser}
            editPermission={Permissions.USERS_UPDATE}
            deletePermission={Permissions.USERS_DELETE}
            componentLabel="employee"
          />
        </div>
        {!actionsLoading && userForEdit && (
          <EmployeeOverview employee={userForEdit} catalogs={catalogs} />
        )}
        {!actionsLoading && userForEdit && (
          <EmployeeDetails employee={userForEdit} catalogs={catalogs} />
        )}

        {!actionsLoading && userForEdit && (
          <Policies
            filter={{ seller: userForEdit.id }}
            subTitle="Latest policies sold by this employee."
            context="employee"
          />
        )}
        {!actionsLoading && userForEdit && (
          <Applications
            filter={{ seller: userForEdit.id }}
            subTitle="Latest services processed by this employee."
            context="employee"
          />
        )}
      </div>
    </Fragment>
  );
};
