export interface ItemPayment {
  id: string;
  amount?: number;
  balance?: number;
  code: string;
  company: string;
  date?: Date;
  sale: string;
  endorsement: string;
  endorsementItem: string;
  createdBy?: string;
  updatedBy?: string;
}
