export const IMMIGRANT_STATUS_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "CITIZEN", name: "Citizen" },
  { id: "RESIDENT", name: "Resident" },
  { id: "NON-IMMIGRANT", name: "Non-immigrant" },
  { id: "UNDOCUMENTED-IMMIGRANT", name: "Undocumented immigrant" },
  { id: "NOT-TO-SAY", name: "I prefer not to say" },
  { id: "OTHER", name: "Other" },
];
