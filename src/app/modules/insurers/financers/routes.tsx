// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import Financers from "./pages/Financers";
import DeleteDialog from "../components/DeleteDialog";
import Details from "./pages/Details";
import Edit from "./pages/edit/Edit";

export const financerRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.FINANCERS_READ}>
        <Financers />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.FINANCERS_CREATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.FINANCERS_UPDATE}>
        <Edit />
      </RequirePermission>
    ),
  },

  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.FINANCERS_READ}>
        <Details />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <DeleteDialog label="financer" path="/financers" />,
  },
];
