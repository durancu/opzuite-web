import { useSnackbar } from "notistack";
import { Fragment, useEffect } from "react";
import { useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { BackdropLoader, Error } from "src/app/components";
import {
  CarrierDetails,
  CarrierOverview,
  Policies,
  PageDetailTopButtons,
} from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { insurersAPI } from "../../redux/insurerSlice";
import { breadcrumbs } from "../breadcrumbs";

const Details = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: carrier,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(`${code}?layout=FULL`, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [deleteCarrier, { isLoading: isDeleting }] =
    insurersAPI.useDeleteInsurerMutation();

  const goToEditCarrier = () => {
    navigate(`/carriers/${code}/edit`);
  };

  async function handleDelete() {
    try {
      await deleteCarrier(code).unwrap();
      enqueueSnackbar("Carrier deleted", {
        variant: "success",
      });
      navigate("/carriers");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.CARRIER" })}
      </PageTitle>
      <Toolbar>
        <PageDetailTopButtons
          editFunction={goToEditCarrier}
          deleteFunction={handleDelete}
          editPermission={Permissions.CARRIERS_UPDATE}
          deletePermission={Permissions.CARRIERS_DELETE}
          componentLabel="carrier"
        />
      </Toolbar>

      <CarrierOverview carrier={carrier} />
      <CarrierDetails carrier={carrier} />

      <Policies filter={{ carrier: carrier?.id }} context="carriers" />
    </Fragment>
  );
};

export default Details;
