import { FormikValues } from "formik";

export const CustomerStatusCssClasses = ["success", "info", ""];
export const CustomerStatusTitles = ["Selling", "Sold"];
export const CustomerConditionCssClasses = ["success", "danger", ""];
export const CustomerConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "createdAt", order: "desc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

const searchColumns = ["email", "name", "phone", "business.usDOT"];

export const defaultFilter = {
  filter: {},
  sortOrder: "desc",
  sortField: "createdAt",
  pageNumber: 1,
  pageSize: 50,
};

export const initialValues = {
  type: "",
  searchText: "",
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {};

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    sortOrder: "desc",
    sortField: "createdAt",
    pageNumber: 1,
    pageSize: 50,
  };
};
