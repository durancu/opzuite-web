import React, { Fragment } from "react";
import { Button, OverlayTrigger, Tooltip, Modal } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { useAppDispatch } from "src/app/hooks";
import { feedbacksSlice } from "../../Feedback/_redux/feedbackSlice";
import { GenericFunction } from "../../interfaces/FunctionInterfaces";

interface Props {
  itemTitle: string;
  action: GenericFunction;
  hidden?: boolean;
}

export function ItemDeleteModal({ itemTitle, action, hidden }: Props) {
  const dispatch = useAppDispatch();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deleteAction = () => {
    action();
    dispatch(
      feedbacksSlice.actions.enqueueFeedbackMessage({
        message: `${itemTitle} deleted successfully.`,
        options: {
          key: new Date().getTime() + Math.random(),
          variant: "success",
        },
      } as any)
    );
  };

  const intl = useIntl();

  return (
    <Fragment>
      <OverlayTrigger
        placement="top"
        overlay={<Tooltip className="text-capitalize">Delete</Tooltip>}
      >
        <Button
          hidden={hidden}
          variant="danger"
          onClick={() => handleOpen()}
          className="btn-sm pr-0 pl-2"
        >
          <i className="fs-4 bi bi-trash3"></i>
        </Button>
      </OverlayTrigger>
      <Modal show={open} centered>
        <Modal.Header>
          <Modal.Title className="my-0">{`Delete ${itemTitle}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <FormattedMessage
              id="GENERAL.MODAL.TEXT.SURE_WANT_TO_DELETE_PERMANENTLY.MODEL"
              values={{
                this: intl.formatMessage({
                  id: "GENERAL.JOINT.THIS.MALE.SINGULAR",
                }),
                model: itemTitle,
              }}
            />
          </p>
          <p>
            <FormattedMessage id="ENDORSEMENT.MODAL.DELETE.WARNING" />
          </p>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="light" onClick={handleClose}>
            <FormattedMessage id="BUTTON.CANCEL" />
          </Button>
          <Button
            variant="danger"
            onClick={() => {
              deleteAction();
              handleClose();
            }}
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}
