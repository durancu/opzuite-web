// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import { InputLabel, MenuItem } from "@mui/material";
import { Field, Form, useFormikContext } from "formik";
import { Select, Switch, TextField } from "formik-material-ui";
import moment from "moment";
import { useEffect } from "react";
import { InputGroup } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { WizardHiddenButtons } from "src/app/components/Wizard/WizardHiddenButtons";
import { AutoComplete, DatePickerCustom } from "src/app/partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";

const FormCustomerSetting = () => {
  const { values, setFieldValue } = useFormikContext<any>();
  const { currentState } = useSelector(
    (state: any) => ({ currentState: state.catalogs }),
    shallowEqual
  );

  const { entities } = currentState;

  useEffect(() => {
    switch (values?.renewalFrequency) {
      case "ANNUAL":
        setFieldValue("expiresAt", moment(values.effectiveAt).add("year", 1));
        break;
      case "SEMI-ANNUAL":
        setFieldValue(
          "expiresAt",
          moment(values.effectiveAt).add("quarters", 2)
        );
        break;
      case "QUARTERLY":
        setFieldValue(
          "expiresAt",
          moment(values.effectiveAt).add("quarter", 1)
        );
        break;
      case "MONTHLY":
        setFieldValue("expiresAt", moment(values.effectiveAt).add("month", 1));
        break;
      default:
        break;
    }
  }, [setFieldValue, values?.effectiveAt, values?.renewalFrequency]);

  useEffect(() => {
    if (moment(values?.expiresAt).diff(values?.effectiveAt, "year", true) === 1)
      setFieldValue("renewalFrequency", "ANNUAL");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "quarters", true) ===
      2
    )
      setFieldValue("renewalFrequency", "SEMI-ANNUAL");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "quarter", true) === 1
    )
      setFieldValue("renewalFrequency", "QUARTERLY");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "month", true) === 1
    )
      setFieldValue("renewalFrequency", "MONTHLY");
    else setFieldValue("renewalFrequency", "VARIABLE");
  }, [setFieldValue, values.expiresAt, values.effectiveAt]);

  const validateBack = (): boolean => true;

  return (
    <Form className="form form-label-right">
      <div className="form-group row">
        <div className="col-md-3">
          <InputLabel htmlFor="renewalFrequency">Renewal Frequency</InputLabel>
          <Field
            component={Select}
            name="renewalFrequency"
            inputProps={{
              id: "renewalFrequency",
            }}
          >
            <MenuItem key="annual" value="ANNUAL">
              Annual
            </MenuItem>
            <MenuItem key="semi-annual" value="SEMI-ANNUAL">
              Semi-Annual
            </MenuItem>
            <MenuItem key="quarterly" value="QUARTERLY">
              Quarterly
            </MenuItem>
            <MenuItem key="monthly" value="MONTHLY">
              Monthly
            </MenuItem>
            <MenuItem key="variable" value="VARIABLE">
              Variable
            </MenuItem>
          </Field>
        </div>
        <div className="col-md-3">
          <DatePickerCustom
            label="Policy Effective At"
            name="effectiveAt"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </div>
        <div className="col-md-3">
          <DatePickerCustom
            label="Policy Expires At"
            name="expiresAt"
            format={DEFAULT_DATEPICKER_FORMAT}
          />
        </div>
        <div className="col-md-3">
          <InputGroup>
            <Field
              component={Switch}
              as={InputGroup.Text}
              type="checkbox"
              name="autoRenew"
            />
            <label className="mt-3">Auto Renew </label>
          </InputGroup>
        </div>
      </div>
      <div className="form-group row">
        <div className="col-md-3">
          <AutoComplete
            name={`financerCompany`}
            label="Financer (PFA)"
            labelField="name"
            options={entities?.financers}
          />
        </div>
        <div className="col-md-3">
          <Field
            component={TextField}
            label="Recurring Payment"
            name="monthlyPayment"
            type="number"
          />
        </div>
      </div>
      <WizardHiddenButtons validateBack={validateBack} />
    </Form>
  );
};

export default FormCustomerSetting;
