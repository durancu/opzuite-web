const initPayroll = {
  location: "",
  normalHoursWorked: 0,
  payPeriodStartedAt: null,
  payPeriodEndedAt: null,
  periodCode: "",
  payStubs: [],
  totalSaleBonus: 0,
  totalExtraBonus: 0,
  totalExtraBonusDescription: "",
  totalDiscount: 0,
  totalDiscountDescription: "",
  totalReimbursement: 0,
  totalReimbursementDescription: "",
};

export const payrollInitData = () => {
  return initPayroll;
};

export const payrollSteps = (isEditPayroll: boolean) => {
  return isEditPayroll ? ["Salary Metrics"] : ["Setup", "Salary Metrics"];
};
