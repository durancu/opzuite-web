import { useEffect, useMemo, useState } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";
import { calculatePermitMetrics } from "../functions/calculatePermitMetrics";
import { Permissions } from "src/app/constants";
import { isPermitted } from "src/app/utils";
import { MetricCard } from "./MetricCard";

interface Props {
  sale: any;
}

export const PermitMetricsAccordion = ({ sale }: Props) => {
  const [metrics, setMetrics] = useState<any[]>([]);
  const { currentState, user } = useSelector(
    (state: any) => ({
      currentState: state.catalogs?.entities,
      user: state.auth.user,
    }),
    shallowEqual
  );

  const providers = useMemo(() => {
    return [currentState?.brokers, currentState?.carriers];
  }, [currentState?.brokers, currentState?.carriers]);

  useEffect(() => {
    const recalculatedSale = calculatePermitMetrics(sale);
    const metrics = [
      { title: "Checksum/Sanity", value: recalculatedSale.checksumSanity },
      { title: "Premium", value: recalculatedSale.totalPremium },
      { title: "Non Premium", value: recalculatedSale.totalNonPremium },
      { title: "Taxes And Fees", value: recalculatedSale.totalTaxesAndFees },
      { title: "Payables", value: recalculatedSale.totalPayables },
      { title: "Receivables", value: recalculatedSale.totalReceivables },
      { title: "Financed", value: recalculatedSale.totalFinanced },
      {
        title: "Agent Commission",
        value: recalculatedSale.totalAgentCommission,
      },
    ];

    if (isPermitted(Permissions.MY_COMPANY, user.permissions)) {
      metrics.push({
        title: "Agency Commission",
        value: recalculatedSale.totalAgencyCommission,
      });
    }

    setMetrics(metrics);
  }, [sale, providers, user]);

  return (
    <Card>
      <Card.Title as="h3" className="text-primary mb-10">
        <FormattedMessage id="GENERAL.METRICS" />
      </Card.Title>
      <MetricCard metrics={metrics} />
    </Card>
  );
};
