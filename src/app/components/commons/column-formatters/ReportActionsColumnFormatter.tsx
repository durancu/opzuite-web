/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const ReportActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openEditReportPage,
    openDeleteReportDialog,
    openDetailReportPage,
    user,
  }: any
) => (
  <>
    <TableActionsMenu
      readFunction={openDetailReportPage}
      deleteFunction={openDeleteReportDialog}
      editPermission={Permissions.REPORTS_UPDATE}
      hideEditCondition={row?.author !== user?.name}
      editFunction={openEditReportPage}
      readPermission={Permissions.REPORTS_READ}
      deletePermission={Permissions.REPORTS_DELETE}
      hideDeleteCondition={row?.author !== user?.name}
      code={row.code}
    />
  </>
);
