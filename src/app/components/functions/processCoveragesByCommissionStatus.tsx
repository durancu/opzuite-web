import { upsertNonPremiumCommissionEndorsementItem } from ".";

export const processCoveragesByCommissionStatus = async (
  setFieldValue: any,
  commissionPlans: any[],
  coverages: any[],
  endorsements: any[],
  userId: string
): Promise<any> => {
  const result = {
    totalDownPayment: 0,
    totalPremium: 0,
  };
  coverages
    .filter(({ deleted }: any) => deleted === false)
    .forEach(
      ({
        status,
        amount,
        premium,
        carrier,
        nonCommissionableReferenceId,
      }: any) => {
        //if current coverage carrier has it own Commission Plans (not based in monthly sales)
        const carrierCommissionPlans: any = commissionPlans
          ? commissionPlans.find(
              (carrierPlans: any) => carrierPlans.insurer === carrier
            )
          : [];

        if (["ACTIVE", "PENDING"].includes(status)) {
          //add premium and down payment to first endorsement summary if carrier does not have custom commission plans
          if (!carrierCommissionPlans) {
            result.totalDownPayment += parseFloat(amount);
            result.totalPremium += parseFloat(premium);
          } else {
            //upsert new commission item for each commission plan party (agent, agency, sharers)
            //find agent and update/replace
            ["AGENT", "SHARER", "AGENCY"].forEach(async (type: string) => {
              const basePremiumItems: any = endorsements[0].items;
              await upsertNonPremiumCommissionEndorsementItem(
                setFieldValue,
                basePremiumItems,
                nonCommissionableReferenceId,
                premium,
                type,
                carrierCommissionPlans,
                coverages,
                endorsements,
                userId
              );
            });
          }
        }
      }
    );
  return result;
};
