import { Formik, FormikValues } from "formik";
import { useSnackbar } from "notistack";
import { Fragment, useState } from "react";
import { Alert, Button, Modal, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { BackdropLoader, FormAutoComplete } from "src/app/components";
import { insurersAPI } from "src/app/modules/insurers/redux/insurerSlice";
import { holderSchema, holderValues, holdersQuery } from "../helpers";
import { HolderInfo } from "./HolderInfo";

export const Holders = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [isOpen, toggleOpen] = useState<Record<string, any>>({
    modal: false,
    delete: false,
  });

  const {
    data: holders,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetAllInsurersQuery(holdersQuery);

  const [create, { isLoading: isCreating }] =
    insurersAPI.useCreateInsurerMutation();

  // modal helper
  function toggleAlert(modal: string) {
    toggleOpen((current) => {
      return { ...current, [modal]: !current[modal] };
    });
  }

  async function processHolder(values: FormikValues) {
    try {
      await create(values).unwrap();
      refetch();
      enqueueSnackbar("Holder created successfully", {
        variant: "success",
      });
      toggleAlert("modal");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading) return <BackdropLoader />;
  if (isError) {
    return (
      <Alert show={isError} variant="danger">
        <Alert.Heading as="h2">Something went wrong</Alert.Heading>
        <p className="my-4">
          We {`couldn't`} fetch certificate holders. Please try again
        </p>
        <Button onClick={() => refetch()} variant="danger">
          Try again
        </Button>
      </Alert>
    );
  }

  return (
    <Fragment>
      <div className="d-flex gap-3 mb-4 align-items-start justify-content-center">
        <div className="flex-grow-1">
          <FormAutoComplete
            name="holder"
            labelText="Certificate Holder"
            options={holders?.entities ?? []}
          />
        </div>
        <div className="d-flex gap-3 mt-8">
          <Button
            className="btn btn-light-primary text-nowrap"
            onClick={() => toggleAlert("modal")}
          >
            Add New
          </Button>
        </div>
      </div>
      <Modal
        size="xl"
        show={isOpen.modal}
        onHide={() => toggleAlert("modal")}
        centered
        backdrop="static"
      >
        <Modal.Header className="bg-light">
          <Modal.Title>Add New Certificate Holder</Modal.Title>
        </Modal.Header>

        <Formik
          initialValues={holderValues}
          validationSchema={holderSchema}
          onSubmit={processHolder}
        >
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Modal.Body>
                <HolderInfo />
              </Modal.Body>
              <Modal.Footer>
                <Button variant="light" onClick={() => toggleAlert("modal")}>
                  <FormattedMessage id="BUTTON.CANCEL" />
                </Button>
                <Button type="submit" variant="primary" disabled={isCreating}>
                  {isCreating ? (
                    <div className="d-flex gap-3 align-items-center">
                      <Spinner />
                      <span>Processing</span>
                    </div>
                  ) : (
                    <FormattedMessage id="BUTTON.SAVE" />
                  )}
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    </Fragment>
  );
};
