/* eslint-disable no-script-url */

import { Fragment } from "react";

export const RoleColumnFormatter = (role: any): any => (
    <Fragment>
        <span className="text-dark fw-semibold fs-6">
            {role || "-"}
        </span>
    </Fragment>
);
