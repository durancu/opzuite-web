import React, { Fragment, ReactNode, useState } from "react";
import clsx from "clsx";
import {
  Form,
  Formik,
  FormikConfig,
  FormikHelpers,
  FormikValues,
} from "formik";
import { useSnackbar } from "notistack";

import { Alert } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import ReactJson from "react-json-view";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "src/app/hooks";

interface Props extends FormikConfig<FormikValues> {
  children: ReactNode;
  onCancel?: () => void;
}

export const WizardForm = ({
  children,
  initialValues,
  validationSchema,
  onSubmit,
  onCancel,
}: Props) => {
  const intl = useIntl();
  const { enqueueSnackbar } = useSnackbar();
  const role = useAppSelector((state) => state.auth?.user?.role);
  const navigate = useNavigate();
  // ensure only valid react elements are children
  const steps = React.Children.toArray(children) as React.ReactElement[];

  const [stepNumber, setStepNumber] = useState(0);
  const [snapshot, setSnapshot] = useState(initialValues);

  const step = steps[stepNumber];
  const totalSteps = steps.length;
  const isLastStep = stepNumber === totalSteps - 1;

  const nextStep = (values: FormikValues) => {
    setSnapshot(values);
    setStepNumber(Math.min(stepNumber + 1, totalSteps - 1));
  };

  const prevStep = (values: FormikValues) => {
    setSnapshot(values);
    setStepNumber(Math.max(stepNumber - 1, 0));
  };

  const handleSubmit = async (
    values: FormikValues,
    actions: FormikHelpers<FormikValues>
  ) => {
    if (step.props.onSubmit) {
      await step.props.onSubmit(values, actions);
    }
    if (isLastStep) {
      actions.setSubmitting(true);
      return onSubmit(values, actions);
    } else {
      actions.setTouched({});
      nextStep(values);
    }
  };

  const handleCancel = () => {
    if (onCancel && typeof onCancel === "function") {
      onCancel();
    } else {
      navigate(-1);
    }
  };

  function alertIfError() {
    enqueueSnackbar(
      "You missed a few fields please check and fill them before proceeding",
      {
        variant: "error",
      }
    );
  }

  return (
    <section className="stepper stepper-links p-10">
      <nav className="stepper-nav mb-5">
        {steps.map((step, index) => (
          <div
            key={index}
            className={clsx("stepper-item", {
              current: stepNumber === index,
            })}
          >
            <h3 className="stepper-title">
              {step.props.labelTextId ? (
                <FormattedMessage id={step.props.labelTextId} />
              ) : (
                step.props.label
              )}
            </h3>
          </div>
        ))}
      </nav>

      <Formik
        onSubmit={handleSubmit}
        initialValues={snapshot}
        validationSchema={validationSchema || step.props.validationSchema}
      >
        {(formik) => (
          <Fragment>
            <Alert
              variant="danger"
              className="my-4 p-5"
              show={
                Object.keys(formik.errors).length > 0 && role.hierarchy < 30
              }
            >
              <Alert.Heading>
                {intl.formatMessage({ id: "FORM.ERROR.VALIDATION" })}
              </Alert.Heading>
              <div className="mt-1">
                <ReactJson src={formik.errors} />
              </div>
            </Alert>

            <Form className="mx-auto w-100 pt-8 pb-10">
              <div
                data-kt-stepper-element="content"
                className="current d-block"
              >
                {step}
              </div>

              {/* Controls */}
              <div className="d-flex justify-content-between align-items-center pt-8">
                <button
                  type="button"
                  className="btn btn-lg btn-light d-flex align-items-center"
                  onClick={handleCancel}
                >
                  <span className="indicator-label">
                    <FormattedMessage id="BUTTON.CANCEL" />
                  </span>
                </button>

                <div className="d-flex gap-3">
                  {stepNumber > 0 && (
                    <button
                      type="button"
                      className="btn btn-lg btn-light-primary d-flex align-items-center"
                      onClick={() => prevStep(formik.values)}
                    >
                      <i className="fs-4 bi bi-chevron-left"></i>
                      <span className="indicator-label">Back</span>
                    </button>
                  )}

                  <button
                    type="submit"
                    className={clsx(
                      "btn btn-lg ms-auto d-flex align-items-center gap-2",
                      isLastStep ? "btn-success" : "btn-dark"
                    )}
                    disabled={formik.isSubmitting}
                    onClick={() => {
                      if (Object.keys(formik.errors).length > 0) {
                        alertIfError();
                      }
                    }}
                  >
                    {isLastStep ? (
                      <Fragment>
                        <i className="fs-4 bi bi-save-fill"></i>
                        <span className="indicator-label">
                          <FormattedMessage id="BUTTON.SAVE_AND_EXIT" />
                        </span>
                      </Fragment>
                    ) : (
                      <Fragment>
                        <span className="indicator-label">Continue</span>
                        <i className="fs-4 bi bi-chevron-right"></i>
                      </Fragment>
                    )}
                  </button>
                </div>
              </div>
              {/* End controls */}
            </Form>
          </Fragment>
        )}
      </Formik>
    </section>
  );
};
