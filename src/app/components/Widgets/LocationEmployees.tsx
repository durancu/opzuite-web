import { Card } from "react-bootstrap";
import { AgentsTable } from "src/app/modules/adminConsole/modules/users/components/AgentsTable";
import { FilterUIProvider } from "../Filter";

interface Props {
  id: string;
}

export const LocationEmployees = ({ id }: Props) => {
  const defaultFilter = {
    filter: {
      location: id,
    },
    sortOrder: "asc",
    sortField: "name",
    pageNumber: 1,
    pageSize: 50,
  };

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="mb-3 text-primary">
        Employees
      </Card.Title>
      <p className="mb-8">Employees assigned to this location.</p>
      <FilterUIProvider context="users" defaultFilter={defaultFilter}>
        <AgentsTable showFilter={false} />
      </FilterUIProvider>
    </Card>
  );
};
