import clsx from "clsx";
import { ErrorMessage, FastField, Field, FieldProps } from "formik";
import { InputHTMLAttributes, TextareaHTMLAttributes, useState } from "react";
import { Form } from "react-bootstrap";
import { useIntl } from "react-intl";
import MaskedInput from "react-text-mask";
import { PHONE_NUMBER_MASK, SSN_MASK } from "src/app/constants/RegularExpressions";

type Props = InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  labelText?: string;
  labelTextId?: string;
  helperText?: string;
  helperTextId?: string;
};

export const FormInput = ({
  name,
  type = "text",
  labelText,
  labelTextId,
  helperText,
  helperTextId,
  placeholder,
  disabled,
  required,
  className,
  onChange,
  value,
}: Props) => {
  const intl = useIntl();
  const [toggle, setToggle] = useState(false);

  return (
    <Field name={name}>
      {({ field, meta }: FieldProps) => (
        <Form.Group controlId={name} className="w-100">
          {(labelText || labelTextId) && (
            <Form.Label className={clsx({ required })}>
              {labelTextId
                ? intl.formatMessage({ id: labelTextId })
                : labelText}
            </Form.Label>
          )}

          <div className="position-relative">
            <Form.Control
              {...field}
              type={toggle ? "text" : type}
              disabled={disabled}
              placeholder={placeholder}
              isInvalid={meta.error ? true : false}
              className={clsx(
                {
                  "border-secondary": !meta.error,
                },
                className
              )}
              value={value ? value : field.value}
              onChange={(evt: any) => {
                field.onChange(evt);
                if (onChange) {
                  onChange(evt);
                }
              }}
            />
            {type === "password" && (
              <button
                type="button"
                className={clsx(
                  "btn position-absolute top-0 bottom-0 end-0 d-flex align-items-center p-0",
                  meta.error ? "me-12" : "me-2"
                )}
                onClick={() => setToggle(!toggle)}
              >
                <i
                  className={clsx(
                    "fs-2 bi",
                    toggle ? "bi-eye" : "bi-eye-slash"
                  )}
                ></i>
              </button>
            )}
          </div>
          <Form.Text className="text-danger my-1 d-block">
            <ErrorMessage name={name} />
          </Form.Text>
          <Form.Text className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    </Field>
  );
};

type SelectProps = Props & {
  options: ListItem[];
};

export const FormSelect = ({
  name,
  labelText,
  labelTextId,
  helperText,
  helperTextId,
  placeholder,
  options = [],
  disabled,
  required,
  className,
}: SelectProps) => {
  const intl = useIntl();
  return (
    <FastField name={name}>
      {({ field, meta }: FieldProps) => (
        <Form.Group className="w-100">
          {(labelText || labelTextId) && (
            <Form.Label className={clsx({ required })}>
              {labelTextId
                ? intl.formatMessage({ id: labelTextId })
                : labelText}
            </Form.Label>
          )}
          <Form.Select
            {...field}
            placeholder={placeholder}
            isInvalid={meta.error ? true : false}
            className={clsx(
              {
                "border-secondary": !meta.error,
              },
              className
            )}
            disabled={disabled}
          >
            <option value="">Select an option</option>
            {options?.map(({ id, name }) => (
              <option key={id} value={id}>
                {name}
              </option>
            ))}
          </Form.Select>
          <Form.Text className="text-danger my-1 d-block">
            <ErrorMessage name={name} />
          </Form.Text>
          <Form.Text className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    </FastField>
  );
};

export const PhoneNumberInput = ({
  name,
  labelText,
  labelTextId = "FORM.LABELS.PHONE_NUMBER",
  helperText,
  helperTextId,
  className,
  required,
}: Props) => {
  const intl = useIntl();

  return (
    <FastField name={name}>
      {({ field, meta }: FieldProps) => (
        <Form.Group className="w-100">
          {(labelText || labelTextId) && (
            <Form.Label className={clsx({ required })}>
              {labelTextId && !labelText
                ? intl.formatMessage({ id: labelTextId })
                : labelText}
            </Form.Label>
          )}
          <MaskedInput
            {...field}
            id={name}
            mask={PHONE_NUMBER_MASK}
            aria-describedby="helperText"
            className={clsx(
              "form-control",
              {
                "border-secondary": !meta.error,
                "is-invalid": meta.error,
              },
              className
            )}
          />
          <Form.Text className="text-danger my-1 d-block">
            <ErrorMessage name={name} />
          </Form.Text>
          <Form.Text id="helperText" className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    </FastField>
  );
};

export const SSNInput = ({
  name,
  labelText,
  labelTextId = "FORM.LABELS.SSN",
  helperText,
  helperTextId,
  className,
  required,
}: Props) => {
  const intl = useIntl();
  const [toggle, setToggle] = useState(false);

  return (
    <Field name={name}>
      {({ field, meta }: FieldProps) => (
        <Form.Group className="w-100">
          {(labelText || labelTextId) && (
            <Form.Label className={clsx({ required })}>
              {labelTextId && !labelText
                ? intl.formatMessage({ id: labelTextId })
                : labelText}
            </Form.Label>
          )}

          <div className="position-relative">
            <MaskedInput
              {...field}
              id={name}
              mask={SSN_MASK}
              type={toggle ? "text" : "password"}
              aria-describedby="helperText"
              autoComplete="off"
              className={clsx(
                "form-control",
                {
                  "border-secondary": !meta.error,
                  "is-invalid": meta.error,
                },
                className
              )}
            />
            <button
              type="button"
              className={clsx(
                "btn position-absolute top-0 bottom-0 end-0 d-flex align-items-center p-0",
                meta.error ? "me-12" : "me-2"
              )}
              onClick={() => setToggle(!toggle)}
            >
              <i
                className={clsx("fs-2 bi", toggle ? "bi-eye" : "bi-eye-slash")}
              ></i>
            </button>
          </div>
          <Form.Text className="text-danger my-1 d-block">
            <ErrorMessage name={name} />
          </Form.Text>
          <Form.Text id="helperText" className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    </Field>
  );
};

type ToggleProps = Props & {
  type?: "checkbox" | "switch";
};

export const FormToggle = ({
  type,
  checked,
  disabled,
  onChange,
  labelText,
  labelTextId = "FORM.LABELS.DEFAULT",
}: ToggleProps) => {
  const intl = useIntl();
  return (
    <Form.Check
      type={type}
      checked={checked}
      disabled={disabled}
      onChange={onChange}
      label={labelText || intl.formatMessage({ id: labelTextId })}
    />
  );
};

type TextAreaProps = BaseInputProps &
  TextareaHTMLAttributes<HTMLTextAreaElement>;

export const FormTextArea = ({
  rows = 4,
  name,
  labelText,
  labelTextId,
  helperText,
  helperTextId,
  placeholder,
  disabled,
  required,
  className,
}: TextAreaProps) => {
  const intl = useIntl();
  return (
    <FastField name={name}>
      {({ field, meta }: FieldProps) => (
        <Form.Group controlId={name} className="w-100">
          {(labelText || labelTextId) && (
            <Form.Label className={clsx({ required })}>
              {labelTextId
                ? intl.formatMessage({ id: labelTextId })
                : labelText}
            </Form.Label>
          )}

          <Form.Control
            {...field}
            as="textarea"
            rows={rows}
            disabled={disabled}
            placeholder={placeholder}
            isInvalid={meta.error ? true : false}
            className={clsx(
              {
                "border-secondary": !meta.error,
              },
              className
            )}
          />

          <Form.Text className="text-danger my-1 d-block">
            <ErrorMessage name={name} />
          </Form.Text>
          <Form.Text className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    </FastField>
  );
};
