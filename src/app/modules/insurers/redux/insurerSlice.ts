import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

interface Data {
  entities: Insurer[];
  limit: number;
  page: number;
  totalCount: number;
}

export const insurersAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllInsurers: build.query<Data, QueryParams>({
      query: (queryParams) => ({
        url: "/insurers/search",
        method: "post",
        body: { queryParams },
      }),
    }),
    createInsurer: build.mutation<any, any>({
      query: (data) => ({
        url: "/insurers",
        method: "post",
        body: data,
      }),
    }),
    getInsurerByCode: build.query<any, any>({
      query: (code) => `/insurers/${code}`,
    }),
    updateInsurer: build.mutation<any, any>({
      query: (data) => ({
        url: `/insurers/${data.code}`,
        method: "put",
        body: data,
      }),
    }),
    deleteInsurer: build.mutation<any, any>({
      query: (code) => ({
        url: `/insurers/${code}`,
        method: "delete",
      }),
    }),
  }),
  overrideExisting: false,
});

export const insurerSlice = createSlice({
  name: "insurers",
  initialState: {},
  reducers: {},
});

export default insurerSlice.reducer;
