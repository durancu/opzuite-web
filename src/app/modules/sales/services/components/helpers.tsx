import { FormikValues } from "formik";

export const PermitstatusCssClasses = ["success", "info", ""];
export const defaultSorted = [{ dataField: "code", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
  { text: "200", value: 200 },
  { text: "500", value: 500 },
  { text: "1000", value: 1000 },
];

const searchColumns = [
  "seller.name",
  "customer.name",
  "customer.business.usDOT",
  "location.business.name",
  "soldAt",
  "code",
];

export const defaultFilter = {
  filter: { type: "PERMIT" },
  searchColumns,
  searchText: "",
  sortOrder: "desc",
  sortField: "soldAt",
  pageNumber: 1,
  pageSize: 100,
};

export const initialValues = {
  seller: "",
  product: "",
  status: "",
  customer: "",
  location: "",
  type: "PERMIT",
  saleDateFrom: null,
  saleDateTo: null,
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "PERMIT",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 100,
  };
};
