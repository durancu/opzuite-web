import { createAction, createSelector, createSlice } from "@reduxjs/toolkit";
import { Profile } from "src/app/modules/profile/types";
import { API } from "src/redux/api";
import { RootState } from "src/redux/store";

interface AuthState {
  user: Partial<Profile> | undefined;
  accessToken: string;
}

const initialState: AuthState = { user: undefined, accessToken: "" };

export const authAPI = API.injectEndpoints({
  endpoints: (build) => ({
    login: build.mutation<any, Partial<{ email: string; password: string }>>({
      query: (data) => ({
        url: "/auth/login",
        method: "POST",
        body: data,
      }),
    }),
    register: build.mutation<
      any,
      Partial<{
        email: string;
        password: string;
        fullname: string;
        username: string;
      }>
    >({
      query: (data) => ({
        url: "/auth/register",
        method: "POST",
        body: data,
      }),
    }),
    getUser: build.query<Partial<Profile>, void>({
      query: () =>
        "/profile?withRole=true&withCompany=true&withLocation=false&withSupervisor=false",
    }),
    confirmEmailAddress: build.mutation<any, Partial<{ confirmToken: string }>>(
      {
        query: (data) => ({
          url: "/auth/confirm-email",
          method: "POST",
          body: data,
        }),
      }
    ),
    requestPassword: build.mutation<any, Partial<{ email: string }>>({
      query: (data) => ({
        url: "/auth/forgot-password",
        method: "POST",
        body: data,
      }),
    }),
    resetPassword: build.mutation<
      any,
      Partial<{ token: string; password: string }>
    >({
      query: (data) => ({
        url: "/auth/reset-password",
        method: "POST",
        body: data,
      }),
    }),
  }),
  overrideExisting: false,
});

export const { useLoginMutation, usePrefetch } = authAPI;

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    saveAuth: (state, { payload }) => {
      return {
        ...state,
        ...payload,
      };
    },
    clearAuth: () => initialState,
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      authAPI.endpoints.login.matchFulfilled,
      (state, { payload }) => {
        return {
          ...state,
          ...payload,
        };
      }
    );
    builder.addMatcher(
      authAPI.endpoints.getUser.matchFulfilled,
      (state, { payload }) => {
        return {
          ...state,
          user: payload,
        };
      }
    );
  },
});

// auth selector
export const authSelector = (state: RootState) => state.auth;
export const userSelector = createSelector(authSelector, (auth) => auth.user);

// logout action
export const logout = createAction("LOG_OUT");

export const { saveAuth, clearAuth } = authSlice.actions;
export default authSlice.reducer;
