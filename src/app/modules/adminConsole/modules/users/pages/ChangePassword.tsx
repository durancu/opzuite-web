import { useEffect } from "react";
import { Form, Formik } from "formik";
import { Modal, ProgressBar, Button } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { useParams, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import * as actions from "../redux/usersActions";
import { FormInput } from "src/app/components";

export const ChangePassword = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { actionsLoading, userForEdit } = useAppSelector((state: any) => ({
    actionsLoading: state.users.actionsLoading,
    userForEdit: state.users.userForEdit,
  }));

  function goBack() {
    navigate("/manager/users");
  }

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      goBack();
    }
    dispatch(actions.fetchUser(id));
  }, [id]);

  const updatePassword = (user: any) => {
    // server request for deleting user by id
    dispatch(actions.updateUser(user)).then(() => {
      // refresh list after deletion
      goBack();
    });
  };

  return (
    <Modal show onHide={goBack} centered backdrop="static">
      <Formik
        initialValues={userForEdit || {}}
        validationSchema={Yup.object().shape({
          password: Yup.string()
            .min(8, "Minimum 8 character")
            .required("Please enter a password"),
          confirmPassword: Yup.string().when("password", {
            is: (val: string) => (val && val.length > 0 ? true : false),
            then: Yup.string()
              .oneOf([Yup.ref("password")], "Both password need to be the same")
              .required("please confirm password"),
          }),
        })}
        onSubmit={(values) => {
          updatePassword({
            id: userForEdit.id,
            password: values.password,
          });
        }}
      >
        {() => {
          return (
            <Form>
              {actionsLoading && <ProgressBar variant="query" />}
              <Modal.Header closeButton>
                <Modal.Title>Change Password</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <FormInput
                  name="password"
                  labelText="Password"
                  type="password"
                  required
                />
                <br />
                <FormInput
                  name="confirmPassword"
                  labelText="Confirm Password"
                  type="password"
                  required
                />
              </Modal.Body>
              <Modal.Footer>
                <Button
                  type="button"
                  variant="light"
                  onClick={goBack}
                  disabled={actionsLoading}
                >
                  <FormattedMessage id="BUTTON.CANCEL" />
                </Button>
                <Button
                  type="submit"
                  variant="primary"
                  disabled={actionsLoading}
                >
                  <span className="mr-3">
                    <FormattedMessage id="BUTTON.SAVE_AND_CLOSE" />
                  </span>
                </Button>
              </Modal.Footer>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
};
