import { Card } from "react-bootstrap";
import { LocationsTable } from "src/app/modules/adminConsole/modules/locations/components/LocationsTable";
import { FilterUIProvider } from "../Filter";

interface Props {
  id: string;
}

export const CompanyLocations = ({ id }: Props) => {
  const defaultFilter = {
    filter: {
      location: id,
    },
    sortOrder: "asc",
    sortField: "name",
    pageNumber: 1,
    pageSize: 50,
  };

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="mb-3 text-primary">
        Locations
      </Card.Title>
      <p className="mb-8">This company locations.</p>

      <FilterUIProvider context="locations" defaultFilter={defaultFilter}>
        <LocationsTable showFilter={false} />
      </FilterUIProvider>
    </Card>
  );
};
