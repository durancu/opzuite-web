import { Col, Row, Card } from "react-bootstrap";
import { formatPhoneNumber } from "../functions";
import { AddressBlock } from "./AddressBlock";
import { FormattedMessage } from "react-intl";

interface Props {
  location: any;
}

export const LocationDetails = ({ location }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.DETAILS" />
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>
              {location && (
                <AddressBlock
                  address={location && location.business?.address}
                />
              )}
            </h4>
            <span>Address </span>
          </Col>
          <Col>
            <h4>{location && location.business?.email}</h4>
            <span>Email</span>
          </Col>
          <Col>
            <h4>
              {location && formatPhoneNumber(location.business?.primaryPhone)}
            </h4>
            <span>Phone number</span>
          </Col>
          <Col>
            <h4>{location && location.ipAddress}</h4>
            <span>IP Address</span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
