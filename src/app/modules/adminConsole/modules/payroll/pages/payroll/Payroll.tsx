import { Fragment, useMemo } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { isPermitted } from "src/app/utils";
import { breadcrumbs } from "../../../../breadcrumbs";
import * as payrollActions from "../../_redux/payrollsActions";
import { PayrollsTable } from "../../components/PayrollsTable";
import { defaultFilter } from "../../components/helpers";

export const Payroll = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);

  const filter = useMemo(() => {
    const filterByCountry = isPermitted(
      Permissions.MY_COMPANY,
      user?.permissions || []
    );

    return filterByCountry
      ? { ...defaultFilter, filter: { country: user?.country } }
      : defaultFilter;
  }, [user, defaultFilter, isPermitted]);

  const generatePayrolls = () => {
    dispatch(payrollActions.generateMonthPayroll());
  };

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Payroll</PageTitle>
      <Card>
        <Card.Header>
          <Card.Title className="fw-bolder">Payroll</Card.Title>
          <button
            onClick={generatePayrolls}
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.PAYROLL.GENERATE" />
          </button>
        </Card.Header>
        <Card.Body className="p-8">
          <FilterUIProvider
            context="sales"
            menuActions={menuActions}
            defaultFilter={filter}
          >
            <PayrollsTable />
          </FilterUIProvider>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
