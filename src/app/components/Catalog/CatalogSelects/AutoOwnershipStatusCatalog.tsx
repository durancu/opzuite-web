export const AUTO_OWNERSHIP_STATUS_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "OWNED", name: "Owned" },
  { id: "FINANCED", name: "Financed" },
  { id: "LEASED", name: "Leased" },
];
