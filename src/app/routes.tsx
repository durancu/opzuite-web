// dashboard and submodule routing
import { RouteObject } from "react-router-dom";
import { AdminLayout, AdminRoutes } from "./modules/adminConsole";
import { agentRoutes } from "./modules/agents";
import { carrierRoutes } from "./modules/insurers/carriers";
import { CertificateRoutes } from "./modules/certificates";
import { customerRoutes } from "./modules/customers";
import { financerRoutes } from "./modules/insurers/financers";
import { MGARoutes } from "./modules/insurers/mgas";
import { policyRoutes } from "./modules/sales/policies";
import { ProfileLayout, ProfileRoutes } from "./modules/profile";
import { serviceRoutes } from "./modules/sales/services";
import { Dashboard } from "./partials/dashboards/Dashboard";

export const AppRoutes: RouteObject[] = [
  { index: true, element: <Dashboard /> },
  {
    path: "policies",
    children: policyRoutes,
  },
  { path: "services", children: serviceRoutes },
  {
    path: "customers",
    children: customerRoutes,
  },
  { path: "carriers", children: carrierRoutes },
  { path: "mgas", children: MGARoutes },
  {
    path: "financers",
    children: financerRoutes,
  },
  { path: "manager", element: <AdminLayout />, children: AdminRoutes },
  { path: "profile", element: <ProfileLayout />, children: ProfileRoutes },
  { path: "agents", children: agentRoutes },
  { path: "certificates", children: CertificateRoutes },
];
