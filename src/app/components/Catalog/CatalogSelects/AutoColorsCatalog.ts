export const AUTO_COLORS_CATALOG = [
  { id: "WHITE", name: "White" },
  { id: "BLACK", name: "Black" },
  { id: "GRAY", name: "Gray" },
  { id: "SILVER", name: "Silver" },
  { id: "BLUE", name: "Blue" },
  { id: "RED", name: "Red" },
  { id: "GREEN", name: "Green" },
  { id: "BEIGE", name: "Beige" },
  { id: "BROWN", name: "Brown" },
  { id: "ORANGE", name: "Orange" },
  { id: "PURPLE", name: "Purple" },
  { id: "YELLOW", name: "Yellow" },
  { id: "BURGUNDY", name: "Burgundy" },
  { id: "MULTIPLE_TONE", name: "Multiple Tone" },
  { id: "OTHER", name: "Other" },
];
