/* eslint-disable no-script-url */
import { FormattedMessage } from "react-intl";

export const DateColumnHeaderFormatter = (
  column: any,
  index: any,
  { sortElement
  }: any
) => (

  < div className="d-flex gap-2 align-items-center" >
    {column.text || <FormattedMessage id="TABLE.HEADER.DATE" />}
    {sortElement}
  </div >
);
