import PropTypes from "prop-types";
import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { Permissions } from "src/app/constants/Permissions";
import { isPermitted } from "src/app/utils";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { useGetUserAuth } from "src/app/hooks";
import { customersAPI } from "../redux";
import { FilterForm } from "./FilterForm";
import { initialValues, prepareFilter, sizePerPageList } from "./helpers";

export const CustomersTable = ({ showFilter = true }) => {
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const user = useGetUserAuth();
  const { states } = useGetCatalog();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = customersAPI.useGetAllCustomersQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });
  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.NameColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedCustomerWithLinkColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
        idField: "code",
        textField: "name",
        defaultText: "N/A",
      },
      align: "left",
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "usDOT",
      text: "DOT",
      headerFormatter: headerFormatters.DOTColumnHeaderFormatter,
      formatter: columnFormatters.DOTColumnFormatter,
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
    },
    {
      dataField: "type",
      text: "Type",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.TypeColumnHeaderFormatter,
      formatter: columnFormatters.CustomerTypeColumnFormatter,
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
    },
    {
      dataField: "state",
      text: "State",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.StateColumnHeaderFormatter,
      formatter: columnFormatters.USAStateColumnFormatter,
      formatExtraData: {
        states,
      },
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
    },
    {
      dataField: "createdBy",
      text: "Coordinator",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CoordinatorColumnHeaderFormatter,
      formatter: columnFormatters.CreatedByAndLocationColumnFormatter,
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
    },
    {
      dataField: "createdAt",
      text: "Created",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.DateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
    },
    {
      dataField: "action",
      text: "",
      formatter: columnFormatters.CustomerActionsColumnFormatter,
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      align: "left",
      hidden: !isPermitted(
        [
          Permissions.CUSTOMERS_UPDATE,
          Permissions.CUSTOMERS_DELETE,
          Permissions.CUSTOMERS_READ,
        ],
        user.permissions
      ),
      formatExtraData: {
        openEditCustomerPage: menuActions.edit,
        openDeleteCustomerDialog: menuActions.delete,
        openDetailCustomerPage: menuActions.details,
        openCertificates: menuActions.certificates,
        user: {
          id: user.id,
          role: user.role,
        },
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;
  return (
    <Fragment>
      {showFilter && (
        <FilterPanel
          initialValues={initialValues}
          prepareFilter={prepareFilter}
          filterForm={<FilterForm />}
        />
      )}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table"
                bordered={false}
                remote
                keyField="id"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "createdAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

CustomersTable.propTypes = {
  showFilter: PropTypes.bool,
};
