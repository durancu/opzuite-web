import { camelCase, startCase } from "lodash";
import { Card, Col, Row } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

interface Props {
  carrier: any;
}

export const CarrierOverview = ({ carrier }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body className="text-center">
        <Row>
          <Col>
            <h4>{carrier && startCase(camelCase(carrier.type))}</h4>
            <span>Type</span>
          </Col>
          <Col>
            <h4>{carrier && carrier.name}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>{(carrier && carrier.email) || "-"}</h4>
            <span>Email</span>
          </Col>
          <Col>
            <h4>{carrier && (carrier.phone || "-")}</h4>
            <span>Phone number </span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
