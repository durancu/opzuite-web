import { FormattedMessage } from "react-intl";

export const MetricsHeader = () => {
  return (
    <div className="d-flex flex-row mt-3">
      <h4 className="card-title flex-column mt-2">
        <span className="card-label font-weight-bold">
          <FormattedMessage id="GENERAL.METRICS" />
        </span>
      </h4>
    </div>
  );
};
