import { Insurer } from "../interfaces/Insurer";
import { Sale } from "../interfaces/Sale";
import { calculatePermitMetrics } from "./calculatePermitMetrics";

export const refreshPermitMetrics = async (
  sale: Partial<Sale>,
  providers: Partial<Insurer>[],
  setFieldValue: (e: string, a: any) => void
): Promise<any> => {
  const recalculatedSale: Partial<Sale> = await calculatePermitMetrics(sale);

  recalculatedSale.checksumSanity !== sale.checksumSanity &&
    setFieldValue("checksumSanity", recalculatedSale.checksumSanity);
  recalculatedSale.totalAgentCommission !== sale.totalAgentCommission &&
    setFieldValue(
      "totalAgentCommission",
      recalculatedSale.totalAgentCommission
    );
  recalculatedSale.totalAgentCommission !== sale.totalAgentCommission &&
    setFieldValue(
      "totalAgentCommission",
      recalculatedSale.totalAgentCommission
    );
  recalculatedSale.totalFinanced !== sale.totalFinanced &&
    setFieldValue("totalFinanced", recalculatedSale.totalFinanced);
  recalculatedSale.totalFinancedPaid !== sale.totalFinancedPaid &&
    setFieldValue("totalFinancedPaid", recalculatedSale.totalFinancedPaid);
  recalculatedSale.totalPayables !== sale.totalPayables &&
    setFieldValue("totalPayables", recalculatedSale.totalPayables);
  recalculatedSale.totalPaid !== sale.totalPaid &&
    setFieldValue("totalPaid", recalculatedSale.totalPaid);
  recalculatedSale.totalReceivables !== sale.totalReceivables &&
    setFieldValue("totalReceivables", recalculatedSale.totalReceivables);
  recalculatedSale.totalReceived !== sale.totalReceived &&
    setFieldValue("totalReceived", recalculatedSale.totalReceived);
  recalculatedSale.totalPremium !== sale.totalPremium &&
    setFieldValue("totalPremium", recalculatedSale.totalPremium);
  recalculatedSale.totalNonPremium !== sale.totalNonPremium &&
    setFieldValue("totalNonPremium", recalculatedSale.totalNonPremium);
  recalculatedSale.totalTaxesAndFees !== sale.totalTaxesAndFees &&
    setFieldValue("totalTaxesAndFees", recalculatedSale.totalTaxesAndFees);
  recalculatedSale.totalCoveragesPremium !== sale.totalCoveragesPremium &&
    setFieldValue(
      "totalCoveragesPremium",
      recalculatedSale.totalCoveragesPremium
    );
  recalculatedSale.totalCoveragesDownPayment !==
    sale.totalCoveragesDownPayment &&
    setFieldValue(
      "totalCoveragesDownPayment",
      recalculatedSale.totalCoveragesDownPayment
    );
  recalculatedSale.totalPermits !== sale.totalPermits &&
    setFieldValue("totalPermits", recalculatedSale.totalPermits);
};
