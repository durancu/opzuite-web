import { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import { FormAutoComplete } from "src/app/components";
import { useAppDispatch } from "src/app/hooks";

export const FilterForm = () => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  const { currentState } = useSelector(
    (state: any) => ({
      currentState: state.catalogs?.entities,
    }),
    shallowEqual
  );

  return (
    <FormAutoComplete
      name="location"
      labelText="Location"
      options={currentState?.locations}
      labelField="name"
    />
  );
};
