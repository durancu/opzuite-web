import { Fragment } from "react";

export const EnhancedCustomerWithLinkColumnFormatter = (
  cellContent: any,
  customer: any,
  rowIndex: any,
  {
    openDetailPageFunction,
    idField,
    textField,
    defaultText = "",
  }: any
) => (
  <Fragment>
    <a
      className="text-dark fw-semibold text-hover-primary fs-6 cursor-pointer"
      onClick={() => {
        openDetailPageFunction(customer[idField]);
      }}
    >
      {customer[textField] || defaultText}
    </a>
  </Fragment>
);
