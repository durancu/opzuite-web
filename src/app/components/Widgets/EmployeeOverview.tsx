import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { shallowEqual, useSelector } from "react-redux";

interface Props {
  employee: any;
  catalogs: any;
}

export const EmployeeOverview = (props: Props) => {
  const { employee } = props;

  const { currentState } = useSelector(
    (state: any) => ({ currentState: state.catalogs }),
    shallowEqual
  );

  const { entities }: any = currentState;

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body>
        <Row className="text-center">
          <Col>
            <h4>{employee && (employee?.name || "-")}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>
              {employee &&
                (entities?.locations.find(
                  ({ id }: any) => id === employee?.location
                )?.name ||
                  "-")}
            </h4>
            <FormattedMessage id="FORM.LABELS.LOCATION" />
          </Col>
          <Col>
            <h4>{employee && (employee?.employeeInfo.position || "-")}</h4>
            <FormattedMessage id="FORM.LABELS.POSITION" />
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
