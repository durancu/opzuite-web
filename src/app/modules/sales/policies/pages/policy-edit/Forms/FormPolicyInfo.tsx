import { Fragment, useEffect } from "react";
import { useFormikContext } from "formik";
import { Col, Row } from "react-bootstrap";
import { useIntl } from "react-intl";
import { Toolbar } from "src/_metronic/layout/core";
import { FormInput, FormSelect, RequirePermission } from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  AgentAutocompleteField,
  CustomerAutocompleteField,
  PolicyStatusSelectField,
} from "src/app/components/FormControls";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch, useGetUserAuth } from "src/app/hooks";
import { DatePickerCustom } from "src/app/partials/controls";
import { expirationDateByRenewalFrequency } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { AddCustomerMenu } from "src/app/modules/sales/components/AddCustomerMenu";
import { saleActions } from "src/app/modules/sales/redux/saleSlice";

interface Props {
  policyType: "edit" | "renew" | undefined;
}

const FormPolicyInfo = ({ policyType }: Props) => {
  const intl = useIntl();
  const dispatch = useAppDispatch();
  const user = useGetUserAuth();
  const { renewalFrequencies } = useGetCatalog();
  const { values, setFieldValue } = useFormikContext<any>();

  /*
   * optionally skip and create customer before resuming policy creation
   * we save the policy values in redux to be able to resume the policy creation
   */
  const cachePolicy = () => {
    dispatch(
      saleActions.save({
        type: "savedPolicy",
        values,
      })
    );
  };

  useEffect(() => {
    if (user && user.role.hierarchy > 30) {
      setFieldValue("seller", user.id);
    }
  }, [setFieldValue, user]);

  useEffect(() => {
    if (values.soldAt) {
      setFieldValue("endorsements.0.endorsedAt", values.soldAt);
      setFieldValue("endorsements.0.items.0.endorsedAt", values.soldAt);
    }

    if (values.seller) {
      setFieldValue("endorsements.0.seller", values.seller);
      setFieldValue("endorsements.0.items.0.seller", values.seller);
    }

    if (values.renewalFrequency && values.effectiveAt) {
      const expiresAt: any = expirationDateByRenewalFrequency(
        values.renewalFrequency,
        values.effectiveAt
      );
      setFieldValue("expiresAt", expiresAt);
    }
  }, [
    values.soldAt,
    values.seller,
    values.renewalFrequency,
    values.effectiveAt,
  ]);

  return (
    /* BASIC INFO */
    <Fragment>
      {policyType !== "renew" && (
        <Toolbar>
          <AddCustomerMenu onAdd={cachePolicy} />
        </Toolbar>
      )}

      <Row className="mb-4">
        <Col>
          <CustomerAutocompleteField disabled={policyType === "renew"} />
        </Col>

        <RequirePermission
          permissions={[
            Permissions.MY_LOCATION,
            Permissions.MY_COUNTRY,
            Permissions.MY_COMPANY,
          ]}
        >
          <Col>
            <AgentAutocompleteField />
          </Col>
        </RequirePermission>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormInput name="number" labelText="Policy Number" />
        </Col>
        <Col>
          <DatePickerCustom
            label={
              policyType === "renew"
                ? intl.formatMessage({ id: "FORM.LABELS.RENEWAL_DATE" })
                : intl.formatMessage({ id: "FORM.LABELS.SOLD_AT" })
            }
            name="soldAt"
            maxDate={new Date()}
            helperTextId="FORM.LABELS.HELPER.SOLD_AT"
          />
        </Col>
        <Col>
          <PolicyStatusSelectField />
        </Col>
        <Col hidden={values.status !== "CANCELLED"}>
          <DatePickerCustom
            hidden={values.status !== "CANCELLED"}
            label="Policy Cancelled At"
            name="cancelledAt"
            maxDate={new Date()}
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <DatePickerCustom label="Policy Effective At" name="effectiveAt" />
        </Col>
        <Col>
          <DatePickerCustom label="Policy Expires At" name="expiresAt" />
        </Col>
        <Col>
          <FormSelect
            name="renewalFrequency"
            labelText="Renewal Frequency"
            options={renewalFrequencies}
          />
        </Col>
      </Row>
    </Fragment>
  );
};

export default FormPolicyInfo;
