import { Step, StepLabel, Stepper } from "@mui/material";
import { useWizardUIContext } from "./WizardUIContext";

export const WizardHorizontalStepper = () => {
  const config = useWizardUIContext();
  return (
    <Stepper alternativeLabel activeStep={config.activeStep}>
      {config.steps.map((label) => (
        <Step key={label}>
          <StepLabel>{label}</StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

export default WizardHorizontalStepper;
