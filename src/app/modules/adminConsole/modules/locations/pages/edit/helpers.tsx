type InitLocation = {
  alias: string;
  company: string;
  manager: string;
  ipAddress: string;
  business: {
    address: {
      address1: string;
      address2: string;
      city: string;
      state: string;
      country: string;
      zip: string;
    };
    email: string;
    fax: string;
    industry: string;
    logo: string;
    name: string;
    otherPhones: string[];
    primaryPhone: string | null;
    primaryPhoneExtension: string;
    secondaryPhone: string | null;
    secondaryPhoneExtension: string;
    sector: string;
    startedAt: string | null;
    type: string;
    website: string;
  };
  payFrequency: string;
};

const initLocation: InitLocation = {
  alias: "",
  company: "",
  manager: "",
  ipAddress: "",
  business: {
    address: {
      address1: "",
      address2: "",
      city: "",
      state: "",
      country: "USA",
      zip: "",
    },
    email: "",
    fax: "",
    industry: "",
    logo: "",
    name: "",
    otherPhones: [""],
    primaryPhone: null,
    primaryPhoneExtension: "",
    secondaryPhone: null,
    secondaryPhoneExtension: "",
    sector: "",
    startedAt: null,
    type: "",
    website: "",
  },
  payFrequency: "MONTHLY",
};

export const locationInitData = (): InitLocation => {
  return initLocation;
};

export const locationSteps = (): string[] => {
  return ["Location Info"];
};
