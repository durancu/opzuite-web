import { camelCase, startCase } from "lodash";
import { Col, Row, Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

interface Props {
  customer: Customer;
}

export const CustomerOverview = ({ customer }: Props) => {
  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Card.Body>
        <Row className="my-0 text-center">
          <Col>
            <h4>{customer && startCase(camelCase(customer.type))}</h4>
            <FormattedMessage id="FORM.LABELS.TYPE" />
          </Col>
          <Col>
            <h4>{customer && customer.name}</h4>
            <FormattedMessage id="FORM.LABELS.NAME" />
          </Col>
          <Col>
            <h4>{(customer && customer.email) || "-"}</h4>
            <span>Email</span>
          </Col>

          <Col>
            <h4>{customer && (customer.phone || "-")}</h4>
            <span>Phone number </span>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
