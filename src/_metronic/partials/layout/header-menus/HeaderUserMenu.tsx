import { Fragment } from "react";
import { Link } from "react-router-dom";
import { Languages } from "./Languages";
import { toAbsoluteUrl } from "../../../helpers";
import { useGetProfileQuery } from "src/app/modules/profile/redux";
import { logout } from "src/app/modules/auth/redux";
import { useAppDispatch } from "src/app/hooks";
export const HeaderUserMenu = () => {
  const dispatch = useAppDispatch();
  const { data: user = {} } = useGetProfileQuery();

  function handleLogout() {
    dispatch(logout());
  }

  return (
    <Fragment>
      <button
        className="btn btn-icon cursor-pointer symbol symbol-35px symbol-md-40px"
        data-kt-menu-trigger="{default:'click', lg: 'hover'}"
        data-kt-menu-attach="parent"
        data-kt-menu-placement="bottom-end"
      >
        <img
          src={
            user?.profilePhoto || toAbsoluteUrl("/media/avatars/default.png")
          }
          alt=""
        />
      </button>
      <div
        className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
        data-kt-menu="true"
      >
        <div className="menu-item px-3">
          <div className="menu-content d-flex align-items-center px-3">
            <div className="symbol symbol-50px me-5">
              <img
                alt="Logo"
                src={
                  user?.profilePhoto ||
                  toAbsoluteUrl("/media/avatars/default.png")
                }
              />
            </div>

            <div className="d-flex flex-column">
              <div className="fw-bolder d-flex align-items-center fs-5">
                {user?.firstName} {user?.lastName}
                <span className="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">
                  Pro
                </span>
              </div>
              <a
                href="#"
                className="fw-bold text-muted text-hover-primary fs-7"
              >
                {user?.email}
              </a>
            </div>
          </div>
        </div>

        <div className="separator my-2"></div>

        <div className="menu-item px-5">
          <Link to="profile" className="menu-link px-5">
            My Profile
          </Link>
        </div>

        <div className="separator my-2"></div>

        <Languages />

        <div className="menu-item px-5 my-1">
          <Link to="profile/settings" className="menu-link px-5">
            Account Settings
          </Link>
        </div>

        <div className="menu-item px-5">
          <button className="btn menu-link px-5 w-100" onClick={handleLogout}>
            Sign Out
          </button>
        </div>
      </div>
    </Fragment>
  );
};
