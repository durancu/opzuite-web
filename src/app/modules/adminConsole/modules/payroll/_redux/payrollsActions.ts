import { Dispatch } from "redux";
import { variantAccordingToHttpCode } from "../../../../../components/Feedback";
import { feedbacksSlice } from "../../../../../components/Feedback/_redux/feedbackSlice";
import * as requestFromServer from "./payrollsCrud";
import { callTypes, payrollsSlice } from "./payrollsSlice";

const { actions } = payrollsSlice;

export const fetchPayrolls = (queryParams: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.list } as any));
  return requestFromServer
    .findPayrolls(queryParams)
    .then((response) => {
      const { totalCount, entities } = response.data;
      dispatch(actions.payrollsFetched({ totalCount, entities } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find payrolls";
      dispatch(actions.catchError({ error, callType: callTypes.list } as any));
    });
};

export const fetchPayroll = (code: any) => (dispatch: Dispatch) => {
  if (!code) {
    return dispatch(
      actions.payrollFetched({ payrollForEdit: undefined } as any)
    );
  }

  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getPayrollByCode(code)
    .then((response) => {
      const payroll = response.data;
      dispatch(actions.payrollFetched({ payrollForEdit: payroll } as any));
    })
    .catch((error) => {
      error.clientMessage = "Can't find payroll";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const deletePayroll = (code: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deletePayroll(code)
    .then((response) => {
      dispatch(actions.payrollDeleted({ code } as any));
      dispatch(
        feedbacksSlice.actions.feedbackSetMessage({
          message: "Payroll deleted successfully.",
          httpCode: response.status,
        })
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete payroll";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const getPayrollAvailableDates =
  (locationId: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .getPayrollAvailableDates(locationId)
      .then((response) => {
        const payroll = response.data;
        dispatch(actions.payrollAvailableDates({ payroll } as any));
      })
      .catch((error) => {
        error.clientMessage = "Can't get any available dates for this payroll";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };

export const initializePayroll =
  (payrollForCreation: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .initializePayroll(payrollForCreation)
      .then((response) => {
        const payroll = response.data;
        dispatch(actions.payrollInitialized({ payroll } as any));
        dispatch(
          feedbacksSlice.actions.feedbackSetMessage({
            message: "Payroll initialized successfully.",
            httpCode: response.status,
          })
        );

        return payroll;
      })
      .catch((error) => {
        error.clientMessage = "Can't initialize payroll";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };

export const createPayroll =
  (payrollForCreation: any) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .createPayroll(payrollForCreation)
      .then((response) => {
        const payroll = response.data;
        dispatch(actions.payrollCreated({ payroll } as any));
        dispatch(
          feedbacksSlice.actions.feedbackSetMessage({
            message: "Payroll created successfully.",
            httpCode: response.status,
          })
        );
        return payroll;
      })
      .catch((error) => {
        error.clientMessage = "Can't create payroll";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };

export const generateMonthPayroll = () => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .generateMonthPayrolls()
    .then((response) => {
      const payroll = response.data;
      dispatch(actions.payrollsCreated());
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: "Payrolls created successfully.",
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(response.status),
          },
        } as any)
      );
      return payroll;
    })
    .catch((error) => {
      error.clientMessage = "Can't create payrolls";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: error.clientMessage,
          options: {
            key: new Date().getTime() + Math.random(),
            variant: variantAccordingToHttpCode(
              error?.response?.data?.statusCode || 404
            ),
          },
        } as any)
      );
    });
};

export const executePayroll = (payrollID: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .executePayroll(payrollID)
    .then((response) => {
      //const payroll = response.data;
      dispatch(actions.payrollExecuted());
      dispatch(
        feedbacksSlice.actions.feedbackSetMessage({
          message: "Payroll executed successfully.",
          httpCode: response.status,
        })
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't create payroll";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const updatePayroll = (payroll: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .updatePayroll(payroll)
    .then((response) => {
      dispatch(actions.payrollUpdated({ payroll } as any));
      dispatch(
        feedbacksSlice.actions.feedbackSetMessage({
          message: "Payroll updated successfully.",
          httpCode: response.status,
        })
      );
      return payroll;
    })
    .catch((error) => {
      error.clientMessage = "Can't update payroll";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const deletePayrolls = (ids: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .deletePayrolls(ids)
    .then((response) => {
      dispatch(actions.payrollsDeleted({ ids } as any));
      dispatch(
        feedbacksSlice.actions.feedbackSetMessage({
          message: "Payrolls deleted successfully.",
          httpCode: response.status,
        })
      );
    })
    .catch((error) => {
      error.clientMessage = "Can't delete payrolls";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};
