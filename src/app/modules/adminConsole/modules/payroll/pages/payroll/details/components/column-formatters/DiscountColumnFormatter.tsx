import { formatAmount } from "../../../../../../../../../components/functions";

export const DiscountColumnFormatter = (cellContent: any) => (
  <>({formatAmount(cellContent)})</>
);
