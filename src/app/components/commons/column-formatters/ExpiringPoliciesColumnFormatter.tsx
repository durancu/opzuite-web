import { nanoid } from "@reduxjs/toolkit";
import { truncate } from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";

export const ExpiringPoliciesColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { coverages }: any
) => {
  return (
    <div style={{ width: "100%", flexWrap: "wrap" }} className="d-inline-flex">
      {!!row.policies &&
        row.policies.map((policy: any) => {
          const product = coverages?.find(
            (type: any) => type["id"] === policy.product
          );

          const sameCarrierAndBroker: boolean =
            policy.brokerName &&
            policy.carrierName &&
            policy.carrierName === policy.brokerName;
          const providerName: string = sameCarrierAndBroker
            ? policy.brokerName
            : "";
          const providerLabel: any = sameCarrierAndBroker ? (
            <>
              <FormattedMessage id="FORM.LABELS.MGA" />/
              <FormattedMessage id="FORM.LABELS.CARRIER" />
            </>
          ) : (
            <>
              <FormattedMessage id="FORM.LABELS.MGA" />
            </>
          );
          const showConditions: any =
            !policy.deleted &&
            (policy.carrierName.length > 0 || policy.brokerName.length > 0);

          return (
            showConditions && (
              <>
                <div className="inline" key={nanoid()}>
                  <div className="p-0 mx-1 my-1 d-inline-flex">
                    <OverlayTrigger
                      placement="top"
                      overlay={
                        <Tooltip id="quick-actions-tooltip">
                          <strong>{product?.name}</strong>
                          <br />
                          {product?.description}
                        </Tooltip>
                      }
                    >
                      <span className="pt-2 pb-1 px-2 inline badge-product-icon rounded-left">
                        <i className={`fa fa-${product?.iconClass}`} />
                      </span>
                    </OverlayTrigger>
                    <OverlayTrigger
                      placement="top"
                      overlay={
                        <Tooltip id="quick-actions-tooltip">
                          <span className="font-weight-bolder">
                            {providerLabel} :
                          </span>{" "}
                          <span className="font-weight-boldest">
                            {providerName}
                          </span>
                        </Tooltip>
                      }
                    >
                      <span
                        hidden={policy.brokerName?.length === 0}
                        className="text-truncate py-1 px-2 inline badge-broker symbol-label font-weight-bold"
                      >
                        {truncate(policy.brokerName, {
                          length: 12,
                        })}
                      </span>
                    </OverlayTrigger>

                    {!sameCarrierAndBroker && (
                      <>
                        <span
                          hidden={policy.brokerName?.length === 0}
                          className="text-truncate px-1 pt-1 inline badge-product-arrow symbol-label"
                        >
                          <i className="fa fa-chevron-right" />
                        </span>

                        <OverlayTrigger
                          placement="top"
                          overlay={
                            <Tooltip id="quick-actions-tooltip">
                              <span className="font-weight-bolder">
                                <FormattedMessage id="FORM.LABELS.CARRIER" /> :
                              </span>{" "}
                              <span className="font-weight-boldest">
                                {policy.carrierName}
                              </span>
                            </Tooltip>
                          }
                        >
                          <span
                            hidden={!policy.carrierName}
                            className="text-truncate py-1 px-2 inline badge-carrier symbol-label font-size-small font-weight-bold rounded-right"
                          >
                            {truncate(policy.carrierName, {
                              length: 12,
                            })}
                          </span>
                        </OverlayTrigger>
                      </>
                    )}
                  </div>
                </div>
              </>
            )
          );
        })}
    </div>
  );
};
