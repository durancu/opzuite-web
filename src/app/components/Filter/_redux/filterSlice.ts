import { createSlice } from "@reduxjs/toolkit";

const initialCustomersState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  filterForEdit: undefined,
  lastError: null,
  dashboardParams: undefined,
};
export const callTypes = {
  list: "list",
  action: "action",
};

export const filtersSlice = createSlice({
  name: "filters",
  initialState: initialCustomersState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // findCustomers
    filtersGetAll: (state: any, action: any) => {
      const entities = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
    },
    filtersSetAll: (state: any, action: any) => {
      const entities = action.payload;
      state.listLoading = false;
      state.error = null;
      state.filterForEdit = entities.filterForEdit;
    },
    filtersClearAll: (state: any, action: any) => {
      const entities = action.payload;
      state.listLoading = false;
      state.error = null;
      state.filterForEdit = entities.filterForEdit;
    },
    setDashboardParams: (state: any, action: any) => {
      state.dashboardParams = action.payload;
      state.error = null;
      state.actionsLoading = false;
    },
  },
});
