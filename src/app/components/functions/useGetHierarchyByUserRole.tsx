import { useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { getHierarchyRoleByUserAuth } from ".";
import * as actionCatalogs from "../Catalog/redux/catalogActions";

export const useGetHierarchyByUserRole = () => {
  const { catalogs, userRole } = useSelector(
    (state: any) => ({
      catalogs: state.catalogs?.entities,
      userRole: state.auth?.user?.employeeInfo.category,
    }),
    shallowEqual
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(actionCatalogs.getAllCatalog());
  }, [dispatch]);

  return getHierarchyRoleByUserAuth(userRole, catalogs?.roles);
};
