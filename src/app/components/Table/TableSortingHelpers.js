import clsx from "clsx";

export const sortCaret = (order) => (
  <div className="d-flex align-items-center">
    <i
      className={clsx(
        "bi text-dark fs-6 mt-1 p-0",
        order === "asc" ? "bi-caret-up-fill" : "bi-caret-up"
      )}
    ></i>
    <i
      className={clsx(
        "bi text-dark fs-6 mt-1 p-0",
        order === "desc" ? "bi-caret-down-fill" : "bi-caret-down"
      )}
    ></i>
  </div>
);
