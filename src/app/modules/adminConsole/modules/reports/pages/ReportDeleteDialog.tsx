import { useEffect } from "react";
import { Modal, ProgressBar } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import * as actions from "../_redux/reportsActions";

export const ReportDeleteDialog = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const isLoading = useAppSelector(
    (state: any) => state.reports.actionsLoading
  );

  function goBack() {
    navigate("/manager/reports");
  }

  // if !code we should close modal
  useEffect(() => {
    if (!code) {
      goBack();
    }
  }, [code]);

  // looking for loading/dispatch

  const deleteReport = () => {
    // server request for deleting report by code
    dispatch(actions.deleteReport(code)).then(() => {
      // refresh list after deletion
      goBack();
    });
  };

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete-report"
      centered
      backdrop="static"
    >
      {isLoading && <ProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="delete-report">Report Delete</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this report?</span>
        )}
        {isLoading && <span>Report is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={goBack}
            className="btn btn-light btn-elevate"
          >
            <FormattedMessage id="BUTTON.CANCEL" />
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteReport}
            className="btn btn-danger btn-elevate"
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
};
