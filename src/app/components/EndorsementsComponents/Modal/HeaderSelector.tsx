import { EndorsementType } from "../EndorsementTypeAndEnum";
import { useEndorsementUIContext } from "../EndorsementUIContext";
import { HeaderApplication } from "./HeaderType/HeaderApplication";
import { HeaderContainer } from "./HeaderType/HeaderContainer";
import { HeaderItem } from "./HeaderType/HeaderItem";

export const HeaderSelector = () => {
  const { endorsementType } = useEndorsementUIContext();

  switch (endorsementType) {
    case EndorsementType.CONTAINER:
      return <HeaderContainer />;
    case EndorsementType.ITEM:
      return <HeaderItem />;
    case EndorsementType.APPLICATION:
      return <HeaderApplication />;

    default:
      return <></>;
  }
};
