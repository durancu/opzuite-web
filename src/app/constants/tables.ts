import { SizePerPageList } from "react-bootstrap-table";

export const sizePerPageList: SizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

export const defaultSortOrder = [{ dataField: "createdAt", order: "desc" }];

export const BASE_FILTER_OPTIONS = {
  filter: {},
  sortOrder: "desc",
  sortField: "createdAt",
  pageNumber: 1,
  pageSize: 20,
  searchText: "",
};
