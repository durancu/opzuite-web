import { isEqual, isFunction } from "lodash";
import { createContext, useCallback, useContext, useState } from "react";
import { initialFilter } from "./DashboardsUIHelpers";

const DashboardsUIContext = createContext(null);

export function useDashboardsUIContext() {
  return useContext(DashboardsUIContext);
}

export const DashboardsUIConsumer = DashboardsUIContext.Consumer;

export function DashboardsUIProvider({ dashboardsUIEvents, children }: any) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams: any) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value: any = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newDashboardButtonClick: dashboardsUIEvents.newDashboardButtonClick,
    openEditDashboardPage: dashboardsUIEvents.openEditDashboardPage,
    openDetailDashboardPage: dashboardsUIEvents.openDetailDashboardPage,
    openDeleteDashboardDialog: dashboardsUIEvents.openDeleteDashboardDialog,
    openDeleteDashboardsDialog: dashboardsUIEvents.openDeleteDashboardsDialog,
    openFetchDashboardsDialog: dashboardsUIEvents.openFetchDashboardsDialog,
    openUpdateDashboardsStatusDialog:
      dashboardsUIEvents.openUpdateDashboardsStatusDialog,
  };

  return (
    <DashboardsUIContext.Provider value={value}>
      {children}
    </DashboardsUIContext.Provider>
  );
}
