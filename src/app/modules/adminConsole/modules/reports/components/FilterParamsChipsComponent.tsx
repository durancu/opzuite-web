import { Chip } from "@mui/material";
import { Col, Row } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { listingChipGenerator } from "./listingChipGenerator";

export const FilterParamsChipsComponent = () => {
  const { catalog, filterParams } = useSelector(
    (state: any) => ({
      catalog: state.catalogs?.entities,
      filterParams: state.reports.appliedParams,
    }),
    shallowEqual
  );

  return (
    <Row
      className="mt-3 d-flex align-items-center gap-4"
      hidden={
        !(
          filterParams?.filters?.length > 0 ||
          filterParams?.dateFrom ||
          filterParams?.dateTo
        )
      }
    >
      <Col>
        <span className="">Filters: </span>
        {listingChipGenerator(filterParams || {}, catalog).map(
          ({ label }: any) => (
            <Chip label={label} key={label} size="small" />
          )
        )}
      </Col>
    </Row>
  );
};
