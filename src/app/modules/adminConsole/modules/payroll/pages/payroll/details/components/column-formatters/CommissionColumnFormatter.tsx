import {
  formatAmount,
  roundAmount,
} from "../../../../../../../../../components/functions";

export const CommissionColumnFormatter = (cellContent: any, row: any) => (
  <>
    {formatAmount(
      roundAmount(row.totalSaleBonus) +
        roundAmount(row.totalExtraBonus) +
        roundAmount(row.totalPermits * 0.2) +
        roundAmount(row.totalFees * 0.2)
    )}
  </>
);
