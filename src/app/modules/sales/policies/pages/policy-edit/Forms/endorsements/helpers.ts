export const endorsement = {
  amount: 0,
  company: "",
  createdBy: "",
  description: "",
  endorsedAt: new Date(),
  followUpDate: new Date(),
  items: [],
  sale: "",
  seller: "",
  status: "COMPLETE",
  totalAgencyCommission: 0,
  totalAgentCommission: 0,
  totalFinanced: 0,
  totalFinancedPaid: 0,
  totalNonCommissionablePremium: 0,
  totalNonPremium: 0,
  totalPaid: 0,
  totalPayables: 0,
  totalPremium: 0,
  totalReceivables: 0,
  totalReceived: 0,
  totalTaxesAndFees: 0,
  type: "",
  deleted: false,
  code: "",
  createdAt: "",
  updatedAt: "",
  updatedBy: "",
  id: "",
};

export function initEndorsement(sellerId: string) {
  return {
    ...endorsement,
    seller: sellerId,
  };
}
