import { useFormikContext } from "formik";
import { Fragment, useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import {
  CountriesAutocompleteField,
  RolesAutocompleteField,
  StatesAutocompleteField,
} from "src/app/components/FormControls";
import { DatePickerCustom } from "src/app/partials/controls";
import { FormInput, PhoneNumberInput } from "src/app/components";
const FormUserBasicInfo = () => {
  const { values }: any = useFormikContext();

  const [isUSALocation, setIsUSALocation]: any = useState(true);

  useEffect(() => {
    setIsUSALocation(values?.address?.country === "USA");
  }, [values?.address?.country]);

  return (
    <Fragment>
      <Row className="mb-4">
        <Col md={3}>
          <FormInput name="firstName" labelTextId="FORM.LABELS.FIRST_NAME" />
        </Col>
        <Col md={3}>
          <FormInput name="lastName" labelTextId="FORM.LABELS.LAST_NAME" />
        </Col>
        <Col md={3}>
          <DatePickerCustom name="dob" labelTextId="FORM.LABELS.DOB" />
        </Col>
        <Col md={3}>
          <PhoneNumberInput
            name="mobilePhone"
            labelTextId="FORM.LABELS.MOBILE_NUMBER"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col md={3}>
          <CountriesAutocompleteField fieldName="address.country" />
        </Col>
        <Col md={3}>
          <RolesAutocompleteField
            fieldName="role"
            helperTextId="FORM.LABELS.HELPER.ROLE"
          />
        </Col>
        <Col md={3}>
          <FormInput name="email" labelTextId="FORM.LABELS.EMAIL" />
        </Col>
        <Col md={3}>
          <FormInput name="address.zip" labelTextId="FORM.LABELS.ZIP_CODE" />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col md={3}>
          <FormInput
            name="address.address1"
            labelTextId="FORM.LABELS.ADDRESS_LINE_1"
          />
        </Col>
        <Col md={3}>
          <FormInput
            name="address.address2"
            labelTextId="FORM.LABELS.ADDRESS_LINE_2"
          />
        </Col>
        <Col md={3}>
          <FormInput name="address.city" labelTextId="FORM.LABELS.CITY" />
        </Col>
        {!isUSALocation && (
          <Col md={3}>
            <StatesAutocompleteField
              fieldName="address.state"
              labelTextId="FORM.LABELS.STATE"
            />
          </Col>
        )}
        {isUSALocation && (
          <Col md={3}>
            <StatesAutocompleteField fieldName="address.state" />
          </Col>
        )}
      </Row>
    </Fragment>
  );
};

export default FormUserBasicInfo;
