import { Box, Collapse, IconButton } from "@mui/material";
import { Close } from "@material-ui/icons";
import { Alert, AlertTitle } from "@material-ui/lab";
import React from "react";
import { FormattedMessage } from "react-intl";

export function WelcomeAboardWidget({ name }: any) {
  const [open, setOpen] = React.useState(true);

  return (
    <Box className="mb-6">
      <Collapse in={open}>
        <Alert
          severity="info"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setOpen(false);
              }}
            >
              <Close />
            </IconButton>
          }
        >
          <AlertTitle>
            <FormattedMessage
              id="CONTENTS.HOME.WELCOME.TITLE"
              values={{ name: name }}
            />
          </AlertTitle>
          <FormattedMessage id="CONTENTS.HOME.WELCOME.PARAGRAPH1" />
        </Alert>
      </Collapse>
    </Box>
  );
}
