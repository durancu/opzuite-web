import { Navigate, RouteObject } from "react-router-dom";
import { locationRoutes } from "./modules/locations";
import { payrollRoutes } from "./modules/payroll";
import { reportRoutes } from "./modules/reports";
import { agentRoutes } from "./modules/users";

export const AdminRoutes: RouteObject[] = [
  { index: true, element: <Navigate to="users" /> },
  { path: "users", children: agentRoutes },
  { path: "locations", children: locationRoutes },
  { path: "reports", children: reportRoutes },
  { path: "payrolls", children: payrollRoutes },
];
