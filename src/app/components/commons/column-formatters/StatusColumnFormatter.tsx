import { ReactElement } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { UserStatus } from "src/app/components/functions/UserStatus";

export const StatusColumnFormatter = (status: any): ReactElement => {

  return (
    <OverlayTrigger
      overlay={
        <Tooltip id="column-tooltip">
          <FormattedMessage
            id={`TABLE.HEADER.USER_STATUS.TOOLTIP.${status}`}
          />{" "}
        </Tooltip>
      }
    >

      <span className={`badge badge-light-${UserStatus(status)?.color}`}>
        <FormattedMessage
          id={`TABLE.HEADER.USER_STATUS.LABEL.${status}`}
        />{" "}
      </span>

    </OverlayTrigger>
  );
};
