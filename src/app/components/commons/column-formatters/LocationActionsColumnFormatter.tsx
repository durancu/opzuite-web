/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const LocationActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openEditLocationPage,
    openDeleteLocationDialog,
    openDetailLocationPage,
    openChangeIpLocationDialog,
  }: any
) => (
  <TableActionsMenu
    readFunction={openDetailLocationPage}
    editFunction={openEditLocationPage}
    deleteFunction={openDeleteLocationDialog}
    changeIpLocationFunction={openChangeIpLocationDialog}
    editPermission={Permissions.LOCATIONS_UPDATE}
    readPermission={Permissions.LOCATIONS_READ}
    deletePermission={Permissions.LOCATIONS_DELETE}
    code={row.code}
  />
);
