/* eslint-disable no-script-url */
import { FormattedMessage } from "react-intl";

export const CreatedByColumnHeaderFormatter = (
  column: any,
  colIndex: any,
  { sortElement }: any
) => (
  <div className="d-flex gap-2 align-items-center">
    <FormattedMessage id="TABLE.HEADER.CREATED_BY" />
    {sortElement}
  </div>
);
