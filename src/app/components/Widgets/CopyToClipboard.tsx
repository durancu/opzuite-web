import clsx from "clsx";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { useAppDispatch } from "src/app/hooks";
import { feedbacksSlice } from "../Feedback/_redux/feedbackSlice";

interface Props {
  content: any;
  className?: any;
}

export const CopyToClipboard = ({ content, className }: Props) => {
  const dispatch: any = useAppDispatch();
  const intl = useIntl();

  const copyClipboard = () => {
    navigator.clipboard.writeText(content).then(() => {
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: intl.formatMessage(
            { id: "RESPONSES.FEEDBACK.CLIPBOARD_COPY_SUCCESS" },
            { field: intl.formatMessage({ id: "FORM.LABELS.CODE" }) }
          ),
          options: {
            key: new Date().getTime() + Math.random(),
            variant: "success",
          },
        } as any)
      );
    });
  };

  return (
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="quick-actions-tooltip">
          <FormattedMessage
            id="COMPONENTS.TOOLTIP.CLIPBOARD_COPY"
            values={{
              field: '',
              value: content,
            }}
          />
        </Tooltip>
      }
    >
      <button
        onClick={copyClipboard}
        className={clsx("bg-transparent border-0", className)}
      >
        <i className="far fa-copy" />
      </button>
    </OverlayTrigger>
  );
};
