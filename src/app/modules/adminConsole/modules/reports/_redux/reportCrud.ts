import axios from "axios";

export const REPORTS_URL = "api/v2/reports";

// CREATE =>  POST: add a new report to the server
export function createReport(report: any) {
  return axios.post(REPORTS_URL, report);
}

// READ
export function getAllReports() {
  return axios.get(REPORTS_URL);
}

export function getReportById(reportCode: any) {
  return axios.get(`${REPORTS_URL}/${reportCode}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findReports(queryParams: any) {
  return axios.post(`${REPORTS_URL}/search`, { queryParams });
}

export function reportResults(queryParams: any, code: string) {
  return axios.post(`${REPORTS_URL}/${code}/results`, queryParams);
}

export function getReportChartData(code: string, queryParams: any = {}) {
  return axios.post(`${REPORTS_URL}/${code}/chart-data`, queryParams);
}

// UPDATE => PUT: update the report on the server
export function updateReport(report: any) {
  return axios.put(`${REPORTS_URL}/${report.code}`, report);
}

// UPDATE Status
export function updateStatusForReports(ids: any, status: any) {
  return axios.post(`${REPORTS_URL}/updateStatusForReports`, {
    ids,
    status,
  });
}

// DELETE => delete the report from the server
export function deleteReport(reportId: any) {
  return axios.delete(`${REPORTS_URL}/${reportId}`);
}

// DELETE Reports by ids
export function deleteReports(codes: any) {
  return axios.post(`${REPORTS_URL}/delete`, { codes });
}
