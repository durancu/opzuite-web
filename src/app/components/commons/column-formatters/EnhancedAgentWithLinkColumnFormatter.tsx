import { Fragment } from "react";

export const EnhancedAgentWithLinkColumnFormatter = (
  cellContent: any,
  agent: any,
  rowIndex: any,
  {
    openDetailPageFunction,
    idField,
    textField,
    defaultText = "",
  }: any
) => (
  <Fragment>
    <a
      className="text-dark fw-semibold text-hover-primary fs-6 cursor-pointer"
      onClick={() => {
        openDetailPageFunction(agent[idField]);
      }}
    >
      {agent[textField] || defaultText}
    </a>
  </Fragment>
);
