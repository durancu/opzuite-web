import { Col, Row, Card } from "react-bootstrap";
import { formatDate } from "src/app/components/functions";
import { FilterParamsChipsComponent } from "./FilterParamsChipsComponent";
import { ReportParamChips } from "./ReportParamsChips";
import { FormattedMessage } from "react-intl";

interface Props {
  report: any;
}

export const ReportOverview = ({ report }: Props) => {
  return (
    <Card className="mb-4 p-8 overview-section">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.OVERVIEW" />
      </Card.Title>
      <Row className="my-8">
        <Col>
          <h3 className="card-title align-items-start flex-column mb-3">
            {report && report.name}
          </h3>
          <span className="text-muted mt-0">
            {report && report.description}
          </span>
          <ReportParamChips />
          <FilterParamsChipsComponent />
        </Col>
        <Col className="text-end">
          <p className="mb-3">
            <span className="small">Author: </span>
            <span>{report && report.createdBy.name}</span>
          </p>
          <p>
            <span className="small">Updated:</span>{" "}
            {report &&
              formatDate(
                report.updatedAt ? report.updatedAt : report.createdAt
              )}
          </p>
        </Col>
      </Row>
    </Card>
  );
};
