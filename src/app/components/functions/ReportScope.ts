import { toUpper } from "lodash";

export const ReportScope = (status: any): any => {
    switch (toUpper(status)) {
        case "PUBLIC":
            return {
                icon: "fas fa-check",
                color: "success"
            };
        case "PRIVATE":
        default:
            return {
                icon: "fas fa-shield",
                color: "danger"
            };
    }
};