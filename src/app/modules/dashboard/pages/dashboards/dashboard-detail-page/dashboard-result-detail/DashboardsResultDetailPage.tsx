import { DashboardsResultDetailCard } from "./DashboardsResultDetailCard";
import { DashboardsResultDetailUIProvider } from "./DashboardsResultDetailUIContext";

export function DashboardsResultDetailPage() {
  const dashboardsUIEvents = {};

  return (
    <DashboardsResultDetailUIProvider dashboardsUIEvents={dashboardsUIEvents}>
      {/* <DashboardsResultDetailLoadingDialog /> */}
      <DashboardsResultDetailCard />
    </DashboardsResultDetailUIProvider>
  );
}
