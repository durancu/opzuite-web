import { Fragment } from "react";
import { useFormikContext } from "formik";
import { PolicyMetricsAccordion } from "src/app/components/Widgets";
import RepeaterPolicyCoverages from "./coverages/RepeaterPolicyCoverages";
import RepeaterPolicyEndorsements from "./endorsements/RepeaterPolicyEndorsements";

const FormPolicyProducts = () => {
  const { values } = useFormikContext<any>();

  return (
    <Fragment>
      <RepeaterPolicyCoverages />
      <RepeaterPolicyEndorsements />

      <div className="my-8">
        <PolicyMetricsAccordion policyDetail={values} />
      </div>
    </Fragment>
  );
};

export default FormPolicyProducts;
