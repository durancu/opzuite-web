import { FormattedMessage } from "react-intl";
export const BoostrapTableTotalPaginator = (from: any, to: any, size: any) => (
  <span className="react-bootstrap-table-pagination-total">
    <FormattedMessage
      id="TABLE.PAGINATOR.TOTAL.TEXT"
      values={{ from: from, to: to, size: size }}
    />
  </span>
);
