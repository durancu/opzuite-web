import PropTypes from "prop-types";
import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { useLocation } from "react-router-dom";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { Permissions } from "src/app/constants/Permissions";
import { useGetUserAuth } from "src/app/hooks";
import { isPermitted } from "src/app/utils";
import { salesAPI } from "../../redux/saleSlice";
import { FilterForm } from "./FilterForm";
import { initialValues, prepareFilter, sizePerPageList } from "./helpers";

export const PoliciesTable = ({ showFilter = true }) => {
  const location = useLocation();
  const user = useGetUserAuth();

  const { context, menuActions, queryParams, updateQueryParams } =
    useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetAllSalesQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "code",
      text: "InZuite ID",
      headerFormatter: headerFormatters.CodeColumnHeaderFormatter,
      formatter: columnFormatters.CodeColumnFormatter,
      headerClasses: "fw-bold text-muted",
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
      },
    },
    {
      dataField: "number",
      text: "Policy Number",
      sort: true,
      sortCaret: sortCaret,
      headerAlign: "left",
      align: "left",
      headerFormatter: headerFormatters.PolicyNumberColumnHeaderFormatter,
      formatter: columnFormatters.PolicyNumberColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "customerName",
      text: "Insured",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CustomerColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedCustomerNameColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "soldAt",
      text: "Sold At",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.DateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "items",
      text: "Coverages",
      headerFormatter: headerFormatters.CoveragesColumnHeaderFormatter,
      formatter: columnFormatters.InsurersColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "totalPremium",
      text: "Premium",
      sort: true,
      sortCaret: sortCaret,
      headerAlign: "left",
      align: "left",
      headerFormatter: headerFormatters.PremiumColumnHeaderFormatter,
      formatter: columnFormatters.AmountColumnFormatter,
      headerClasses: "fw-bold text-muted",
      hidden: location.pathname.indexOf("policies") === -1,
    },
    {
      dataField: "sellerName",
      text: "Agent",
      sort: true,
      sortCaret: sortCaret,
      hidden: !isPermitted(
        [
          Permissions.MY_COMPANY,
          Permissions.MY_COUNTRY,
          Permissions.MY_LOCATION,
        ],
        user.permissions
      ),
      headerFormatter: headerFormatters.CoordinatorColumnHeaderFormatter,
      formatter: columnFormatters.SellerAndLocationColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "action",
      text: "", // prevents console errors since its required
      headerAlign: "left",
      align: "left",
      hidden:
        context !== "sales" ||
        !isPermitted(
          [
            Permissions.POLICIES_UPDATE,
            //Permissions.POLICIES_RENEW,
            Permissions.POLICIES_DELETE,
            Permissions.POLICIES_READ,
          ],
          user.permissions
        ),
      //headerFormatter: headerFormatters.ActionsColumnHeaderFormatter,
      formatter: columnFormatters.PolicyActionsColumnFormatter,
      headerClasses: "fw-bold text-muted",
      formatExtraData: {
        openEditPolicyPage: menuActions.edit,
        openDeletePolicyDialog: menuActions.delete,
        //openRenewPolicyPage: menuActions.renew,
        openDetailPolicyPage: menuActions.details,
        openCertificates: menuActions.certificates,
      },
      classes: "text-right pr-0",
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams?.pageSize || 20,
    page: queryParams?.pageNumber || 1,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      {showFilter && (
        <FilterPanel
          initialValues={initialValues}
          prepareFilter={prepareFilter}
          filterForm={<FilterForm />}
        />
      )}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-left overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="code"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "soldAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

PoliciesTable.propTypes = {
  showFilter: PropTypes.bool,
};
