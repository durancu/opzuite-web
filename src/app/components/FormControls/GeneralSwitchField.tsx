import { FormControlLabel, FormHelperText, Switch } from "@mui/material";
import { useIntl } from "react-intl";
import { GenericFunction } from "../interfaces/FunctionInterfaces";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  value: boolean;
  handleChange: GenericFunction;
}

export function GeneralSwitchField({
  value,
  handleChange,
  helperText,
  helperTextId,
  labelTextId = "DEFAULT",
  labelText,
}: Props) {
  const intl = useIntl();

  return (
    <>
      <FormControlLabel
        control={
          <Switch
            checked={value}
            onChange={() => {
              handleChange(() => !value);
            }}
            color="primary"
          />
        }
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
      />
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
