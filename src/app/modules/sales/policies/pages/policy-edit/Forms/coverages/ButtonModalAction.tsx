import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { GenericFunction } from "src/app/components/interfaces/FunctionInterfaces";

interface Props {
  actions: GenericFunction;
  tooltipText?: string;
  className?: string;
  actionName?: string;
}

export const ButtonModalAction = ({ actions, actionName }: Props) => {
  return (
    <OverlayTrigger
      placement="top"
      overlay={<Tooltip className="text-capitalize">{actionName}</Tooltip>}
    >
      <Button variant="primary" onClick={() => actions()} className="px-4 py-2">
        {actionName === "renew" ? (
          <i className="fs-3 bi bi-arrow-repeat"></i>
        ) : (
          <i className="fs-3 bi bi-pencil-square"></i>
        )}
      </Button>
    </OverlayTrigger>
  );
};
