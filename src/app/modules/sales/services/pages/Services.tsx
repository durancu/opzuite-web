import { Fragment } from "react";
import { ServicesTable } from "../components/ServicesTable";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { breadcrumbs } from "../breadcrumbs";
import { defaultFilter } from "../components/helpers";

export const Services = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
    renew: (code: string) => navigate(`${code}/renew`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Services</PageTitle>
      <RequirePermission permissions={Permissions.APPLICATIONS_CREATE}>
        <Toolbar>
          <Link
            to="/services/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.APPLICATION.NEW" />
          </Link>
        </Toolbar>
      </RequirePermission>

      <Card className="p-8">
        <FilterUIProvider
          context="sales"
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <ServicesTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
