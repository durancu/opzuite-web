import { useSnackbar } from "notistack";
import { Fragment, useEffect } from "react";
import { useIntl } from "react-intl";
import { Link, useNavigate, useParams } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { BackdropLoader, Error, RequirePermission } from "src/app/components";
import {
  PageDetailTopButtons,
  PolicyMetricsAccordion,
  PolicyOverview,
} from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { salesAPI } from "src/app/modules/sales/redux/saleSlice";
import { breadcrumbs } from "../../breadcrumbs";
import { CoverageTable } from "./CoverageTable";
import { EndorsementTable } from "./EndorsementTable";

export const PolicyDetailPage = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: policy,
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetSaleByCodeQuery(
    `${code}?layout=FULL&withCustomer=true&withSeller=true`,
    {
      skip: !code,
      refetchOnFocus: true,
      refetchOnReconnect: true,
      refetchOnMountOrArgChange: true,
    }
  );

  const [deletePolicy, { isLoading: isDeleting }] =
    salesAPI.useDeleteSaleMutation();

  const goToEditPolicy = () => {
    navigate(`/policies/${code}/edit`);
  };

  const openRenewPolicyPage = () => {
    navigate(`/policies/${code}/renew`);
  };

  async function handleDelete() {
    try {
      await deletePolicy(code).unwrap();
      enqueueSnackbar("Policy deleted", {
        variant: "success",
      });
      navigate("/policies");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.POLICY" })}
      </PageTitle>
      <Toolbar>
        <div className="d-flex gap-3">
          <RequirePermission permissions={Permissions.POLICIES_CERTIFICATES}>
            <Link
              to={`/certificates/new?customer=${policy?.customer?.id}`}
              className="btn btn-dark d-flex align-items-center gap-1 justify-items-center"
            >
              <span>Certificate</span>
              <i className="fs-4 bi bi-file-earmark-text"></i>
            </Link>
          </RequirePermission>
          <PageDetailTopButtons
            editFunction={goToEditPolicy}
            renewFunction={openRenewPolicyPage}
            deleteFunction={handleDelete}
            editPermission={Permissions.POLICIES_UPDATE}
            renewPermission={Permissions.POLICIES_RENEW}
            deletePermission={Permissions.POLICIES_DELETE}
            renew
            componentLabel="policy"
          />
        </div>
      </Toolbar>

      <PolicyOverview sale={policy} />
      <div className="mb-4 p-8 bg-white">
        <PolicyMetricsAccordion policyDetail={policy} />
      </div>
      <CoverageTable items={policy.items} />
      <RequirePermission permissions={Permissions.POLICIES_ENDORSE}>
        <EndorsementTable endorsements={policy?.endorsements} />
      </RequirePermission>
    </Fragment>
  );
};
