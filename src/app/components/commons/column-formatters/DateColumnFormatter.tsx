import { FormattedDate } from "react-intl";

export const DateColumnFormatter = (date: any) => (
  <div>
    <span className="text-dark fw-semibold mb-1 fs-6">
      <FormattedDate value={date} />
    </span>
  </div>
);
