import { Link } from "react-router-dom";

export function UserFullNameColumnFormatter(name: any, user: any) {
  return (
    <>
      <Link
        className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
        to={`/agents/${user?.id}/detail`}
      >
        {user.name}
      </Link>
      {/*       <span className="text-muted fw-semibold text-muted text-hover-primary d-block fs-7">
        {user?.position}
      </span> */}
    </>
  );
}
