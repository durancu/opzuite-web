import { useState } from "react";
import { KTSVG, toAbsoluteUrl } from "src/_metronic/helpers";
import { NavLink } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { profileAPI } from "./redux";
import { Modal, Spinner } from "react-bootstrap";
import { useSnackbar } from "notistack";
import clsx from "clsx";

export const ProfileHeader = () => {
  const [options, toggleOptions] = useState(false);
  const [open, setOpen] = useState(false);
  const [file, setFile] = useState<File>();
  const { data: user = {}, refetch } = profileAPI.useGetProfileQuery();
  const [updatePhoto, { isLoading }] = profileAPI.useUpdatePhotoMutation();
  const [deletePhoto, { isLoading: deleting }] =
    profileAPI.useDeletePhotoMutation();
  const { enqueueSnackbar } = useSnackbar();

  function closeModal() {
    setOpen(false);
    toggleOptions(false);
  }

  async function handleUpdate() {
    const formData = new FormData();
    if (file) {
      formData.append("file", file);
    }
    try {
      await updatePhoto(formData).unwrap();
      refetch();
      enqueueSnackbar("Update succesfull", {
        variant: "success",
      });
      closeModal();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleDelete() {
    try {
      await deletePhoto().unwrap();
      refetch();
      enqueueSnackbar("Profile photo removed", {
        variant: "success",
      });
      closeModal();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  // don't display header if no profile
  if (!Object.keys(user).length) return null;

  return (
    <div className="card mb-5 mb-xl-10">
      <div className="card-body pt-9 pb-0">
        <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
          <div
            className="me-7 mb-4"
            onMouseEnter={() => toggleOptions(true)}
            onMouseLeave={() => toggleOptions(false)}
          >
            <div className="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative border rouded-md p-2">
              <img
                src={
                  user?.profilePhoto ||
                  toAbsoluteUrl("/media/avatars/blank.png")
                }
                alt="Profile photo"
              />
              <div className="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
            </div>
            {options && (
              <div className="d-flex gap-2 mt-4">
                <button
                  className="btn btn-sm btn-danger"
                  onClick={() => handleDelete()}
                >
                  {!deleting ? "Remove" : <Spinner size="sm" />}
                </button>
                <button
                  className="btn btn-sm btn-primary"
                  onClick={() => setOpen(true)}
                >
                  Change
                </button>
              </div>
            )}
            <Modal show={open} centered onHide={() => closeModal()}>
              <Modal.Header closeButton>
                <Modal.Title>Change Photo</Modal.Title>
              </Modal.Header>

              <Modal.Body className="p-8">
                <label htmlFor="file" className="form-label">
                  Select a photo
                </label>
                <input
                  className="form-control"
                  type="file"
                  name="file"
                  required
                  onChange={(evt) => {
                    if (evt.target.files) {
                      setFile(evt.target.files[0]);
                    }
                  }}
                />
              </Modal.Body>
              <Modal.Footer>
                <button
                  className="btn btn-light"
                  disabled={isLoading}
                  onClick={() => closeModal()}
                >
                  Cancel
                </button>
                <button
                  className="btn btn-primary"
                  disabled={isLoading}
                  onClick={() => handleUpdate()}
                >
                  {!isLoading ? "Change Photo" : <Spinner size="sm" />}
                </button>
              </Modal.Footer>
            </Modal>
          </div>

          <div className="flex-grow-1">
            <div className="d-flex justify-content-between align-items-start flex-wrap mb-2">
              <div className="d-flex flex-column">
                <div className="d-flex align-items-center mb-2">
                  <span className="text-gray-800 text-hover-primary fs-2 fw-bolder me-1">
                    {user?.firstName} {user?.lastName}
                  </span>
                  <KTSVG
                    path="/media/icons/duotune/general/gen026.svg"
                    className="svg-icon-1 svg-icon-primary"
                  />
                </div>

                <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                  <span className="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                    <KTSVG
                      path="/media/icons/duotune/communication/com006.svg"
                      className="svg-icon-4 me-1"
                    />
                    {user?.username}
                  </span>
                  <span className="d-flex align-items-center text-gray-400 text-hover-primary me-5 mb-2">
                    <KTSVG
                      path="/media/icons/duotune/general/gen018.svg"
                      className="svg-icon-4 me-1"
                    />
                    {user?.address?.address1}
                  </span>
                  <span className="d-flex align-items-center text-gray-400 text-hover-primary mb-2">
                    <KTSVG
                      path="/media/icons/duotune/communication/com011.svg"
                      className="svg-icon-4 me-1"
                    />
                    {user?.email}
                  </span>
                </div>

                <div className="">
                  <div className="d-flex align-items-center mb-2">
                    <span className="text-dark fw-bold mr-2 text-muted">
                      Gender :
                    </span>
                    <span className="text-dark fw-bold text-hover-primary">
                      &nbsp; {user?.gender}
                    </span>
                  </div>
                  <div className="d-flex align-items-center  mb-2">
                    <span className="text-dark fw-bold mr-2 text-muted">
                      Language :
                    </span>
                    <span className="text-dark fw-bold text-uppercase">
                      &nbsp; {user?.language}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex overflow-auto h-55px">
          <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
            <li className="nav-item">
              <NavLink
                end
                to="/profile"
                className={({ isActive }) =>
                  clsx("nav-link text-active-primary me-6", {
                    active: isActive,
                  })
                }
              >
                <FormattedMessage id="MENU.MAIN.OVERVIEW" />
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                end
                to="/profile/settings"
                className={({ isActive }) =>
                  clsx("nav-link text-active-primary me-6", {
                    active: isActive,
                  })
                }
              >
                Settings
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
