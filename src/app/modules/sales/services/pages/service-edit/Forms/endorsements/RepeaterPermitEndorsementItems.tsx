import { Fragment, useState } from "react";

import { FieldArray, useFormikContext } from "formik";

import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  ItemDeleteModal,
  // EndorsementType,
  EndorsementItemType,
  ENDORSEMENT_ITEM_TYPE,
  ModalEndorsementActionType,
  ButtonModalEndorsementAction,
} from "src/app/components/EndorsementsComponents";
import { Sale } from "src/app/components/interfaces/Sale";
import { EndorsementItemDropdownMenu } from "src/app/components/Menu";
import { EndorsementItemModal } from "./components";
import { Accordion, Col, Row, Card } from "react-bootstrap";
import { FakeField } from "src/app/partials/controls";
import objectPath from "object-path";
import { MissingFieldsAlert } from "src/app/components";

interface Props {
  endorsementIndex: number; // actual endorsement
}

type MODAL_ACTION = string | "EDIT" | "ADD" | "";

interface LocalState {
  type: ENDORSEMENT_ITEM_TYPE;
  action: MODAL_ACTION;
  index: number | null;
  modal: boolean;
}

// function minSoldAtDate() {
//   const today = moment();
//   return today.subtract(1, "month").date(21).toDate();
// }

export const RepeaterPermitEndorsementItems = ({ endorsementIndex }: Props) => {
  const { values, errors, setFieldValue }: any = useFormikContext<Sale>();
  const {
    endorsementItems,
    permitEndorsementItems,
    permitEndorsementItemTypes,
    remitToOptions,
    financers,
  } = useGetCatalog();

  const [localState, setLocalState] = useState<LocalState>({
    type: "RECEIVABLE", // endorsementType
    action: "EDIT", // endorsement action, Add, edit ...
    index: null, // items index in endorsements items array
    modal: false, // show or hide modal
  });

  const createEndorsementItem = (option: any) => {
    const item = {
      amount: 0,
      description: "",
      endorsedAt: new Date(),
      followUpDate: null,
      followUpPerson: null,
      seller: values.seller,
      otherDetails: {},
      payments: [],
      status: "",
      type: "",
      accountingClass: option.id,
    };

    const updatedItems = [...values.endorsements[endorsementIndex].items, item];
    setFieldValue(`endorsements.${endorsementIndex}.items`, updatedItems);
    setLocalState((current) => {
      return {
        ...current,
        type: option.id,
        action: ModalEndorsementActionType.ADD,
        index: updatedItems.length - 1, // we add at the bottom
        modal: true,
      };
    });
  };

  const findEndorsementItemType = (endorsementItem: any) => {
    return permitEndorsementItems?.find(
      (item: any) => item.id === endorsementItem?.accountingClass
    );
  };

  return (
    <Fragment>
      <EndorsementItemModal
        type={localState.type}
        action={localState.action}
        index={localState.index}
        endorsementIndex={endorsementIndex}
        show={localState.modal}
        onHide={() => {
          setLocalState({
            type: "RECEIVABLE",
            action: "EDIT",
            index: null,
            modal: false,
          });
        }}
      />
      <FieldArray
        name={`endorsements.${endorsementIndex}.items`}
        render={(helpers) => (
          <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
            {values.endorsements[endorsementIndex]?.items?.map(
              (endorsementItem: any, index: number) => {
                const itemConfig: any =
                  findEndorsementItemType(endorsementItem);
                return (
                  <Accordion.Item
                    key={index}
                    eventKey={index.toString()}
                    className="mb-6 ml-0"
                  >
                    <Card
                      key={index}
                      className="mb-3"
                      style={{
                        border: `1px solid ${itemConfig?.color + "33" || ""}`,
                        backgroundColor: `${itemConfig?.color + "07" || ""}`,
                      }}
                    >
                      <Accordion.Button
                        className="px-6 py-4"
                        style={{
                          backgroundColor: `${itemConfig?.color + "15" || ""}`,
                        }}
                      >
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <h4 className="fw-bold">
                            Detail {`A${endorsementIndex + 1}`}.{index + 1}
                            <span
                              className="mx-3 px-2 py-0"
                              style={{
                                border: "1px solid",
                                borderRadius: "50px",
                                borderColor: `${itemConfig?.color || ""}`,
                              }}
                            >
                              <small
                                className="fw-bold"
                                style={{
                                  color: `${itemConfig?.color || "#000"}`,
                                }}
                              >
                                {itemConfig?.name}
                              </small>
                            </span>
                          </h4>
                          <div className="d-flex justify-content-between align-items-center gap-3">
                            <MissingFieldsAlert
                              show={objectPath.get(
                                errors,
                                `endorsements.${endorsementIndex}.items`
                              )}
                            />
                            <ButtonModalEndorsementAction
                              className="mr-2"
                              actionName={ModalEndorsementActionType.EDIT}
                              tooltipText="Edit Detail"
                              anotherActions={() => {
                                setLocalState((current) => {
                                  return {
                                    ...current,
                                    type: itemConfig?.id,
                                    action: ModalEndorsementActionType.EDIT,
                                    index: index,
                                    modal: true,
                                  };
                                });
                              }}
                            />
                            <ItemDeleteModal
                              action={() => {
                                helpers.remove(index);
                              }}
                              hidden={
                                index === 0 &&
                                objectPath.get(
                                  values,
                                  `endorsements.${endorsementIndex}.type`
                                ) !== "CUSTOM"
                              }
                              itemTitle={`Detail A${endorsementIndex + 1}.${
                                index + 1
                              }`}
                            />
                          </div>
                        </div>
                      </Accordion.Button>
                      <Accordion.Body className="py-6 px-6">
                        <Row className="mb-3">
                          <Col sm="2">
                            <FakeField
                              value={endorsementItem.type}
                              label="Type"
                              type="select"
                              options={
                                permitEndorsementItemTypes[
                                  endorsementItem.accountingClass
                                ]
                              }
                            />
                          </Col>
                          <Col sm="1">
                            <FakeField
                              value={endorsementItem.endorsedAt}
                              label={
                                endorsementItem.accountingClass !==
                                EndorsementItemType.FEE_TAX
                                  ? "Due Date"
                                  : "Date"
                              }
                              type="date"
                            />
                          </Col>
                          <Col sm="1">
                            <FakeField
                              value={endorsementItem.amount}
                              label="Amount"
                              type="number"
                            />
                          </Col>

                          <Col
                            sm="2"
                            hidden={
                              endorsementItem.accountingClass !== "PAYABLE"
                            }
                          >
                            <FakeField
                              value={endorsementItem.otherDetails.remitTo}
                              label="Remit To"
                              type="select"
                              options={remitToOptions}
                            />
                          </Col>

                          <Col
                            sm="2"
                            hidden={
                              endorsementItem.accountingClass !==
                              "FINANCING_DIRECT_BILL"
                            }
                          >
                            <FakeField
                              type="checkbox"
                              value={
                                endorsementItem.otherDetails.processedByAgency
                              }
                              label="Processed By Agency"
                            />
                          </Col>

                          <Col
                            sm="2"
                            hidden={
                              endorsementItem.accountingClass !==
                              "FINANCING_DIRECT_BILL"
                            }
                          >
                            <FakeField
                              value={endorsementItem.financerCompany}
                              label="Financer (PFA)"
                              options={financers}
                              type="select"
                            />
                          </Col>

                          <Col
                            sm="2"
                            hidden={
                              endorsementItem.accountingClass !==
                              "FINANCING_DIRECT_BILL"
                            }
                          >
                            <FakeField
                              value={endorsementItem.otherDetails.description}
                              label="Description"
                              type="text"
                            />
                          </Col>

                          <Col
                            sm="2"
                            hidden={
                              !["FEE_TAX", "PAYABLE", "RECEIVABLE"].includes(
                                endorsementItem.accountingClass
                              )
                            }
                          >
                            <FakeField
                              value={endorsementItem.description}
                              label="Description"
                              type="text"
                            />
                          </Col>
                        </Row>
                      </Accordion.Body>
                    </Card>
                  </Accordion.Item>
                );
              }
            )}

            <EndorsementItemDropdownMenu
              options={endorsementItems || []}
              itemAction={(option: any) => createEndorsementItem(option)}
            />
          </Accordion>
        )}
      />
    </Fragment>
  );
};
