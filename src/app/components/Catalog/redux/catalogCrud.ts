import axios from "axios";

export const CATALOGS_URL = "api/v2/catalogs/search";

const API = {
  URL: "https://parseapi.back4app.com/classes/Carmodels_Car_Model_List?",
  headers: {
    "X-Parse-Application-Id": "BEaYhKZycU7m51GgdVdNgivSLOCTRQYXPa68U2Hu", // This is your app's application id
    "X-Parse-REST-API-Key": "T6qZ8qpY9FT51UvxD6gNaNy64VvNlsqFZYWEYss4", // This is your app's REST API key
  },
};

// READ
export function getAllCatalogs() {
  return axios.post(CATALOGS_URL, {});
}

export async function getVehicleModelCatalog(year?: number) {
  const thisYear = new Date().getFullYear();

  const where = encodeURIComponent(
    JSON.stringify({
      Year: year || thisYear,
      Make: {
        $exists: true,
      },
      Model: {
        $exists: true,
      },
      Category: {
        $exists: true,
      },
    })
  );

  const response = await fetch(
    `${API.URL}limit=1000&order=-Year,Make,Model,Category&where=${where}`,
    {
      headers: API.headers,
    }
  );

  return await response.json();
}
