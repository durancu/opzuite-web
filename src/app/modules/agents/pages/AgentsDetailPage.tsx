import { Fragment, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import { useParams } from "react-router-dom";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as actionsCatalog from "src/app/components/Catalog/redux/catalogActions";
import {
  Applications,
  EmployeeDetails,
  EmployeeOverview,
  Policies,
} from "src/app/components/Widgets";
import * as actions from "../redux/usersActions";
import { ProgressBar } from "react-bootstrap";
import { PageTitle } from "src/_metronic/layout/core";
import { breadcrumbs } from "../breadcrumbs";

export const AgentsDetailPage = () => {
  const { id }: any = useParams();
  const dispatch = useAppDispatch();
  const { actionsLoading, userForEdit, catalogs } = useSelector(
    (state: any) => ({
      actionsLoading: state.users.actionsLoading,
      userForEdit: state.users.userForEdit,
      catalogs: state.catalogs.entities,
      user: state.auth?.user,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchUser(id));
    dispatch(actionsCatalog.getAllCatalog());
  }, [id, dispatch]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Agent details</PageTitle>
      {actionsLoading && <ProgressBar />}

      {!actionsLoading && userForEdit && (
        <EmployeeOverview employee={userForEdit} catalogs={catalogs} />
      )}
      {!actionsLoading && userForEdit && (
        <EmployeeDetails employee={userForEdit} catalogs={catalogs} />
      )}

      {!actionsLoading && userForEdit && (
        <Policies
          filter={{ seller: userForEdit.id }}
          subTitle="Latest policies sold by this employee."
          context="employee"
        />
      )}
      {!actionsLoading && userForEdit && (
        <Applications
          filter={{ seller: userForEdit.id }}
          subTitle="Latest services processed by this employee."
          context="employee"
        />
      )}
    </Fragment>
  );
};
