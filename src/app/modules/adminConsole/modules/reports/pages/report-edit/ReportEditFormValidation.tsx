import * as Yup from "yup";

export const reportCreateValidationSchema = Yup.object().shape({
  config: Yup.object(),
  permissions: Yup.array(),
  name: Yup.string().required("Report Name is required."),
  description: Yup.string(),
  params: Yup.object().shape({
    model: Yup.string().required("Metrics Source is required"),
    filters: Yup.array().of(
      Yup.object().shape({
        field: Yup.string().required("Field is required."),
        operator: Yup.string().required("Operator is required."),
        value: Yup.string().required("Value is required."),
      })
    ),
    groupByModel: Yup.string().required("Group by is required."),
    dateFrom: Yup.mixed(),
    dateTo: Yup.mixed(),
    fields: Yup.array(),
    withCount: Yup.boolean(),
    withRecords: Yup.boolean(),
  }),
  scope: Yup.string(),
});
