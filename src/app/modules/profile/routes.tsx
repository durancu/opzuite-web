// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { Overview } from "./Overview";
import { Settings } from "./settings/Settings";

export const ProfileRoutes: RouteObject[] = [
  {
    index: true,
    element: <Overview />,
  },
  { path: "settings", element: <Settings /> },
];
