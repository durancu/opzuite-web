import { Dispatch } from "redux";
import * as requestFromServer from "./catalogCrud";
import { catalogsSlice, callTypes } from "./catalogSlice";

const { actions } = catalogsSlice;

export const getAllCatalog = () => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  return requestFromServer
    .getAllCatalogs()
    .then((response) => {
      const catalog = response.data;
      dispatch(actions.catalogsGetAll(catalog));
    })
    .catch((error) => {
      error.clientMessage = "Can't find catalog";
      dispatch(
        actions.catchError({ error, callType: callTypes.action } as any)
      );
    });
};

export const getVehicleModelCatalog =
  (year?: number) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    return requestFromServer
      .getVehicleModelCatalog(year)
      .then((response: any) => {
        const vehicleModels = response.results;
        dispatch(actions.getVehiclesModelFetched({ vehicleModels } as any));
      })
      .catch((error) => {
        error.clientMessage = "Can't find vehicle model catalog";
        dispatch(
          actions.catchError({ error, callType: callTypes.action } as any)
        );
      });
  };
