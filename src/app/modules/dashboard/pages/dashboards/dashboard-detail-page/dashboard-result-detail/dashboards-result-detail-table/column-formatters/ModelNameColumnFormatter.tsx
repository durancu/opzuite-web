export const ModelNameColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { model }: any
) => {
  if (model) {
    switch (model) {
      case "seller":
      case "customer":
        return <>{`${row?._id?.firstName} ${row?._id?.lastName}`}</>;
      case "location":
        return <>{`${row?._id?.label}`}</>;
      default:
        return <>{`${row?._id?.label}`}</>;
    }
  }
};
