import { FormikValues } from "formik";

const searchColumns = ["name"];

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "CERTIFICATES",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 20,
  };
};
