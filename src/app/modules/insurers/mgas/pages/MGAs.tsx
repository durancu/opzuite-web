import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { MGAsTable } from "../components/MGAsTable";
import { breadcrumbs } from "../breadcrumbs";
import { defaultFilter } from "../components/helpers";

export const MGAs = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>MGA</PageTitle>
      <RequirePermission permissions={Permissions.MGAS_CREATE}>
        <Toolbar>
          <Link to="/mgas/new" className="btn btn-primary align-self-center">
            <FormattedMessage id="BUTTON.BROKER.NEW" />
          </Link>
        </Toolbar>
      </RequirePermission>
      <Card className="p-8">
        <FilterUIProvider
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <MGAsTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
