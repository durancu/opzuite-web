export const CoverageInsurerColumnFormatter = (insurer: any) => {
  return insurer?.name || "-";
};
