import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { useIntl } from "react-intl";
import {
  FormToggle,
  WizardForm,
  WizardFormStep,
} from "src/app/components/forms";
import FormCustomerPersonalInfo from "../../forms/FormCustomerPersonalInfo";
import FormCustomerSetting from "../../forms/FormCustomerSetting";
import { BusinessContactInfo } from "../../forms/business/BusinessContactInfo";
import { CohortsRepeater } from "../../forms/cohorts/CohortsRepeater";
import { VehiclesRepeater } from "../../forms/vehicles/VehiclesRepeater";
import { customersAPI } from "../../redux";
import {
  businessSchema,
  cohortsSchema,
  customerSettingsSchema,
  personalInfoSchema,
  vehiclesSchema,
} from "./schemas";
import { CUSTOMER_TYPES } from "src/app/constants/General";
import { useSnackbar } from "notistack";
import { FormikHelpers, FormikValues } from "formik";
import { BackdropLoader } from "src/app/components";

interface Props {
  show: boolean;
  onHide: () => void;
  customer: Customer;
}

export const CustomerEditModal = ({ show, onHide, customer }: Props) => {
  const intl = useIntl();
  const { enqueueSnackbar } = useSnackbar();
  const isBusinessCustomer = customer?.type === CUSTOMER_TYPES.BUSINESS;

  const [toggle, setToggle] = useState({
    cohorts: false,
    vehicles: false,
  });

  const [updateCustomer, { isLoading }] =
    customersAPI.useUpdateCustomerMutation();

  function toggleSection(section: string, value: boolean) {
    setToggle((current) => {
      return { ...current, [section]: value };
    });
  }

  useEffect(() => {
    if (customer) {
      customer?.cohorts?.length > 0 && toggleSection("cohorts", true);
      customer?.vehicles?.length > 0 && toggleSection("vehicles", true);
    }
  }, [customer]);

  async function handleUpdate(
    values: FormikValues,
    actions: FormikHelpers<any>
  ) {
    try {
      await updateCustomer(values).unwrap();
      enqueueSnackbar("Customer updated successfully", {
        variant: "success",
      });
      actions.setSubmitting(false);
      onHide();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading) return <BackdropLoader />;

  return (
    <Modal size="xl" show={show} onHide={onHide} backdrop="static">
      <Modal.Header className="bg-light">
        <Modal.Title>
          Edit Insured {isBusinessCustomer ? "(Business)" : "(Individual)"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0">
        <WizardForm
          initialValues={customer}
          onSubmit={handleUpdate}
          onCancel={onHide}
        >
          {isBusinessCustomer && (
            <WizardFormStep
              label={intl.formatMessage({
                id: "WIZARD.CUSTOMER.BUSINESS.INFO",
              })}
              validationSchema={businessSchema}
            >
              <BusinessContactInfo />
            </WizardFormStep>
          )}

          <WizardFormStep
            label={intl.formatMessage({
              id: "WIZARD.CUSTOMER.BUSINESS.MANAGER_INFO",
            })}
            validationSchema={personalInfoSchema}
          >
            <FormCustomerPersonalInfo />
            {/* Toggle section */}
            <div className="d-flex gap-4 align-items-center my-10">
              <FormToggle
                name="toggle"
                labelTextId="FORM.LABELS.ADD_VEHICLES"
                disabled={customer?.vehicles?.length > 0}
                checked={toggle.vehicles}
                onChange={(evt) => {
                  toggleSection("vehicles", evt.target.checked);
                }}
              />

              <FormToggle
                name="toggle"
                labelTextId="FORM.LABELS.ADD_RELATIONSHIPS"
                disabled={customer?.cohorts?.length > 0}
                checked={toggle.cohorts}
                onChange={(evt) => {
                  toggleSection("cohorts", evt.target.checked);
                }}
              />
            </div>
          </WizardFormStep>

          {toggle.vehicles && (
            <WizardFormStep
              label={intl.formatMessage({ id: "WIZARD.CUSTOMER.VEHICLES" })}
              validationSchema={vehiclesSchema}
            >
              <VehiclesRepeater />
            </WizardFormStep>
          )}

          {toggle.cohorts && (
            <WizardFormStep
              label={intl.formatMessage({ id: "WIZARD.CUSTOMER.COHORTS" })}
              validationSchema={cohortsSchema}
            >
              <CohortsRepeater />
            </WizardFormStep>
          )}

          <WizardFormStep
            label={intl.formatMessage({ id: "WIZARD.CUSTOMER.SETTINGS" })}
            validationSchema={customerSettingsSchema}
          >
            <FormCustomerSetting />
          </WizardFormStep>
        </WizardForm>
      </Modal.Body>
    </Modal>
  );
};
