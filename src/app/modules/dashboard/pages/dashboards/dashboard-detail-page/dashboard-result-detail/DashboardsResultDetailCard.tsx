import { useSnackbar } from "notistack";
import { useEffect } from "react";
import { Card, ProgressBar } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageDetailTopButtons } from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch } from "src/app/hooks";
import * as actions from "../../../../_redux/dashboards/dashboardsActions";
import { DashboardOverview } from "../../DashboardOverview";
import { DashboardsResultDetailFilterComponent } from "./dashboards-result-detail-filter/DashboardsResultDetailFilterComponent";
import { DashboardsResultDetailTable } from "./dashboards-result-detail-table/DashboardsResultDetailTable";

export function DashboardsResultDetailCard() {
  const { code }: any = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { actionsLoading, dashboardForEdit, error } = useSelector(
    (state: any) => ({
      actionsLoading: state.dashboards.actionsLoading,
      dashboardForEdit: state.dashboards.dashboardForEdit,
      error: state.dashboards.error,
    }),
    shallowEqual
  );

  const { enqueueSnackbar } = useSnackbar();
  useEffect(() => {
    error &&
      enqueueSnackbar(error, {
        variant: "error",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
    error && navigate(`/not-found`);
  }, [enqueueSnackbar, error, history]);

  const goToEditDashboard = () => {
    navigate(`/dashboards/${code}/edit`);
  };

  const deleteDashboard = () =>
    dispatch(actions.deleteDashboard(code)).then(() => {
      navigate("/dashboards");
    });

  return (
    <>
      {actionsLoading && <ProgressBar />}
      {
        <PageDetailTopButtons
          editFunction={goToEditDashboard}
          deleteFunction={deleteDashboard}
          editPermission={Permissions.DASHBOARDS_UPDATE}
          deletePermission={Permissions.DASHBOARDS_DELETE}
          componentLabel="dashboard"
        />
      }
      <DashboardOverview dashboard={dashboardForEdit} />
      <Card>
        <Card.Body>
          <DashboardsResultDetailFilterComponent />
          <DashboardsResultDetailTable />
        </Card.Body>
      </Card>
    </>
  );
}
