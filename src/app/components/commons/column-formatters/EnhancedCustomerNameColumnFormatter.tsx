import { isEmpty } from "lodash";
import { Fragment } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link } from "react-router-dom";
import { CopyToClipboardLink } from "../../Widgets";

export const EnhancedCustomerNameColumnFormatter = (
  customerName: any,
  row: any
) => (
  <Fragment>
    <Link
      to={`/customers/${row.customerCode}/detail`}
      className="text-dark fw-semibold text-hover-primary fs-6"
    >
      {customerName}
    </Link>
    <br />
    <span className="text-muted fw-semibold text-muted d-block fs-7">
      <span>
        {!isEmpty(row.usDOT) && (
          <CopyToClipboardLink content={row.usDOT} fieldId={"FORM.LABELS.DOT"}>
            <span>DOT&nbsp;</span> {row.usDOT}
          </CopyToClipboardLink>
        )}
      </span>
    </span>

    {isEmpty(row.usDOT) && (
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="quick-actions-tooltip">
            <span className="">DOT is missing</span>
          </Tooltip>
        }
      >
        <div className="text-muted fw-semibold text-muted d-block fs-7">
          <span>DOT&nbsp;</span>{" "}
          <i className="fa fa-exclamation-circle text-warning"></i>
        </div>
      </OverlayTrigger>
    )}
  </Fragment>
);
