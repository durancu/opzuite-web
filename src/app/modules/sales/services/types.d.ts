// Generated with quicktype.io
interface Service {
  _id: string;
  __v: number;
  checksumSanity: number;
  code: string;
  company: string;
  country: string;
  createdAt: Date;
  createdBy: string;
  customer: string;
  deleted: boolean;
  details: Details;
  isChargeItemized: boolean;
  items: any[];
  location: string;
  products: Product[];
  seller: string;
  soldAt: Date;
  states: string;
  status: string;
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalCoveragesDownPayment: number;
  totalCoveragesPremium: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPermits: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;
  type: string;
  updatedAt: Date;
  autoRenew: boolean;
  number: string;
  renewalFrequency: string;
  renewalReferences: any[];
  updatedBy: string;
  wasRenewed: boolean;
  endorsements: Endorsement[];
}

interface Endorsement {
  _id: string;
  amount: number;
  items: Item[];
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonCommissionablePremium: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;
  deleted: boolean;
  description: string;
  endorsedAt: Date;
  status: string;
  type: string;
  name: string;
  seller: string;
  createdBy: string;
  company: string;
  sale: string;
  createdAt: Date;
  updatedAt: Date;
  code: string;
  updatedBy: string;
  followUpDate: Date;
  followUpPerson?: string;
  id: string;
}

interface Item {
  accountingClass: string;
  amount: number;
  amountPaid: number;
  balance: number;
  code: string;
  commissionUnit: string;
  description: string;
  endorsedAt: Date;
  followUpDate: null;
  otherDetails: OtherDetails;
  payments: any[];
  seller: string;
  status: string;
  type: string;
  _id: string;
}

interface OtherDetails {
  remitTo?: string;
  description?: string;
  processedByAgency?: boolean;
}

interface Product {
  id?: string;
  iconLabel: string;
  name: string;
  description: string;
  type?: string;
  amount?: number;
  applicationType?: string;
}
