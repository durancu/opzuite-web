import { roundAmount } from ".";
import { Commission } from "../interfaces/Commission";
import { Insurer } from "../interfaces/Insurer";
import { SaleItem } from "../interfaces/SaleItem";

export const agencyCommissionByCoverage = (
  providers: Partial<Insurer>[],
  item: SaleItem
): number => {
  let agencyCommission = 0;

  const coverageProvider: any = item.broker ? item.broker : item.carrier;

  const foundProvider = providers.find(
    (provider) => provider?.id === coverageProvider
  );

  if (foundProvider) {
    let commission: Partial<Commission> = { percent: 0 };

    commission = foundProvider.commissions?.find(
      (commission) => commission.coverage === item.product
    );

    if (commission) {
      agencyCommission = roundAmount(
        ((commission.percent || 0) / 100) * (item.premium || 0)
      );
    }
  }

  return agencyCommission;
};
