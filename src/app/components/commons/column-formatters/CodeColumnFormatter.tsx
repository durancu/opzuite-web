/* eslint-disable no-script-url */
import { CopyToClipboard } from "../../Widgets";

export const CodeColumnFormatter = (cellContent: any) => (
  <>
    <CopyToClipboard content={cellContent} className="mr-2" />
  </>
);
