// Stats
export { PoliciesAmountByWeekDayWidget } from "./stats/PoliciesAmountByWeekDayWidget";
export { TotalSalesByLocationWidget } from "./stats/TotalSalesByLocationWidget";
export { TotalSalesBySellerWidget } from "./stats/TotalSalesBySellerWidget";
export { InfoMessageWidget } from "./texts/InfoMessageWidget";
