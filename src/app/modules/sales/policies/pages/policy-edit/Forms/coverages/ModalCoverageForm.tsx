import { Field, useFormikContext } from "formik";
import { nanoid } from "nanoid";
import objectPath from "object-path";
import { Fragment, useEffect, useState } from "react";
import { Button, Col, Form, Modal, Row, Tab, Tabs } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import {
  CohortsMultiselect,
  FormAutoComplete,
  FormInput,
  FormTextArea,
  VehiclesMultiselect,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { useGetUserAuth } from "src/app/hooks";
import { GenericFunction } from "src/app/components/interfaces/FunctionInterfaces";
import {
  AddCohortModal,
  AddVehicleModal,
} from "src/app/modules/customers/forms";
import { customersAPI } from "src/app/modules/customers/redux";
import { CoverageActions } from "./CoverageActionTypes";
import ItemReplaceModal from "./modal-replace-coverage/ItemReplaceModal";

type MODAL_ACTION = "EDIT" | "REPLACE" | "ADD" | "";

interface Props {
  index: number;
  show: boolean;
  onHide: () => void;
  actionType: MODAL_ACTION;
  recalculateInitialBasePremium: GenericFunction;
  insuredByBroker: boolean;
}

const ModalCoverageForm = ({
  index,
  show,
  onHide,
  recalculateInitialBasePremium,
  actionType,
  insuredByBroker,
}: Props) => {
  const user = useGetUserAuth();

  const { setFieldValue, values, errors } = useFormikContext<any>();

  const item = values?.items?.[index] || {};

  const [commissionSwitchEnabled, setCommissionSwitchEnabled] =
    useState<boolean>(true);

  const { linesOfBusiness, brokers, carriers, saleStatus, users, coverages } =
    useGetCatalog();

  useEffect(() => {
    let notCommissionable: any = false;
    const commissionPlans =
      user?.company?.settings?.insurersCommissionPlans || [];

    if (commissionPlans && item?.carrier) {
      notCommissionable = commissionPlans.find(
        (plan: any) => plan.insurer === item?.carrier
      );

      setFieldValue(
        `items.${index}.isNonComissionablePremium`,
        notCommissionable ? true : false
      );
    }

    setCommissionSwitchEnabled(!notCommissionable);
  }, [item?.carrier, user?.company]);

  const savePolicy = () => {
    if (!item.nonCommissionableReferenceId) {
      item.nonCommissionableReferenceId = nanoid(6);
    }

    setFieldValue(`items.${index}`, item);
    recalculateInitialBasePremium(true);
    onHide();
  };

  const replacePolicy = () => {
    setFieldValue(`items.${index}.history`, [
      ...item.history,
      {
        customer: values.customer,
        policy: item,
        action: CoverageActions.replace,
        deleted: false,
      },
    ]);
    recalculateInitialBasePremium(true);
    onHide();
  };

  function handleClose() {
    if (item.newItem || actionType === "ADD") {
      setFieldValue("items", values.items.slice(0, index));
    }
    onHide();
  }

  function formatName(name: string) {
    return index !== undefined ? `items.${index}.${name}` : name;
  }

  const { data: customer } = customersAPI.useGetCustomerByIdQuery(
    values.customer
  );

  return (
    <Modal show={show} size="xl" centered backdrop="static">
      <Modal.Header className="p-6">
        <Modal.Title className="text-primary">
          Coverage {item?.name}
        </Modal.Title>

        <Form.Group className="d-flex gap-2 align-self-end">
          <Field
            as={Form.Check}
            type="switch"
            name={formatName("isNonCommissionablePremium")}
            checked={item["isNonCommissionablePremium"]}
            disabled={!commissionSwitchEnabled}
          />
          <Form.Label>Remove from sale commissions calculations</Form.Label>
        </Form.Group>
      </Modal.Header>
      <Modal.Body>
        <Tabs defaultActiveKey="base" className="mb-4">
          <Tab eventKey="base" title="Base Coverage">
            <Row className="mb-8">
              <Col>
                {linesOfBusiness && (
                  <FormAutoComplete
                    name={formatName("lineOfBusiness")}
                    labelText="Line of Business"
                    options={linesOfBusiness}
                    disabled
                  />
                )}
              </Col>
              <Col>
                {coverages && (
                  <FormAutoComplete
                    name={formatName("product")}
                    labelText="Coverage"
                    options={coverages}
                    disabled
                  />
                )}
              </Col>
              <Col>
                <FormAutoComplete
                  name={formatName("broker")}
                  labelText="MGA"
                  options={brokers}
                  disabled
                />
              </Col>
            </Row>

            {/* Status and seller */}
            <Row className="mb-8">
              <Col>
                <FormAutoComplete
                  name={formatName("carrier")}
                  labelText="Carrier"
                  options={carriers}
                  disabled={!insuredByBroker}
                  onChange={(option) => {
                    const toggle = option.name === "Clear Blue";
                    setFieldValue(`items.${index}.carrier`, option.id);
                    setFieldValue(
                      `items.${index}.isNonCommissionablePremium`,
                      toggle
                    );
                    setCommissionSwitchEnabled(!toggle);
                  }}
                />
              </Col>
              <Col>
                <FormAutoComplete
                  name={formatName("status")}
                  labelText="Status"
                  options={saleStatus}
                  value={item?.status || "PENDING"}
                  helperText="Status should be set as Pending while the policy is not
                  effective."
                />
              </Col>
              <Col>
                <FormAutoComplete
                  name={formatName("seller")}
                  labelText="Seller"
                  options={users}
                />
              </Col>
            </Row>

            {/* Premium to deductibles */}
            <Row className="mb-8">
              <Col>
                <FormInput
                  type="number"
                  labelText="Premium"
                  name={formatName("premium")}
                />
              </Col>
              <Col>
                <FormInput
                  type="number"
                  labelText="Down Payment"
                  name={formatName("amount")}
                />
              </Col>
              <Col>
                <FormInput
                  type="number"
                  labelText="Limit"
                  name={formatName("limit")}
                />
              </Col>
              <Col>
                <FormInput
                  type="number"
                  labelText="Deductible"
                  name={formatName("deductible")}
                />
              </Col>
              <Col>
                <FormInput
                  type="number"
                  labelText="Compensation"
                  name={formatName("compensation")}
                />
              </Col>
            </Row>
          </Tab>

          {/* General specific fields, Vehicles and Cohorts */}
          <Tab eventKey="other" title="Additional Information">
            {/* General, Automobile Umbrella/Excess and Workers/Employers Sepcific fields */}
            {[
              "GENERAL_LIABILITY",
              "AUTOMOBILE_LIABILITY",
              "EXCESS_UMBRELLA_LIABILITY",
              "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
            ].includes(item?.product) && (
              <Fragment>
                {/* Workers and Employers */}
                {item?.product ===
                  "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
                  <Fragment>
                    <Row className="mb-8">
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.hasExcludedPeople}
                          name={formatName("details.hasExcludedPeople")}
                          label="Any Proprietor/Partner/Executive/Officer/Member Excluded?"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.perStatute}
                          name={formatName("details.perStatute")}
                          label="Per Statute"
                        />
                      </Col>

                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.other}
                          name={formatName("details.other")}
                          label="Other"
                        />
                      </Col>

                      {item?.details?.hasExcludedPeople && (
                        <Col md={12} className="my-4">
                          <FormTextArea
                            labelText="Description of People Excluded"
                            name={formatName(
                              "details.excludedPeopleDescription"
                            )}
                          />
                        </Col>
                      )}
                    </Row>
                    <Row className="mb-8">
                      <Col>
                        <FormInput
                          type="number"
                          labelText="Employer's Liability Each Accident Limit"
                          name={formatName(
                            "details.employerLiabilityEachAccidentLimit"
                          )}
                        />
                      </Col>
                      <Col>
                        <FormInput
                          type="number"
                          labelText="Employer's Liability Each Employee Limit"
                          name={formatName(
                            "details.employerLiabilityEachEmployeeLimit"
                          )}
                        />
                      </Col>
                      <Col>
                        <FormInput
                          type="number"
                          labelText="Employer's Liability Disease Policy Limit"
                          name={formatName(
                            "details.employerLiabilityDiseasePolicyLimit"
                          )}
                        />
                      </Col>
                    </Row>
                  </Fragment>
                )}
                {/* Comercial Auto specific */}
                {item?.product === "AUTOMOBILE_LIABILITY" && (
                  <Fragment>
                    <Row className="mb-8">
                      <Col>
                        <FormAutoComplete
                          name={formatName("details.autoCoverageScope")}
                          labelText="Auto Coverage"
                          options={[
                            { name: "Any Auto", id: "ANY" },
                            { name: "Scheduled Auto", id: "SCHEDULED" },
                          ]}
                          onChange={(value) => {
                            let toggle = false;
                            if (value.id === "ANY") {
                              toggle = true;
                            }

                            setFieldValue(
                              `items.${index}.details.includeOwnedAutos`,
                              toggle
                            );
                            setFieldValue(
                              `items.${index}.details.includeHiredAutos`,
                              toggle
                            );

                            setFieldValue(
                              `items.${index}.details.includeNonOwnedAutos`,
                              toggle
                            );
                          }}
                        />
                      </Col>
                    </Row>

                    <Row className="mb-8 d-flex gap-4 align-items-start">
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeOwnedAutos}
                          disabled={item?.details?.autoCoverageScope === "ANY"}
                          name={formatName("details.includeOwnedAutos")}
                          label="Include Owned Autos"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeHiredAutos}
                          disabled={item?.details?.autoCoverageScope === "ANY"}
                          name={formatName("details.includeHiredAutos")}
                          label="Include Hired Autos"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeNonOwnedAutos}
                          disabled={item?.details?.autoCoverageScope === "ANY"}
                          name={formatName("details.includeNonOwnedAutos")}
                          label="Include Non-Owned Autos"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeOtherCoveredAutoA}
                          name={formatName("details.includeOtherCoveredAutoA")}
                          label="Include Other Covered Auto A"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeOtherCoveredAutoB}
                          name={formatName("details.includeOtherCoveredAutoB")}
                          label="Include Other Covered Auto B"
                        />
                      </Col>
                    </Row>

                    <Row className="mb-8">
                      {item?.details?.includeOtherCoveredAutoA && (
                        <Col>
                          <FormInput
                            labelText="Other Covered Auto A Description"
                            name={formatName(
                              "details.otherCoveredAutoADescription"
                            )}
                          />
                        </Col>
                      )}
                      {item?.details?.includeOtherCoveredAutoB && (
                        <Col>
                          <FormInput
                            labelText="Other Covered Auto B Description"
                            name={formatName(
                              "details.otherCoveredAutoBDescription"
                            )}
                          />
                        </Col>
                      )}
                    </Row>

                    <Row className="mb-8">
                      <Col>
                        <FormInput
                          type="number"
                          labelText="Combined Single Limit (ea Accident)"
                          name={formatName("details.combinedSingleLimit")}
                        />
                      </Col>
                      <Col>
                        <FormInput
                          type="number"
                          labelText="Bodily Injury (Per Person)"
                          name={formatName(
                            "details.bodilyInjuryPerPersonLimit"
                          )}
                        />
                      </Col>

                      <Col>
                        <FormInput
                          type="number"
                          labelText="Bodily Injury (Per Accident)"
                          name={formatName(
                            "details.bodilyInjuryPerAccidentLimit"
                          )}
                        />
                      </Col>

                      <Col>
                        <FormInput
                          type="number"
                          labelText="Property Damage (Per Accident)"
                          name={formatName(
                            "details.propertyDamagePerAccidentLimit"
                          )}
                        />
                      </Col>
                    </Row>
                  </Fragment>
                )}

                {/* Umbrella/Excess specific */}
                {item?.product === "EXCESS_UMBRELLA_LIABILITY" && (
                  <Row className="mb-8">
                    <Col>
                      <FormAutoComplete
                        name={formatName("details.coverageFormUsed")}
                        labelText="Coverage Form Used"
                        options={[
                          { name: "Umbrella Liability", id: "UMBRELLA" },
                          { name: "Excess Liability", id: "EXCESS" },
                        ]}
                      />
                    </Col>

                    <Col>
                      <FormInput
                        type="number"
                        labelText="Retention Amount"
                        name={formatName("details.deductibleOrRetentionAmount")}
                      />
                    </Col>
                    <Col className="d-flex justify-content-center align-items-center">
                      <Field
                        as={Form.Check}
                        checked={item?.details?.hasDeductible}
                        name={formatName("details.hasDeductible")}
                        label="Has Deductible"
                        className="mt-8"
                      />
                    </Col>
                    <Col className="d-flex justify-content-center align-items-center">
                      <Field
                        as={Form.Check}
                        checked={item?.details?.hasRetention}
                        name={formatName("details.hasRetention")}
                        label="Has Retention"
                        className="mt-8"
                      />
                    </Col>
                  </Row>
                )}

                {/* Shared rows, for excess and general only  */}
                {![
                  "AUTOMOBILE_LIABILITY",
                  "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
                ].includes(item?.product) && (
                  <Row className="mb-8">
                    <Col>
                      <FormAutoComplete
                        name={formatName("details.policyBasedForm")}
                        labelText="Policy Based Form"
                        options={[
                          { name: "Claims-based form", id: "CLAIMS" },
                          { name: "Occurrence-based form", id: "OCCURENCE" },
                        ]}
                      />
                    </Col>

                    <Col className="mb-4">
                      <FormInput
                        type="number"
                        labelText="Each Occurrence Limit"
                        name={formatName("details.eachOccurrenceLimit")}
                      />
                    </Col>
                    <Col className="mb-4">
                      <FormInput
                        type="number"
                        labelText="General Aggregate Limit "
                        name={formatName("details.generalAggregateLimit")}
                      />
                    </Col>
                  </Row>
                )}

                {/* Checkboxes  */}
                <Row className="mb-8 d-flex gap-4 align-items-center">
                  {item?.product !==
                    "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
                    <Col>
                      <Field
                        as={Form.Check}
                        checked={item?.details?.additionalInsured}
                        name={formatName("details.additionalInsured")}
                        label="Additional Insured"
                      />
                    </Col>
                  )}
                  <Col>
                    <Field
                      as={Form.Check}
                      checked={item?.details?.waiverOfSubrogation}
                      name={formatName("details.waiverOfSubrogation")}
                      label="Waiver of Subrogation?"
                    />
                  </Col>
                </Row>

                {/* General Specific */}
                {item?.product === "GENERAL_LIABILITY" && (
                  <Fragment>
                    <Row className="mb-8 d-flex gap-4 align-items-start">
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeOtherCoverageA}
                          name={formatName("details.includeOtherCoverageA")}
                          label="Include Other Coverage (A)?"
                        />
                      </Col>
                      <Col>
                        <Field
                          as={Form.Check}
                          checked={item?.details?.includeOtherCoverageB}
                          name={formatName("details.includeOtherCoverageB")}
                          label="Include Other Coverage (B)?"
                        />
                      </Col>
                    </Row>

                    <Row className="mb-8">
                      {item?.details?.includeOtherCoverageA && (
                        <Col>
                          <FormInput
                            labelText="Other Coverage (A) Description"
                            name={formatName(
                              "details.otherCoverageADescription"
                            )}
                          />
                        </Col>
                      )}
                      {item?.details?.includeOtherCoverageB && (
                        <Col>
                          <FormInput
                            labelText="Other Coverage (B) Description"
                            name={formatName(
                              "details.otherCoverageBDescription"
                            )}
                          />
                        </Col>
                      )}
                    </Row>
                    <Row className="mb-8">
                      <Col md={4}>
                        <FormAutoComplete
                          name={formatName(
                            "details.generalAggregateLimitAppliesPer"
                          )}
                          labelText="General Aggregate Limit Applies Per"
                          options={[
                            { name: "Policy", id: "POLICY" },
                            { name: "Project", id: "PROJECT" },
                            { name: "Location", id: "LOCATION" },
                            { name: "Other", id: "OTHER" },
                          ]}
                        />
                      </Col>

                      {item?.details?.generalAggregateLimitAppliesPer ===
                        "OTHER" && (
                        <Col md={4}>
                          <FormInput
                            labelText="Enter Other"
                            name={formatName(
                              "details.generalAggregateLimitAppliesPerOther"
                            )}
                          />
                        </Col>
                      )}
                      <Col md={4} className="mb-4">
                        <FormInput
                          type="number"
                          labelText="Damage to Rented Premises Limit"
                          name={formatName(
                            "details.damageToRentedPremisesLimit"
                          )}
                        />
                      </Col>
                      <Col md={4} className="mb-4">
                        <FormInput
                          type="number"
                          labelText="Medical Expenses Limit"
                          name={formatName("details.medicalExpensesLimit")}
                        />
                      </Col>
                      <Col md={4} className="mb-4">
                        <FormInput
                          type="number"
                          labelText="Personal and Advertising Injury Limit"
                          name={formatName(
                            "details.personalAndAdvertisingInjuryLimit"
                          )}
                        />
                      </Col>

                      <Col md={4} className="mb-4">
                        <FormInput
                          type="number"
                          labelText="Products/Completed Operations Aggregate Limit"
                          name={formatName(
                            "details.productCompletedOperationsAggregateLimit"
                          )}
                        />
                      </Col>
                    </Row>
                  </Fragment>
                )}

                {/* Other  limits except workers and employers*/}
                {item?.product !==
                  "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY" && (
                  <Row className="mb-8">
                    <Col>
                      <FormInput
                        labelText="Other Limit 1 Name"
                        name={formatName("details.other1LimitName")}
                      />
                    </Col>
                    <Col>
                      <FormInput
                        type="number"
                        labelText="Other Limit 1 Amount"
                        name={formatName("details.other1LimitAmount")}
                      />
                    </Col>

                    {/* Shared limits, for excess and general only */}
                    {item?.product !== "AUTOMOBILE_LIABILITY" && (
                      <Fragment>
                        <Col>
                          <FormInput
                            labelText="Other Limit 2 Name"
                            name={formatName("details.other2LimitName")}
                          />
                        </Col>
                        <Col>
                          <FormInput
                            type="number"
                            labelText="Other Limit 2 Amount"
                            name={formatName("details.other2LimitAmount")}
                          />
                        </Col>
                      </Fragment>
                    )}
                  </Row>
                )}
              </Fragment>
            )}

            {/* Vehicles and cohorts */}
            <Row className="form-group mb-6 mt-9">
              {![
                "WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY",
                "OCCUPATIONAL_ACCIDENT",
              ].includes(item?.product) && (
                <Col>
                  <VehiclesMultiselect
                    value={item.vehicles || []}
                    options={customer?.vehicles || []}
                    labelText="Co-insured Vehicles"
                    helperText="Attach an existent vehicle or add a new one."
                    onChange={(value: any) => {
                      setFieldValue(`items.${index}.vehicles`, value);
                    }}
                  />
                  <br />
                  <AddVehicleModal customerId={values.customer} />
                </Col>
              )}
              {((![
                "AUTOMOBILE_LIABILITY",
                "PHYSICAL_DAMAGE",
                "TRAILER_INTERCHANGE_PHYSICAL_DAMAGE",
              ].includes(item?.product) &&
                !["BOBTAIL_LIABILITY", "NON_OWNED_TRAILER_LIABILITY"].includes(
                  item?.lineOfBusiness
                )) ||
                item?.details?.autoCoverageScope === "SCHEDULED") && (
                <Col>
                  <CohortsMultiselect
                    value={item.cohorts || []}
                    options={customer?.cohorts || []}
                    labelText="Co-insured People"
                    helperText="Attach an existent customer's related people or add a new one."
                    onChange={(value: any) => {
                      setFieldValue(`items.${index}.cohorts`, value);
                    }}
                  />
                  <br />
                  <AddCohortModal customerId={values.customer} />
                </Col>
              )}
            </Row>
          </Tab>
        </Tabs>
      </Modal.Body>

      <Modal.Footer>
        <Row>
          <Col className="text-left">
            <Button onClick={handleClose} variant="secondary" color="inherit">
              <FormattedMessage id="BUTTON.CANCEL" />
            </Button>
          </Col>
          <Col className="text-right">
            {actionType === "REPLACE" ? (
              <ItemReplaceModal
                itemTitle={item?.name}
                replaceAction={replacePolicy}
              />
            ) : (
              <Button
                onClick={() => savePolicy()}
                disabled={objectPath.get(errors, `items.${index}`)}
                className="btn btn-lg btn-dark ms-auto d-flex align-items-center gap-2"
              >
                <span className="indicator-label">
                  <FormattedMessage id="BUTTON.CONFIRM" />
                </span>
                <i className="fa fs-4 fa-check"></i>
              </Button>
            )}
          </Col>
        </Row>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalCoverageForm;
