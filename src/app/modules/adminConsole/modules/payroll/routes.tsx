// routes module, to be imported in admin-console/routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Payroll } from "./pages/payroll/Payroll";
import { PayrollDetailPage } from "./pages/payroll/details/Details";
import { Edit } from "./pages/edit/Edit";

export const payrollRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.PAYROLLS_READ} isRoute>
        <Payroll />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.PAYROLLS_CREATE} isRoute>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: "generate",
    element: (
      <RequirePermission permissions={Permissions.MY_COMPANY} isRoute>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.PAYROLLS_UPDATE} isRoute>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.PAYROLLS_READ} isRoute>
        <PayrollDetailPage />
      </RequirePermission>
    ),
  },
];
