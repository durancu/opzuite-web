
import { IPV4_ADDRESS_REGEX, PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const locationFormSchema = Yup.object().shape({
  alias: Yup.string(),
  ipAddress: Yup.string()
    .matches(IPV4_ADDRESS_REGEX, "Must be a valid IP v4 address (x.x.x.x)")
    .required("IP Address is required."),
  business: Yup.object().shape({
    email: Yup.string().email("Must be a valid email address"),
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      country: Yup.string(),
      city: Yup.string(),
      state: Yup.string().required("Please select a state"),
      zip: Yup.string(),
    }),
    name: Yup.string().required("Name is required."),
    otherPhones: Yup.array(),
    primaryPhone: Yup.string()
      .nullable()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    payFrequency: Yup.string(),
    primaryPhoneExtension: Yup.string(),
    secondaryPhone: Yup.string()
      .nullable()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    secondaryPhoneExtension: Yup.string(),
    startedAt: Yup.mixed(),
  }),
  type: Yup.string(),
  website: Yup.string(),
  manager: Yup.string().required("Please select a manager"),
  payFrequency: Yup.string().required("Please select pay frequency"),
});
