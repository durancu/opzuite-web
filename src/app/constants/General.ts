export const CUSTOMER_TYPES = {
  INDIVIDUAL: "INDIVIDUAL",
  BUSINESS: "BUSINESS",
};

export const METRIC_COLORS: any = {
  premium: "#333333",
  "non-premium": "#cccccc",
  "taxes-and-fees": "#ef7a1a",
  payables: "#4781eb",
  receivables: "#7f32ec",
  financed: "#4781eb",
  "agency-commission": "#1a9e2b",
  "agent-commission": "#d4a121",
  "checksum-sanity": "#bd0808",
};

export const SELECT_ALL = { name: "All", id: "all" };
