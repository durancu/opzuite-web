import clsx from "clsx";
import { FormattedMessage } from "react-intl";
import { NavLink } from "react-router-dom";
import { KTSVG, toAbsoluteUrl } from "src/_metronic/helpers";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { useGetProfileQuery } from "../../profile/redux";

export const AdminHeader = () => {
  const { data: user = {} } = useGetProfileQuery();

  // don't display header if no profile
  if (!Object.keys(user).length) return null;

  const { address1, address2, city, state, country, zip } = user.address;
  const fullAddress = `${address1}${
    address2 ? ` ${address2}` : ""
  }, ${city}, ${state}, ${country} ${zip}`;

  return (
    <div className="card mb-5 mb-xl-10">
      <div className="card-body pt-9 pb-0">
        <div className="d-flex flex-wrap flex-sm-nowrap mb-3">
          <div className="me-7 mb-4">
            <div className="symbol symbol-80px symbol-lg-95px symbol-fixed position-relative border">
              <img
                src={
                  user?.company.settings.logo ||
                  toAbsoluteUrl("/media/logos/logo-icon.png")
                }
                alt="Inzuite LLC"
              />
              <div className="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-15px w-15px"></div>
            </div>
          </div>

          <div className="flex-grow-1">
            <div className="d-flex justify-content-between align-items-start flex-wrap mb-2">
              <div className="d-flex flex-column">
                <div className="d-flex align-items-center mb-2">
                  <span className="text-gray-800 fs-2 fw-bolder me-1">
                    {user?.company?.name}
                  </span>

                  <KTSVG
                    path="/media/icons/duotune/general/gen026.svg"
                    className="svg-icon-1 svg-icon-primary"
                  />
                </div>

                <div className="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
                  <span className="d-flex align-items-center text-gray-400 me-5 mb-2">
                    <KTSVG
                      path="/media/icons/duotune/communication/com006.svg"
                      className="svg-icon-4 me-1"
                    />
                    {user.company.name}
                  </span>
                  <span className="d-flex align-items-center text-gray-400 me-5 mb-2">
                    <KTSVG
                      path="/media/icons/duotune/general/gen018.svg"
                      className="svg-icon-4 me-1"
                    />
                    {fullAddress}
                  </span>
                  <span className="d-flex align-items-center text-gray-400 mb-2">
                    <KTSVG
                      path="/media/icons/duotune/communication/com011.svg"
                      className="svg-icon-4 me-1"
                    />
                    {user.company.email}
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex overflow-auto h-55px">
          <ul className="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bolder flex-nowrap">
            {/* <li className="nav-item">
              <NavLink
                end
                to="/manager"
                className={({ isActive }) =>
                  clsx("nav-link text-active-primary me-6", {
                    active: isActive,
                  })
                }
              >
                <FormattedMessage id="MENU.MAIN.OVERVIEW" />
              </NavLink>
            </li> */}
            <RequirePermission
              permissions={[
                Permissions.USERS,
                Permissions.USERS_CREATE,
                Permissions.USERS_UPDATE,
                Permissions.USERS_DELETE,
              ]}
            >
              <li className="nav-item">
                <NavLink
                  to="users"
                  className={({ isActive }) =>
                    clsx("nav-link text-active-primary me-6", {
                      active: isActive,
                    })
                  }
                >
                  <FormattedMessage id="MENU.MAIN.EMPLOYEES" />
                </NavLink>
              </li>
            </RequirePermission>
            <RequirePermission permissions={Permissions.COMPANIES}>
              <li className="nav-item">
                <NavLink
                  to="companies"
                  className={({ isActive }) =>
                    clsx("nav-link text-active-primary me-6", {
                      active: isActive,
                    })
                  }
                >
                  <FormattedMessage id="MENU.MAIN.COMPANIES" />
                </NavLink>
              </li>
            </RequirePermission>
            <RequirePermission permissions={Permissions.LOCATIONS}>
              <li className="nav-item">
                <NavLink
                  to="locations"
                  className={({ isActive }) =>
                    clsx("nav-link text-active-primary me-6", {
                      active: isActive,
                    })
                  }
                >
                  <FormattedMessage id="MENU.MAIN.LOCATIONS" />
                </NavLink>
              </li>
            </RequirePermission>
            <RequirePermission permissions={Permissions.REPORTS}>
              <li className="nav-item">
                <NavLink
                  to="reports"
                  className={({ isActive }) =>
                    clsx("nav-link text-active-primary me-6", {
                      active: isActive,
                    })
                  }
                >
                  <FormattedMessage id="MENU.MAIN.REPORTS" />
                </NavLink>
              </li>
            </RequirePermission>
            <RequirePermission permissions={Permissions.PAYROLLS}>
              <li className="nav-item">
                <NavLink
                  to="payrolls"
                  className={({ isActive }) =>
                    clsx("nav-link text-active-primary me-6", {
                      active: isActive,
                    })
                  }
                >
                  <FormattedMessage id="MENU.MAIN.PAYROLLS" />
                </NavLink>
              </li>
            </RequirePermission>
          </ul>
        </div>
      </div>
    </div>
  );
};
