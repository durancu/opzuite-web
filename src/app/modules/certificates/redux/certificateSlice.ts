import { createSlice } from "@reduxjs/toolkit";
import type { RootState } from "src/redux/store";
import type { Certificate } from "../types";

import { API } from "src/redux/api";

export const certificatesAPI = API.injectEndpoints({
  endpoints: (build) => ({
    searchCertificates: build.query<any, QueryParams>({
      query: (queryParams) => ({
        url: "/certificates/search",
        method: "post",
        body: {
          queryParams,
        },
      }),
    }),
    findByCode: build.query<any, any>({
      query: (code) => `/certificates/${code}`,
    }),
    init: build.query<any, any>({
      query: (id) => ({
        url: "/certificates/init",
        method: "post",
        body: {
          customer: id,
        },
      }),
    }),
    generate: build.mutation<any, any>({
      query: (data) => ({
        url: "/certificates/generate",
        method: "post",
        body: data,
        responseHandler: (response) => response.blob(),
      }),
      transformResponse: (response: Blob) => URL.createObjectURL(response),
    }),
    preview: build.query<any, any>({
      query: (code) => ({
        url: `/certificates/${code}/preview`,
        responseHandler: (response) => response.blob(),
      }),
      transformResponse: (response: Blob) => URL.createObjectURL(response),
    }),
    send: build.mutation<any, any>({
      query: (data) => ({
        url: "/certificates",
        method: "post",
        body: data,
      }),
    }),
  }),
  overrideExisting: false,
});

const initialState: Certificate | any = {};

export const certificateSlice = createSlice({
  name: "certificate",
  initialState,
  reducers: {
    saveCertificate: (state, { payload }) => {
      return {
        ...state,
        ...payload,
      };
    },
    clearCertificate: () => initialState,
  },
});

// auth selector
export const certificateSelector = (state: RootState) => state.certificates;

export const { saveCertificate, clearCertificate } = certificateSlice.actions;
export default certificateSlice.reducer;
