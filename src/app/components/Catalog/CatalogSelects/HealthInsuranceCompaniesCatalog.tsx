export const HEALTH_INSURANCE_COMPANIES_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "UHN", name: "United Healthcare (UNH)" },
  { id: "CVS", name: "CVS (CVS)" },
  { id: "ANTM", name: "Anthem (ANTM)" },
  { id: "CI", name: "Cigna (CI)" },
  { id: "HUM", name: "Humana (HUM)" },
  { id: "CNC", name: "Centene Corporation (CNC)" },
  { id: "MOH", name: "Molina Healthcare (MOH)" },
  { id: "BHG", name: "Bright Health Group (BHG)" },
  { id: "MPLN", name: "MultiPlan Corporation (MPLN)" },
  { id: "ALHC", name: "Alignment Healthcare (ALHC)" },
  { id: "OTHER", name: "Other" },
];
