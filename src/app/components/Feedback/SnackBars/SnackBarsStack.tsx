import { SnackbarProvider } from "notistack";

interface Props {
  children: any;
}

export const SnackBarsStack = (props: Props) => {
  return (
    <SnackbarProvider
      maxSnack={5}
      preventDuplicate
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
    >
      {props.children}
    </SnackbarProvider>
  );
};
