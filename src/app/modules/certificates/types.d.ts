// generated with the help of https://app.quicktype.io/

export interface Certificate {
  name: string;
  holder: string;
  ccFaxNumbers: any[];
  ccEmails: any[];
  coverPage: string;
  masterTemplate: string;
  cohortsIncluded: any[];
  vehiclesIncluded: any[];
  policiesIncluded: any[];
  equipment: any[];
  properties: any[];
  email: Email;
  description: Description;
}

export interface Description {
  template: string;
  message: string;
  addToAcord101: boolean;
}

export interface Email {
  subject: string;
  message: string;
  template: string;
  sendToMe: boolean;
  attachFilesForms: boolean;
  sendCopyToInsured: boolean;
}
