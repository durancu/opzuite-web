// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Policies } from "./pages/Policies";
import { Renewals } from "./pages/Renewals";
import { PolicyDeleteDialog } from "./pages/PolicyDeleteDialog";
import { PolicyDetailPage } from "./pages/policy-details-page/PolicyDetailPage";
import { PolicyEdit } from "./pages/policy-edit/PolicyEdit";

export const policyRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.POLICIES_READ}>
        <Policies />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.POLICIES_CREATE}>
        <PolicyEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail", // must always be before :code/:type
    element: (
      <RequirePermission permissions={Permissions.POLICIES_READ}>
        <PolicyDetailPage />
      </RequirePermission>
    ),
  },
  {
    path: ":code/:type", // type = edit | renew
    element: (
      <RequirePermission permissions={Permissions.POLICIES_UPDATE}>
        <PolicyEdit />
      </RequirePermission>
    ),
  },

  {
    path: "renewals",
    element: (
      <RequirePermission permissions={Permissions.POLICIES_READ}>
        <Renewals />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <PolicyDeleteDialog />,
  },
];
