import { Field, useFormikContext } from "formik";
import { Fragment, useEffect } from "react";
import { Col, Row, Form } from "react-bootstrap";
import { timezonesOptions } from "src/app/components/functions";
import { FormattedMessage } from "react-intl";
import { FormAutoComplete } from "src/app/components";

const timezones = timezonesOptions();

const FormUserSettings = () => {
  const { values, setFieldValue }: any = useFormikContext();

  useEffect(() => {
    // identify and set user timezone if exists. Defaults to US/Central
    const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    if (timezone) {
      setFieldValue("timezone", timezone);
    }
  }, [setFieldValue]);

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <Form.Group className="col">
            <Form.Label>
              <FormattedMessage id="FORM.LABELS.LANGUAGE" />
            </Form.Label>
            <Field as={Form.Select} name="language">
              <option value="en">English</option>
              <option value="es">Spanish</option>
            </Field>
          </Form.Group>
        </Col>
        <Col>
          <FormAutoComplete
            name="timezone"
            labelText="Timezone"
            options={timezones || []}
            labelField="name"
            helperText="Agent location/home (if remote worker) timezone."
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Form.Group className="col">
          <Form.Label>Email notifications</Form.Label>
          <Field
            as={Form.Check}
            type="switch"
            checked={values["communication"]["email"]}
            name="communication.email"
            label="Do you want to receive notifications by email?"
          />
        </Form.Group>
        <Form.Group className="col">
          <Form.Label>SMS notifications</Form.Label>
          <br />
          <Field
            as={Form.Check}
            type="switch"
            checked={values["communication"]["sms"]}
            name="communication.sms"
            label=" Do you want to receive notifications by sms?"
          />
        </Form.Group>
        <Form.Group className="col">
          <Form.Label>Phone notifications</Form.Label>
          <br />
          <Field
            as={Form.Check}
            type="switch"
            checked={values["communication"]["phone"]}
            name="communication.phone"
            label="Do you want to receive notifications by phone?"
          />
        </Form.Group>
      </Row>
      <Row>
        <Col md={12} hidden={values.code && values.code.length > 0}>
          <div className="mb-10 alert alert-custom alert-light-success alert-dismissible">
            <div className="alert-text font-weight-bold">
              After saving, the new employee will get an email with instructions
              to confirm their email address, and set a new password.
            </div>
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

export default FormUserSettings;
