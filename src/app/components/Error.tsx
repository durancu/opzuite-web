import { Alert, Button } from "react-bootstrap";

interface Props {
  show: boolean;
  title?: string;
  description?: string;
  actionLabel?: string;
  action: () => void;
}

export const Error = ({
  show,
  title,
  description,
  actionLabel,
  action = () => null,
}: Props) => {
  return (
    <Alert show={show} variant="danger">
      <Alert.Heading as="h2">{title || "Something went wrong"}</Alert.Heading>
      <p className="my-4 fs-6">
        {description || "The request was unsuccessful. Please try again"}
      </p>
      <Button onClick={() => action()} variant="danger">
        {actionLabel || "Try again"}
      </Button>
    </Alert>
  );
};
