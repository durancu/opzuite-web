import { GeneralTextField } from "./GeneralTextField";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function EmailTextField({
  helperText,
  helperTextId,
  labelText,
  labelTextId = "FORM.LABELS.EMAIL",
  fieldName = "email",
}: Props) {
  return (
    <GeneralTextField
      fieldName={fieldName}
      type="email"
      labelTextId={labelTextId}
      labelText={labelText}
      helperTextId={helperTextId}
      helperText={helperText}
    />
  );
}
