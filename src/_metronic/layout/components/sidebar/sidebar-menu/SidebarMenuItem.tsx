import clsx from "clsx";
import { FC } from "react";
import { NavLink } from "react-router-dom";
import { KTSVG, WithChildren } from "../../../../helpers";
import { useLayout } from "../../../core";

type Props = {
  to: string;
  title: string;
  icon?: string;
  fontIcon?: string;
  hasBullet?: boolean;
  target?: string;
  end?: boolean; // match route exactly or use as parent route
};

const SidebarMenuItem: FC<Props & WithChildren> = ({
  to,
  target,
  end,
  title,
  icon,
  fontIcon,
  hasBullet = false,
  children,
}) => {
  const { config } = useLayout();
  const { app } = config;

  return (
    <div className="menu-item">
      <NavLink
        to={to}
        end={end}
        target={target}
        className={({ isActive }) =>
          clsx("menu-link without-sub", { active: isActive })
        }
      >
        {hasBullet && (
          <span className="menu-bullet">
            <span className="bullet bullet-dot"></span>
          </span>
        )}
        {icon && app?.sidebar?.default?.menu?.iconType === "svg" && (
          <span className="menu-icon">
            <KTSVG path={icon} className="svg-icon-2" />
          </span>
        )}
        {fontIcon && app?.sidebar?.default?.menu?.iconType === "font" && (
          <i className={clsx("fs-3 menu-icon", fontIcon)}></i>
        )}
        <span className="menu-title">{title}</span>
      </NavLink>
      {children}
    </div>
  );
};

export { SidebarMenuItem };
