import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { ReportsTable } from "../components/ReportsTable";
import { FormattedMessage } from "react-intl";
import { PageTitle } from "src/_metronic/layout/core";
import { Link, useNavigate } from "react-router-dom";
import { breadcrumbs } from "../../../breadcrumbs";
import { FilterUIProvider } from "src/app/components";
import { defaultFilter } from "../components/helpers";

export const Reports = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (id: string) => navigate(`${id}/edit`),
    details: (id: string) => navigate(`${id}/detail`),
    delete: (id: string) => navigate(`${id}/delete`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Reports</PageTitle>
      <Card>
        <Card.Header>
          <Card.Title className="fw-bolder">Reports</Card.Title>
          <Link
            to="/manager/reports/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.REPORT.NEW" />
          </Link>
        </Card.Header>
        <Card.Body className="p-8">
          <FilterUIProvider
            context="users"
            menuActions={menuActions}
            defaultFilter={defaultFilter}
          >
            <ReportsTable />
          </FilterUIProvider>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
