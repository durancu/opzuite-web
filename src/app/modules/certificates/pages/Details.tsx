import { Fragment } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { breadcrumbs } from "../breadcrumbs";
import { certificatesAPI } from "../redux";
import { Alert, Button } from "react-bootstrap";
import { BackdropLoader } from "src/app/components";
import { PDFViewer } from "./certificates/components";

export const Details = () => {
  const { code } = useParams();
  const navigate = useNavigate();

  const {
    data: file,
    isLoading,
    isError,
    refetch,
  } = certificatesAPI.usePreviewQuery(code);

  if (isLoading) return <BackdropLoader />;

  if (isError) {
    return (
      <Fragment>
        <PageTitle breadcrumbs={breadcrumbs}>Certificate Details</PageTitle>
        <Alert show={isError} variant="danger">
          <Alert.Heading as="h2">Something went wrong</Alert.Heading>
          <p className="my-4 fs-6">
            We {`couldn't`} fetch certificate details. Please try again
          </p>
          <Button onClick={() => refetch()} variant="danger">
            Try again
          </Button>
        </Alert>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Certificate Details</PageTitle>

      <PDFViewer
        file={file}
        show={file ? true : false}
        onHide={() => navigate("/certificates")}
        pagesCount={1}
      />
    </Fragment>
  );
};
