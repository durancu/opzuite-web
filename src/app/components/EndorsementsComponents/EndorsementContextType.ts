import { ObjectSchema } from "yup";
import {
  ENDORSEMENT_TYPE,
  MODAL_ENDORSEMENT_ACTION_TYPE,
} from "./EndorsementTypeAndEnum";

export type EndorsementContextType = {
  modalIsOpen: boolean;
  setModalIsOpen: (e: boolean) => void;
  endorsementActionType: MODAL_ENDORSEMENT_ACTION_TYPE;
  setEndorsementActionType: (e: MODAL_ENDORSEMENT_ACTION_TYPE) => void;
  coverageCode: string;
  setCoverageCode: (e: string) => void;
  indexEndorsement: number;
  setIndexEndorsement: (e: number) => void;
  indexItemEndorsement: number;
  setIndexItemEndorsement: (e: number) => void;
  endorsementToEdit: any;
  setEndorsementToEdit: (e: any) => void;
  buttonCloseRef: any;
  buttonSaveRef: any;
  handleClickButtonClose: (arg?: any) => any;
  handleClickButtonSave: (arg?: any) => any;
  endorsementItemType: any;
  setEndorsementItemType: (e: any) => void;
  setDefaultValues: (arg?: any) => any;
  cardTitle: string;
  setCardTitle: (e: string) => void;
  endorsementType: ENDORSEMENT_TYPE;
  setEndorsementType: (e: ENDORSEMENT_TYPE) => void;
  errors: any;
  setErrors: (e: any) => void;
  schemaValidation: ObjectSchema<Record<string, never>>;
  mixSchema: (e: ObjectSchema<Record<string, never>>[]) => void;
};
