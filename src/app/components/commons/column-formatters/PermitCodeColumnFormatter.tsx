/* eslint-disable no-script-url */

import { CopyToClipboard } from "../../Widgets";

export const PermitCodeColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openDetailPermitPage }: any
) => (
  <>
    <CopyToClipboard content={cellContent} className="mr-2" />
    <a
      className="text-primary"
      onClick={() => {
        openDetailPermitPage(row.code);
      }}
    >
      {row.code || "Pending"}
    </a>
  </>
);
