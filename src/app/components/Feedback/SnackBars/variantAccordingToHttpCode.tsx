import { VariantType } from "notistack";

export const variantAccordingToHttpCode = (httpCode: number): VariantType => {
  if (httpCode >= 200 && httpCode <= 299) return "success";
  if (httpCode >= 400 && httpCode <= 599) return "error";
  return "default";
};
