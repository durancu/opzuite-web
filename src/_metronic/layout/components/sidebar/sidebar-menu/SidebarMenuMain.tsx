/* eslint-disable react/jsx-no-target-blank */
import { Fragment } from "react";
import { useIntl } from "react-intl";
import { useLocation } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { SidebarMenuItem } from "./SidebarMenuItem";

const SidebarMenuMain = () => {
  const intl = useIntl();
  const location = useLocation();

  return (
    <Fragment>
      <div className="menu-item">
        <div className="menu-content pt-8 pb-2">
          <span className="menu-section text-muted text-uppercase fs-8 ls-1">
            Main
          </span>
        </div>
      </div>

      <SidebarMenuItem
        end
        to="/"
        title={intl.formatMessage({ id: "MENU.MAIN.HOME" })}
        fontIcon="bi-grid-1x2"
      />

      <RequirePermission
        permissions={[
          Permissions.REPORTS_READ,
          Permissions.POLICIES,
          Permissions.POLICIES_RENEW,
          Permissions.POLICIES_UPDATE,
        ]}
      >
        <SidebarMenuItem
          end
          to="policies/renewals"
          title={intl.formatMessage({ id: "MENU.MAIN.RENEWAL_CENTER" })}
          fontIcon="bi-recycle"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.POLICIES_CERTIFICATES}>
        <SidebarMenuItem
          to="certificates"
          title={intl.formatMessage({ id: "MENU.MAIN.CERTIFICATES" })}
          fontIcon="bi-file-earmark-text"
        />
      </RequirePermission>

      {/* start sales*/}
      <div className="menu-item">
        <div className="menu-content pt-8 pb-2">
          <span className="menu-section text-muted text-uppercase fs-8 ls-1">
            {intl.formatMessage({ id: "MENU.MAIN.HEADER.SALES" })}
          </span>
        </div>
      </div>
      <RequirePermission permissions={Permissions.POLICIES_READ}>
        <SidebarMenuItem
          to="policies"
          end={location.pathname.includes("renewals")} // prevent renewal url hilights
          title={intl.formatMessage({ id: "MENU.MAIN.POLICIES" })}
          fontIcon="bi-shield-shaded"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.APPLICATIONS_READ}>
        <SidebarMenuItem
          to="services"
          title={intl.formatMessage({ id: "MENU.MAIN.APPLICATIONS" })}
          fontIcon="bi-app-indicator"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.CUSTOMERS_READ}>
        <SidebarMenuItem
          to="customers"
          title={intl.formatMessage({ id: "MENU.MAIN.CUSTOMERS" })}
          fontIcon="bi-person-fill"
        />
      </RequirePermission>
      {/* end sales*/}

      {/* start resources */}
      <div className="menu-item">
        <div className="menu-content pt-8 pb-2">
          <span className="menu-section text-muted text-uppercase fs-8 ls-1">
            {intl.formatMessage({ id: "MENU.MAIN.HEADER.RESOURCES" })}
          </span>
        </div>
      </div>
      <RequirePermission permissions={Permissions.CARRIERS_READ}>
        <SidebarMenuItem
          to="carriers"
          title={intl.formatMessage({ id: "MENU.MAIN.CARRIERS" })}
          fontIcon="bi-umbrella-fill"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.MGAS_READ}>
        <SidebarMenuItem
          to="mgas"
          title={intl.formatMessage({ id: "MENU.MAIN.MGA" })}
          fontIcon="fa-regular fa-handshake"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.FINANCERS_READ}>
        <SidebarMenuItem
          to="financers"
          title={intl.formatMessage({ id: "MENU.MAIN.FINANCERS" })}
          fontIcon="bi-cash-coin"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.USERS_READ}>
        <SidebarMenuItem
          to="agents"
          title={intl.formatMessage({ id: "MENU.MAIN.AGENTS" })}
          fontIcon="bi-people"
        />
      </RequirePermission>

      {/* end resources  */}

      {/* <SidebarMenuItem
        to="https://vl17.academy.inzuite.com"
        target="_blank"
        title={intl.formatMessage({ id: "MENU.ACADEMY.TITLE" })}
        fontIcon="bi-journals"
      /> */}
      {/* end extra */}
    </Fragment>
  );
};

export { SidebarMenuMain };
