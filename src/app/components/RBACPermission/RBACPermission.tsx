import Rbac from "@build-security/react-rbac-ui-manager";
import { createTheme, ThemeProvider } from "@mui/material";
import {
  AddCircleOutline,
  DeleteOutline,
  UpdateOutlined,
} from "@material-ui/icons";
import { useState } from "react";
import styled from "styled-components";
//import { PermissionsObject } from "@build-security/react-rbac-ui-manager/dist/types";
import { JSONViewer } from "../JSONViewer";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#4994E4",
      light: "#3C74C3",
    },
    info: {
      main: "#D6EBFF",
    },
    text: {
      primary: "#363C44",
    },
  },
});

const data = {
  _roles: {
    user: {
      permissions: [],
    },
    admin: {
      permissions: [],
    },
    "super admin": {
      permissions: [],
    },
  },
  _resources: {
    users: {
      name: "Users",
      _resources: {
        "users.create": {
          name: "create",
          _resources: {},
        },
        "users.delete": {
          name: "delete",
          _resources: {},
        },
        "users.read": {
          name: "read",
          _resources: {},
        },
        "users.update": {
          name: "update",
          _resources: {},
        },
      },
    },
    groups: {
      name: "Groups",
      _resources: {
        "groups.view": {
          name: "view",
          _resources: {},
        },
        "groups.manage": {
          name: "manage",
          _resources: {},
        },
      },
    },
    documents: {
      name: "Documents",
      _resources: {
        "documents.upload": {
          name: "upload",
          _resources: {},
        },
        "documents.create": {
          name: "create",
          _resources: {},
        },
        "documents.share": {
          name: "share",
          _resources: {},
        },
        "documents.managesharing": {
          name: "manage sharing",
          _resources: {},
        },
      },
    },
    policies: {
      name: "Policies",
      _resources: {
        "policies.create": {
          name: "create",
          _resources: {},
        },
        "policies.read": {
          name: "read",
          _resources: {},
        },
      },
    },
    gates: {
      name: "Gates",
      _resources: {
        "gates.control": {
          name: "control",
          _resources: {},
        },
        "gates.open": {
          name: "open",
          _resources: {},
        },
      },
    },
  },
};

const addIcon = styled(AddCircleOutline)`
  color: blue;

  &.MuiSvgIcon-colorError {
    color: black;
  }
`;
const deleteIcon = styled(DeleteOutline)`
  color: red;

  &.MuiSvgIcon-colorError {
    color: black;
  }
`;
const editIcon = styled(UpdateOutlined)`
  color: green;

  &.MuiSvgIcon-colorError {
    color: black;
  }
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars

export const RBACPermission = () => {
  const [rbacData, setRbacData] = useState(data);

  const handleChange = (value: any) => {
    setRbacData(value);
  };

  return (
    <ThemeProvider theme={theme}>
      <Rbac
        defaultValue={data}
        onChange={handleChange}
        /*  buttons={{
                     cancelButton: colorButtonWithIcon,
                     closeButton: colorButton,
                     saveButton: colorButton,
                     deleteButton: colorButtonWithIcon,
                 }} */
        icons={{
          treeAddIcon: addIcon,
          editIcon: editIcon,
          deleteIcon: deleteIcon,
          /* treeNodeIcon: addIcon,
                    treeParentIcon: addIcon,
                    treeCollapseIcon: addIcon,
                    treeExpandIcon: addIcon, */
        }}
        /*  components={{
                 addResource: () => { return <>Test</> },
                 addRole: () => { return <>Test 1</> },
                 roleTag: (props) => { return <>{props.children}</> }
             }} */
      />
      <JSONViewer data={rbacData} />
    </ThemeProvider>
  );
};
