import { Fragment, useState } from "react";
import { useFormikContext } from "formik";

import { Button, OverlayTrigger, Tooltip, Modal } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";

interface Props {
  index: number;
  itemTitle: string;
  item: any;
  recalculateInitialBasePremium: any;
}

const ItemDeleteModal = (props: Props) => {
  const { itemTitle, item, index, recalculateInitialBasePremium } = props;

  const [open, setOpen] = useState(false);
  const { setFieldValue, values } = useFormikContext<any>();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const deleteAction = () => {
    if (item.nonCommissionableReferenceId) {
      const firstEndorsementItems: any[] = values.endorsements[0].items;
      const filteredFirstEndorsementItems = firstEndorsementItems.filter(
        (endorsementItem: any) =>
          !endorsementItem.nonCommissionableReference ||
          endorsementItem.nonCommissionableReference
            .relatedCoverageReferenceId !== item.nonCommissionableReferenceId
      );
      setFieldValue(`endorsements.0.items`, filteredFirstEndorsementItems);
    }

    setFieldValue(`items.${index}`, { ...item, deleted: true });
    recalculateInitialBasePremium(true);
  };

  const intl = useIntl();

  return (
    <Fragment>
      <OverlayTrigger
        placement="top"
        overlay={<Tooltip className="text-capitalize">Delete</Tooltip>}
      >
        <Button
          variant="danger"
          onClick={() => handleOpen()}
          className="btn-sm pr-0 pl-2"
        >
          <i className="fs-4 bi bi-trash3"></i>
        </Button>
      </OverlayTrigger>
      <Modal show={open}>
        <Modal.Header>
          <Modal.Title className="my-0">Delete Coverage</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <FormattedMessage
              id="GENERAL.MODAL.TEXT.SURE_WANT_TO_DELETE_PERMANENTLY.MODEL"
              values={{
                this: intl.formatMessage({
                  id: "GENERAL.JOINT.THIS.MALE.SINGULAR",
                }),
                model: itemTitle,
              }}
            />
          </p>
          <p>
            <FormattedMessage id="GENERAL.MODAL.DELETE.WARNING" />
          </p>
        </Modal.Body>

        <Modal.Footer>
          <Button type="button" variant="light" onClick={handleClose}>
            <FormattedMessage id="BUTTON.CANCEL" />
          </Button>
          <Button
            type="button"
            variant="danger"
            onClick={() => {
              deleteAction();
              handleClose();
            }}
          >
            <FormattedMessage id="BUTTON.DELETE" />
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};

export default ItemDeleteModal;
