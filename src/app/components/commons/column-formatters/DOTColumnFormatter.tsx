import { isEmpty } from "lodash";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { CopyToClipboardLink } from "../../Widgets";

export const DOTColumnFormatter = (dotNumber: any) => (
  <>
    {!isEmpty(dotNumber) && (
      <CopyToClipboardLink content={dotNumber} fieldId={"FORM.LABELS.DOT"}>
        {dotNumber}
      </CopyToClipboardLink>
    )}
    {isEmpty(dotNumber) && (
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="missing-dot-tooltip">
            <span className="text-warning">
              <FormattedMessage id="FORM.LABELS.MISSING_DOT" />
            </span>
          </Tooltip>
        }
      >
        <i className="fas fa-exclamation-circle text-warning font-size-sm" />
      </OverlayTrigger>
    )}
  </>
);
