import { Autocomplete } from "@mui/material";
import clsx from "clsx";
import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { useIntl } from "react-intl";

interface Props {
  name: string;
  label?: any;
  labelField?: string;
  options: any[];
  handleChange: any;
  value: any;
  variant?: string;
  disabled?: boolean;
  required?: boolean;
  hidden?: boolean;
  error?: boolean;
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  errorText?: string;
}

export const AutoCompleteNoFormik = ({
  handleChange,
  value,
  label,
  labelText,
  labelTextId,
  labelField = "name",
  options,
  name,
  error,
  errorText,
  helperText,
  helperTextId,
  hidden,
  disabled,
  required,
}: Props) => {
  const [localValue, setLocalValue] = useState({
    id: "",
    [labelField]: "",
    _id: "",
    type: "",
    name: "",
    email: "",
    phone: "",
  });

  useEffect(() => {
    const valueOption = options?.find((element: any) => element.id === value);
    setLocalValue(valueOption);
  }, [options, value]);

  const handleChangeComponent = (param: any) => {
    const newE = { target: { value: param?.id || "", name } };
    handleChange(newE);
    setLocalValue(param);
  };

  const intl = useIntl();

  return (
    <Autocomplete
      disabled={disabled}
      hidden={hidden}
      options={options || []}
      getOptionLabel={(option: any) => {
        return option[labelField] || "N/A";
      }}
      onChange={(_, value) => {
        handleChangeComponent(value);
      }}
      value={localValue}
      ListboxProps={{
        className:
          "list-group list-group-flush position-absolute z-index-2 w-100 mh-400px overflow-auto rounded border",
      }}
      renderOption={(props, option, state) => (
        <li
          key={state.index}
          {...props}
          className={clsx(
            "list-group-item list-group-item-action p-3 cursor-pointer fw-normal",
            {
              "bg-light-dark": state.selected,
            }
          )}
        >
          <span className="mb-1 fs-5 d-block text-gray-600">{option.name}</span>
          <span
            className="fs-7 d-block text-gray-600"
            hidden={!option.description}
          >
            {Array.isArray(option.description)
              ? Array.from(option.description).join(" | ")
              : option.description}
          </span>
        </li>
      )}
      renderInput={({ inputProps, InputProps }) => (
        <Form.Group className="position-relative">
          <Form.Label>
            {labelTextId
              ? intl.formatMessage({ id: labelTextId })
              : labelText || label}
          </Form.Label>
          <div className="position-relative" ref={InputProps.ref}>
            <input
              {...inputProps}
              required={required}
              className={clsx("form-control", {
                "is-invalid": error,
              })}
            />
            <span
              className={clsx(
                "position-absolute top-0 bottom-0 end-0 w-50px",
                error ? "me-10" : "me-2"
              )}
            >
              {InputProps?.endAdornment}
            </span>
          </div>
          <Form.Text className="text-danger my-1 d-block">
            {error && errorText}
          </Form.Text>
          <Form.Text className="my-1 d-block">
            {helperTextId
              ? intl.formatMessage({ id: helperTextId })
              : helperText}
          </Form.Text>
        </Form.Group>
      )}
    />
  );
};
