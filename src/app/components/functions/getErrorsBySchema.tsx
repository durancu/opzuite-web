import { ObjectSchema } from "yup";
import { yupToFormErrors } from "formik";

export const getErrorsBySchema = async (
  schema: ObjectSchema<Record<string, never>>,
  objectToValidate = {}
): Promise<any> => {
  return await schema
    .validate(objectToValidate, { abortEarly: false })
    .then(() => ({}))
    .catch((err: any) => {
      return yupToFormErrors(err);
    });
};
