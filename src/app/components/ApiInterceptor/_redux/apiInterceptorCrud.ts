import axios from "axios";

export function apiInterceptorResponse() {
  return axios.interceptors.response.use((res) => res);
}

export function apiInterceptorRequest() {
  return axios.interceptors.request.use((res) => res);
}
