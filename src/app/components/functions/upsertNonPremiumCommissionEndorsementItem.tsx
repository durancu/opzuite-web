import { roundAmount } from ".";
import { DefaultNonPremiumCommissionableEndorsementItem } from "src/app/modules/sales/policies/pages/policy-edit/helpers";

export const upsertNonPremiumCommissionEndorsementItem = async (
  setFieldValue: (e: any, a: any) => null,
  basePremiumItems: any[], //endorsement 1 (base premium): items
  coverageNonCommissionableReferenceId: string, //current coverage reference id
  coveragePremium: number, //premium del current coverage
  type: string, //can be AGENT, AGENCY, SHARER
  commissionsPlan: any,
  coverages: any[],
  endorsements: any[],
  userId: string
): Promise<any> => {
  if (coverages && endorsements) {
    // Buscar en los items del endorsement 1 aquellos que:
    // - sean del tipo AGENT_COMMISSION
    // - sean producto de una logica de NonCommissionable coverage
    // - coincidan en su reference.relatedCoverageReferenceId y reference.type

    const nonCommissionableReferenceIndex = basePremiumItems.findIndex(
      (basePremiumItem: any) =>
        basePremiumItem.nonCommissionableReference &&
        basePremiumItem.nonCommissionableReference
          .relatedCoverageReferenceId ===
          coverageNonCommissionableReferenceId &&
        basePremiumItem.nonCommissionableReference.type === type
    );

    let commissionUnit = "";
    let commissionValue = 0;

    switch (type) {
      case "AGENCY":
        commissionUnit = commissionsPlan.agencyCommissionPlan.premiumUnit;
        commissionValue = commissionsPlan.agencyCommissionPlan.premium;
        break;
      case "AGENT":
        commissionUnit = commissionsPlan.agentCommissionPlan.premiumUnit;
        commissionValue = commissionsPlan.agentCommissionPlan.premium;
        break;
      case "SHARER":
        commissionUnit = commissionsPlan.sharersCommissionPlan[0].premiumUnit;
        commissionValue = commissionsPlan.sharersCommissionPlan[0].premium;
        break;
      default:
    }

    if (commissionValue > 0) {
      const commissionAmount = roundAmount(
        commissionUnit === "%"
          ? coveragePremium * (commissionValue / 100)
          : commissionValue
      );

      if (
        nonCommissionableReferenceIndex >= 0 &&
        endorsements?.length > 0 &&
        endorsements[0]?.items &&
        endorsements[0]?.items.length
      ) {
        const modifiedItem = {
          ...endorsements[0].items[nonCommissionableReferenceIndex],
        };
        modifiedItem.amount = commissionAmount;

        setFieldValue(
          `endorsements.0.items.${nonCommissionableReferenceIndex}.amount`,
          commissionAmount
        );
        //await new Promise((resolve) => setTimeout(resolve, 1000));
      } else {
        const newCommissionItem: any = {
          ...DefaultNonPremiumCommissionableEndorsementItem,
        };

        switch (type) {
          case "AGENCY":
            newCommissionItem.accountingClass = "AGENCY_COMMISSION";
            newCommissionItem.description =
              "Agency custom commission from partner non commissionable premium plan";
            break;
          case "AGENT":
            newCommissionItem.seller = userId;
            newCommissionItem.accountingClass = "AGENT_COMMISSION";
            newCommissionItem.description =
              "Agent custom commission from partner non commissionable premium plan";
            break;
          case "SHARER":
            newCommissionItem.accountingClass = "AGENT_COMMISSION";
            newCommissionItem.description =
              "Partner custom commission from partner non commissionable premium plan";
            newCommissionItem.seller =
              commissionsPlan.sharersCommissionPlan[0].sharerId;
            break;
          default:
            newCommissionItem.accountingClass = "";
        }

        newCommissionItem.amount = commissionAmount;

        newCommissionItem.nonCommissionableReference = {
          id: newCommissionItem.seller,
          relatedCoverageReferenceId: coverageNonCommissionableReferenceId,
          type: type,
        };

        if (newCommissionItem.accountingClass) {
          //objectPath.set(values.endorsements, `0.items`, [...basePremiumItems, newCommissionItem]);
          setFieldValue(`endorsements.0.items`, [
            ...basePremiumItems,
            newCommissionItem,
          ]);
        }
      }
    }
  }
};
