import { Fragment, useEffect } from "react";

import { lowerCase } from "lodash";
import { useSnackbar } from "notistack";
import { useIntl } from "react-intl";
import { Link, useNavigate, useParams } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { BackdropLoader, Error, RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import {
  Applications,
  CustomerDetails,
  CustomerOverview,
  PageDetailTopButtons,
  Policies,
} from "src/app/components/Widgets";
import { hasCountryManagerRoleAccess } from "src/app/components/functions/rolesHierarchyHelpers";
import { useGetUserAuth } from "src/app/hooks";
import { breadcrumbs } from "../breadcrumbs";
import { customersAPI } from "../redux";

export const CustomerDetailPage = () => {
  const intl = useIntl();
  const user = useGetUserAuth();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: customer,
    isLoading,
    isError,
    refetch,
  } = customersAPI.useGetCustomerByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [deleteCustomer, { isLoading: isDeleting }] =
    customersAPI.useDeleteCustomerMutation();

  function goToEditCustomer() {
    navigate(`/customers/${lowerCase(customer?.type)}/${code}/edit`);
  }

  async function handleDeleteCustomer() {
    try {
      await deleteCustomer(code).unwrap();
      enqueueSnackbar("Customer deleted", {
        variant: "success",
      });
      navigate("/customers");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.CUSTOMER" })}
      </PageTitle>
      <Toolbar>
        <div className="d-flex gap-3">
          <RequirePermission permissions={Permissions.POLICIES_CERTIFICATES}>
            <Link
              to={`/certificates/new?customer=${customer?.id}`}
              className="btn btn-dark d-flex align-items-center gap-1 justify-items-center"
            >
              <span>Certificate</span>
              <i className="fs-4 bi bi-file-earmark-text"></i>
            </Link>
          </RequirePermission>
          <PageDetailTopButtons
            editFunction={goToEditCustomer}
            deleteFunction={handleDeleteCustomer}
            editPermission={Permissions.CUSTOMERS_UPDATE}
            deletePermission={Permissions.CUSTOMERS_DELETE}
            componentLabel="customer"
            hidden={
              user?.id === customer?.createdBy ||
              hasCountryManagerRoleAccess(user?.role)
            }
          />
        </div>
      </Toolbar>

      {
        //REVISAR CON LIUVER LA VALIDACION DE ESTE PERMISO
        /* {user.id === customer?.createdBy ||
          (getHierarchyRoleByUserAuth(user.employeeInfo.category, roles) <=
            5 && (
              ))} */
      }
      <CustomerOverview customer={customer} />
      <CustomerDetails customer={customer} />
      <Policies filter={{ customer: customer?.id }} context="customers" />
      <Applications filter={{ customer: customer?.id }} context="customers" />
    </Fragment>
  );
};
