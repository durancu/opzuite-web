import { Fragment, useState } from "react";
import { Modal, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { GenericFunction } from "../interfaces/FunctionInterfaces";

interface Props {
  title: string;
  itemName: string;
  deleteAction: GenericFunction;
  toolTipLabel: string;
  anotherAction?: GenericFunction;
}

export const ItemDeleteModal = ({
  title,
  itemName,
  deleteAction,
  anotherAction,
}: Props) => {
  const intl = useIntl();
  const [open, setOpen] = useState(false);

  const deleteActionExecute = () => {
    deleteAction();
    anotherAction && anotherAction();
    setOpen(false);
  };

  return (
    <Fragment>
      <OverlayTrigger
        placement="top"
        overlay={<Tooltip className="text-capitalize">Delete</Tooltip>}
      >
        <Button
          variant="danger"
          onClick={() => setOpen(true)}
          className="btn-sm"
        >
          <i className="fs-4 bi bi-trash3"></i>
        </Button>
      </OverlayTrigger>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        show={open}
        centered
      >
        <Modal.Header>
          <Modal.Title className="my-0">{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <FormattedMessage
              id="GENERAL.MODAL.TEXT.SURE_WANT_TO_DELETE_PERMANENTLY.MODEL"
              values={{
                this: intl.formatMessage({
                  id: "GENERAL.JOINT.THIS.MALE.SINGULAR",
                }),
                model: itemName,
              }}
            />
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={() => setOpen(false)}>
            <FormattedMessage id="BUTTON.CANCEL" />
          </Button>
          <Button variant="danger" onClick={() => deleteActionExecute()}>
            <FormattedMessage id="BUTTON.DELETE" />
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
};
