import { Col, Row, Accordion } from "react-bootstrap";

import {
  FormInput,
  FormSelect,
  FormTextArea,
  PhoneNumberInput,
  FormAutoComplete,
  SSNInput,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

import * as CATALOG from "src/app/components/Catalog/CatalogSelects";
import { GendersAutocompleteField } from "src/app/components/FormControls";
import { DatePickerCustom } from "src/app/partials/controls";

interface Props {
  index?: number;
}

export const CohortFields = ({ index }: Props) => {
  const catalog = useGetCatalog();

  // form could be standalone as used in coverages or in a field array
  function formatName(name: string) {
    return index !== undefined ? `cohorts.${index}.${name}` : name;
  }

  return (
    <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
      <Accordion.Item eventKey="0">
        <Accordion.Header className="bg-light-primary border rounded-xl shadow-xl">
          <h3 className="fw-bold text-primary">Contact Info</h3>
        </Accordion.Header>
        <Accordion.Body>
          <Row className="mb-4">
            <Col>
              <FormInput
                name={formatName("firstName")}
                labelTextId="FORM.LABELS.FIRST_NAME"
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("lastName")}
                labelTextId="FORM.LABELS.LAST_NAME"
              />
            </Col>
            <Col>
              <FormSelect
                labelTextId="FORM.LABELS.RELATIONSHIP"
                name={formatName("relationship")}
                options={CATALOG.RELATION_SHIP_CATALOG}
              />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col>
              <FormInput
                type="email"
                name={formatName("email")}
                labelTextId="FORM.LABELS.EMAIL"
              />
            </Col>
            <Col>
              <PhoneNumberInput
                name={formatName("phone")}
                labelTextId="FORM.LABELS.WORK_NUMBER"
              />
            </Col>
            <Col>
              <PhoneNumberInput
                name={formatName("mobilePhone")}
                labelTextId="FORM.LABELS.MOBILE_NUMBER"
              />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col>
              <FormInput
                name={formatName("address.address1")}
                labelTextId="FORM.LABELS.ADDRESS_LINE_1"
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("address.address2")}
                labelTextId="FORM.LABELS.ADDRESS_LINE_2"
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("address.city")}
                labelTextId="FORM.LABELS.CITY"
              />
            </Col>
          </Row>

          <Row className="mb-4">
            <Col>
              <FormAutoComplete
                name={formatName("address.state")}
                labelTextId="FORM.LABELS.STATE"
                options={catalog?.states || []}
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("address.zip")}
                labelTextId="FORM.LABELS.ZIP_CODE"
              />
            </Col>
            <Col>
              <FormAutoComplete
                name={formatName("address.country")}
                labelTextId="FORM.LABELS.COUNTRY"
                helperTextId="FORM.LABELS.HELPER.COUNTRIES"
                options={catalog?.countries || []}
                disabled={true}
              />
            </Col>
          </Row>
        </Accordion.Body>
      </Accordion.Item>

      <Accordion.Item eventKey="1">
        <Accordion.Header className="bg-light-primary border rounded-md">
          <h3 className="fw-bold text-primary">Other Details</h3>
        </Accordion.Header>
        <Accordion.Body>
          <Row className="mb-4">
            <Col>
              <DatePickerCustom
                name={formatName("dob")}
                labelTextId="FORM.LABELS.DOB"
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("driverLicense")}
                labelTextId="FORM.LABELS.DRIVER_LICENSE"
              />
            </Col>
            <Col>
              <FormAutoComplete
                name={formatName("driverLicenseState")}
                labelTextId="FORM.LABELS.DRIVER_LICENSE.STATE"
                options={catalog?.states || []}
              />
            </Col>
            <Col>
              <SSNInput
                name={formatName("ssn")}
                labelTextId="FORM.LABELS.SSN"
              />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col>
              <GendersAutocompleteField fieldName={formatName("gender")} />
            </Col>
            <Col>
              <FormSelect
                name={formatName("ethnicity")}
                labelTextId="FORM.LABELS.ETHNICITY"
                options={CATALOG.ETHNICITY_CATALOG}
              />
            </Col>
            <Col>
              <FormSelect
                name={formatName("maritalStatus")}
                labelTextId="FORM.LABELS.MARITAL_STATUS"
                options={CATALOG.MARITAL_STATUS_CATALOG}
              />
            </Col>
            <Col>
              <FormSelect
                name={formatName("educationLevel")}
                labelTextId="FORM.LABELS.EDUCATION_LEVEL"
                options={CATALOG.EDUCATION_LEVEL_CATALOG}
              />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col>
              <FormSelect
                name={formatName("immigrantStatus")}
                labelTextId="FORM.LABELS.IMMIGRANT_STATUS"
                options={CATALOG.IMMIGRANT_STATUS_CATALOG}
              />
            </Col>
            <Col>
              <FormSelect
                name={formatName("identityCardType")}
                labelTextId="FORM.LABELS.IDENTITY_CARD_TYPE"
                options={CATALOG.IDENTITY_CARD_TYPE_CATALOG}
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("identityCardId")}
                labelTextId="FORM.LABELS.IDENTITY_CARD.ID"
              />
            </Col>
          </Row>
          <Row className="mb-4">
            <Col>
              <FormSelect
                name={formatName("employmentStatus")}
                labelTextId="FORM.LABELS.EMPLOYEMENT_STATUS"
                options={CATALOG.EMPLOYMENT_STATUS_CATALOG}
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("employer")}
                labelTextId="FORM.LABELS.EMPLOYER"
              />
            </Col>
            <Col>
              <FormInput
                name={formatName("yearlyGrossIncome")}
                labelTextId="FORM.LABELS.YEARLY_GROSS_INCOME"
                type="number"
              />
            </Col>
          </Row>
        </Accordion.Body>
      </Accordion.Item>

      <Accordion.Item eventKey="2">
        <Accordion.Header className="bg-light-primary border rounded-md">
          <h3 className="fw-bold text-primary">Insurance Coverages</h3>
        </Accordion.Header>

        <Accordion.Body>
          <Row className="mb-4">
            <Col>
              <FormSelect
                name={formatName("healthInsuranceCompany")}
                labelTextId="FORM.LABELS.HEALTH_INSURANCE_COMPANY"
                options={CATALOG.HEALTH_INSURANCE_COMPANIES_CATALOG}
              />
            </Col>

            <Col>
              <FormSelect
                name={formatName("autoInsuranceCompany")}
                labelText="Auto Insurance Company"
                options={CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG}
              />
            </Col>
            <Col>
              <FormSelect
                name={formatName("lifeInsuranceCompany")}
                labelTextId="FORM.LABELS.LIFE_INSURANCE_COMPANY"
                options={CATALOG.LIFE_INSURANCE_COMPANIES_CATALOG}
              />
            </Col>

            <Col>
              <FormSelect
                name={formatName("homeInsuranceCompany")}
                labelTextId="FORM.LABELS.HOME_INSURANCE_COMPANY"
                options={CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG}
              />
            </Col>
          </Row>
        </Accordion.Body>
      </Accordion.Item>

      <Row className="mb-4 p-5">
        <Col>
          <FormTextArea labelText="Notes" name={formatName("notes")} />
        </Col>
      </Row>
    </Accordion>
  );
};
