import { ItemPayment } from "./ItemPayment";

export interface EndorsementItem {
  nonCommissionableReference: {
    id: string;
    type: string;
    relatedCoverageReferenceId: string;
  };
  id?: string;
  accountingClass?: string;
  amount?: number;
  code?: string;
  commissionUnit?: string;
  company?: string;
  description?: string;
  endorsedAt?: Date;
  endorsement?: string;
  followUpDate?: Date;
  followUpPerson?: string;
  seller?: string;
  otherDetails?: any;
  payments?: ItemPayment[];
  sale?: string;
  status?: string;
  type?: string;
  createdBy?: string;
  updatedBy?: string;
  amountPaid?: number;
  balance?: number;
  coverageNonCommissionableReferenceId: any;
}
