import { FormControl, TextField } from "@mui/material";
import { Formik } from "formik";
import { isEqual } from "lodash";
import { useMemo } from "react";
import { useIntl } from "react-intl";
import { useDashboardsUIContext } from "../DashboardsUIContext";

const prepareFilter = (queryParams: any, values: any) => {
  const { type, searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter: any = {};
  // Filter by status
  if (type && type !== "") {
    filter.type = type;
  }
  // Filter by all fields
  if (searchText) {
    filter["seller.firstName"] = searchText;
    filter["seller.lastName"] = searchText;
    filter["customer.business.name"] = searchText;
    filter["customer.contact.firstName"] = searchText;
    filter["customer.contact.lastName"] = searchText;
    filter["location.business.name"] = searchText;
    filter["code"] = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export const DashboardsFilter = () => {
  const intl = useIntl();
  // Dashboards UI Context
  const dashboardsUIContext: any = useDashboardsUIContext();

  const dashboardsUIProps: any = useMemo(() => {
    return {
      setQueryParams: dashboardsUIContext.setQueryParams,
      queryParams: dashboardsUIContext.queryParams,
    };
  }, [dashboardsUIContext]);

  const applyFilter = (values: any) => {
    const newQueryParams = prepareFilter(dashboardsUIProps.queryParams, values);
    if (!isEqual(newQueryParams, dashboardsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      dashboardsUIProps.setQueryParams(newQueryParams);
    }
  };

  return (
    <>
      <Formik
        validateOnBlur={false}
        validateOnChange={false}
        initialValues={{
          type: "",
          searchText: "",
        }}
        onSubmit={(values) => {
          applyFilter(values);
        }}
      >
        {({ values, handleSubmit, handleBlur, setFieldValue }) => (
          <form onSubmit={handleSubmit} className="form form-label-right">
            <div className="form-group row">
              <div className="col-md-3">
                <FormControl>
                  <TextField
                    name="searchText"
                    label={`${intl.formatMessage({
                      id: "FORM.FILTERS.SEARCH_ALL",
                    })}`}
                    onBlur={handleBlur}
                    value={values.searchText}
                    onChange={(e) => {
                      setFieldValue("searchText", e.target.value);
                      handleSubmit();
                    }}
                  />
                </FormControl>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
};
