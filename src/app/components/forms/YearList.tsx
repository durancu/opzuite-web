import { useMemo } from "react";
import { FormAutoComplete } from "src/app/components";

type Props = {
  name: string;
  labelText: string;
  helperText?: string;
  startYear?: number;
  endYear?: number;
};

const generateYears = (startYear: number, endYear: number) => {
  const data = [];
  for (let i = endYear; i > endYear - startYear; i--) {
    data.push(i);
  }
  return data;
};

export const YearList = ({
  name,
  labelText,
  helperText,
  startYear = 1985,
  endYear = new Date().getFullYear(),
}: Props) => {
  const list = useMemo(() => {
    return generateYears(startYear, endYear);
  }, [startYear, endYear]);

  return (
    <FormAutoComplete
      name={name}
      labelText={labelText}
      helperText={helperText}
      options={list}
    />
  );
};
