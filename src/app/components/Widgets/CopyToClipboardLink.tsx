/* eslint-disable @typescript-eslint/no-unused-vars */
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { useAppDispatch } from "src/app/hooks";
import { feedbacksSlice } from "../Feedback/_redux/feedbackSlice";

interface Props {
  content: any;
  className?: any;
  fieldId: any;
  children: any;
}

export const CopyToClipboardLink = (props: Props) => {
  const { content, className, fieldId, children } = props;
  const dispatch = useAppDispatch();
  const intl = useIntl();

  const copyClipboard = () => {
    navigator.clipboard.writeText(content).then(() => {
      dispatch(
        feedbacksSlice.actions.enqueueFeedbackMessage({
          message: intl.formatMessage(
            { id: "RESPONSES.FEEDBACK.CLIPBOARD_COPY_SUCCESS" },
            { field: intl.formatMessage({ id: fieldId }) }
          ),
          options: {
            key: new Date().getTime() + Math.random(),
            variant: "success",
          },
        } as any)
      );
    });
  };

  return (
    <>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="copy-to-clipboard-link" className="">
            <FormattedMessage
              id="COMPONENTS.TOOLTIP.CLIPBOARD_COPY_NO_VALUE"
              values={{
                field: intl.formatMessage({ id: fieldId }),
              }}
            />
          </Tooltip>
        }
      >
        <span
          onClick={copyClipboard}
          className={`${className} link `}
          role="button"
          style={{ borderBottom: "1px dotted #333" }}
        >
          {children}
        </span>
      </OverlayTrigger>
    </>
  );
};
