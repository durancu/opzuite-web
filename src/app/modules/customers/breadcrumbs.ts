import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Customers",
    path: "customers",
    isSeparator: false,
    isActive: true,
  },
];
