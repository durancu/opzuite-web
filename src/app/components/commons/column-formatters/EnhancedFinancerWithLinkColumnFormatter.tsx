import { Fragment } from "react";

export const EnhancedFinancerWithLinkColumnFormatter = (
  cellContent: any,
  insurer: any,
  rowIndex: any,
  {
    openDetailPageFunction,
    idField,
    textField,
    defaultText = "",
  }: any
) => (
  <Fragment>
    <a
      className="text-dark fw-semibold text-hover-primary fs-6 cursor-pointer"
      onClick={() => {
        openDetailPageFunction(insurer[idField]);
      }}
    >
      {insurer[textField] || defaultText}
    </a>
  </Fragment>
);
