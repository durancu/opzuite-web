export const CustomerNameColumnFormatter = (row: any) => (
  <>{`${row?._id?.name}`}</>
);
