// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { Details } from "./pages/Details";
import { OverView } from "./pages/Overview";
import { Certificates } from "./pages/certificates/Certificates";

export const CertificateRoutes: RouteObject[] = [
  {
    index: true,
    element: <OverView />,
  },
  {
    path: ":code/detail",
    element: <Details />,
  },
  {
    path: ":code/preview",
    element: <Details />,
  },
  {
    path: "new",
    element: <Certificates />,
  },
];
