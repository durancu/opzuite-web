import Tags from "@yaireo/tagify/dist/react.tagify"; // React-wrapper file
import "@yaireo/tagify/dist/tagify.css"; // Tagify CSS

interface Props {
  name: string;
  label: string;
  value: any[];
  handleChange: any;
}

export const Tagify = ({ name, label = "", value, handleChange }: Props) => {
  return (
    <>
      {label && <label>{label}</label>}
      <Tags
        name={name}
        value={value}
        className="form-control"
        onChange={(e) => handleChange(name, JSON.parse(e.detail.value))}
      />
    </>
  );
};
