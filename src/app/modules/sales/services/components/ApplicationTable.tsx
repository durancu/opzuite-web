import { Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import { NoRecordsFoundMessage, PleaseWaitMessage } from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  AmountColumnFormatter,
  EndorsementStatusColumnFormatter,
} from "src/app/components/commons/column-formatters";
import { getMetricColorByTitle } from "src/app/utils";

interface Props {
  applications: Endorsement[];
}

export const ApplicationTable = ({ applications = [] }: Props) => {
  const entities = useGetCatalog();
  const endorsementMetricTypeHeaderFormatter = (column: any) => {
    return (
      <OverlayTrigger
        placement="top"
        overlay={<Tooltip id="quick-actions-tooltip">{column.text}</Tooltip>}
      >
        <i
          className="fs-1 rounded bi bi-square-fill"
          style={{
            color: getMetricColorByTitle(column.text),
          }}
        ></i>
      </OverlayTrigger>
    );
  };

  const columns = [
    {
      dataField: "name",
      text: "Type",
      sort: true,
      headerClasses: "fw-bold",
    },
    {
      dataField: "description",
      text: "Description",
      sort: true,
      headerClasses: "fw-bold",
    },
    {
      dataField: "status",
      text: "Status",
      sort: true,
      headerClasses: "fw-bold",
      formatter: EndorsementStatusColumnFormatter,
      formatExtraData: {
        entities,
      },
    },
    {
      dataField: "totalPremium",
      text: "Sale Amount",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalTaxesAndFees",
      text: "Taxes and Fees",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalPayables",
      text: "Processing costs",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalReceivables",
      text: "Receivables",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalFinanced",
      text: "Financed",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalAgentCommission",
      text: "Agent Commission",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalAgencyCommission",
      text: "Agency Commission",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      formatter: AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
  ];

  return (
    <Card className="mb-4 p-8 endorsements-table">
      <Card.Title as="h3" className="text-primary">
        Details
      </Card.Title>
      <BootstrapTable
        wrapperClasses="table-responsive endorsements-table"
        classes="table table-head-custom table-vertical-center overflow-hidden"
        bootstrap4
        bordered={false}
        remote
        keyField="code"
        data={applications}
        columns={columns}
      />
      <PleaseWaitMessage entities={applications} />
      <NoRecordsFoundMessage entities={applications} />
    </Card>
  );
};
