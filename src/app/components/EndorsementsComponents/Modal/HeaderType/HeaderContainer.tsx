import { useGetCatalog } from "../../../Catalog/hooks";
import { ModalEndorsementActionType } from "../../EndorsementTypeAndEnum";
import { useEndorsementUIContext } from "../../EndorsementUIContext";

export const HeaderContainer = () => {
  const { indexEndorsement, endorsementToEdit, endorsementActionType } =
    useEndorsementUIContext();
  const { endorsementTypes } = useGetCatalog();
  return (
    <div
      className="px-6 py-4"
      style={{
        backgroundColor: `${indexEndorsement === 0 ? "#f3f9f3" : "#f0f0f0"}`,
      }}
    >
      <p className="fw-bold">
        {endorsementActionType === ModalEndorsementActionType.ADD
          ? "Add "
          : "Edit "}
        {indexEndorsement === 0 && `Coverages Summary`}
        {` ${
          endorsementTypes.find((e: any) => e.id === endorsementToEdit.type)
            ?.name || ""
        }`}
      </p>
    </div>
  );
};
