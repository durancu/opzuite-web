export const vehicle = {
  vinNumber: "",
  licensePlate: "",
  hasAutoInsurance: false,
  autoInsuranceCompany: "",
  notes: "",
  make: "",
  model: "",
  year: "",
  type: "",
  trim: "",
  color: "",
  condition: "",
  ownershipStatus: "", //Owned, Financed, Leased
  primaryUse: "", // Commuting, Pleasure, Business
  drivingDaysPerWeek: "",
  drivingMilesPerWeek: "",
  annualMileage: "",
  newItem: true,
};
