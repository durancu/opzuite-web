import { useAppSelector } from "./typed-hooks";
import { userSelector } from "src/app/modules/auth/redux";

export function useGetUserAuth() {
  const user = useAppSelector(userSelector);
  return user;
}
