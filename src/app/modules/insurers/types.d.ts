// To parse this data:
//
//   import { Convert, Welcome } from "./file";
//
//   const welcome = Convert.toWelcome(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

interface Insurer {
  _id: string;
  business: Business;
  company: string;
  contact: Contact;
  createdBy: string;
  email: string;
  name: string;
  phone: string;
  type: string;
  deleted: boolean;
  code: string;
  commissions: any[];
  createdAt: Date;
  updatedAt: Date;
  __v: number;
  updatedBy: string;
  id: string;
}

interface Business {
  address: Address;
  code: string;
  contacts: any[];
  email: string;
  fax: string;
  industry: string;
  logo: string;
  mcNumber: string;
  naicCode: string;
  name: string;
  otherPhones: any[];
  primaryPhone: string;
  primaryPhoneExtension: string;
  secondaryPhone: null;
  secondaryPhoneExtension: string;
  sector: string;
  txDMV: string;
  usDOT: string;
  vehicles: any[];
  website: string;
  _id: string;
}

interface Address {
  address1: string;
  address2: string;
  city: string;
  country: string;
  county: string;
  state: string;
  zip: string;
  _id: string;
}

interface Contact {
  address: Address;
  autoInsuranceCompany: string;
  code: string;
  driverLicense: string;
  driverLicenseState: string;
  educationLevel: string;
  email: string;
  employer: string;
  employmentStatus: string;
  ethnicity: string;
  firstName: string;
  gender: string;
  hasAutoInsurance: boolean;
  hasHealthInsurance: boolean;
  hasHomeInsurance: boolean;
  hasLifeInsurance: boolean;
  healthInsuranceCompany: string;
  homeInsuranceCompany: string;
  identityCardId: string;
  identityCardType: string;
  immigrantStatus: string;
  language: string;
  lastName: string;
  lifeInsuranceCompany: string;
  maritalStatus: string;
  mobilePhone: string;
  notes: string;
  phone: string;
  relationship: string;
  ssn: string;
  timezone: string;
  vehicles: any[];
  website: string;
  _id: string;
}
