// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10

import { nanoid } from "@reduxjs/toolkit";
import { Form, useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useWizardUIContext } from "src/app/components/Wizard/WizardUIContext";
import { Switch, TextField, FormControl, FormHelperText } from "@mui/material";

const FormCustomerSetting = () => {
  const { validateForm, setTouched, setFieldValue, values }: any =
    useFormikContext();
  const [isAllCommissions, setIsAllCommissionsState] = useState(false);
  const [allCommissionsValue, setAllCommissionsValue] = useState(0);
  const [commissionsState, setCommissionsState] = useState([]);
  const { setIsBlockStep, btnBackRef, btnNextRef, btnSaveRef } =
    useWizardUIContext();

  const { currentState } = useSelector(
    (state: any) => ({ currentState: state.catalogs }),
    shallowEqual
  );

  const { entities }: any = currentState;
  const { linesOfBusiness } = entities;

  useEffect(() => {
    let tempCommissions: [];
    if (values.commissions.length > 0) tempCommissions = values.commissions;
    else
      tempCommissions = linesOfBusiness.map((product: any) => ({
        lineOfBusiness: product.id,
        percent: 0,
      }));

    setCommissionsState(tempCommissions);
  }, [linesOfBusiness, values]);

  const setCommissionsValues = (value = 0) => {
    const tempCommissions = linesOfBusiness.map((product: any) => ({
      lineOfBusiness: product.id,
      percent: value,
    }));
    setCommissionsState(tempCommissions);
  };

  const handleChangeCommissions = (index: any, id: any, event: any) => {
    if (isAllCommissions) {
      setAllCommissionsValue(event.target.value);
      setCommissionsValues(event.target.value);
    } else {
      const values: any = [...commissionsState];
      values[index] = { lineOfBusiness: id, percent: event.target.value };
      setCommissionsState(values);
    }

    setFieldValue("commissions", commissionsState);
  };

  useEffect(() => {
    setIsBlockStep(false);
  }, [setIsBlockStep]);

  return (
    <Form className="form form-label-right">
      <div className="form-group row">
        <div className="col-md-3">
          <label>Set same percentage for all products.</label>
          <br />
          <Switch
            name="allCommissions"
            value={isAllCommissions}
            checked={isAllCommissions === true}
            onChange={() => {
              setIsAllCommissionsState(!isAllCommissions);
              !isAllCommissions && setCommissionsValues(allCommissionsValue);
            }}
          />
          <FormHelperText>Use same value for all products?</FormHelperText>
        </div>
      </div>
      {/* <h3>Commissionss</h3>
            <hr/> */}
      <div className="form-group row" hidden={!isAllCommissions}>
        <div className="col-md-3">
          <FormControl>
            <TextField
              name="allCommissionsValue"
              onChange={(e: any) => {
                handleChangeCommissions(null, null, e);
              }}
              label="General Commission (%)"
            />
          </FormControl>
          <br />
        </div>
      </div>
      <div className="form-group row" hidden={isAllCommissions}>
        {linesOfBusiness !== null &&
          linesOfBusiness.map((lineOfBusiness: any, index: any) => (
            <div className="col-md-3" key={nanoid()}>
              <FormControl>
                <TextField
                  name={lineOfBusiness.id}
                  label={`${lineOfBusiness.name} (%)`}
                  onChange={(e: any) => {
                    handleChangeCommissions(index, lineOfBusiness.id, e);
                  }}
                  placeholder=""
                  defaultValue={
                    commissionsState.length > 0
                      ? commissionsState[index]["percent"]
                      : 0
                  }
                />
              </FormControl>
            </div>
          ))}
      </div>
      <button
        type="button"
        style={{ display: "none" }}
        onClick={() => {
          validateForm().then((validation: any) => {
            setTouched(validation);
          });
        }}
        ref={btnBackRef}
      ></button>
      <button
        type="button"
        style={{ display: "none" }}
        onClick={() => {
          validateForm().then((validation: any) => {
            setTouched(validation);
          });
        }}
        ref={btnNextRef}
      ></button>
      <button
        type="submit"
        style={{ display: "none" }}
        ref={btnSaveRef}
      ></button>
    </Form>
  );
};

export default FormCustomerSetting;
