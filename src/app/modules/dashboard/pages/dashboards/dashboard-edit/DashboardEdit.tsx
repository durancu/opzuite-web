import { useEffect } from "react";
import { Card } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import * as actions from "../../../_redux/dashboards/dashboardsActions";
import { dashboardInitData } from "./DashboardInitData";
import { WizardForm, WizardFormStep, BackdropLoader } from "src/app/components";
import { dashboardCreateValidationSchema } from "./DashboardEditFormValidation";
import FormDashboardCreate from "./Forms/FormDashboardCreate";

export const DashboardEdit = () => {
  const navigate = useNavigate();
  const { code } = useParams();

  const dispatch = useAppDispatch();

  const { actionsLoading, dashboardForEdit } = useSelector(
    (state: any) => ({
      actionsLoading: state.dashboards.actionsLoading,
      dashboardForEdit: state.dashboards.dashboardForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchDashboard(code));
    dispatch(catalogActions.getAllCatalog());
  }, [code, dispatch]);

  const goToDashboardDetail = (dashboard: any) => {
    navigate(`/dashboards/${dashboard.code}/detail`);
  };

  const saveDashboard = (values: any) => {
    if (!code) {
      dispatch(actions.createDashboard(values)).then((dashboard: any) =>
        goToDashboardDetail(dashboard)
      );
    } else {
      dispatch(actions.updateDashboard(values)).then(() =>
        backToDashboardsList()
      );
    }
  };

  const backToDashboardsList = () => {
    navigate(-1);
  };

  if (actionsLoading) return <BackdropLoader />;

  return (
    <Card>
      <WizardForm
        initialValues={code ? dashboardForEdit : dashboardInitData}
        onSubmit={saveDashboard}
        onCancel={backToDashboardsList}
      >
        <WizardFormStep
          label="WIZARD.REPORT_DETAIL"
          validationSchema={dashboardCreateValidationSchema}
        >
          <FormDashboardCreate />
        </WizardFormStep>
      </WizardForm>
    </Card>
  );
};
