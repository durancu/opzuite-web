import { Col, Row, Card } from "react-bootstrap";
import { formatDate } from "../../../../components/functions";

interface Props {
  dashboard: any;
}

export const DashboardOverview = (props: Props) => {
  const { dashboard } = props;

  return (
    <Card
      className="mb-4 p-4 overview-section"
      style={{ background: "#f6f6f6" }}
    >
      <Card.Body>
        <Row className="mb-0">
          <Col>
            <h3 className="card-title align-items-start flex-column mb-3">
              {dashboard && dashboard.name}
            </h3>
            <span className="text-muted mt-0">
              {dashboard && dashboard.description}
            </span>
          </Col>
          <Col className="text-right">
            <p className="mb-3">
              <span className="small">Author: </span>
              <span>{dashboard && dashboard.createdBy.name}</span>
            </p>
            <p>
              <span className="small">Updated:</span>{" "}
              {dashboard &&
                formatDate(
                  dashboard.updatedAt
                    ? dashboard.updatedAt
                    : dashboard.createdAt
                )}
            </p>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};
