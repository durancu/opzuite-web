import { ReactElement, useEffect, useState } from "react";
import { injectIntl } from "react-intl";
import { Navigate, useParams } from "react-router-dom";
import { confirmEmailAddress } from "../__mocks__/authCrud";

function ConfirmEmailAddress(): ReactElement {
  const { confirmToken }: any = useParams();

  const [isConfirmed, setIsConfirmed] = useState(false);
  const [isRequested, setIsRequested] = useState(false);
  const [passwordToken, setPasswordToken] = useState(null);
  const [message, setMessage] = useState<any>({
    text: "",
    type: "info",
  });

  useEffect(() => {
    confirmEmailAddress(confirmToken)
      .then((res: any) => {
        if (res.errorCode) {
          if (res.statusCode && res.statusCode === 404) {
            setMessage({ text: "Invalid token or expired.", type: "error" });
          } else {
            setMessage({ text: "Unkown error.", type: "error" });
          }
          setIsConfirmed(false);
        } else {
          setMessage({
            text: "Password was resetted successfully.",
            type: "success",
          });
          setPasswordToken(res.data);
          setIsConfirmed(true);
        }
        setIsRequested(true);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [confirmToken]);

  return (
    <div className="">
      <div className="text-center mb-10 mb-lg-10">
        <h3 className="font-size-h1">Confirm Email Address</h3>
      </div>
      {/* begin::Head */}
      {!isConfirmed && !isRequested && (
        <div className="form-group fv-plugins-icon-container">
          <div className={`mb-10 alert alert-custom alert-light`}>
            <div className="alert-text font-weight-bold">
              <div className="font-size-12 mb-3">
                Please wait while we confirm your email address...
              </div>
            </div>
          </div>
        </div>
      )}
      {isRequested && isConfirmed && (
        <Navigate to={`/auth/reset-password/${passwordToken}`} />
      )}
      {isRequested && !isConfirmed && (
        <Navigate to="/auth/login" state={{ ...message }} />
      )}
    </div>
  );
}

export default injectIntl(ConfirmEmailAddress);
