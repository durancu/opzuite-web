/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const FinancerActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  {
    openEditFinancerPage,
    openDeleteFinancerDialog,
    openDetailFinancerPage,
  }: any
) => (
  <TableActionsMenu
    readFunction={openDetailFinancerPage}
    editFunction={openEditFinancerPage}
    deleteFunction={openDeleteFinancerDialog}
    editPermission={Permissions.FINANCERS_UPDATE}
    readPermission={Permissions.FINANCERS_READ}
    deletePermission={Permissions.FINANCERS_DELETE}
    code={row.code}
  />
);
