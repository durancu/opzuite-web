import { camelCase, startCase } from "lodash";

export const CustomerTypeColumnFormatter = (cellContent: any, row: any) => (
  <>{startCase(camelCase(row.type))}</>
);
