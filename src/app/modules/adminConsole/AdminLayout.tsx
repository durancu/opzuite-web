import { Fragment } from "react";
import { useIntl } from "react-intl";
import { Outlet } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { AdminHeader } from "./components/AdminHeader";
import { breadcrumbs } from "./breadcrumbs";

export const AdminLayout = () => {
  const intl = useIntl();
  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "MENU.DROPDOWN.ADMIN.TITLE" })}
      </PageTitle>
      <AdminHeader />
      <Outlet />
    </Fragment>
  );
};
