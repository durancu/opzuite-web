import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Carriers",
    path: "carriers",
    isSeparator: false,
    isActive: true,
  },
];
