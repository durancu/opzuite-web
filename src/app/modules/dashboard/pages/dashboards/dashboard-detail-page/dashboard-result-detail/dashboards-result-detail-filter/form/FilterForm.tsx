import {
  Button,
  Checkbox,
  FormControl,
  FormHelperText,
  TextField as TextFieldNoFormik,
} from "@mui/material";
import { CheckBox, CheckBoxOutlineBlankOutlined } from "@material-ui/icons";
import { Autocomplete as NoFormikAutocomplete } from "@material-ui/lab";
import { nanoid } from "@reduxjs/toolkit";
import { Field, FieldArray, Form, useFormikContext } from "formik";
import { TextField } from "formik-material-ui";
import React, { useEffect } from "react";
import { Col, InputGroup, Row } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "../../../../../../../../components/Catalog/redux/catalogActions";
import {
  AutoComplete,
  DatePickerCustom,
  SelectField,
} from "../../../../../../../../partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "../../../../../../../../partials/controls/forms/DateRange/dateFactory";

const icon = <CheckBoxOutlineBlankOutlined fontSize="small" />;
const checkedIcon = <CheckBox fontSize="small" />;

export const FilterForm = () => {
  const { handleSubmit, values, errors, setFieldValue } =
    useFormikContext<any>();
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  const { currentState } = useSelector(
    (state: any) => ({
      currentState: state.catalogs?.entities,
    }),
    shallowEqual
  );

  const {
    dashboardFilterModel,
    saleStatus,
    countries,
    customers,
    locations,
    saleTypes,
    customerTypes,
  }: any = currentState;

  return (
    <Form className="h-100 mb-3" style={{ maxWidth: "100%" }}>
      <Row className="mb-6">
        <Col>
          <FormControl className="form-control">
            <AutoComplete
              name="model"
              label="Source"
              options={dashboardFilterModel}
              labelField="name"
              disabled
            />
          </FormControl>
        </Col>
      </Row>
      <Row className="mb-6">
        <Col>
          <FormControl className="form-control">
            <AutoComplete
              disabled={!values.model}
              name="groupByModel"
              label="Group by"
              options={
                dashboardFilterModel.find(({ id }: any) => id === values.model)
                  ?.groupByFields || []
              }
              labelField="name"
            />
          </FormControl>
        </Col>
      </Row>
      <Row className="mb-6 pt-4">
        <Col>
          <FormControl className="form-control">
            <NoFormikAutocomplete
              disabled={!values.model}
              multiple
              disableCloseOnSelect
              options={
                dashboardFilterModel
                  .find(({ id }: any) => id === values.model)
                  ?.filterFields.map(({ id, name }: any) => ({
                    field: id,
                    operator: "=",
                    value: "",
                    name,
                  })) || []
              }
              getOptionSelected={(option, value) =>
                (Array.isArray(value) ? value : [value]).find(
                  (item) => item["field"] === option["field"]
                )
              }
              getOptionLabel={(option: any) => {
                return option["field"];
              }}
              onChange={(_, value) => {
                setFieldValue("filters", value);
              }}
              value={values.filters}
              renderOption={(option, { selected }) => (
                <React.Fragment>
                  <span>{selected}</span>
                  <Checkbox
                    icon={icon}
                    checkedIcon={checkedIcon}
                    style={{ marginRight: 8 }}
                    checked={selected}
                  />
                  <div>
                    {option["field"]}
                    <FormHelperText>{option.description}</FormHelperText>
                  </div>
                </React.Fragment>
              )}
              renderInput={(params) => (
                <TextFieldNoFormik {...params} label="Filter Fields" />
              )}
            />
          </FormControl>
        </Col>
      </Row>

      <Row className="mb-6">
        <Col>
          <FieldArray
            name="filters"
            render={() => {
              return values.filters.map((filter: any, index: number) => {
                const nameField = `filters.${index}`;
                let options: any[] = [];
                switch (filter.field) {
                  case "type":
                    if (values.model === "sale") {
                      options = saleTypes;
                    } else if (values.model === "customer") {
                      options = customerTypes;
                    }
                    break;
                  case "country":
                    options = countries;
                    break;
                  case "location":
                    options = locations;
                    break;
                  case "customer":
                    options = customers;
                    break;
                  case "status":
                    options = saleStatus;
                    break;
                  default:
                    break;
                }

                return (
                  <Row key={nanoid()}>
                    <Col>
                      <InputGroup>
                        <FormControl className="form-control">
                          <TextFieldNoFormik
                            disabled
                            /*  name={`${nameField}.field`} */
                            label="Field"
                            value={values.filters[index].field}
                          />
                        </FormControl>
                        <FormControl className="form-control">
                          <Field
                            name={`${nameField}.operator`}
                            component={TextField}
                            label="Operator"
                            disabled
                          />
                        </FormControl>
                        <FormControl className="form-control">
                          <SelectField
                            name={`${nameField}.value`}
                            label="Value"
                            labelField="name"
                            options={options}
                          />
                        </FormControl>
                      </InputGroup>
                    </Col>
                  </Row>
                );
              });
            }}
          />
        </Col>
      </Row>

      <Row className="mb-6 pt-6">
        <Col sm="6">
          <FormControl className="form-control">
            <DatePickerCustom
              label="Date from"
              name="dateFrom"
              format={DEFAULT_DATEPICKER_FORMAT}
              /*  onChange={(date: Date) =>
             setFieldValue("dateFrom", handleDateChanged(date))
           } */
            />
          </FormControl>
        </Col>
        <Col sm="6">
          <FormControl className="form-control">
            <DatePickerCustom
              label="Date to"
              name="dateTo"
              format={DEFAULT_DATEPICKER_FORMAT}
              /*  onChange={(date: Date) =>
             setFieldValue("dateTo", handleDateChanged(date))
           } */
            />
          </FormControl>
        </Col>
      </Row>

      <Row className="mt-18">
        <Col>
          <Button
            variant="contained"
            size="medium"
            type="reset"
            style={{ minWidth: "120px" }}
            className="mr-3"
          >
            <span>Reset</span>
          </Button>
        </Col>
        <Col className="text-right">
          <Button
            variant="contained"
            color="primary"
            size="medium"
            type="submit"
            style={{ minWidth: "120px" }}
            onClick={() => {
              !errors && handleSubmit();
            }}
          >
            <span>Apply</span>
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
