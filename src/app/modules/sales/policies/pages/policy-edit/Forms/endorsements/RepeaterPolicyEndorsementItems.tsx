import { Fragment, useState } from "react";
import { FieldArray, useFormikContext } from "formik";
import { Accordion, Card, Col, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  ButtonModalEndorsementAction,
  ENDORSEMENT_ITEM_TYPE,
  ItemDeleteModal,
  ModalEndorsementActionType,
} from "src/app/components/EndorsementsComponents";
import { EndorsementItemDropdownMenu } from "src/app/components/Menu";
import { Sale } from "src/app/components/interfaces/Sale";
import { useGetUserAuth } from "src/app/hooks";
import { FakeField } from "src/app/partials/controls";
import { EndorsementItemModal } from "./components";
import objectPath from "object-path";
import clsx from "clsx";
import { MissingFieldsAlert } from "src/app/components";

interface Props {
  endorsementIndex: number; // actual endorsement
}

type MODAL_ACTION = string | "EDIT" | "ADD" | "";

interface LocalState {
  type: ENDORSEMENT_ITEM_TYPE;
  action: MODAL_ACTION;
  index: number | null;
  modal: boolean;
}

export const RepeaterPolicyEndorsementItems = ({ endorsementIndex }: Props) => {
  const user = useGetUserAuth();
  const catalog = useGetCatalog();
  const { values, errors, setFieldValue }: any = useFormikContext<Sale>();

  const [localState, setLocalState] = useState<LocalState>({
    type: "RECEIVABLE", // endorsementType
    action: "EDIT", // endorsement action, Add, edit ...
    index: null, // items index in endorsements items array
    modal: false, // show or hide modal
  });

  const createEndorsementItem = (option: any) => {
    const item = {
      ...option.defaultInitialStatus,
      accountingClass: option.id,
      seller: user?.id,
      type: "",
      financerCompany: "",
      otherDetails: {
        remitTo: "",
        description: "",
      },
    };

    const updatedItems = [...values.endorsements[endorsementIndex].items, item];
    setFieldValue(`endorsements.${endorsementIndex}.items`, updatedItems);
    setLocalState((current) => {
      return {
        ...current,
        type: option.id,
        action: ModalEndorsementActionType.ADD,
        index: updatedItems.length - 1, // we add at the bottom
        modal: true,
      };
    });
  };

  const findEndorsementItemType = (endorsementItem: any) => {
    return catalog.endorsementItems?.find(
      (item: any) => item.id === endorsementItem?.accountingClass
    );
  };

  return (
    <Fragment>
      <EndorsementItemModal
        type={localState.type}
        action={localState.action}
        index={localState.index}
        endorsementIndex={endorsementIndex}
        show={localState.modal}
        onHide={() => {
          setLocalState({
            type: "RECEIVABLE",
            action: "EDIT",
            index: null,
            modal: false,
          });
        }}
      />

      <FieldArray
        name={`endorsements.${endorsementIndex}.items`}
        render={(endorsementItems) => (
          <Accordion defaultActiveKey={["0"]} flush alwaysOpen>
            {values.endorsements[endorsementIndex]?.items?.map(
              (endorsementItem: any, index: number) => {
                const itemConfig: any =
                  findEndorsementItemType(endorsementItem);
                return (
                  <Accordion.Item
                    key={index}
                    eventKey={index.toString()}
                    className="mb-6 ml-0"
                  >
                    <Card
                      key={index}
                      style={{
                        border: `1px solid ${itemConfig?.color + "33" || ""}`,
                        backgroundColor: `${itemConfig?.color + "07" || ""}`,
                      }}
                    >
                      <Accordion.Button
                        className="px-6 py-4"
                        style={{
                          backgroundColor: `${itemConfig?.color + "15" || ""}`,
                        }}
                      >
                        <div className="w-100 d-flex justify-content-between align-items-center me-4">
                          <span className="d-flex align-items-start flex-column">
                            <h4 className="fw-bold">
                              Detail{" "}
                              {endorsementIndex > 0
                                ? `E${endorsementIndex}`
                                : "A"}
                              .{index + 1}
                              <span
                                className="mx-3 px-2 py-0"
                                style={{
                                  border: "1px solid",
                                  borderRadius: "50px",
                                  borderColor: `${itemConfig?.color || ""}`,
                                }}
                              >
                                <small
                                  className="fw-bold"
                                  style={{
                                    color: `${itemConfig?.color || "#000"}`,
                                  }}
                                >
                                  {itemConfig?.name}
                                </small>
                              </span>
                            </h4>
                          </span>

                          <div
                            className={clsx(
                              "d-flex justify-content-between align-items-center gap-3",
                              {
                                "d-none":
                                  endorsementIndex === 0 &&
                                  index === 0 &&
                                  objectPath.get(
                                    values,
                                    `endorsements.${0}.type`
                                  ) === "INITIAL_BASE_PREMIUM" &&
                                  objectPath.get(
                                    values,
                                    `endorsements.${0}.items.${0}.type`
                                  ) === "DOWN_PAYMENT",
                              }
                            )}
                          >
                            <MissingFieldsAlert
                              show={objectPath.get(
                                errors,
                                `endorsements.${endorsementIndex}.items`
                              )}
                            />
                            <ButtonModalEndorsementAction
                              className="mr-2"
                              actionName={ModalEndorsementActionType.EDIT}
                              tooltipText="Edit"
                              anotherActions={() => {
                                setLocalState((current) => {
                                  return {
                                    ...current,
                                    type: itemConfig?.id,
                                    action: ModalEndorsementActionType.EDIT,
                                    index: index,
                                    modal: true,
                                  };
                                });
                              }}
                            />
                            <ItemDeleteModal
                              action={() => {
                                endorsementItems.remove(index);
                              }}
                              itemTitle={` Detail ${
                                endorsementIndex > 0
                                  ? `E${endorsementIndex}`
                                  : "A"
                              }.
                            ${index + 1}`}
                            />
                          </div>
                        </div>
                      </Accordion.Button>

                      <Accordion.Body className="p-6">
                        {/*Shared fields/defaults */}
                        <Row className="mb-4">
                          <Col md={3}>
                            <FakeField
                              label="Type"
                              type="select"
                              value={endorsementItem?.type}
                              options={
                                catalog.endorsementItemTypes[itemConfig?.id]
                              }
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              type="date"
                              label="Date"
                              value={endorsementItem?.endorsedAt}
                            />
                          </Col>
                          <Col md={3}>
                            <FakeField
                              type="number"
                              label="Amount"
                              value={endorsementItem?.amount}
                            />
                          </Col>
                        </Row>

                        {/* shared by agent comission and fee/tax */}
                        {["AGENT_COMMISSION", "FEE_TAX"].includes(
                          itemConfig?.id
                        ) && (
                          <Row className="mb-4">
                            <Col md={3}>
                              <FakeField
                                type="select"
                                label="Status"
                                value={endorsementItem?.status}
                                options={catalog.endorsementStatus}
                              />
                            </Col>
                            <Col md={3}>
                              <FakeField
                                type="select"
                                label="Agent"
                                value={endorsementItem?.seller}
                                options={catalog.employees}
                              />
                            </Col>
                          </Row>
                        )}

                        {endorsementItem?.accountingClass ===
                          "FINANCING_DIRECT_BILL" && (
                          <Row className="mb-4">
                            <Col md={3}>
                              <FakeField
                                type="select"
                                label="Finance Company (PFA)"
                                value={endorsementItem?.financerCompany}
                                options={catalog.financers}
                              />
                            </Col>
                          </Row>
                        )}

                        {/* Shared between payable and fee/tax */}
                        {["PAYABLE", "FEE_TAX"].includes(
                          endorsementItem?.accountingClass
                        ) && (
                          <Row className="mb-4">
                            <FakeField
                              type="select"
                              label="Remit To"
                              value={endorsementItem?.otherDetails?.remitTo}
                              options={catalog.remitToOptions}
                            />
                          </Row>
                        )}

                        {/* Shared description */}
                        <Row className="mb-4">
                          {!["RECEIVABLE", "PAYABLE"].includes(
                            itemConfig?.id
                          ) && (
                            <Col md={12} className="mb-4">
                              <FakeField
                                type="text"
                                label="Description"
                                value={endorsementItem?.description}
                              />
                            </Col>
                          )}

                          {/* those that need other details */}
                          {[
                            "FINANCING_DIRECT_BILL",
                            "AGENT_COMMISSION",
                          ].includes(endorsementItem?.accountingClass) && (
                            <Col md={12} className="mb-4">
                              <FakeField
                                type="text"
                                label="Other descriptions"
                                value={
                                  endorsementItem?.otherDetails?.description
                                }
                              />
                            </Col>
                          )}
                        </Row>
                      </Accordion.Body>
                    </Card>
                  </Accordion.Item>
                );
              }
            )}

            <EndorsementItemDropdownMenu
              options={catalog.endorsementItems || []}
              itemAction={(option: any) => createEndorsementItem(option)}
            />
          </Accordion>
        )}
      />
    </Fragment>
  );
};

export default RepeaterPolicyEndorsementItems;
