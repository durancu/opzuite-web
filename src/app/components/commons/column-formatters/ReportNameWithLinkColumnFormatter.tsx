import { Link } from "react-router-dom";

export function ReportNameWithLinkColumnFormatter(reportName: any, report: any) {
  return (
    <>
      <Link
        className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
        to={`/manager/reports/${report?.code}/detail`}
      >
        {reportName}
      </Link>
    </>
  );
}
