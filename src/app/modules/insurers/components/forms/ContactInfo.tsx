import { Col, Row, Button } from "react-bootstrap";
import {
  FormInput,
  PhoneNumberInput,
  WizardFormSection,
} from "src/app/components";
import { useFormikContext } from "formik";

const ContactInfo = () => {
  const { values, setFieldValue }: Record<string, any> = useFormikContext();

  function copyInfo() {
    setFieldValue("contact.address", values.business.address);
    setFieldValue("contact.email", values.business.email);
    setFieldValue("contact.phone", values.business.primaryPhone);
    setFieldValue("contact.mobilePhone", values.business.fax);
  }

  return (
    <WizardFormSection
      label="Contact Info"
      action={<Button onClick={copyInfo}>Copy from Business Info</Button>}
    >
      <Row className="mb-4">
        <Col>
          <FormInput
            name="contact.firstName"
            labelTextId="FORM.LABELS.FIRST_NAME"
          />
        </Col>
        <Col>
          <FormInput
            name="contact.lastName"
            labelTextId="FORM.LABELS.LAST_NAME"
          />
        </Col>
      </Row>
      <Row className="mb-4">
        <Col>
          <FormInput
            type="email"
            name="contact.email"
            labelTextId="FORM.LABELS.EMAIL"
          />
        </Col>
        <Col>
          <PhoneNumberInput name="contact.phone" />
        </Col>
        <Col>
          <PhoneNumberInput
            name="contact.mobilePhone"
            labelTextId="FORM.LABELS.MOBILE_NUMBER"
          />
        </Col>
      </Row>
    </WizardFormSection>
  );
};

export default ContactInfo;
