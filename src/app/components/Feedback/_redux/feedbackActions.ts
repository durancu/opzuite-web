import { VariantType } from "notistack";
import { Dispatch } from "redux";
import { callTypes, feedbacksSlice } from "./feedbackSlice";

const { actions } = feedbacksSlice;

interface Params {
  message: string;
  options: {
    key: number;
    variant: VariantType;
  };
}

export const feedbackEnqueueMessage =
  (params: Params) => (dispatch: Dispatch) => {
    dispatch(actions.startCall({ callType: callTypes.action } as any));
    dispatch(actions.enqueueFeedbackMessage(params as any));
  };

export const feedbackRemoveMessage = (key: any) => (dispatch: Dispatch) => {
  dispatch(actions.startCall({ callType: callTypes.action } as any));
  dispatch(actions.removeFeedbackMessage({ key } as any));
};
