export const AUTO_TYPE_COMMERCIAL_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "TRACTOR-TRAILER", name: "Tractor-Trailer" },
  { id: "TRUCK-TRACTOR", name: "Truck-Tractor" },
  { id: "BOX-TRUCK", name: "Box Truck" },
  { id: "DUMP-AND-GARBAGE", name: "Dump & Garbage Truck" },
  { id: "TANKER-TRUCK", name: "Tanker Truck" },
  { id: "FLATBED-TRUCK", name: "Flatbed Truck" },
  { id: "TOW-TRUCK", name: "Tow Truck" },
  { id: "BUS", name: "Bus" },
  { id: "MOVING-TRUCK", name: "Moving Truck" },
];
