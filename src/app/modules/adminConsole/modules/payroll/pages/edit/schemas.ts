import * as Yup from "yup";

export const payrollSetupValidationSchema = Yup.object().shape({
  location: Yup.string().required("Location is required."),
  payPeriodStartedAt: Yup.mixed().required("Pay Period is required."),
  payPeriodEndedAt: Yup.mixed().required("Pay Period is required."),
});

export const payrollSalaryMetricsValidationSchema = Yup.object().shape({
  payStubs: Yup.array().of(
    Yup.object().shape({
      employeeName: Yup.string(),
      normalHoursWorked: Yup.number(),
      overtimeHoursWorked: Yup.number(),
      addons: Yup.array().of(
        Yup.object().shape({
          amount: Yup.number(),
          description: Yup.string(),
        })
      ),
    })
  ),
});
