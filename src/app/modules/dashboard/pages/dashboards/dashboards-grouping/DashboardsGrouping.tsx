import { useMemo } from "react";
import { useDashboardsUIContext } from "../DashboardsUIContext";

export function DashboardsGrouping() {
  // Dashboards UI Context
  const dashboardsUIContext: any = useDashboardsUIContext();
  const dashboardsUIProps: any = useMemo(() => {
    return {
      ids: dashboardsUIContext.ids,
      setIds: dashboardsUIContext.setIds,
      openDeleteDashboardsDialog:
        dashboardsUIContext.openDeleteDashboardsDialog,
      openFetchDashboardsDialog: dashboardsUIContext.openFetchDashboardsDialog,
      openUpdateDashboardsStatusDialog:
        dashboardsUIContext.openUpdateDashboardsStatusDialog,
    };
  }, [dashboardsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{dashboardsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={dashboardsUIProps.openDeleteDashboardsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              {/*               &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={dashboardsUIProps.openFetchDashboardsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={dashboardsUIProps.openUpdateDashboardsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
