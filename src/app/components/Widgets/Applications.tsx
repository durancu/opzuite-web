import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Permissions } from "src/app/constants/Permissions";
import { useGetUserAuth } from "src/app/hooks";
import { ServicesTable } from "src/app/modules/sales/services/components/ServicesTable";
import { isPermitted } from "src/app/utils";
import { FilterUIProvider } from "../Filter";

interface Props {
  title?: string;
  subTitle?: string;
  context?: string;
  filter: Record<string, any>;
}

export const Applications = ({
  title,
  subTitle,
  context = "sales",
  filter,
}: Props) => {
  const user = useGetUserAuth();

  function getPermittedFilters() {
    const filter: any = {};
    const permissions = user?.permissions || [];
    if (isPermitted([Permissions.MY_COUNTRY], permissions)) {
      filter["country"] = user?.country;
    } else if (isPermitted([Permissions.MY_LOCATION], permissions)) {
      filter["location"] = user?.location.id;
    } else if (!isPermitted([Permissions.MY_COMPANY], permissions)) {
      filter["seller"] = user?.id;
    }
    return filter;
  }

  const defaultFilter = {
    filter: { ...filter, type: "PERMIT", ...getPermittedFilters() },
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 10,
  };

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className=" mb-3 text-primary">
        {title || "Services"}
      </Card.Title>
      <p className="mb-4">
        {subTitle || (
          <FormattedMessage id="GENERAL.WIDGETS.APPLICATIONS.TABLE.SUBHEADER" />
        )}
      </p>
      <FilterUIProvider context={context} defaultFilter={defaultFilter}>
        <ServicesTable showFilter={false} />
      </FilterUIProvider>
    </Card>
  );
};
