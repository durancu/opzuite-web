import { Card } from "react-bootstrap";
import { Permissions } from "src/app/constants/Permissions";
import { useGetUserAuth } from "src/app/hooks";
import { PoliciesTable } from "src/app/modules/sales/policies/components/PoliciesTable";
import { isPermitted } from "src/app/utils";
import { FilterUIProvider } from "../Filter";

interface Props {
  title?: string;
  subTitle?: string;
  context?: string;
  filter: Record<string, any>;
}

export const Policies = ({
  title,
  subTitle,
  context = "sales",
  filter,
}: Props) => {
  const user = useGetUserAuth();

  function getPermittedFilters() {
    const filter: any = {};
    const permissions = user?.permissions || [];
    if (isPermitted([Permissions.MY_COUNTRY], permissions)) {
      filter["country"] = user?.country;
    } else if (isPermitted([Permissions.MY_LOCATION], permissions)) {
      filter["location"] = user?.location.id;
    } else if (!isPermitted([Permissions.MY_COMPANY], permissions)) {
      filter["seller"] = user?.id;
    }
    return filter;
  }

  const defaultFilter = {
    filter: { ...filter, type: "POLICY", ...getPermittedFilters() },
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 10,
  };

  return (
    <Card className="mb-4 p-8">
      <Card.Title as="h3" className="mb-3 text-primary">
        {title || "Policies"}
      </Card.Title>

      <p className="mb-10">
        {subTitle || "Policies with coverage provided by this institution."}
      </p>

      <FilterUIProvider context={context} defaultFilter={defaultFilter}>
        <PoliciesTable showFilter={false} />
      </FilterUIProvider>
    </Card>
  );
};
