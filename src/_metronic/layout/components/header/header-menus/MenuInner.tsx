import { Fragment } from "react";
import { useIntl } from "react-intl";
import { RequirePermission } from "src/app/components";
import { ADMIN_CONSOLE_PERMISSIONS } from "src/app/constants/AdminConsolePermissions";
import { MenuInnerWithSub } from "./MenuInnerWithSub";
import { MenuItem } from "./MenuItem";

export const MenuInner = () => {
  const intl = useIntl();
  return (
    <Fragment>
      {/* <MenuItem
        end
        to="/"
        title={intl.formatMessage({ id: "MENU.MAIN.HOME" })}
        fontIcon="bi-grid-1x2"
      /> 
       <RequirePermission permissions={Permissions.POLICIES_READ}>
        <MenuItem
          to="policies"
          title={intl.formatMessage({ id: "MENU.MAIN.POLICIES" })}
          fontIcon="bi-shield-shaded"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.APPLICATIONS_READ}>
        <MenuItem
          to="services"
          title={intl.formatMessage({ id: "MENU.MAIN.APPLICATIONS" })}
          fontIcon="bi-app-indicator"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.CUSTOMERS_READ}>
        <MenuItem
          to="customers"
          title={intl.formatMessage({ id: "MENU.MAIN.CUSTOMERS" })}
          fontIcon="bi-person-fill"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.CARRIERS_READ}>
        <MenuItem
          to="carriers"
          title={intl.formatMessage({ id: "MENU.MAIN.CARRIERS" })}
          fontIcon="bi-umbrella-fill"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.MGAS_READ}>
        <MenuItem
          to="mgas"
          title={intl.formatMessage({ id: "MENU.MAIN.MGA" })}
          fontIcon="fa-regular fa-handshake"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.USERS_READ}>
        <MenuItem
          to="agents"
          title={intl.formatMessage({ id: "MENU.MAIN.AGENTS" })}
          fontIcon="bi-people"
        />
      </RequirePermission>

      <RequirePermission permissions={Permissions.FINANCERS_READ}>
        <MenuItem
          to="financers"
          title={intl.formatMessage({ id: "MENU.MAIN.FINANCERS" })}
          fontIcon="bi-cash-coin"
        />
      </RequirePermission>

      <RequirePermission
        permissions={[
          Permissions.REPORTS_READ,
          Permissions.POLICIES,
          Permissions.POLICIES_RENEW,
          Permissions.POLICIES_UPDATE,
        ]}
      >
        <MenuItem
          to="policies/renewals"
          title={intl.formatMessage({ id: "MENU.MAIN.RENEWAL_CENTER" })}
          fontIcon="bi-recycle"
        />
      </RequirePermission> */}

      <RequirePermission permissions={ADMIN_CONSOLE_PERMISSIONS}>
        <MenuInnerWithSub
          title={intl.formatMessage({ id: "MENU.DROPDOWN.ADMIN.TITLE.ABBR" })}
          to="manager"
          menuPlacement="bottom-start"
          menuTrigger={`{default:'click', lg: 'hover'}`}
          fontIcon="bi-grid-1x2"
        >
          {/* <MenuItem
            to="manager"
            title={intl.formatMessage({ id: "MENU.MAIN.HOME" })}
            fontIcon="bi-grid-1x2"
          /> */}
          <MenuItem
            to="manager/users"
            title={intl.formatMessage({ id: "MENU.MAIN.EMPLOYEES" })}
            fontIcon="bi-people"
          />
          {/* <MenuItem
            to="manager/companies"
            title={intl.formatMessage({ id: "MENU.MAIN.COMPANIES" })}
            fontIcon="bi-building"
          /> */}
          <MenuItem
            to="manager/locations"
            title={intl.formatMessage({ id: "MENU.MAIN.LOCATIONS" })}
            fontIcon="bi-geo-alt"
          />

          <MenuItem
            to="manager/reports"
            title={intl.formatMessage({ id: "MENU.MAIN.REPORTS" })}
            fontIcon="bi-graph-up"
          />
          <MenuItem
            to="manager/payrolls"
            title={intl.formatMessage({ id: "MENU.MAIN.PAYROLLS" })}
            fontIcon="bi-cash"
          />
        </MenuInnerWithSub>
      </RequirePermission>
    </Fragment>
  );
};
