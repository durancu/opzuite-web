import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { PageLink, PageTitle } from "src/_metronic/layout/core";
import { FilterUIProvider } from "src/app/components";
import { RenewalsTable } from "../components/RenewalsTable";
import { defaultRenewalFilter } from "../components/helpers";

const breadcrumbs: Array<PageLink> = [
  {
    title: "All Renewals",
    path: "policies/renewals",
    isSeparator: false,
    isActive: true,
  },
];

export const Renewals = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`/policies/${code}/edit`),
    details: (code: string) => navigate(`/policies/${code}/detail`),
    delete: (code: string) => navigate(`/policies/${code}/delete`),
    renew: (code: string) => navigate(`/policies/${code}/renew`),
    certificates: (customerId: string) => {
      navigate(`/certificates/new?customer=${customerId}`);
    },
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Renewals</PageTitle>
      <Card className="p-8">
        <FilterUIProvider
          context="sales"
          menuActions={menuActions}
          defaultFilter={defaultRenewalFilter}
        >
          <RenewalsTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
