import { shallowEqual, useSelector } from "react-redux";

export const useGetCoverages = () => {
  const { coverages } = useSelector(
    (state: any) => ({
      coverages: state.catalogs?.entities?.coverages,
    }),
    shallowEqual
  );

  return coverages;
};
