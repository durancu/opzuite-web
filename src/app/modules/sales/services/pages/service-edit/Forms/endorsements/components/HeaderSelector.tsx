import { useMemo } from "react";
import { ModalEndorsementActionType } from "src/app/components/EndorsementsComponents";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

type MODAL_ACTION = string | "EDIT" | "ADD" | "";

interface Props {
  type: any;
  index: any;
  action: MODAL_ACTION;
  endorsementIndex: number;
}

export const HeaderSelector = ({
  type,
  action,
  index,
  endorsementIndex,
}: Props) => {
  const { endorsementItems } = useGetCatalog();

  const details = useMemo(() => {
    const item = endorsementItems.find((option) => option.id === type);
    return item ? item : {};
  }, [type]);

  return (
    <div
      className="px-6 py-4"
      style={{
        backgroundColor: `${details?.color + "15" || ""}`,
      }}
    >
      <span className="fw-bold">
        {action === ModalEndorsementActionType.ADD ? "Add " : "Edit "} Detail{" "}
        {endorsementIndex > 0
          ? `E${endorsementIndex}`
          : `A${endorsementIndex + 1}`}
        .{index + 1}{" "}
        <span
          className="mx-3 px-2"
          style={{
            border: "1px solid",
            borderRadius: "50px",
            color: `${details?.color || "#000"}`,
            borderColor: `${details?.color || ""}`,
          }}
        >
          <small className="fw-bold p-1">{details?.name}</small>
        </span>
      </span>
    </div>
  );
};
