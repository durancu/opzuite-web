import { Fragment } from "react";

export const EmailColumnFormatter = (email: any) => (
    <Fragment>
        <span className="text-dark fw-semibold fs-6">
            {email}
        </span>
    </Fragment>
);
