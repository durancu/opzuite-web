import { Fragment, useState } from "react";
import * as Yup from "yup";
import clsx from "clsx";
import { Link, Navigate } from "react-router-dom";
import { useFormik } from "formik";
import { requestPassword } from "../__mocks__/authCrud";
import { FormattedMessage, useIntl } from "react-intl";

const initialValues = {
  email: "",
};

export const ForgotPassword = () => {
  const intl = useIntl();
  const [loading] = useState(false);
  const [hasErrors] = useState<boolean | undefined>(undefined);
  const [isRequested, setIsRequested] = useState(false);
  const [message, setMessage] = useState<any>({
    text: "",
    type: "info",
  });

  const forgotPasswordSchema = Yup.object().shape({
    email: Yup.string()
      .email("Wrong email format")
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
  });
  const formik = useFormik({
    initialValues,
    validationSchema: forgotPasswordSchema,
    onSubmit: (values, { setStatus, setSubmitting }) => {
      requestPassword(values.email).then((res: any) => {
        if (res.errorCode) {
          if (res.statusCode && res.statusCode === 404) {
            setStatus(
              "User not found. Please check your email address and try again."
            );
            setMessage(
              "User not found. Please check your email address and try again."
            );
          }
          if (res.statusCode && res.statusCode === 409) {
            setStatus(
              "It seems that you already asked for a new password. Please check your email inbox, including your spam folder for an email with subject like 'You asked to reset your password'"
            );
            setMessage(
              "It seems that you already asked for a new password. Please check your email inbox, including your spam folder for an email with subject like 'You asked to reset your password'"
            );
          }
          setSubmitting(false);
        } else {
          setStatus(
            "We just sent you an email with instructions to reset your password."
          );
          setMessage({
            text: "We just sent you an email with instructions to reset your password.",
            type: "success",
          });
          setIsRequested(true);
        }
      });
    },
  });

  return (
    <Fragment>
      {isRequested && <Navigate to="/auth/login" state={{ ...message }} />}
      {!isRequested && (
        <form
          className="form w-100 fv-plugins-bootstrap5 fv-plugins-framework"
          noValidate
          id="kt_login_password_reset_form"
          onSubmit={formik.handleSubmit}
        >
          <div className="text-center mb-10">
            {/* begin::Title */}
            <h1 className="text-dark fw-bolder mb-3">Forgot Password ?</h1>
            {/* end::Title */}

            {/* begin::Link */}
            <div className="text-gray-500 fw-semibold fs-6">
              Enter your email to reset your password.
            </div>
            {/* end::Link */}
          </div>

          {/* begin::Title */}
          {hasErrors === true && (
            <div className="mb-lg-15 alert alert-danger">
              <div className="alert-text font-weight-bold">
                Sorry, looks like there are some errors detected, please try
                again.
              </div>
            </div>
          )}

          {hasErrors === false && (
            <div className="mb-10 bg-light-info p-8 rounded">
              <div className="text-info">
                Sent password reset. Please check your email
              </div>
            </div>
          )}
          {/* end::Title */}

          {/* begin::Form group */}
          <div className="fv-row mb-8">
            <label className="form-label fw-bolder text-gray-900 fs-6">
              Email
            </label>
            <input
              type="email"
              placeholder=""
              autoComplete="off"
              {...formik.getFieldProps("email")}
              className={clsx(
                "form-control bg-transparent",
                { "is-invalid": formik.touched.email && formik.errors.email },
                {
                  "is-valid": formik.touched.email && !formik.errors.email,
                }
              )}
            />
            {formik.touched.email && formik.errors.email && (
              <div className="fv-plugins-message-container">
                <div className="fv-help-block">
                  <span role="alert">{formik.errors.email}</span>
                </div>
              </div>
            )}
          </div>
          {/* end::Form group */}

          {/* begin::Form group */}
          <div className="d-flex flex-wrap justify-content-center pb-lg-0">
            <button
              type="submit"
              id="kt_password_reset_submit"
              className="btn btn-primary me-4"
            >
              <span className="indicator-label">Submit</span>
              {loading && (
                <span className="indicator-progress">
                  Please wait...
                  <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                </span>
              )}
            </button>
            <Link to="/auth/login">
              <button
                type="button"
                id="kt_login_password_reset_form_cancel_button"
                className="btn btn-light"
                disabled={formik.isSubmitting || !formik.isValid}
              >
                <FormattedMessage id="BUTTON.CANCEL" />
              </button>
            </Link>
          </div>
          {/* end::Form group */}
        </form>
      )}
    </Fragment>
  );
};
