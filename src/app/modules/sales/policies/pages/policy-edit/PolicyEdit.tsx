import { Fragment, useMemo } from "react";
import { startCase } from "lodash";
import { useNavigate, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "src/app/hooks";
import { Card } from "react-bootstrap";
import { Alert } from "src/app/components/Feedback";
import { expirationDateByRenewalFrequency } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { PageTitle } from "src/_metronic/layout/core";
import {
  BackdropLoader,
  WizardForm,
  WizardFormStep,
  Error,
} from "src/app/components";

import FormPolicyInfo from "./Forms/FormPolicyInfo";
import FormPolicyProducts from "./Forms/FormPolicyProducts";
import {
  policyBasicInfoValidationSchema,
  policyDetailValidationSchema,
} from "./PolicyEditFormValidation";
import { breadcrumbs } from "../../breadcrumbs";
import { salesAPI, saleActions } from "src/app/modules/sales/redux/saleSlice";
import { defaultPolicy } from "./helpers";
import { useSnackbar } from "notistack";
import { FormikValues } from "formik";

interface routeParams {
  code?: string;
  type?: "edit" | "renew";
}

export const PolicyEdit = () => {
  const { code, type }: routeParams = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const savedPolicy = useAppSelector((state) => state.sales.savedPolicy);
  const dispatch = useAppDispatch();
  const {
    data: policy,
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetSaleByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [createPolicy, { isLoading: isCreating }] =
    salesAPI.useCreateSaleMutation();

  const [updatePolicy, { isLoading: isUpdating }] =
    salesAPI.useUpdateSaleMutation();

  const [renewPolicy, { isLoading: isRenewing }] =
    salesAPI.useRenewSaleMutation();

  const checkedPolicy = useMemo(() => {
    // always resume if theres a saved policy
    if (savedPolicy?.customer) {
      return savedPolicy;
    } else if (code && policy && type === "renew") {
      const newPolicy = { ...policy };
      newPolicy.soldAt = new Date();
      newPolicy.effectiveAt = newPolicy.expiresAt;
      newPolicy.expiresAt = expirationDateByRenewalFrequency(
        newPolicy.renewalFrequency,
        newPolicy.effectiveAt
      );
      delete newPolicy["id"];
      delete newPolicy["_id"];
      return {
        ...newPolicy,
        isRenewal: true,
        renewalReferences: [policy.id],
      };
    }
    return policy;
  }, [policy, code, type, savedPolicy]);

  const backToPoliciesList = () => {
    dispatch(saleActions.clear());
    navigate("/policies");
  };

  const goToPolicyDetail = (code: string) => {
    navigate(`/policies/${code}/detail`);
  };

  async function handleCreatePolicy(values: FormikValues) {
    try {
      const response = await createPolicy(values).unwrap();
      enqueueSnackbar("Policy created successfully", {
        variant: "success",
      });
      goToPolicyDetail(response.code);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleRenewPolicy(values: FormikValues) {
    const request = {
      ...values,
      renewalReferenceCodes: [values.code],
    };

    try {
      const response = await renewPolicy(request).unwrap();
      enqueueSnackbar("Policy renewed successfully", {
        variant: "success",
      });
      goToPolicyDetail(response.code);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleUpdatePolicy(values: FormikValues) {
    try {
      await updatePolicy(values).unwrap();
      enqueueSnackbar("Policy updated successfully", {
        variant: "success",
      });
      navigate(-1);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handlePolicy(values: FormikValues) {
    if (code && type === "edit") {
      await handleUpdatePolicy(values);
    } else if (code && type === "renew") {
      await handleRenewPolicy(values);
    } else {
      await handleCreatePolicy(values);
    }
  }

  if (isLoading || isCreating || isUpdating || isRenewing) {
    return <BackdropLoader />;
  }

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!code ? `New Policy` : `${startCase(type)} Policy`}
      </PageTitle>
      <Card>
        <Alert
          show={Object.keys(savedPolicy).length > 0}
          variant="warning"
          labelComponent="policy"
          action={() => dispatch(saleActions.clear())}
        />
        <WizardForm
          initialValues={checkedPolicy || defaultPolicy}
          onSubmit={handlePolicy}
          onCancel={backToPoliciesList}
        >
          <WizardFormStep
            labelTextId="WIZARD.POLICY.BASIC.INFO"
            validationSchema={policyBasicInfoValidationSchema}
          >
            <FormPolicyInfo policyType={type} />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.POLICY.DETAIL"
            validationSchema={policyDetailValidationSchema}
          >
            <FormPolicyProducts />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
