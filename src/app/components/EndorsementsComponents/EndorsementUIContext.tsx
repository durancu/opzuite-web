import {
  createContext,
  MutableRefObject,
  ReactElement,
  useContext,
  useRef,
  useState,
} from "react";
import { ObjectSchema } from "yup";
import { EndorsementContextType } from "./EndorsementContextType";
import {
  EndorsementType,
  ENDORSEMENT_TYPE,
  MODAL_ENDORSEMENT_ACTION_TYPE,
} from "./EndorsementTypeAndEnum";

interface Props {
  children: ReactElement[] | ReactElement;
}

const EndorsementUIContext = createContext<EndorsementContextType | null>(null);

export function useEndorsementUIContext(): EndorsementContextType {
  return useContext(EndorsementUIContext) as EndorsementContextType;
}

export const EndorsementUIConsumer = EndorsementUIContext.Consumer;

export function EndorsementUIProvider({ children }: Props): ReactElement {
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false);
  const [coverageCode, setCoverageCode] = useState<string>("");
  const [indexEndorsement, setIndexEndorsement] = useState<number>(-1);
  const [endorsementToEdit, setEndorsementToEdit] = useState({});
  const [indexItemEndorsement, setIndexItemEndorsement] = useState<number>(-1);
  const [endorsementActionType, setEndorsementActionType] =
    useState<MODAL_ENDORSEMENT_ACTION_TYPE>("");
  const [endorsementItemType, setEndorsementItemType] = useState({});
  const [cardTitle, setCardTitle] = useState<string>("");
  const [endorsementType, setEndorsementType] = useState<ENDORSEMENT_TYPE>(
    EndorsementType.CONTAINER
  );
  const [errors, setErrors] = useState<any>({});
  const [schemaValidation, setSchemaValidation] = useState<any>();

  const buttonCloseRef: MutableRefObject<any> = useRef();
  const buttonSaveRef: MutableRefObject<any> = useRef();

  function handleClickButtonClose() {
    buttonCloseRef.current.click();
  }

  function handleClickButtonSave() {
    buttonSaveRef.current.click();
  }

  const setDefaultValues = () => {
    setCoverageCode("");
    setIndexEndorsement(-1);
    setIndexItemEndorsement(-1);
    setEndorsementActionType("");
    setEndorsementItemType({});
    setErrors({});
  };

  const mixSchema = (e: ObjectSchema<Record<string, never>>[]): void => {
    const newSchema = e[0];

    e.forEach((element, index) => {
      index != 0 && newSchema.concat(element);
    });

    setSchemaValidation(newSchema);
  };

  const value: EndorsementContextType = {
    modalIsOpen,
    setModalIsOpen,
    endorsementActionType,
    setEndorsementActionType,
    coverageCode,
    setCoverageCode,
    indexEndorsement,
    setIndexEndorsement,
    indexItemEndorsement,
    setIndexItemEndorsement,
    endorsementToEdit,
    setEndorsementToEdit,
    buttonCloseRef,
    buttonSaveRef,
    handleClickButtonClose,
    handleClickButtonSave,
    endorsementItemType,
    setEndorsementItemType,
    setDefaultValues,
    cardTitle,
    setCardTitle,
    endorsementType,
    setEndorsementType,
    errors,
    setErrors,
    schemaValidation,
    mixSchema,
  };

  return (
    <EndorsementUIContext.Provider value={value}>
      {children}
    </EndorsementUIContext.Provider>
  );
}
