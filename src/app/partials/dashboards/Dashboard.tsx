import { nanoid } from "@reduxjs/toolkit";
import parse from "html-react-parser";
import { unescape } from "lodash";
import { Fragment, useMemo, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { useIntl } from "react-intl";
import { PageLink, PageTitle, Toolbar } from "src/_metronic/layout/core";
import { useGetUserAuth } from "src/app/hooks";
import { DateRangeFilter } from "../controls";
import {
  DateRangeEnum,
  dateRangeByName,
} from "../controls/forms/DateRange/dateFactory";
import { ChartWidget } from "../widgets/charts";

const breadcrumbs: Array<PageLink> = [
  {
    title: "Seller Dashboard",
    path: "/",
    isSeparator: false,
    isActive: true,
  },
];

const defaultDateRange = dateRangeByName(DateRangeEnum.THIS_FISCAL_MONTH);

const ChartColumn = ({ children, withKey = false }: any) => {
  return (
    <Fragment>
      {withKey && <div className="break"></div>}
      {children}
    </Fragment>
  );
};

export const Dashboard = () => {
  const intl = useIntl();
  const user = useGetUserAuth();
  const [filter, setFilter] = useState(defaultDateRange);

  const dashboardLayout = useMemo(() => {
    if (user?.preferences) {
      if (user.preferences.dashboard) {
        return user.preferences.dashboard.layout;
      } else {
        if (user.company.settings.defaults) {
          return user.company.settings.defaults.dashboard.layout;
        }
      }
    }
  }, [user]);

  const htmlDecode = (input: string) => {
    const htmlText: string = unescape(input);
    return <div key={nanoid(3)}>{parse(htmlText)}</div>;
  };

  const renderSection = (sectionContents: any[]) => {
    let contents: any[] = [];
    if (sectionContents && sectionContents.length > 0) {
      let columnsCount = 0;
      contents = sectionContents.map(({ type, content, columnsWidth }: any) => {
        const chartId = nanoid(3);
        let output;
        switch (type) {
          case "chart":
            output = <ChartWidget code={content} filter={filter} />;
            break;
          case "component":
            // output = getComponentByName(content);
            break;
          case "html":
            output = htmlDecode(content);
            break;
          default:
            output = "Empty component";
        }

        let dashboardColumn;
        if (columnsCount + columnsWidth > 12) {
          dashboardColumn = (
            <ChartColumn key={chartId} withKey>
              <Col className={`col-${columnsWidth}`}>{output}</Col>
            </ChartColumn>
          );
          columnsCount = columnsWidth;
        } else {
          dashboardColumn = (
            <ChartColumn key={chartId}>
              <Col className={`col-${columnsWidth}`}>{output}</Col>
            </ChartColumn>
          );
          columnsCount += columnsWidth;
        }

        return dashboardColumn;
      });
    }
    return contents;
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "MENU.MAIN.HOME" })}
      </PageTitle>

      <Toolbar>
        <DateRangeFilter
          values={filter}
          onChange={(values) => setFilter(values)}
        />
      </Toolbar>

      <Row>
        <Col hidden={!dashboardLayout?.left} className="col-3">
          {renderSection(dashboardLayout?.left)}
        </Col>
        <Col>
          {/* TOP-SECTION */}
          <Row hidden={!dashboardLayout?.top}>
            <Col>{renderSection(dashboardLayout?.top)}</Col>
          </Row>
          {/* MAIN-SECTION */}
          <Row hidden={!dashboardLayout?.main}>
            {renderSection(dashboardLayout?.main)}
          </Row>
          {/* BOTTOM-SECTION */}
          <Row hidden={!dashboardLayout?.bottom}>
            <Col>{renderSection(dashboardLayout?.bottom)}</Col>
          </Row>
        </Col>
        {/* RIGHT-SIDE-BAR */}
        <Col hidden={!dashboardLayout?.right} className="col-3">
          {renderSection(dashboardLayout?.right)}
        </Col>
      </Row>
    </Fragment>
  );
};
