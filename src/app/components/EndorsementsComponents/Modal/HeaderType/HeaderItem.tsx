import { ModalEndorsementActionType } from "../../EndorsementTypeAndEnum";
import { useEndorsementUIContext } from "../../EndorsementUIContext";

export const HeaderItem = () => {
  const {
    endorsementItemType,
    indexItemEndorsement,
    indexEndorsement,
    endorsementActionType,
  } = useEndorsementUIContext();
  return (
    <div
      className="px-6 py-4"
      style={{
        backgroundColor: `${endorsementItemType?.color + "15" || ""}`,
      }}
    >
      <span className="fw-bold">
        {endorsementActionType === ModalEndorsementActionType.ADD
          ? "Add "
          : "Edit "}{" "}
        Detail{" "}
        {indexEndorsement > 0
          ? `E${indexEndorsement}`
          : `A${indexEndorsement + 1}`}
        .{indexItemEndorsement + 1}{" "}
        <span
          className="mx-3 px-2"
          style={{
            border: "1px solid",
            borderRadius: "50px",
            color: `${endorsementItemType?.color || "#000"}`,
            borderColor: `${endorsementItemType?.color || ""}`,
          }}
        >
          <small className="fw-bold p-1">{endorsementItemType?.name}</small>
        </span>
      </span>
    </div>
  );
};
