import { FieldArray, useFormikContext } from "formik";
import { Card, Col, OverlayTrigger, Row, Tooltip } from "react-bootstrap";
import { FormInput } from "src/app/components";

function FormCreatePayroll() {
  const { values } = useFormikContext<any>();

  return (
    <FieldArray name="payStubs">
      {() =>
        values.payStubs &&
        values.payStubs.length > 0 &&
        values.payStubs.map((item: any, index: number) => (
          <Card key={index} className="mb-4 rounded shadow-xs p-8">
            <Card.Title as="h3" className="mb-8 fw-bold text-primary">
              {item.employeeName}
            </Card.Title>

            <Row>
              <Col md={2}>
                <Row className="">
                  <Col>
                    <FormInput
                      min={0}
                      type="number"
                      name={`payStubs.${index}.normalHoursWorked`}
                      labelText="Regular Hours"
                    />
                  </Col>
                </Row>
                <Row className="my-4">
                  <Col>
                    <p className="fw-bold">Monthly Base Salary</p>
                    <p className="fs-5">
                      {`$${(item.normalHoursWorked * item.hourlyRate).toFixed(
                        2
                      )}`}
                    </p>
                  </Col>
                </Row>
                <Row className="my-4">
                  <Col>
                    <p className="fw-bold">Total Premium Sales</p>
                    <p className="fs-5">{`$${item.totalPremium.toFixed(2)}`}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={2}>
                <Row>
                  <Col>
                    <p className="d-flex gap-2 align-items-center">
                      <span className="fw-bold">Premium Commissions</span>
                      <OverlayTrigger
                        placement="bottom"
                        overlay={<Tooltip>Based on premium sales</Tooltip>}
                      >
                        <i className="bi bi-question-circle"></i>
                      </OverlayTrigger>
                    </p>
                    <p className="fs-5">{`$${item.totalSaleBonus.toFixed(
                      2
                    )}`}</p>
                  </Col>
                </Row>
                <Row className="mt-6 d-inline-flex">
                  <Col>
                    <p className="d-flex gap-2 align-items-center">
                      <span className="fw-bold">Fees Commissions</span>
                      <OverlayTrigger
                        placement="bottom"
                        overlay={<Tooltip>30% of fee sales</Tooltip>}
                      >
                        <i className="bi bi-question-circle"></i>
                      </OverlayTrigger>
                    </p>
                    <p className="fs-5">
                      {`$${(item.totalFees * 0.3).toFixed(2)}`}
                    </p>
                  </Col>
                </Row>
                <Row className="mt-6 d-inline-flex">
                  <Col>
                    <p className="d-flex gap-2 align-items-center">
                      <span className="fw-bold">Service Commissions</span>
                      <OverlayTrigger
                        placement="bottom"
                        overlay={<Tooltip>20% of service sales</Tooltip>}
                      >
                        <i className="bi bi-question-circle"></i>
                      </OverlayTrigger>
                    </p>
                    <p className="fs-5">
                      {`$${(item.totalPermits * 0.2).toFixed(2)}`}
                    </p>
                  </Col>
                </Row>
                <Row className="mt-6 d-inline-flex">
                  <Col>
                    <p className="d-flex gap-2 align-items-center">
                      <span className="fw-bold">Tips Commissions</span>
                      <OverlayTrigger
                        placement="bottom"
                        overlay={<Tooltip>100% of total sales</Tooltip>}
                      >
                        <i className="bi bi-question-circle"></i>
                      </OverlayTrigger>
                    </p>
                    <p className="fs-5">{`$${item.totalTips.toFixed(2)}`}</p>
                  </Col>
                </Row>
              </Col>
              <Col md={6}>
                <Row>
                  <Col md="4">
                    <FormInput
                      type="number"
                      name={`payStubs.${index}.totalExtraBonus`}
                      labelText="Other Commissions"
                      helperText=" Extra employee compensation.
                          "
                    />
                  </Col>
                  <Col md="8">
                    <FormInput
                      name={`payStubs.${index}.totalExtraBonusDescription`}
                      labelText="Description"
                      helperText="   Provide details about the entered commission amount."
                    />
                  </Col>
                </Row>
                <Row className="mt-6">
                  <Col md="4">
                    <FormInput
                      type="number"
                      name={`payStubs.${index}.totalDiscount`}
                      labelText="Discounts"
                      helperText="
                          Amount owed by the employee to the employer.
                          "
                    />
                  </Col>
                  <Col md="8">
                    <FormInput
                      name={`payStubs.${index}.totalDiscountDescription`}
                      labelText="Description"
                      helperText="
                         Provide details about the entered discount amount.
                         "
                    />
                  </Col>
                </Row>
                <Row className="mt-6">
                  <Col md="4">
                    <FormInput
                      type="number"
                      name={`payStubs.${index}.totalReimbursement`}
                      labelText="Reimbursements"
                      helperText=" Money owed to employee due to business expenses.
                        "
                    />
                  </Col>
                  <Col md="8">
                    <FormInput
                      name={`payStubs.${index}.totalReimbursementDescription`}
                      labelText="Description"
                      helperText="
                        Provide details about the entered reimbursement amount.
                        "
                    />
                  </Col>
                </Row>
              </Col>
              <Col
                md={2}
                className="d-flex justify-content-center align-items-center"
              >
                <div className="d-grid text-center justify-content-center align-items-center rounded-circle h-200px w-200px border border-3 border-success">
                  <div className="text-center w-100">
                    <h3 className="fw-bolder">
                      {(
                        parseFloat(item.normalHoursWorked || 0) *
                          parseFloat(item.hourlyRate) +
                        parseFloat(item.totalExtraBonus || 0) +
                        parseFloat(item.totalTips || 0) +
                        parseFloat(item.totalReimbursement || 0) -
                        parseFloat(item.totalDiscount || 0) +
                        parseFloat(item.totalPermits || 0) * 0.2 +
                        parseFloat(item.totalFees || 0) * 0.3 +
                        parseFloat(item.totalSaleBonus || 0)
                      ).toFixed(2)}
                    </h3>
                    <p className="fw-light">Total Salary</p>
                  </div>
                </div>
              </Col>
            </Row>
          </Card>
        ))
      }
    </FieldArray>
  );
}

export default FormCreatePayroll;
