import { Modal } from "react-bootstrap";
import {
  useRouteError,
  useNavigate,
  isRouteErrorResponse,
} from "react-router-dom";

export const ErrorBoundary = () => {
  const error = useRouteError();
  const navigate = useNavigate();
  const isRouteError = isRouteErrorResponse(error);

  function goBack() {
    if (isRouteError) {
      navigate(-1);
    }
    navigate("/");
  }

  return (
    <Modal show size="lg" onHide={() => navigate(-1)} centered>
      <Modal.Body className="text-center py-20">
        <h1 className="fw-bolder fs-2hx mb-4">
          {isRouteError ? "Page Not Found!" : "Sorry!"}
        </h1>

        <p className="fs-3 mb-7 my-5 text-gray-900">
          {isRouteError
            ? "The page you requested is currently unavailable."
            : "An error occured while you were working. Please try again"}
        </p>

        <button onClick={goBack} className="btn btn-primary px-8 py-4">
          Lets go back
        </button>
      </Modal.Body>
    </Modal>
  );
};
