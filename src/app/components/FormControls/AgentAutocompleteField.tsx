import { useGetCatalog } from "../Catalog/hooks";
import { FormAutoComplete } from "../forms/FormAutoComplete";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export function AgentAutocompleteField({
  labelText,
  helperText,
  helperTextId = "FORM.LABELS.HELPER.AGENT",
  labelTextId = "FORM.LABELS.COORDINATOR",
  fieldName = "seller",
}: Props) {
  const { employees = [] } = useGetCatalog(false);
  return (
    <FormAutoComplete
      name={fieldName}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      labelText={labelText}
      options={employees}
    />
  );
}
