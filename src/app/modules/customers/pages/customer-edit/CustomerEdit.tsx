import { Fragment, useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { useIntl } from "react-intl";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import {
  Error,
  FormToggle,
  WizardForm,
  WizardFormStep,
  BackdropLoader,
} from "src/app/components";
import { saleActions } from "src/app/modules/sales/redux/saleSlice";
import { CUSTOMER_TYPES } from "src/app/constants/General";
import { breadcrumbs } from "../../breadcrumbs";
import FormCustomerPersonalInfo from "../../forms/FormCustomerPersonalInfo";
import FormCustomerSetting from "../../forms/FormCustomerSetting";
import { BusinessContactInfo } from "../../forms/business/BusinessContactInfo";
import { CohortsRepeater } from "../../forms/cohorts/CohortsRepeater";
import { VehiclesRepeater } from "../../forms/vehicles/VehiclesRepeater";
import { customersAPI } from "../../redux";
import { initCustomer } from "./helpers";
import {
  businessSchema,
  cohortsSchema,
  customerSettingsSchema,
  personalInfoSchema,
  vehiclesSchema,
} from "./schemas";
import { useAppDispatch } from "src/app/hooks";
import { FormikValues } from "formik";
import { useSnackbar } from "notistack";

interface routeParams {
  code?: string;
  type?: "business" | "individual";
}

export const CustomerEdit = () => {
  const { code, type }: routeParams = useParams();

  const intl = useIntl();
  const navigate = useNavigate();
  const location = useLocation();

  const dispatch = useAppDispatch();
  const { enqueueSnackbar } = useSnackbar();

  const [createCustomer, { isLoading: isCreating }] =
    customersAPI.useCreateCustomerMutation();

  const [updateCustomer, { isLoading: isUpdating }] =
    customersAPI.useUpdateCustomerMutation();

  const {
    data: customer,
    isFetching,
    isError,
    refetch,
  } = customersAPI.useGetCustomerByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [toggle, setToggle] = useState({
    cohorts: false,
    vehicles: false,
  });

  function toggleSection(section: string, value: boolean) {
    setToggle((current) => {
      return { ...current, [section]: value };
    });
  }

  const isBusinessCustomer = type === "business";

  const defaultCustomer = initCustomer(
    type?.toUpperCase() || CUSTOMER_TYPES.INDIVIDUAL
  );

  useEffect(() => {
    if (customer) {
      customer?.cohorts?.length > 0 && toggleSection("cohorts", true);
      customer?.vehicles?.length > 0 && toggleSection("vehicles", true);
    }
  }, [customer, code]);

  async function handleCreateCustomer(values: FormikValues) {
    try {
      const response = await createCustomer(values).unwrap();
      // coming from policy or service creation
      if (location.state && location.state.from) {
        const payload = { customer: response.id };
        if (location.state.from.includes("policies")) {
          dispatch(
            saleActions.save({
              type: "savedPolicy",
              values: payload,
            })
          );
        } else if (location.state.from.includes("services")) {
          dispatch(
            saleActions.save({
              type: "savedService",
              values: payload,
            })
          );
        }
        navigate(location.state.from);
        return;
      }

      navigate(`/customers/${response?.code}/detail`);

      enqueueSnackbar("Customer created successfully", {
        variant: "success",
      });
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleUpdateCustomer(values: FormikValues) {
    try {
      await updateCustomer(values).unwrap();
      enqueueSnackbar("Customer updated successfully", {
        variant: "success",
      });
      navigate(-1);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isFetching || isCreating || isUpdating) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {code ? "Edit" : "New Insured"}
        {isBusinessCustomer ? "(Business)" : "(Individual)"}
      </PageTitle>

      <Card>
        <WizardForm
          initialValues={customer || defaultCustomer}
          onSubmit={code ? handleUpdateCustomer : handleCreateCustomer}
        >
          {isBusinessCustomer && (
            <WizardFormStep
              label={intl.formatMessage({
                id: "WIZARD.CUSTOMER.BUSINESS.INFO",
              })}
              validationSchema={businessSchema}
            >
              <BusinessContactInfo />
            </WizardFormStep>
          )}

          <WizardFormStep
            label={intl.formatMessage({
              id: "WIZARD.CUSTOMER.BUSINESS.MANAGER_INFO",
            })}
            validationSchema={personalInfoSchema}
          >
            <FormCustomerPersonalInfo />
            {/* Toggle section */}
            <div className="d-flex gap-4 align-items-center my-10">
              <FormToggle
                name="toggle"
                labelTextId="FORM.LABELS.ADD_VEHICLES"
                disabled={customer?.vehicles?.length > 0}
                checked={toggle.vehicles}
                onChange={(evt) => {
                  toggleSection("vehicles", evt.target.checked);
                }}
              />

              <FormToggle
                name="toggle"
                labelTextId="FORM.LABELS.ADD_RELATIONSHIPS"
                disabled={customer?.cohorts?.length > 0}
                checked={toggle.cohorts}
                onChange={(evt) => {
                  toggleSection("cohorts", evt.target.checked);
                }}
              />
            </div>
          </WizardFormStep>

          {toggle.vehicles && (
            <WizardFormStep
              label={intl.formatMessage({ id: "WIZARD.CUSTOMER.VEHICLES" })}
              validationSchema={vehiclesSchema}
            >
              <VehiclesRepeater />
            </WizardFormStep>
          )}

          {toggle.cohorts && (
            <WizardFormStep
              label={intl.formatMessage({ id: "WIZARD.CUSTOMER.COHORTS" })}
              validationSchema={cohortsSchema}
            >
              <CohortsRepeater />
            </WizardFormStep>
          )}

          <WizardFormStep
            label={intl.formatMessage({ id: "WIZARD.CUSTOMER.SETTINGS" })}
            validationSchema={customerSettingsSchema}
          >
            <FormCustomerSetting />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
