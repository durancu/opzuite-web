import { Fragment, useEffect } from "react";
import { Card } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { BackdropLoader, WizardForm, WizardFormStep } from "src/app/components";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actions from "../../redux/usersActions";
import FormUserBasicInfo from "./Forms/FormUserBasicInfo";
import FormUserEmployeeInfo from "./Forms/FormUserEmployeeInfo";
import FormUserSettings from "./Forms/FormUserSettings";
import { initUser } from "./helpers";
import {
  userAgentInfoValidationSchema,
  userGeneralValidationSchema,
  userSettingsValidationSchema,
} from "./schemas";

export const Edit = () => {
  const navigate = useNavigate();
  const { id }: any = useParams();

  const dispatch = useAppDispatch();
  const { actionsLoading, userForEdit, company } = useSelector(
    (state: any) => ({
      actionsLoading: state.users.actionsLoading,
      userForEdit: state.users.userForEdit,
      company: state.auth.user.company.id,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchUser(id));
    dispatch(catalogActions.getAllCatalog());
  }, [id, dispatch]);

  const saveUser = (values: any) => {
    if (!id) {
      dispatch(actions.createUser(values)).then(() => backToUsersList());
    } else {
      dispatch(actions.updateUser(values)).then(() => backToUsersList());
    }
  };

  const backToUsersList = () => {
    navigate(-1);
  };

  if (actionsLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {!id ? "New" : "Edit"} User
      </PageTitle>
      <Card>
        <WizardForm
          initialValues={id ? userForEdit : { ...initUser, company }}
          onSubmit={saveUser}
          onCancel={backToUsersList}
        >
          <WizardFormStep
            labelTextId="WIZARD.PERSONAL"
            validationSchema={userGeneralValidationSchema}
          >
            <FormUserBasicInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.EMPLOYMENT_INFO"
            validationSchema={userAgentInfoValidationSchema}
          >
            <FormUserEmployeeInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.PREFERENCES"
            validationSchema={userSettingsValidationSchema}
          >
            <FormUserSettings />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
