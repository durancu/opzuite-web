import axios from "axios";

export const DASHBOARDS_URL = "api/v2/dashboards/charts";

// CREATE =>  POST: add a new dashboard to the server
export function createDashboard(dashboard: any) {
  return axios.post(DASHBOARDS_URL, dashboard);
}

// READ
export function getAllDashboards() {
  return axios.get(DASHBOARDS_URL);
}

export function getDashboardById(dashboardCode: any) {
  return axios.get(`${DASHBOARDS_URL}/${dashboardCode}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findDashboards(queryParams: any) {
  return axios.post(`${DASHBOARDS_URL}/search`, { queryParams });
}

export function dashboardResults(queryParams: any, code: string) {
  return axios.post(`${DASHBOARDS_URL}/${code}`, queryParams);
}

export function getDashboardChartData(code: string, queryParams: any = {}) {
  return axios.post(`${DASHBOARDS_URL}/${code}`, queryParams);
}

// UPDATE => PUT: update the dashboard on the server
export function updateDashboard(dashboard: any) {
  return axios.put(`${DASHBOARDS_URL}/${dashboard.code}`, dashboard);
}

// UPDATE Status
export function updateStatusForDashboards(ids: any, status: any) {
  return axios.post(`${DASHBOARDS_URL}/updateStatusForDashboards`, {
    ids,
    status,
  });
}

// DELETE => delete the dashboard from the server
export function deleteDashboard(dashboardId: any) {
  return axios.delete(`${DASHBOARDS_URL}/${dashboardId}`);
}

// DELETE Dashboards by ids
export function deleteDashboards(codes: any) {
  return axios.post(`${DASHBOARDS_URL}/delete`, { codes });
}
