// routes module, to be imported in src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { MGAs } from "./pages/MGAs";
import Edit from "./pages/edit/Edit";
import Details from "./pages/Details";
import DeleteDialog from "../components/DeleteDialog";

export const MGARoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.MGAS_READ}>
        <MGAs />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.MGAS_CREATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/edit",
    element: (
      <RequirePermission permissions={Permissions.MGAS_UPDATE}>
        <Edit />
      </RequirePermission>
    ),
  },

  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.MGAS_READ}>
        <Details />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <DeleteDialog label="mga" path="/mgas" />,
  },
];
