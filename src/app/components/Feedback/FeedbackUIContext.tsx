/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Color } from "@material-ui/lab";
import { createContext, useContext, useState } from "react";

interface Props {
  children: any;
}
type Variant = "filled" | "standard" | "outlined";

export interface FeedbackContext {
  message: string;
  variant: Variant;
  type: Color;
  setMessage: any;
  setStyle: any;
}

const FeedbackUIContext = createContext<FeedbackContext>({
  message: "",
  variant: "outlined",
  type: "info",
  setMessage: (e: string) => e,
  setStyle: (): void => {},
});

export const useFeedbackUIContext = () => {
  return useContext(FeedbackUIContext);
};

export const FeedbackIUConsumer = FeedbackUIContext.Consumer;

export const FeedbackUIProvider = ({ children }: Props) => {
  const [message, setMessage] = useState("");
  const [variant, setVariant] = useState<Variant>("outlined");
  const [type, setType] = useState<Color>("success");

  const setStyle = (variant: Variant, type: Color) => {
    variant && setVariant(variant);
    type && setType(type);
  };

  const feedbackProps = {
    message,
    type,
    variant,
    setMessage,
    setStyle,
  };
  return (
    <FeedbackUIContext.Provider value={feedbackProps}>
      {children}
    </FeedbackUIContext.Provider>
  );
};
