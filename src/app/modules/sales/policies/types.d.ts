// generated with https://app.quicktype.io/

interface Policy {
  _id: string;
  autoRenew: boolean;
  cancelledAt: Date;
  checksumSanity: number;
  company: string;
  country: string;
  createdBy: string;
  customer: string;
  details: WelcomeDetails;
  effectiveAt: Date;
  expiresAt: Date;
  isChargeItemized: boolean;
  items: WelcomeItem[];
  location: string;
  number: string;
  products: Product[];
  renewalFrequency: string;
  renewalReferences: any[];
  seller: string;
  soldAt: Date;
  states: string;
  status: string;
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalCoveragesDownPayment: number;
  totalCoveragesPremium: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPermits: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;
  type: string;
  wasRenewed: boolean;
  deleted: boolean;
  code: string;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
  updatedBy: string;
  endorsements: Endorsement[];
}

interface Endorsement {
  _id: string;
  amount: number;
  company: string;
  createdBy: string;
  description: string;
  endorsedAt: Date;
  followUpDate: null;
  items: EndorsementItem[];
  sale: string;
  seller: string;
  status: string;
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonCommissionablePremium: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;
  type: string;
  deleted: boolean;
  code: string;
  createdAt: Date;
  updatedAt: Date;
  updatedBy: string;
  id: string;
}

interface EndorsementItem {
  accountingClass: string;
  amount: number;
  amountPaid: number;
  balance: number;
  code: string;
  commissionUnit: string;
  description: string;
  endorsedAt: Date;
  followUpDate?: null;
  otherDetails: OtherDetails;
  payments: any[];
  seller: string;
  type: string;
  _id: string;
  status?: string;
}

interface OtherDetails {
  remitTo?: string;
  description?: string;
}

interface WelcomeItem {
  agencyCommission: number;
  amount: number;
  broker: string;
  carrier: string;
  code: string;
  createdBy: string;
  deleted: boolean;
  description: string;
  details: ItemDetails;
  history: any[];
  includeInCommissions: boolean;
  isNonCommissionablePremium: boolean;
  lineOfBusiness: string;
  name: string;
  nonCommissionableReferenceId: string;
  premium: number;
  deductible: number;
  limit: number;
  product: string;
  seller: string;
  status: string;
  updatedBy: string;
  cohorts: Cohort[];
  contacts: any[];
  vehicles: Vehicle[];
  _id: string;
  updatedAt: Date;
  createdAt: Date;
  productItem: Product;
}

interface Cohort {
  address: Address;
  autoInsuranceCompany: string;
  code: string;
  dob: null;
  driverLicense: string;
  driverLicenseState: string;
  educationLevel: string;
  email: string;
  employer: string;
  employmentStatus: string;
  ethnicity: string;
  firstName: string;
  gender: string;
  hasAutoInsurance: boolean;
  hasHealthInsurance: boolean;
  hasHomeInsurance: boolean;
  hasLifeInsurance: boolean;
  healthInsuranceCompany: string;
  homeInsuranceCompany: string;
  identityCardId: string;
  identityCardType: string;
  immigrantStatus: string;
  language: string;
  lastName: string;
  lifeInsuranceCompany: string;
  maritalStatus: string;
  mobilePhone: null;
  notes: string;
  phone: null;
  relationship: string;
  ssn: string;
  timezone: string;
  vehicles: any[];
  website: string;
  yearlyGrossIncome: number;
  _id: string;
}

interface Address {
  address1: string;
  address2: string;
  city: string;
  country: string;
  county: string;
  state: string;
  zip: string;
  _id: string;
}

interface ItemDetails {
  additionalInsured: boolean;
  autoCoverageScope: string;
  bodilyInjuryPerAccidentLimit: number;
  bodilyInjuryPerPersonLimit: number;
  code: string;
  combinedSingleLimit: number;
  coverageFormUsed: string;
  damageToRentedPremisesLimit: number;
  deductibleOrRetentionAmount: number;
  eachOccurrenceLimit: number;
  employerLiabilityDiseasePolicyLimit: number;
  employerLiabilityEachAccidentLimit: number;
  employerLiabilityEachEmployeeLimit: number;
  excludedPeopleDescription: string;
  generalAggregateLimit: number;
  generalAggregateLimitAppliesPer: string;
  generalAggregateLimitAppliesPerOther: null;
  hasDeductible: boolean;
  hasExcludedPeople: boolean;
  hasRetention: boolean;
  includeHiredAutos: boolean;
  includeNonOwnedAutos: boolean;
  includeOtherCoveredAutoA: boolean;
  includeOtherCoveredAutoB: boolean;
  includeOwnedAutos: boolean;
  medicalExpensesLimit: number;
  other: boolean;
  other1LimitAmount: number;
  other1LimitName: string;
  other2LimitAmount: number;
  other2LimitName: string;
  otherCoveredAutoADescription: string;
  otherCoveredAutoBDescription: string;
  perStatute: boolean;
  policyBasedForm: string;
  productCompletedOperationsAggregateLimit: number;
  propertyDamagePerAccidentLimit: number;
  waiverOfSubrogation: boolean;
  includeOtherCoverageA: boolean;
  includeOtherCoverageB: boolean;
  otherCoverageADescription: string;
  otherCoverageBDescription: string;
  _id: string;
  updatedAt: Date;
  createdAt: Date;
  id: string;
}

interface Product {
  id: string;
  product: string;
  name: string;
  description: string;
  iconClass: string;
  iconLabel: string;
}

interface Vehicle {
  annualMileage: string;
  autoInsuranceCompany: string;
  code: string;
  color: string;
  condition: string;
  drivingDaysPerWeek: string;
  drivingMilesPerWeek: string;
  hasAutoInsurance: boolean;
  licensePlate: string;
  make: string;
  model: string;
  notes: string;
  ownershipStatus: string;
  primaryUse: string;
  trim: string;
  type: string;
  vinNumber: string;
  year: string;
  _id: string;
}
