import { upperCase } from "lodash";
import { Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import { FormattedMessage } from "react-intl";
import { NoRecordsFoundMessage } from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { useGetUserAuth } from "src/app/hooks";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

import { getMetricColorByTitle } from "src/app/utils";

interface Props {
  endorsements: any[];
}

export const EndorsementTable = ({ endorsements = [] }: Props) => {
  const user = useGetUserAuth();
  const entities = useGetCatalog();

  const endorsementMetricTypeHeaderFormatter = (column: any) => {
    const metricId: any = upperCase(column.dataField).replace(/ /g, "_");
    return (
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="quick-actions-tooltip">
            <FormattedMessage id={`TABLE.HEADER.${metricId}`} />
          </Tooltip>
        }
      >
        <i
          className="fs-1 rounded bi bi-square-fill"
          style={{
            color: getMetricColorByTitle(column.text),
          }}
        ></i>
      </OverlayTrigger>
    );
  };

  const columns = [
    {
      dataField: "endorsedAt",
      text: "Date",
      sort: true,
      headerFormatter: headerFormatters.DateColumnHeaderFormatter,
      headerClasses: "fw-bold",
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "type",
      text: "Type",
      sort: true,
      headerFormatter: headerFormatters.TypeColumnHeaderFormatter,
      headerClasses: "fw-bold",
      formatter: columnFormatters.EndorsementTypeColumnFormatter,
      formatExtraData: {
        entities,
      },
    },
    {
      dataField: "totalPremium",
      text: "Premium",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalNonPremium",
      text: "Non Premium",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalTaxesAndFees",
      text: "Taxes and Fees",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalPayables",
      text: "Payables",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalReceivables",
      text: "Receivables",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalFinanced",
      text: "Financed",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalAgentCommission",
      text: "Agent Commission",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
    },
    {
      dataField: "totalAgencyCommission",
      text: "Agency Commission",
      sort: true,
      style: {
        minWidth: "80px",
      },
      headerAlign: "center",
      align: "center",
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
      headerFormatter: endorsementMetricTypeHeaderFormatter,
      hidden: user?.role?.hierarchy > 50,
    },
  ];

  return (
    <Card className="mb-4 p-8 endorsements-table">
      <Card.Title as="h3" className="text-primary">
        <FormattedMessage id="GENERAL.CHANGES" />
      </Card.Title>
      <BootstrapTable
        wrapperClasses="table-responsive endorsements-table"
        classes="table table-head-custom table-vertical-center overflow-hidden"
        bootstrap4
        bordered={false}
        remote
        keyField="code"
        data={endorsements}
        columns={columns}
      />
      <NoRecordsFoundMessage entities={endorsements} />
    </Card>
  );
};
