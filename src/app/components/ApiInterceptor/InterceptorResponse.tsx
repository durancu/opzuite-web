import axios from "axios";
import { useDispatch } from "react-redux";
import * as actions from "./_redux/apiInterceptorActions";

export const InterceptorResponse = () => {
  const dispatch = useDispatch<any>();
  axios.interceptors.response.use(
    (res: any) => {
      dispatch(actions.addSuccessResponseInterceptor(res));
      return res;
    },
    (err: any) => {
      dispatch(actions.addErrorResponseInterceptor(err.response.data));
      return err.response.data;
    }
  );

  return null;
};
