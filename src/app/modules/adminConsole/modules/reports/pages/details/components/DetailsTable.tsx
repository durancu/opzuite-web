import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { useParams } from "react-router-dom";
import {
  getHandlerTableChange,
  NoRecordsFoundMessage,
  useFilterUIContext,
  FilterPanel,
  BackdropLoader,
  Error,
} from "src/app/components";
import * as columnFormatters from "./column-formatters";
import { FilterForm } from "./FilterForm";
import { initialValues, prepareFilter } from "../../../components/helpers";
import { reportAPI } from "../../../_redux/reportsSlice";

export const DetailsTable = () => {
  const { code }: any = useParams();
  const { queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = reportAPI.useGetReportResultsQuery(
    { queryParams, code },
    {
      refetchOnMountOrArgChange: true,
    }
  );

  const ModelNameHeaderFormatter = () => {
    return <>{data?.data?.appliedParams?.groupByModel}</>;
  };

  // Table columns
  const columns: any = [
    {
      dataField: "_id.label",
      headerClasses: "fw-bold text-capitalize",
      text: "Model",
      footer: "TOTAL",
      headerFormatter: ModelNameHeaderFormatter,
    },
    {
      dataField: "totalPremium",
      headerClasses: "fw-bold text-capitalize",
      text: "Premium",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
    },
    {
      dataField: "totalTaxesAndFees",
      headerClasses: "fw-bold text-capitalize",
      text: "Taxes And Fees",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      hidden: ["carrier", "broker"].includes(data?.appliedParams?.groupByModel),
    },
    {
      dataField: "totalPayables",
      headerClasses: "fw-bold text-capitalize",
      text: "Payables",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      hidden: ["carrier", "broker"].includes(data?.appliedParams?.groupByModel),
    },
    {
      dataField: "totalReceivables",
      headerClasses: "fw-bold text-capitalize",
      text: "Receivables",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
    },
    {
      dataField: "totalFinanced",
      headerClasses: "fw-bold text-capitalize",
      text: "Financed",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      hidden: ["carrier", "broker"].includes(data?.appliedParams?.groupByModel),
    },
    {
      dataField: "totalAgentCommission",
      headerClasses: "fw-bold text-capitalize",
      text: "Agent Comm.",
      headerAlign: "left",
      formatter: columnFormatters.AmountColumnFormatter,
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      footerFormatter: columnFormatters.AmountFooterTotalFormatter,
      hidden: ["carrier", "broker"].includes(data?.appliedParams?.groupByModel),
    },
    {
      dataField: "count",
      headerClasses: "fw-bold text-capitalize",
      text: "Policies",
      headerAlign: "left",
      footer: columnFormatters.AmountFooterTotal,
      align: "left",
      footerAlign: "left",
      hidden: !["carrier", "broker"].includes(
        data?.appliedParams?.groupByModel
      ),
    },
  ];

  if (!data?.metrics?.length) {
    return (
      <Fragment>
        <h6>No metrics for this report</h6>
        <p>Try to modify parameters and/or filters to get data.</p>
      </Fragment>
    );
  }

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <FilterPanel
        initialValues={initialValues}
        prepareFilter={prepareFilter}
        filterForm={<FilterForm />}
      />
      <BootstrapTable
        wrapperClasses="table-responsive"
        classes="table table-head-custom table-vertical-center overflow-hidden"
        bootstrap4
        bordered={false}
        remote
        keyField="_id.id"
        data={data.metrics || []}
        columns={columns}
        onTableChange={getHandlerTableChange(updateQueryParams)}
      />
      {!isError && <NoRecordsFoundMessage entities={data.metrics} />}
      <Error show={isError} action={refetch} />
    </Fragment>
  );
};
