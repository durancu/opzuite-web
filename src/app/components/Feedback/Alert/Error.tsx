import { Alert } from "@material-ui/lab";
import { Col, Row } from "react-bootstrap";
import { useFeedbackUIContext } from "../FeedbackUIContext";
import { GenerateMessageList } from "../GenerateMessageList";

export const Error = () => {
  const { message, type, variant } = useFeedbackUIContext();

  return (
    <Row hidden={!message || Object.keys(message).length === 0}>
      <Col>
        <Alert className="mb-5 error-alert" severity={type} variant={variant}>
          {/* {message} */}
          <GenerateMessageList messageObject={message} />
        </Alert>
      </Col>
    </Row>
  );
};
