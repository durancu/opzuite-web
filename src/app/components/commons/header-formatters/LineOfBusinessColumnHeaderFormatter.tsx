/* eslint-disable no-script-url */
import { FormattedMessage } from "react-intl";

export const LineOfBusinessColumnHeaderFormatter = (
  column: any,
  colIndex: any,
  { sortElement }: any
) => (
  <div className="d-flex gap-2 align-items-center">
    <FormattedMessage id="TABLE.HEADER.LINE_OF_BUSINESS" />
    {sortElement}
  </div>
);
