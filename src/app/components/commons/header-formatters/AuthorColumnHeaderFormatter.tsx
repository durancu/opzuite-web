/* eslint-disable no-script-url */
import { FormattedMessage } from "react-intl";

export const AuthorColumnHeaderFormatter = (
  column: any,
  colIndex: any,
  { sortElement }: any
) => (
  <div className="d-flex gap-2 align-items-center">
    <FormattedMessage id="TABLE.HEADER.AUTHOR" />
    {sortElement}
  </div>
);
