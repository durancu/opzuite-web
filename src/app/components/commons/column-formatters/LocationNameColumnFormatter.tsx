import { Fragment } from "react";
import { Link } from "react-router-dom";

export const LocationNameColumnFormatter = (location: any, row: any) => (
  <Fragment>

    <Link className="text-dark fw-semibold text-hover-primary fs-6"
      to={`/manager/locations/${row?.locationCode}/detail`}
    >
      {row?.locationName}
    </Link>
    <span className="text-muted fw-semibold text-muted d-block fs-7">
      {row.country}
    </span>
  </Fragment>
);
