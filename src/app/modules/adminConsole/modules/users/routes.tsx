// routes module, to be imported in admin-console/routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { Agents } from "./pages/Agents";
import { ChangePassword } from "./pages/ChangePassword";
import { Delete } from "./pages/Delete";
import { Details } from "./pages/Details";
import { Edit } from "./pages/edit/Edit";

export const agentRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.USERS_READ}>
        <Agents />
      </RequirePermission>
    ),
  },
  {
    path: "new",
    element: (
      <RequirePermission permissions={Permissions.USERS_CREATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":id/edit",
    element: (
      <RequirePermission permissions={Permissions.USERS_UPDATE}>
        <Edit />
      </RequirePermission>
    ),
  },
  {
    path: ":id/detail",
    element: (
      <RequirePermission permissions={Permissions.USERS_READ}>
        <Details />
      </RequirePermission>
    ),
  },
  {
    path: ":id/change-password",
    element: (
      <RequirePermission permissions={Permissions.USERS_READ}>
        <ChangePassword />
      </RequirePermission>
    ),
  },
  {
    path: ":id/delete",
    element: <Delete />,
  },
];
