import { useEffect } from "react";
import moment from "moment";
import { Field, useFormikContext } from "formik";
import { DatePickerCustom } from "src/app/partials/controls";
import { DEFAULT_DATEPICKER_FORMAT } from "src/app/partials/controls/forms/DateRange/dateFactory";
import { Form } from "react-bootstrap";

const FormPolicySetting = () => {
  const { values, setFieldValue } = useFormikContext<any>();

  useEffect(() => {
    switch (values?.renewalFrequency) {
      case "ANNUAL":
        setFieldValue("expiresAt", moment(values.effectiveAt).add(1, "year"));
        break;
      case "SEMI-ANNUAL":
        setFieldValue(
          "expiresAt",
          moment(values.effectiveAt).add(2, "quarters")
        );
        break;
      case "QUARTERLY":
        setFieldValue(
          "expiresAt",
          moment(values.effectiveAt).add(1, "quarter")
        );
        break;
      case "MONTHLY":
        setFieldValue("expiresAt", moment(values.effectiveAt).add(1, "month"));
        break;
      default:
        break;
    }
  }, [setFieldValue, values?.effectiveAt, values?.renewalFrequency]);

  useEffect(() => {
    if (moment(values?.expiresAt).diff(values?.effectiveAt, "year", true) === 1)
      setFieldValue("renewalFrequency", "ANNUAL");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "quarters", true) ===
      2
    )
      setFieldValue("renewalFrequency", "SEMI-ANNUAL");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "quarter", true) === 1
    )
      setFieldValue("renewalFrequency", "QUARTERLY");
    else if (
      moment(values?.expiresAt).diff(values?.effectiveAt, "month", true) === 1
    )
      setFieldValue("renewalFrequency", "MONTHLY");
    else setFieldValue("renewalFrequency", "VARIABLE");
  }, [setFieldValue, values.expiresAt, values.effectiveAt]);

  return (
    <div className="row">
      <Form.Group className="col-md-3">
        <Form.Label htmlFor="renewalFrequency">Renewal Frequency</Form.Label>
        <Field as={Form.Select} name="renewalFrequency" id="renewalFrequency">
          <option key="annual" value="ANNUAL">
            Annual
          </option>
          <option key="semi-annual" value="SEMI-ANNUAL">
            Semi-Annual
          </option>
          <option key="quarterly" value="QUARTERLY">
            Quarterly
          </option>
          <option key="monthly" value="MONTHLY">
            Monthly
          </option>
          <option key="variable" value="VARIABLE">
            Variable
          </option>
        </Field>
      </Form.Group>
      <div className="col-md-3">
        <DatePickerCustom
          label="Policy Effective At"
          name="effectiveAt"
          format={DEFAULT_DATEPICKER_FORMAT}
        />
      </div>
      <div className="col-md-3">
        <DatePickerCustom
          label="Policy Expires At"
          name="expiresAt"
          format={DEFAULT_DATEPICKER_FORMAT}
        />
      </div>
      <div className="col-md-3">
        <Field
          as={Form.Check}
          type="checkbox"
          name="autoRenew"
          label="Auto Renew"
        />
      </div>
    </div>
  );
};

export default FormPolicySetting;
