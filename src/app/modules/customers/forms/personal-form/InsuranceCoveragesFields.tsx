import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";

import * as CATALOG from "src/app/components/Catalog/CatalogSelects";
import { FormSelect } from "src/app/components";

export const InsuranceCoveragesFields = () => {
  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormSelect
            name="contact.healthInsuranceCompany"
            labelTextId="FORM.LABELS.HEALTH_INSURANCE_COMPANY"
            options={CATALOG.HEALTH_INSURANCE_COMPANIES_CATALOG}
          />
        </Col>

        <Col>
          <FormSelect
            name="contact.autoInsuranceCompany"
            labelTextId="FORM.LABELS.AUTO_INSURANCE_COMPANY"
            options={CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG}
          />
        </Col>
        <Col>
          <FormSelect
            name="contact.lifeInsuranceCompany"
            labelTextId="FORM.LABELS.LIFE_INSURANCE_COMPANY"
            options={CATALOG.LIFE_INSURANCE_COMPANIES_CATALOG}
          />
        </Col>

        <Col>
          <FormSelect
            name="contact.homeInsuranceCompany"
            labelTextId="FORM.LABELS.HOME_INSURANCE_COMPANY"
            options={CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG}
          />
        </Col>
      </Row>
    </Fragment>
  );
};
