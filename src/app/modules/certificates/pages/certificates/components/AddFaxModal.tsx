import { Fragment } from "react";
import { Modal, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FieldArray, useFormikContext } from "formik";
import { FormattedMessage } from "react-intl";
import { PhoneNumberInput } from "src/app/components";

interface Props {
  show: boolean;
  onHide: () => void;
}
export const AddFaxModal = ({ show, onHide }: Props) => {
  const { values, errors, setFieldValue }: any = useFormikContext();

  return (
    <Modal size="xl" show={show} onHide={onHide} centered backdrop="static">
      <Modal.Header className="bg-light">
        <Modal.Title>Manage Fax Numbers</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FieldArray
          name="ccFaxNumbers"
          render={(fieldHelpers) => (
            <Fragment>
              {values.ccFaxNumbers.length ? (
                values.ccFaxNumbers.map((holder: any, index: any) => (
                  <div
                    key={index}
                    className="d-flex gap-3 mb-4 align-items-start justify-content-center"
                  >
                    <div className="flex-grow-1">
                      <PhoneNumberInput
                        labelText={`Fax #${index + 1}`}
                        name={`ccFaxNumbers.${index}`}
                      />
                    </div>
                    <div className="mt-8">
                      <OverlayTrigger
                        placement="top"
                        overlay={
                          <Tooltip className="text-capitalize">Delete</Tooltip>
                        }
                      >
                        <Button
                          variant="danger"
                          onClick={() => fieldHelpers.remove(index)}
                        >
                          <i className="fs-4 bi bi-trash3"></i>
                        </Button>
                      </OverlayTrigger>
                    </div>
                  </div>
                ))
              ) : (
                <h2 className="text-center py-5">No available Fax Numbers</h2>
              )}
            </Fragment>
          )}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="light"
          onClick={onHide}
          disabled={errors.ccFaxNumbers?.length ? true : false}
        >
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>
        <Button
          className="btn-light-primary"
          onClick={() => {
            setFieldValue("ccFaxNumbers", ["", ...values.ccFaxNumbers]);
          }}
        >
          Add Fax
        </Button>
        <Button
          variant="primary"
          onClick={onHide}
          disabled={errors.ccFaxNumbers?.length ? true : false}
        >
          <FormattedMessage id="BUTTON.SAVE" />
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
