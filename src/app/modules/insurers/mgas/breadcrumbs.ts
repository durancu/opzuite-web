import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All MGAs",
    path: "mgas",
    isSeparator: false,
    isActive: true,
  },
];
