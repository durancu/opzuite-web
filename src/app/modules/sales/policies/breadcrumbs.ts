import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "All Policies",
    path: "policies",
    isSeparator: false,
    isActive: true,
  },
];
