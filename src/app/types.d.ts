interface QueryParams {
  filter: any;
  sortOrder: string;
  sortField: string;
  pageNumber: number;
  pageSize: number;
}
