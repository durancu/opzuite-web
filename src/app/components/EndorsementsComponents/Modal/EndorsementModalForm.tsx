import { ModalFormComponent } from "./ModalFormComponent";

type Props = { children: any };

export const EndorsementModalForm = ({ children }: Props) => {
  return <ModalFormComponent>{children}</ModalFormComponent>;
};
