import { Fragment } from "react";
import { Link } from "react-router-dom";
import { formatPhoneNumber } from "src/app/components/functions";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { breadcrumbs } from "./breadcrumbs";
import { useGetProfileQuery } from "./redux";
import { BackdropLoader } from "src/app/components";
import { Alert, Button, Badge } from "react-bootstrap";
import clsx from "clsx";

export const Overview = () => {
  const { data: user = {}, isLoading, isError, refetch } = useGetProfileQuery();

  if (isLoading) return <BackdropLoader />;
  if (isError) {
    return (
      <Alert show={isError} variant="danger">
        <Alert.Heading as="h2">Something went wrong</Alert.Heading>
        <p className="my-4 fs-6">
          We {`couldn't`} fetch your profile. Please try again
        </p>
        <Button onClick={() => refetch()} variant="danger">
          Try again
        </Button>
      </Alert>
    );
  }
  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Overview</PageTitle>
      <div className="card mb-5 mb-xl-10">
        {/* details */}
        <div className="card-header cursor-pointer">
          <h3 className="card-title fw-bolder m-0">Profile Details</h3>
          <Toolbar>
            <Link
              to="/profile/settings"
              className="btn btn-primary align-self-center"
            >
              Edit Profile
            </Link>
          </Toolbar>
        </div>

        <div className="card-body p-9">
          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">Full Name</span>

            <div className="col-lg-8">
              <span className="fw-bolder fs-6 text-dark">
                {user?.firstName} {user?.lastName}
              </span>
            </div>
          </div>
          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">DOB</span>

            <div className="col-lg-8 fv-row">
              <span className="fw-bold fs-6">
                {user?.dob ? new Date(user.dob).toLocaleDateString() : "N/A"}
              </span>
            </div>
          </div>

          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">Company</span>

            <div className="col-lg-8 fv-row">
              <span className="fw-bold fs-6">{user?.company?.name}</span>
            </div>
          </div>

          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">
              Contact Phone
              <i
                className="fas fa-exclamation-circle ms-1 fs-7"
                data-bs-toggle="tooltip"
                title="Phone number must be active"
              ></i>
            </span>

            <div className="col-lg-8 d-flex align-items-center">
              <span className="fw-bolder fs-6 me-2">
                {user?.phone && formatPhoneNumber(user?.phone)}
              </span>

              <span className="badge badge-success">Verified</span>
            </div>
          </div>

          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">
              Country
              <i
                className="fas fa-exclamation-circle ms-1 fs-7"
                data-bs-toggle="tooltip"
                title="Country of origination"
              ></i>
            </span>

            <div className="col-lg-8">
              <span className="fw-bolder fs-6 text-dark">
                {user?.country || "N/A"}
              </span>
            </div>
          </div>

          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">
              Timezone
              <i
                className="fas fa-exclamation-circle ms-1 fs-7"
                data-bs-toggle="tooltip"
                title="timezone"
              ></i>
            </span>

            <div className="col-lg-8">
              <span className="fw-bolder fs-6 text-dark">
                {user?.timezone || "N/A"}
              </span>
            </div>
          </div>

          <div className="row mb-7">
            <span className="col-lg-4 fw-bold text-muted">Communication</span>

            <div className="col-lg-8">
              <span className="fw-bolder fs-6 text-dark">
                {Object.keys(user?.communication).map((key, index) => (
                  <span
                    key={index}
                    className={clsx(
                      "text-capitalize me-2",
                      key === "_id" ? "d-none" : "d-inline-block"
                    )}
                  >
                    {key},
                  </span>
                ))}
              </span>
            </div>
          </div>
        </div>
      </div>
      {/* Address */}
      <div className="card mb-5 mb-xl-10">
        <div className="card-header cursor-pointer">
          <h3 className="card-title fw-bolder m-0">Address</h3>
        </div>
        <div className="card-body p-9">
          {Object.keys(user?.address).map((key, index) => (
            <div key={index} className="row mb-10" hidden={key === "_id"}>
              <span className="col-lg-4 fw-bold text-muted text-capitalize">
                {key}
              </span>
              <div className="col-lg-8">
                <span className="fw-bold fs-6">
                  {user?.address[key] || "N/A"}
                </span>
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* Employee Info */}
      <div className="card mb-5 mb-xl-10">
        <div className="card-header cursor-pointer">
          <h3 className="card-title fw-bolder m-0">Employee Info</h3>
        </div>
        <div className="card-body p-9">
          {Object.keys(user?.employeeInfo).map((key, index) => (
            <div key={index} className="row mb-10" hidden={key === "_id"}>
              <span className="col-lg-4 fw-bold text-muted text-capitalize">
                {key}
              </span>
              <div className="col-lg-8">
                <span className="fw-bold fs-6">
                  {user?.employeeInfo[key]?.toString() || "N/A"}
                </span>
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* Permissions */}
      <div className="card mb-5 mb-xl-10">
        <div className="card-header cursor-pointer">
          <h3 className="card-title fw-bolder m-0">Permissions</h3>
        </div>
        <div className="card-body p-9 d-flex gap-4">
          {user?.permissions.map((value: string, index: number) => (
            <Badge
              key={index}
              pill
              bg="secondary"
              className="text-capitalize py-2 px-3 fs-6"
            >
              {value}
            </Badge>
          ))}
        </div>
      </div>
    </Fragment>
  );
};
