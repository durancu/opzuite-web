import { useMemo } from "react";
import { Skeleton } from "@mui/material";
import { Card, Button } from "react-bootstrap";
import { calculateChartTotalMetrics } from "src/app/components/functions";
import { getChartConfig } from "./helpers";
import { dashboardAPI } from "src/app/modules/dashboard/_redux/dashboards/dashboardsSlice";
import Chart from "react-apexcharts";
import clsx from "clsx";

interface Props {
  code: string;
  filter: Record<string, any>;
}

export const ChartWidget = ({ code, filter = {} }: Props) => {
  const {
    data: chart = {},
    isLoading,
    isError,
    refetch,
  } = dashboardAPI.useGetChartDataQuery(
    {
      code,
      queryParams: filter,
    },
    {
      skip: !code || !Object.keys(filter).length,
      refetchOnFocus: true,
      refetchOnReconnect: true,
      refetchOnMountOrArgChange: true,
    }
  );
  const symbolShape = chart?.options?.symbolShape ?? "circle";

  const config = useMemo(() => {
    let config = { icon: "bi bi-grid-1x2", colorClass: "success", options: {} };
    if (chart.name && chart.dataset) {
      config = getChartConfig(chart.name, chart.dataset);
    }
    return config;
  }, [chart.dataset, chart.name]);

  if (isLoading) {
    return (
      <Card className="mb-8 card-custom">
        <Card.Header className="px-4 py-0 d-flex align-items-center justify-content-between flex-grow-1">
          <Skeleton variant="rectangular" height={40} width={80} />
          <Skeleton height={80} width={300} />
        </Card.Header>
        <Card.Body className="p-4">
          <Skeleton variant="rectangular" height={500} />
        </Card.Body>
      </Card>
    );
  }

  if (!chart.dataset && !isError) {
    return (
      <Card className="mb-8">
        <Card.Body className="p-4 h-500px d-flex flex-column gap-8 align-items-center justify-content-center">
          <i
            className="bi bi-bar-chart-line text-secondary"
            style={{ fontSize: "8.5rem" }}
          ></i>
          <h2 className="text-gray-600">No available data for this period</h2>
        </Card.Body>
      </Card>
    );
  }

  if (isError) {
    return (
      <Card className="mb-8">
        <Card.Body className="p-4 h-500px d-flex flex-column gap-8 align-items-center justify-content-center">
          <i
            className="bi bi-bar-chart-line text-danger"
            style={{ fontSize: "8.5rem" }}
          ></i>
          <h2 className="text-gray-600">Failed to load chart statistics</h2>
          <Button onClick={() => refetch()} variant="danger">
            Try again
          </Button>
        </Card.Body>
      </Card>
    );
  }

  return (
    <Card
      className={`mb-8 card-custom ${
        chart?.options?.cssClasses || "card-stretch card-stretch-half gutter-b"
      }`}
    >
      <Card.Header className="p-4">
        <div className="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
          <i
            className={clsx(
              `border p-4 text-${config.colorClass} border border-${config.colorClass}`,
              symbolShape === "cirlce" ? "rounded-circle" : "rounded",
              config.icon
            )}
            style={{ fontSize: "2.5rem" }}
          ></i>

          <div className="text-end">
            <h2 className="text-dark-75 fw-bolder mb-2">
              {chart?.dataset && (
                <span>
                  ${calculateChartTotalMetrics(chart.dataset?.series?.data)}
                </span>
              )}
            </h2>
            <p className="text-gray-700">
              {chart.dataset && chart.dataset?.series?.name}
            </p>
          </div>
        </div>
      </Card.Header>
      <Card.Body className="p-4">
        <Chart
          type="bar"
          series={chart.dataset ? [chart.dataset?.series] : []}
          options={config.options}
          height={450}
        />
      </Card.Body>
    </Card>
  );
};
