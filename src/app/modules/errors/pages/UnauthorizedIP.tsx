import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import { shallowEqual, useSelector } from "react-redux";

export const UnauthorizedIP = () => {
  const { lastError, actionsLoading } = useSelector(
    (state: any) => ({
      lastError: state.apiInterceptor.lastError,
      actionsLoading: state.carriers.actionsLoading,
    }),
    shallowEqual
  );

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(actionsLoading);
  }, [actionsLoading, setIsLoading]);

  return (
    <>
      {/* begin::Title */}
      <h1 className="fw-bolder fs-2hx text-gray-900 mb-4 text-danger">
        IP Address Rejected
      </h1>
      {/* end::Title */}

      {/* begin::Text */}
      <div className="fw-semibold fs-6 text-gray-500 my-10">
        <p className="fw-bold mb-12">
          Your IP address it&apos;s not authorized to access this content.
          <br />
          Let your supervisor know the location IP address needs to be updated
          to: {lastError?.errorCode} <br /> in order to continue enjoing InZuite
          app.
        </p>
      </div>
      {/* end::Text */}

      {/* begin::Link */}
      <div className="mb-0">
        <Link to="/" className="btn btn-sm btn-primary p-4">
          Return Home
        </Link>
      </div>
      {/* end::Link */}
    </>
  );
};
