import { PageLink } from "src/_metronic/layout/core";

export const breadcrumbs: Array<PageLink> = [
  {
    title: "Certificates",
    path: "certificates",
    isSeparator: false,
    isActive: false,
  },
];
