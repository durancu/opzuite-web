import { shallowEqual, useSelector } from "react-redux";
import { getHierarchyRoleByUserAuth } from "../../../../../../components/functions";

export const useInitializerFilterData = () => {
  const { user, roles } = useSelector(
    (state: any) => ({
      user: state.auth?.user,
      roles: state.catalogs?.entities?.roles,
    }),
    shallowEqual
  );

  const hierarchy = getHierarchyRoleByUserAuth(
    user.employeeInfo.category,
    roles
  );

  const setFilterByUserRole = (hierarchy: number) => {
    switch (hierarchy) {
      case 4:
        return [
          {
            field: "country",
            operator: "=",
            value: user.country,
          },
        ];
      case 5:
        return [
          {
            field: "country",
            operator: "=",
            value: user.country,
          },
          {
            field: "location",
            operator: "=",
            value: user.location,
          },
        ];
      default:
        return [];
    }
  };

  return {
    config: {},
    permissions: ["sales.read"],
    name: "",
    description: "",
    params: {
      model: "sale",
      filters: setFilterByUserRole(hierarchy),
      groupByModel: "",
      dateFrom: null,
      dateTo: null,
      fields: [],
      withCount: false,
      withRecords: false,
    },
    scope: "private",
    sortOrder: "desc",
    sortField: "totalPremium",
    pageNumber: 1,
    pageSize: 2000,
  };
};
