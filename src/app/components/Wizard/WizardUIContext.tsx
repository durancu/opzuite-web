/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  createContext,
  MutableRefObject,
  ReactElement,
  useContext,
  useRef,
  useState,
} from "react";

interface WizardUIConfig {
  steps: string[];
}

interface Props {
  wizardUIConfig: WizardUIConfig;
  children: ReactElement;
}

const WizardUIContext = createContext({
  steps: [""],
  activeStep: 0,
  isBlockStep: false,
  isLoading: false,
  btnBackRef: () => null,
  btnNextRef: () => null,
  btnSaveRef: () => null,
  setActiveStep: (e: any) => e,
  handleReset: () => null,
  handleNext: () => null,
  handleBack: () => null,
  setIsBlockStep: (e: boolean) => e,
  setIsLoading: (e: boolean) => e,
  handleClickButtonBack: () => null,
  handleClickButtonNext: () => null,
  handleClickButtonSave: () => null,
});

export const useWizardUIContext = () => {
  return useContext(WizardUIContext);
};

export const WizardIUConsumer = WizardUIContext.Consumer;

export const WizardUIProvider = ({ wizardUIConfig, children }: Props) => {
  const [activeStep, setActiveStep] = useState(0);
  const [isBlockStep, setIsBlockStep] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const btnBackRef: MutableRefObject<any> = useRef();
  const btnSaveRef: MutableRefObject<any> = useRef();
  const btnNextRef: MutableRefObject<any> = useRef();

  function handleReset() {
    setActiveStep(0);
  }

  function handleClickButtonBack() {
    btnBackRef.current.click();
  }
  function handleClickButtonNext() {
    btnNextRef.current.click();
  }

  function handleClickButtonSave() {
    btnSaveRef.current.click();
  }

  function handleNext() {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }

  function handleBack() {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  }

  const wizardProps: any = {
    steps: wizardUIConfig.steps,
    activeStep,
    isBlockStep,
    isLoading,
    btnBackRef,
    btnNextRef,
    btnSaveRef,
    setActiveStep,
    setIsLoading,
    handleReset,
    handleNext,
    handleBack,
    setIsBlockStep,
    handleClickButtonBack,
    handleClickButtonNext,
    handleClickButtonSave,
  };

  return (
    <WizardUIContext.Provider value={wizardProps}>
      {children}
    </WizardUIContext.Provider>
  );
};
