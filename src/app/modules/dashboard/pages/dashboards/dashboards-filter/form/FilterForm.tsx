import { Button } from "@mui/material";
import { Form, useFormikContext } from "formik";
import { useEffect, useState } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useAppDispatch } from "src/app/hooks";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";

export const FilterForm = () => {
  const { handleSubmit } = useFormikContext<any>();

  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(catalogActions.getAllCatalog());
  }, [dispatch]);

  const { currentState } = useSelector(
    (state: any) => ({
      currentState: state.catalogs?.entities,
    }),
    shallowEqual
  );

  const defaultOptions = useState([
    { id: "", business: { name: "Choose any" } },
  ]);

  const [carrierOptions, setCarrierOptions]: any = useState([
    ...defaultOptions,
  ]);
  const [brokerOptions, setBrokerOptions]: any = useState([...defaultOptions]);

  useEffect(() => {
    if (currentState) {
      setCarrierOptions([...defaultOptions, ...currentState.carriers]);

      setBrokerOptions([...defaultOptions, ...currentState.brokers]);
    }
  }, [defaultOptions, currentState, carrierOptions, brokerOptions]);

  return (
    <Form>
      <div className="form-group row">
        <div className="col-12">
          <Button
            type="submit"
            color="primary"
            variant="contained"
            onClick={() => handleSubmit()}
          >
            Apply
          </Button>
        </div>
      </div>
    </Form>
  );
};
