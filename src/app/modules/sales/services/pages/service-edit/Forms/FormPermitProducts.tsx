import { Field, useFormikContext } from "formik";
import { useEffect, useMemo, useState, Fragment } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import { PermitMetricsAccordion } from "src/app/components/Widgets";
import RepeaterPermitEndorsements from "./endorsements/RepeaterPermitEndorsements";
import {
  generateEndorsementOptions,
  permitTypesByStateList,
} from "./permitsFunctionsAuxiliary";

import { FormMultiselect, WizardFormSection } from "src/app/components";

function FormPermitProducts() {
  const { values, setFieldValue, handleChange } = useFormikContext<any>();

  const entities = useGetCatalog();

  const ALL = useMemo(
    () => permitTypesByStateList("ALL", entities, values.seller),
    [entities, values.seller]
  );
  const TX = useMemo(
    () => permitTypesByStateList("TX", entities, values.seller),
    [entities, values.seller]
  );

  const [endorsementOptions, setEndorsementOptions] = useState([]);

  useEffect(() => {
    if (entities?.permitTypes && values?.seller) {
      const options: any = generateEndorsementOptions(
        entities.permitTypes,
        values.seller
      );
      setEndorsementOptions(options);
    }
  }, [entities.permitTypes, values?.seller, setEndorsementOptions]);

  const handleChangeStatus = (e: any) => {
    handleChange(e);

    switch (e.target.value) {
      case "ALL":
        setFieldValue("endorsements", ALL);
        break;
      case "TX":
        setFieldValue("endorsements", TX);
        break;
      default:
        break;
    }
  };

  const executeSetFieldValue = (value: any) => {
    setFieldValue("endorsements", value);
    setFieldValue("states", "NONE");
  };

  return (
    <Fragment>
      <WizardFormSection labelText="Presets">
        <Row className="mb-4">
          <Col md={3}>
            <Form.Group>
              <Form.Label>Set of Services</Form.Label>
              <Field
                as={Form.Select}
                onChange={handleChangeStatus}
                name="states"
                required
              >
                <option key="none" value="NONE">
                  Prefer to select services by myself
                </option>
                <option key="tx" value="TX">
                  For a company that will only operate in Texas
                </option>
                <option key="all" value="ALL">
                  For a company that will only operate throughout the USA
                </option>
              </Field>
              <Form.Text className="mt-1">
                What kind of process are you working on?
              </Form.Text>
            </Form.Group>
          </Col>
          <Col md={9}>
            <FormMultiselect
              labelText="Service presets"
              options={endorsementOptions}
              value={values.endorsements}
              onChange={(_, value) => {
                executeSetFieldValue(value);
              }}
            />
          </Col>
        </Row>
      </WizardFormSection>

      <WizardFormSection className="mb-6" labelText="Services">
        <RepeaterPermitEndorsements />
      </WizardFormSection>

      <div className="mb-4 bg-white">
        <PermitMetricsAccordion sale={values} />
      </div>
    </Fragment>
  );
}

export default FormPermitProducts;
