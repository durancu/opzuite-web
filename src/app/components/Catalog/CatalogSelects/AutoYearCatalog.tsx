import moment from "moment";

export const AUTO_YEAR_CATALOG = (): any[] => {
  const lastYear = 1956;
  const thisYear = parseInt(moment().format("YYYY"));
  const listOfYear = [];

  for (let i = 0; i <= thisYear - lastYear; i++) {
    listOfYear.push({ id: `${thisYear - i}`, name: `${thisYear - i}` });
  }

  return listOfYear;
};
