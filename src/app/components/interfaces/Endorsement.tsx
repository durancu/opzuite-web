import { EndorsementItem } from "./EndorsementItem";

export interface Endorsement {
  id: string;
  amount?: number;
  code: string;
  company: string;
  description?: string;
  endorsedAt?: Date;
  followUpDate?: Date;
  followUpPerson?: string;
  seller?: string;
  name?: string;
  sale: string;
  status?: string;
  type?: string;
  createdBy?: string;
  updatedBy?: string;
  items?: EndorsementItem[];
  accountingClass: string;

  //Metrics
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;
}
