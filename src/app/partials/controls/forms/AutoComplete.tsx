import { TextField } from "@mui/material";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useFormikContext } from "formik";
import objectPath from "object-path";
import { ReactElement, useEffect, useState } from "react";

interface Props {
  name: string;
  label: string;
  labelField: string;
  options: any[];
  variant?: string;
  disabled?: boolean;
  required?: boolean;
  startAdornment?: any;
  endAdornment?: any;
  hidden?: boolean;
  defaultValue?: any;
  onChange?: any;
  othersProps?: any;
}

export function AutoComplete(props: Props): ReactElement {
  const [localValue, setLocalValue] = useState({
    id: "",
    [props.labelField]: "",
  });
  const { handleChange, values, errors } = useFormikContext<any>();

  useEffect(() => {
    const item = objectPath.get(values, props.name);
    const valueOption = props.options?.find(
      (element: any) => element.id === item
    );
    setLocalValue(valueOption);
  }, [props.name, props.options, values]);

  const handleChangeComponent = (value: any) => {
    const newE = { target: { value: value?.id || "", name: props.name || "" } };
    handleChange(newE);
    setLocalValue(value);
  };

  return (
    <>
      <Autocomplete
        disabled={props.disabled}
        hidden={props.hidden}
        options={props.options || []}
        getOptionLabel={(option: any) =>
          objectPath.get(option, props.labelField)
        }
        getOptionSelected={(item, current) => (item === current ? true : true)}
        onChange={(_, value) => {
          handleChangeComponent(value);
        }}
        value={localValue || { id: "", [props.labelField]: "" }}
        defaultValue={props.defaultValue}
        renderInput={(params) => (
          <TextField
            {...params}
            label={props.label}
            required={props.required}
            error={objectPath.get(errors, props.name) ? true : false}
            helperText={objectPath.get(errors, props.name)}
          />
        )}
      />
    </>
  );
}
