/* eslint-disable no-script-url */

export const PermitNumberColumnFormatter = (cellContent: any, row: any) =>
  row.permitNumber || "Pending";
