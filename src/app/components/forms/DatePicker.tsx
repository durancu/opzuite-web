import {
  DesktopDatePicker,
  DesktopDatePickerProps,
  LocalizationProvider,
} from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { Form } from "react-bootstrap";
import { useIntl } from "react-intl";
import clsx from "clsx";

type Props = Omit<DesktopDatePickerProps<any, any>, "renderInput"> & {
  name: string;
  labelText?: string;
  labelTextId?: string;
  helperTextId?: string;
  helperText?: string;
  required?: boolean;
  isInvalid?: boolean;
};

export const DatePicker = ({
  name,
  label,
  required,
  labelText = "",
  labelTextId,
  helperText = "",
  helperTextId,
  isInvalid,
  ...rest
}: Props) => {
  const intl = useIntl();

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopDatePicker
        {...rest}
        label={label}
        renderInput={({ inputRef, inputProps, InputProps, label }) => (
          <Form.Group>
            <Form.Label htmlFor={name} className={clsx({ required })}>
              {labelTextId
                ? intl.formatMessage({ id: labelTextId })
                : labelText || label}
            </Form.Label>
            <div className="position-relative">
              <Form.Control
                ref={inputRef}
                {...inputProps}
                name={name}
                required={required}
                isInvalid={isInvalid}
              />
              <span className="position-absolute top-50 end-0 me-4">
                {InputProps?.endAdornment}
              </span>
            </div>
            <Form.Text id="helperText" className="my-1 d-block">
              {helperTextId
                ? intl.formatMessage({ id: helperTextId })
                : helperText}
            </Form.Text>
          </Form.Group>
        )}
      />
    </LocalizationProvider>
  );
};
