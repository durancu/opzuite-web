// routes module, to be imported in src/Routes
import { RouteObject } from "react-router-dom";
import { DashboardDeleteDialog } from "./pages/dashboards/dashboard-delete-dialog/DashboardDeleteDialog";
import { DashboardsResultDetailPage } from "./pages/dashboards/dashboard-detail-page/dashboard-result-detail/DashboardsResultDetailPage";
import { DashboardEdit } from "./pages/dashboards/dashboard-edit/DashboardEdit";
import { DashboardsDeleteDialog } from "./pages/dashboards/dashboards-delete-dialog/DashboardsDeleteDialog";
import { DashboardsCard } from "./pages/dashboards/DashboardsCard";

export const DashboardRoutes: RouteObject[] = [
  {
    index: true,
    element: <DashboardsCard />,
  },
  {
    path: "new",
    element: <DashboardEdit />,
  },
  {
    path: ":code/edit",
    element: <DashboardEdit />,
  },
  {
    path: ":code/detail",
    element: <DashboardsResultDetailPage />,
  },
  {
    path: "deleteDashboards",
    element: <DashboardsDeleteDialog />,
  },
  {
    path: ":code/delete",
    element: <DashboardDeleteDialog />,
  },
];
