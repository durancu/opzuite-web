import { Link } from "react-router-dom";

export function CreatedByColumnFormatter(name: any, row: any) {
  return (
    <>
      <Link
        hidden={!row?.createdByName}
        className="text-dark fw-semibold text-hover-primary mb-0 fs-6"
        to={`/agents/${row?.createdBy}/detail`}
      >
        {row?.createdByName}
      </Link>


      <Link className="text-muted fw-semibold text-muted text-hover-primary d-block fs-7"
        to={`/manager/locations/${row?.locationCode}/detail`}
      >
        {row?.locationName}
      </Link>
    </>
  );
}
