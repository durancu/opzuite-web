export const EndorsementStatusColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { entities }: any
) => {
  return (
    entities?.endorsementStatus.find(({ id }: any) => id === cellContent)
      ?.name || "-"
  );
};
