import { Endorsement } from "./Endorsement";
import { SaleItem } from "./SaleItem";
export interface Sale {
  id: string;
  code: string;
  company: string;
  customer: string;
  items: SaleItem[];
  isChargeItemized: boolean;
  isRenewal: boolean;
  lineOfBusiness: string;
  location: string;
  monthlyPayment: number;
  effectiveAt: Date;
  expiresAt: Date;
  cancelledAt: Date;
  renewalReferences: string[];
  renewed: boolean;
  seller: string;
  soldAt: Date;
  status: string;
  type: string;
  number: string;
  createdBy?: string;
  updatedBy?: string;
  renewalFrequency: string;
  autoRenew: boolean;
  //Metrics
  checksumSanity: number;
  totalAgencyCommission: number;
  totalAgentCommission: number;
  totalPermits: number;
  totalCoveragesDownPayment: number;
  totalCoveragesPremium: number;
  totalFinanced: number;
  totalFinancedPaid: number;
  totalNonPremium: number;
  totalPaid: number;
  totalPayables: number;
  totalPremium: number;
  totalReceivables: number;
  totalReceived: number;
  totalTaxesAndFees: number;

  endorsements: Endorsement[];
}
