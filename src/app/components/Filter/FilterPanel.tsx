import { Fragment, ReactNode, useEffect, useMemo } from "react";
import { Field, Formik, FormikValues } from "formik";
import { Badge, Form, Offcanvas } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { useLocation, useSearchParams } from "react-router-dom";
import { useFilterUIContext } from "src/app/components/Filter";
import { isEmpty, startCase, capitalize } from "lodash";
import { useGetCatalog } from "../Catalog/hooks";
import clsx from "clsx";

interface Props {
  initialValues?: Record<string, any>;
  filterForm?: ReactNode;
  prepareFilter: (values: FormikValues) => Record<string, any>;
}

export const FilterPanel = ({
  initialValues = {},
  filterForm,
  prepareFilter,
}: Props) => {
  const intl = useIntl();
  const location = useLocation();
  const catalog = useGetCatalog();

  const [searchParams, setSearchParams] = useSearchParams();
  const { defaultFilter, showPanel, togglePanel, updateQueryParams } =
    useFilterUIContext();

  const filterParams = useMemo(() => {
    return Object.fromEntries(searchParams) || {};
  }, [searchParams]);

  function formatParam(param: string) {
    const key = param === "seller" ? "employee" : param; // sellers are actually employees so we want to use that to filter instead
    let value = filterParams[key] ?? filterParams[param]; // if we cant find the new key we use the old one :)
    const collection = catalog[(key + "s") as keyof typeof catalog]; // all catalogs end with an s but the properties do not

    // find the value and use its name instead of id
    if (collection && collection.length) {
      const item = collection.find((item: any) => item.id === value);
      value = item?.name || value;
    }

    // Leave coutry names as is
    return `${startCase(key)} : ${
      key === "country" ? startCase(value) : startCase(capitalize(value))
    }`;
  }

  function handleSubmit(values: FormikValues) {
    const prepared = prepareFilter(values);
    setSearchParams(prepared.filter);
    updateQueryParams(prepared);
    togglePanel(false);
  }

  function handleReset() {
    updateQueryParams(defaultFilter);
    setSearchParams({});
    togglePanel(false);
  }

  function removeFilter(key: string) {
    delete filterParams[key];
    setSearchParams(filterParams);
    handleSubmit(filterParams);
  }

  // initiate filter value, extract query from URL
  useEffect(() => {
    if (Object.keys(filterParams).length) {
      handleSubmit(filterParams);
    }
  }, [filterParams]);

  return (
    <Formik
      initialValues={!isEmpty(filterParams) ? filterParams : initialValues}
      onSubmit={handleSubmit}
      onReset={handleReset}
    >
      {({ handleSubmit, handleReset, setFieldValue }) => (
        <Fragment>
          <form onSubmit={handleSubmit} className="form mb-4">
            <div className="row">
              <Form.Group
                className={clsx("d-flex align-items-center position-relative", {
                  "col-md-10": filterForm, // if theres a form cut some space :)
                })}
              >
                <i
                  className="bi bi-search position-absolute ms-6"
                  aria-label="search"
                ></i>
                <Field
                  type="search"
                  name="searchText"
                  className="form-control form-control-solid ps-14"
                  placeholder={intl.formatMessage({
                    id: "FORM.FILTERS.SEARCH_ALL",
                  })}
                />
                <button
                  type="button"
                  className="btn position-absolute end-0"
                  title="clear"
                  onClick={() => handleReset()}
                >
                  <i className="bi bi-x-lg"></i>
                </button>
              </Form.Group>

              <Form.Group className="col-md-2">
                {/* only show if a form is available */}
                {filterForm && (
                  <button
                    type="button"
                    className="btn btn-light-primary w-100"
                    onClick={() => togglePanel(true)}
                  >
                    <i className="fs-3 bi bi-funnel-fill"></i>
                    <FormattedMessage id="FORM.FILTERS.GENERAL" />
                  </button>
                )}

                <Offcanvas
                  show={showPanel}
                  placement="end"
                  onHide={() => togglePanel(false)}
                  className="w-600px"
                  backdrop="static"
                >
                  <Offcanvas.Header className="" closeButton>
                    <Offcanvas.Title className="fs-5 text-dark fw-bolder">
                      Filter Options
                    </Offcanvas.Title>
                  </Offcanvas.Header>
                  <Offcanvas.Body className="mb-20 overflow-auto">
                    {filterForm}
                  </Offcanvas.Body>

                  <div className="position-absolute bottom-0 w-100 d-flex gap-4 p-4">
                    <button
                      type="reset"
                      className="btn btn-light btn-active-light-primary fw-bold flex-grow-1"
                      onClick={() => handleReset()}
                    >
                      <FormattedMessage id="FORM.FILTERS.BUTTON.RESET" />
                    </button>
                    <button
                      type="submit"
                      className="btn btn-primary fw-bold flex-grow-1"
                      onClick={() => handleSubmit()}
                    >
                      <FormattedMessage id="FORM.FILTERS.BUTTON.APPLY" />
                    </button>
                  </div>
                </Offcanvas>
              </Form.Group>
            </div>
          </form>

          <div className="d-flex gap-2 my-2 flex-wrap">
            {Object.keys(filterParams).map((key: string, index: number) => (
              <div
                key={index}
                className="mb-1"
                hidden={
                  key === "type" && !location.pathname.includes("customers")
                }
              >
                <Badge
                  pill
                  bg="light"
                  className="text-capitalize py-1 px-3 border  d-flex align-items-center gap-2"
                >
                  <span>{formatParam(key)}</span>
                  <button
                    type="button"
                    className="btn text-light-primary p-0"
                    title="clear"
                    onClick={() => {
                      removeFilter(key);
                      setFieldValue(key, "");
                    }}
                  >
                    <i className="bi bi-x-lg"></i>
                  </button>
                </Badge>
              </div>
            ))}
          </div>
        </Fragment>
      )}
    </Formik>
  );
};
