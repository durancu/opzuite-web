import { FormHelperText, InputAdornment } from "@mui/material";
import { useIntl } from "react-intl";
import { OnBlurField } from "../../partials/controls";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName: string;
  disabled?: boolean;
  required?: boolean;
}

export function CommissionTextField({
  helperText,
  helperTextId,
  labelTextId,
  labelText,
  fieldName,
  disabled = false,
  required = false,
}: Props) {
  const intl = useIntl();
  return (
    <>
      <OnBlurField
        name={fieldName}
        type="number"
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
        endAdornment={<InputAdornment position="end">%</InputAdornment>}
        required={required}
        disabled={disabled}
      />
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
