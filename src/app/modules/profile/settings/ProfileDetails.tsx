import { ErrorMessage, Field, Formik } from "formik";
import { useSnackbar } from "notistack";
import React from "react";
import { Alert, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  BackdropLoader,
  FormInput,
  PhoneNumberInput,
} from "src/app/components";
import { StatesAutocompleteField } from "src/app/components/FormControls";
import { timezonesOptions } from "src/app/components/functions";
import { COUNTRIES } from "src/app/constants/Countries";
import { DatePickerCustom } from "src/app/partials/controls";
import { profileAPI } from "../redux";
import { Communication } from "../types";
import { profileSchema } from "./schema";

const timezones = timezonesOptions();

export const ProfileDetails: React.FC = () => {
  const { enqueueSnackbar } = useSnackbar();
  const {
    data: user = {},
    isLoading,
    isError,
    refetch,
  } = profileAPI.useGetProfileQuery();

  const [updateProfile, { isLoading: updating }] =
    profileAPI.useUpdateProfileMutation();

  if (isLoading) return <BackdropLoader />;
  if (isError) {
    return (
      <Alert show={isError} variant="danger">
        <Alert.Heading as="h2">Something went wrong</Alert.Heading>
        <p className="my-4">
          We {`couldn't`} fetch your profile. Please try again
        </p>
        <Button onClick={() => refetch()} variant="danger">
          Try again
        </Button>
      </Alert>
    );
  }

  return (
    <div className="card mb-5 mb-xl-10">
      <div
        className="card-header border-0 cursor-pointer"
        role="button"
        data-bs-toggle="collapse"
        data-bs-target="#kt_account_profile_details"
        aria-expanded="true"
        aria-controls="kt_account_profile_details"
      >
        <div className="card-title m-0">
          <h3 className="fw-bolder m-0">Profile Settings</h3>
        </div>
      </div>

      <Formik
        initialValues={user}
        validationSchema={profileSchema}
        onSubmit={async (values) => {
          // remove profilePhoto to prevent duplication
          delete values.profilePhoto;
          try {
            await updateProfile(values).unwrap();
            refetch();
            enqueueSnackbar("Update succesfull", {
              variant: "success",
            });
          } catch (error) {
            enqueueSnackbar("An error occured", {
              variant: "error",
            });
          }
        }}
      >
        {({ values, handleSubmit }) => {
          return (
            <div id="kt_account_profile_details" className="collapse show">
              <form onSubmit={handleSubmit} noValidate className="form">
                <div className="card-body border-top p-9">
                  <div className="row mb-4">
                    <div className="row">
                      <div className="col-lg-4 fv-row">
                        <FormInput
                          type="text"
                          className="form-control form-control-lg form-control-solid "
                          placeholder="First Name"
                          labelText="First Name"
                          name="firstName"
                          required
                        />
                      </div>

                      <div className="col-lg-4 fv-row">
                        <FormInput
                          type="text"
                          className="form-control form-control-lg form-control-solid"
                          placeholder="Last Name"
                          labelText="Last Name"
                          name="lastName"
                          required
                        />
                      </div>

                      <div className="col-lg-4 fv-row">
                        <DatePickerCustom
                          name="dob"
                          label="DOB"
                          className="form-control form-control-lg form-control-solid"
                          required
                        />
                      </div>
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-md-6 fv-row">
                      <FormInput
                        type="email"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="email@example.com"
                        labelText="Email"
                        name="email"
                        required
                      />
                    </div>
                    <div className="col-md-6 fv-row">
                      <label className="form-label required fw-bold fs-6">
                        Language
                      </label>
                      <Field
                        as="select"
                        className="form-select form-select-solid form-select-lg"
                        name="language"
                      >
                        <option value="">Select a Language...</option>
                        <option value="en">English</option>
                        <option value="es">Español - Spanish</option>
                      </Field>
                      <span className="text-danger my-1 d-block">
                        <ErrorMessage name="language" />
                      </span>
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-md-6">
                      <PhoneNumberInput
                        labelText="Contact Phone"
                        className="form-control form-control-lg form-control-solid"
                        name="phone"
                        required
                      />
                    </div>
                    <div className="col-md-6">
                      <PhoneNumberInput
                        labelText="Mobile Phone"
                        className="form-control form-control-lg form-control-solid"
                        name="mobilePhone"
                        required
                      />
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-md-6">
                      <label className="form-label fw-bold fs-6">
                        <span className="required">Country</span>
                      </label>

                      <Field
                        as="select"
                        className="form-select form-select-solid form-select-lg fw-bold"
                        name="country"
                      >
                        <option value="">Select a Country...</option>
                        {COUNTRIES.map((country, index) => (
                          <option key={index} value={country.code}>
                            {country.name}
                          </option>
                        ))}
                      </Field>
                    </div>

                    <div className="col-md-6">
                      <label className="form-label required fw-bold fs-6">
                        Time Zone
                      </label>

                      <Field
                        as="select"
                        className="form-select form-select-solid form-select-lg"
                        name="timezone"
                      >
                        <option value="">Select a Timezone..</option>
                        {timezones.map((zone, index) => (
                          <option key={index} value={zone.id}>
                            {zone.name}
                          </option>
                        ))}
                      </Field>
                      <span className="text-danger my-1 d-block">
                        <ErrorMessage name="timezone" />
                      </span>
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-md-6">
                      <FormInput
                        type="text"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="Address 1"
                        labelText="Address 1"
                        name="address.address1"
                        required
                      />
                    </div>
                    <div className="col-md-6">
                      <FormInput
                        type="text"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="Address 2"
                        labelText="Address 2"
                        name="address.address2"
                      />
                    </div>
                  </div>

                  <div className="row mb-4">
                    <div className="col-md-4">
                      <StatesAutocompleteField
                        fieldName="address.state"
                        className="form-control form-control-lg form-control-solid"
                      />
                    </div>
                    <div className="col-md-4">
                      <FormInput
                        type="text"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="County"
                        labelText="County"
                        name="address.county"
                      />
                    </div>
                    <div className="col-md-4">
                      <FormInput
                        type="text"
                        className="form-control form-control-lg form-control-solid"
                        placeholder="Zip"
                        labelText="Zip"
                        name="address.zip"
                      />
                    </div>
                  </div>

                  <div className="row my-10">
                    <h3 className="fw-bolder">Communication</h3>

                    <div className="col-lg-8 fv-row">
                      <div className="d-flex align-items-center mt-3">
                        {Object.keys(user?.communication).map((key, index) => (
                          <div
                            key={index}
                            hidden={key === "_id"}
                            className="form-check form-check-solid form-switch fv-row me-4"
                          >
                            <Field
                              className="form-check-input"
                              type="checkbox"
                              name={`communication.${key}`}
                              checked={
                                values.communication[key as keyof Communication]
                              }
                            />
                            <label className="form-label text-capitalize">
                              {key}
                            </label>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>

                  <div className="row mt-10">
                    <h3 className="fw-bolder">Email Preferences</h3>
                    <div className="my-4">
                      <div className="form-check form-check-solid form-switch fv-row mb-4">
                        <Field
                          className="form-check-input"
                          type="checkbox"
                          name="emailSettings.emailNotification"
                          checked={values.emailSettings.emailNotification}
                        />
                        <label className="form-label text-capitalize">
                          Email Notification
                        </label>
                      </div>

                      <div className="form-check form-check-solid form-switch fv-row mb-4">
                        <Field
                          className="form-check-input"
                          type="checkbox"
                          name="emailSettings.sendCopyToPersonalEmail"
                          checked={values.emailSettings.sendCopyToPersonalEmail}
                        />
                        <label className="form-label text-capitalize">
                          Send Copy To Personal Email
                        </label>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card-footer d-flex py-6 px-9 gap-3">
                  <Link
                    to="/profile"
                    className="btn btn-light btn-active-light-primary px-6"
                  >
                    Cancel
                  </Link>
                  <button type="submit" className="btn btn-primary">
                    {!updating ? (
                      "Save Changes"
                    ) : (
                      <span
                        className="indicator-progress"
                        style={{ display: "block" }}
                      >
                        Please wait...
                        <span className="spinner-border spinner-border-sm align-middle ms-2"></span>
                      </span>
                    )}
                  </button>
                </div>
              </form>
            </div>
          );
        }}
      </Formik>
    </div>
  );
};
