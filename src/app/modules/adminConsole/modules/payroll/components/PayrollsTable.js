import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { isPermitted } from "src/app/utils";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { useGetUserAuth } from "src/app/hooks";
import { payrollAPI } from "../_redux/payrollsSlice";
import { FilterForm } from "./FilterForm";
import { initialValues, prepareFilter, sizePerPageList } from "./helpers";

export const PayrollsTable = () => {
  const user = useGetUserAuth();
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = payrollAPI.useGetPayrollQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "locationName",
      text: "Location",
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.LocationColumnHeaderFormatter,
      formatter: columnFormatters.LocationNameColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
      },
    },
    {
      dataField: "payPeriodStartedAt",
      text: "From",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.DateFromColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "payPeriodEndedAt",
      text: "To",
      sort: true,
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.DateToColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
    },
    {
      dataField: "totalSaleBonus",
      text: "Commissions",
      sort: true,
      headerAlign: "left",
      align: "left",
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.CommissionsColumnHeaderFormatter,
      formatter: columnFormatters.AmountColumnFormatter,
    },
    {
      dataField: "totalNetSalary",
      text: "Total Salary",
      sort: true,
      headerAlign: "left",
      align: "left",
      sortCaret: sortCaret,
      headerClasses: "fw-bold",
      headerFormatter: headerFormatters.TotalSalaryColumnHeaderFormatter,
      formatter: columnFormatters.AmountColumnFormatter,
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "right",
      align: "right",
      formatter: columnFormatters.PayrollActionsColumnFormatter,
      hidden: !isPermitted(
        [
          Permissions.PAYROLLS_UPDATE,
          Permissions.PAYROLLS_DELETE,
          Permissions.PAYROLLS_READ,
        ],
        user.permissions
      ),
      formatExtraData: {
        openEditPayrollPage: menuActions.edit,
        openDetailPayrollPage: menuActions.details,
      },
      classes: "text-right pr-0",
      style: {
        minWidth: "100px",
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      <FilterPanel
        initialValues={initialValues}
        prepareFilter={prepareFilter}
        filterForm={<FilterForm />}
      />
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-left overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[
                  { dataField: "payPeriodStartedAt", order: "desc" },
                ]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};
