import { FormHelperText, TextField } from "@mui/material";
import { ErrorMessage, useFormikContext } from "formik";
import objectPath from "object-path";
import { useState } from "react";

interface Props {
  name: string;
  label: string;
  children: any;
  onBlur?: any;
  type?: string;
  required?: boolean;
  disabled?: boolean;
}

export const OnBlurSelect = (props: Props) => {
  const { values, errors, handleChange } = useFormikContext<any>();
  const [value, setValue] = useState<any>(objectPath.get(values, props.name));

  const thisHandleChange = (e: any) => {
    handleChange(e);
    setValue(e.target.value);
  };

  return (
    <>
      <TextField
        {...props}
        select={true}
        onChange={thisHandleChange}
        value={value}
        error={objectPath.get(errors, props.name) ? true : false}
      >
        {props.children}
      </TextField>
      <FormHelperText className="MuiFormHelperText-root Mui-error">
        <ErrorMessage name={props.name} />
      </FormHelperText>
    </>
  );
};
