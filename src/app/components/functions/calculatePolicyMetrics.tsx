import { roundAmount } from ".";
import { Endorsement } from "../interfaces/Endorsement";
import { EndorsementItem } from "../interfaces/EndorsementItem";

const FEE_COMMISSION_PERCENT = 0.3;

export const calculatePolicyMetrics = (endorsements: Endorsement[]): any => {
  let checksumSanity = 0;
  let totalAgencyCommission = 0;
  let totalAgentCommission = 0;
  let totalFinanced = 0;
  let totalFinancedPaid = 0;
  let totalPayables = 0;
  let totalPaid = 0;
  let totalReceivables = 0;
  let totalReceived = 0;
  let totalPremium = 0;
  let totalNonPremium = 0;
  let totalTaxesAndFees = 0;
  let totalCoveragesPremium = 0;
  let totalCoveragesDownPayment = 0;
  const totalPermits = 0;

  //RECUPERAR ESTE CODIGO PARA LUEGO CALCULAR LAS COMISIONES DE LA COMPANIA
  /* if (item.broker || item.carrier) {
    //calculate item profits based on provider commissions
    item.agencyCommission = agencyCommissionByCoverage(providers, item);
    totalAgencyCommission += item.agencyCommission || 0;
  } */

  if (endorsements) {
    endorsements.map((element: Endorsement, index: number) => {
      const endorsement: Endorsement = { ...element };
      endorsement.totalAgencyCommission = 0;
      endorsement.totalAgentCommission = 0;
      endorsement.totalFinanced = 0;
      endorsement.totalFinancedPaid = 0;
      endorsement.totalPayables = 0;
      endorsement.totalPaid = 0;
      endorsement.totalReceivables = 0;
      endorsement.totalReceived = 0;
      endorsement.totalNonPremium = 0;
      endorsement.totalTaxesAndFees = 0;

      endorsement.totalPremium = endorsement.amount || 0;
      if (index === 0) {
        totalCoveragesPremium = endorsement.totalPremium;
      }

      if (endorsement.items) {
        endorsement.items.map((element: EndorsementItem, itemIndex: number) => {
          const item = { ...element };
          item.amount = item.amount || 0;
          item.amountPaid = item.amountPaid || 0;

          if (itemIndex === 0 && item.accountingClass === "RECEIVABLE") {
            totalCoveragesDownPayment = item.amount;
          }

          switch (item.accountingClass) {
            case "PAYABLE":
              endorsement.totalPayables += item.amount;
              endorsement.totalPaid += item.amountPaid;
              break;
            case "RECEIVABLE":
              endorsement.totalReceivables += item.amount;
              endorsement.totalReceived += item.amountPaid;
              break;
            case "FINANCING_DIRECT_BILL":
              endorsement.totalFinanced += item.amount;
              endorsement.totalFinancedPaid += item.amountPaid;
              break;
            case "FEE_TAX":
              endorsement.totalTaxesAndFees += item.amount;
              endorsement.totalNonPremium += item.amount;
              break;
            case "AGENT_COMMISSION":
              endorsement.totalAgentCommission += item.amount;
              break;
            case "AGENCY_COMMISSION":
              endorsement.totalAgencyCommission += item.amount;
              break;
            default:
            //Do nothing
          }
          return item;
        });

        totalAgencyCommission += endorsement.totalAgencyCommission;
        totalAgentCommission += endorsement.totalAgentCommission;
        totalFinanced += endorsement.totalFinanced;
        totalFinancedPaid += endorsement.totalFinancedPaid;
        totalPayables += endorsement.totalPayables;
        totalPaid += endorsement.totalPaid;
        totalReceivables += endorsement.totalReceivables;
        totalReceived += endorsement.totalReceived;
        totalPremium += endorsement.totalPremium;
        totalNonPremium += endorsement.totalNonPremium;
        totalTaxesAndFees += endorsement.totalTaxesAndFees;
      }
      return endorsement;
    });
  }

  checksumSanity =
    totalPremium +
    totalNonPremium -
    totalFinanced -
    totalReceivables +
    totalPayables;

  return {
    checksumSanity: roundAmount(checksumSanity),
    totalFinanced: roundAmount(totalFinanced),
    totalFinancedPaid: roundAmount(totalFinancedPaid),
    totalPayables: roundAmount(totalPayables),
    totalPaid: roundAmount(totalPaid),
    totalReceivables: roundAmount(totalReceivables),
    totalReceived: roundAmount(totalReceived),
    totalPremium: roundAmount(totalPremium),
    totalNonPremium: roundAmount(totalNonPremium),
    totalTaxesAndFees: roundAmount(totalTaxesAndFees),
    totalCoveragesPremium: roundAmount(totalCoveragesPremium),
    totalCoveragesDownPayment: roundAmount(totalCoveragesDownPayment),
    totalPermits: roundAmount(totalPermits),
    totalAgencyCommission: roundAmount(
      totalAgencyCommission + (1 - FEE_COMMISSION_PERCENT) * totalTaxesAndFees
    ),
    totalAgentCommission: roundAmount(
      totalAgentCommission + FEE_COMMISSION_PERCENT * totalTaxesAndFees
    ),
  };
};
