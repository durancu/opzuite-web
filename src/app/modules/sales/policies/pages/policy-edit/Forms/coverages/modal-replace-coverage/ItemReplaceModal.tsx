import { Button, Typography } from "@mui/material";
import { Update } from "@material-ui/icons";
import { ReactElement, useState } from "react";
import { Modal } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { GenericFunction } from "src/app/components/interfaces/FunctionInterfaces";

interface Props {
  replaceAction: GenericFunction;
  itemTitle: string;
}

export default function ItemReplaceModal(props: Props): ReactElement<Props> {
  const { replaceAction, itemTitle } = props;

  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        variant="contained"
        onClick={() => {
          setOpen(true);
        }}
        style={{ minWidth: "120px" }}
        className="btn btn-warning"
      >
        <span className="mr-3">Renew</span>
        <Update />
      </Button>
      <Modal show={open} centered>
        <Modal.Header>
          <Typography variant="h5" component="h2">
            Replace: {itemTitle}
          </Typography>
        </Modal.Header>
        <Modal.Body>
          <p> Are you sure to replace the policy?</p>
          <span>
            If you replace the policy, the information will be change
            permanently.
          </span>
        </Modal.Body>
        <Modal.Footer>
          <div>
            <Button
              type="button"
              onClick={() => {
                setOpen(false);
              }}
              variant="text"
              color="inherit"
            >
              <FormattedMessage id="BUTTON.CANCEL" />
            </Button>
            <> </>
            <Button
              type="button"
              onClick={() => {
                replaceAction();
                setOpen(false);
              }}
              className="btn btn-danger btn-elevate"
            >
              <FormattedMessage id="BUTTON.REPLACE" />
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  );
}
