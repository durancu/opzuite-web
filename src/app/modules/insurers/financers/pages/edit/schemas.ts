import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const financerBusinessValidationSchema = Yup.object().shape({
  business: Yup.object().shape({
    name: Yup.string().required("Business Name is required."),
    primaryPhone: Yup.string()
      .nullable()
      .matches(PHONE_REGEX, "Must be a valid phone number")
      .required("Phone Number is required."),
    email: Yup.string()
      .email("Must be a valid email address.")
      .required("Email is required."),
    fax: Yup.string(),
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      zip: Yup.string(),
      country: Yup.string(),
    }),
    website: Yup.string(),
  }),
});
export const financerContactInfoValidationSchema = Yup.object().shape({
  contact: Yup.object().shape({
    firstName: Yup.string(),
    lastName: Yup.string(),
    email: Yup.string().email("Must be a valid email address."),
    phone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    mobilePhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
  }),
});
