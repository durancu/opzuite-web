import { Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import {
  FormInput,
  FormSelect,
  FormTextArea,
  YearList,
} from "src/app/components";
import * as CATALOG from "src/app/components/Catalog/CatalogSelects";
import { CUSTOMER_TYPES } from "src/app/constants/General";

interface Props {
  index?: number;
  customerType?: string;
}

export const VehicleFields = ({ index, customerType }: Props) => {
  // form could be standalone as used in coverages or in a field array
  function formatName(name: string) {
    return index !== undefined ? `vehicles.${index}.${name}` : name;
  }

  return (
    <Fragment>
      <Row className="mb-4">
        <Col>
          <FormInput labelText="VIN Number" name={formatName("vinNumber")} />
        </Col>
        <Col>
          <YearList
            labelText="Year"
            name={formatName("year")}
            startYear={1900}
          />
        </Col>
        <Col>
          <FormInput labelText="Make" name={formatName("make")} />
        </Col>
        <Col>
          <FormInput labelText="Model" name={formatName("model")} />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormSelect
            labelText="Type"
            name={formatName("type")}
            options={
              customerType === CUSTOMER_TYPES.BUSINESS
                ? CATALOG.AUTO_TYPE_COMMERCIAL_CATALOG
                : CATALOG.AUTO_TYPE_PERSONAL_CATALOG
            }
          />
        </Col>
        <Col>
          <FormInput labelText="Unit Number" name={formatName("unitNumber")} />
        </Col>
        <Col>
          <FormInput
            labelText="License Plate"
            name={formatName("licensePlate")}
          />
        </Col>
        <Col>
          <FormSelect
            labelText="Color"
            name={formatName("color")}
            options={CATALOG.AUTO_COLORS_CATALOG}
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormSelect
            labelText="Condition"
            name={formatName("condition")}
            options={CATALOG.AUTO_CONDITION_CATALOG}
          />
        </Col>

        <Col>
          <FormSelect
            labelText="Ownership Status"
            name={formatName("ownershipStatus")}
            options={CATALOG.AUTO_OWNERSHIP_STATUS_CATALOG}
          />
        </Col>

        <Col>
          <FormSelect
            labelText="Auto Insurance Company"
            name={formatName("autoInsuranceCompany")}
            options={CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG}
          />
        </Col>
      </Row>

      {customerType === "INDIVIDUAL" && (
        <Row className="mb-4">
          <Col md={4} className="mb-4">
            <FormInput labelText="Trim" name={formatName("trim")} />
          </Col>
          <Col md={4} className="mb-4">
            <FormSelect
              labelText="Primary Use"
              name={formatName("primaryUse")}
              options={CATALOG.AUTO_PRIMARY_USE_CATALOG}
            />
          </Col>
          <Col md={4} className="mb-4">
            <FormInput
              type="number"
              labelText="Driving Days Per Week"
              name={formatName("drivingDaysPerWeek")}
            />
          </Col>
          <Col md={4} className="mb-4">
            <FormInput
              type="number"
              labelText="Driving Miles Per Week"
              name={formatName("drivingMilesPerWeek")}
            />
          </Col>
          <Col md={4} className="mb-4">
            <FormSelect
              labelText="Annual Mileage"
              name={formatName("annualMileage")}
              options={CATALOG.AUTO_ANNUAL_MILEAGE_CATALOG}
            />
          </Col>
        </Row>
      )}

      <Row className="mb-4">
        <Col>
          <FormTextArea labelText="Notes" name={formatName("notes")} />
        </Col>
      </Row>
    </Fragment>
  );
};
