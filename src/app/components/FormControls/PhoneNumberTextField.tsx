import { FormGroup, FormHelperText } from "@mui/material";
import { Field } from "formik";
import { TextField } from "formik-material-ui";
import { useIntl } from "react-intl";
import MaskedInput from "react-text-mask";
import { PHONE_NUMBER_MASK } from "src/app/constants/RegularExpressions";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
  extensionFieldName?: string;
  labelExtensionText?: string;
  labelExtensionTextId?: string;
  showExtension?: boolean;
}

export const PhoneNumberTextField = ({
  helperText,
  helperTextId,
  labelText,
  labelTextId = "FORM.LABELS.PHONE_NUMBER",
  labelExtensionText,
  labelExtensionTextId = "FORM.LABELS.EXT",
  fieldName = "phone",
  extensionFieldName = "phoneExtension",
  showExtension = true,
}: Props) => {
  const intl = useIntl();

  return (
    <>
      <FormGroup row={true}>
        <Field
          label={
            labelText ? labelText : `${intl.formatMessage({ id: labelTextId })}`
          }
          style={{ width: `${showExtension ? 70 : 100}%` }}
          name={fieldName}
          component={TextField}
          InputProps={{
            inputComponent: (props: any) => (
              <MaskedInput
                {...props}
                ref={(ref: any) => ref}
                mask={PHONE_NUMBER_MASK}
                placeholderChar={"\u2000"}
              />
            ),
          }}
        />
        {showExtension && (
          <Field
            label={
              labelExtensionText
                ? labelExtensionText
                : `${intl.formatMessage({ id: labelExtensionTextId })}`
            }
            component={TextField}
            style={{ width: "30%" }}
            name={extensionFieldName}
          />
        )}
      </FormGroup>
      {helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : (
        ``
      )}
    </>
  );
};
