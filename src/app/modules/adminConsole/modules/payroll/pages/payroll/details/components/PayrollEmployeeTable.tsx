import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { NoRecordsFoundMessage, PleaseWaitMessage } from "src/app/components";
import * as columnFormatters from "./column-formatters";

interface Props {
  employeeList: any[];
}

export const PayrollEmployeeTable = (props: Props) => {
  const columns = [
    {
      dataField: "employeeName",
      text: "Name",
      sort: true,
      headerClasses: "fw-bold",
    },
    {
      dataField: "totalRegularSalary",
      text: "Base Salary",
      sort: true,
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
    },
    {
      dataField: "totalSaleBonus",
      text: "Commissions",
      sort: true,
      headerClasses: "fw-bold",
      formatter: columnFormatters.CommissionColumnFormatter,
    },
    {
      dataField: "totalReimbursement",
      text: "Reimbursements",
      sort: true,
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
    },
    {
      dataField: "totalDiscount",
      text: "Discounts",
      sort: true,
      headerClasses: "fw-bold",
      formatter: columnFormatters.DiscountColumnFormatter,
    },
    {
      dataField: "totalNetSalary",
      text: "Salary Paid",
      sort: true,
      headerClasses: "fw-bold",
      formatter: columnFormatters.AmountColumnFormatter,
    },
  ];

  return (
    <Fragment>
      <BootstrapTable
        wrapperClasses="table-responsive"
        classes="table table-head-custom table-vertical-center overflow-hidden"
        bootstrap4
        bordered={false}
        remote
        keyField="code"
        data={props.employeeList === null ? [] : props.employeeList}
        columns={columns}
      />
      <PleaseWaitMessage entities={props.employeeList} />
      <NoRecordsFoundMessage entities={props.employeeList} />
    </Fragment>
  );
};
