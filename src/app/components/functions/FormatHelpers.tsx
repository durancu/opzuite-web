import moment from "moment";

export const formatPhoneNumber = (phoneNumberString: string) => {
  const cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    const intlCode = match[1] ? "+1 " : "";
    return [intlCode, "(", match[2], ") ", match[3], "-", match[4]].join("");
  }
  return phoneNumberString;
};

export const formatDate = (value: Date) =>
  (value && `${moment(value).format("MM/DD/YYYY")}`) || "-";

export const formatDateTime = (value: Date) =>
  (value && `${moment(value).format("MM/DD/YY H:m")}`) || "-";

export const formatAmount = (value = 0) =>
  value ? (
    <>{formatAmountWithCommas(parseInt(value.toFixed(2)))}</>
  ) : (
    <>{0.0}</>
  );

export const formatNegativeAmount = (value = 0) =>
  value ? <>({value.toFixed(2)} )</> : <>{0.0}</>;

export function roundAmount(amount: number): number {
  return Math.round((amount || 0) * 100) / 100;
}

export function formatAmountWithCommas(amount: number) {
  return amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const calculateChartTotalMetrics = (data: any[]) => {
  if (data) {
    const rounded = (data || 0)
      .reduce((accumulator: any, value: any) => accumulator + (value || 0), 0)
      .toFixed(0);
    return formatAmountWithCommas(rounded);
  }
  return 0;
};
