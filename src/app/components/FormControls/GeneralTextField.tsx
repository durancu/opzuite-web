import { FormHelperText } from "@mui/material";
import { Field, useFormikContext } from "formik";
import { TextField } from "formik-material-ui";
import { ReactElement } from "react";
import { useIntl } from "react-intl";
import { GenericFunction } from "../interfaces/FunctionInterfaces";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName: string;
  disabled?: boolean;
  type?: string;
  onChange?: GenericFunction;
  multiline?: boolean;
  minRows?: number;
  variant?: string;
}

export function GeneralTextField({
  helperText,
  helperTextId,
  labelTextId = "DEFAULT",
  labelText,
  fieldName,
  disabled = false,
  type = "text",
  onChange,
  ...props
}: Props): ReactElement {
  const intl = useIntl();
  const { handleChange } = useFormikContext();
  return (
    <>
      <Field
        component={TextField}
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
        name={fieldName}
        disabled={disabled}
        type={type}
        onChange={onChange || handleChange}
        {...props}
      />
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
