import { Stack } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useLocation } from "react-router-dom";

interface Props {
  onAdd: () => void;
}

export const AddCustomerMenu = ({ onAdd = () => null }: Props) => {
  const location = useLocation();

  return (
    <Stack direction="horizontal" gap={3}>
      <Link
        state={{ from: location.pathname }}
        to="/customers/business/new"
        className="btn btn-primary align-self-center"
        onClick={() => onAdd()}
      >
        <FormattedMessage id="BUTTON.CUSTOMER.BUSINESS.NEW" />
      </Link>
      <Link
        state={{ from: location.pathname }}
        to="/customers/individual/new"
        className="btn btn-primary align-self-center"
        onClick={() => onAdd()}
      >
        <FormattedMessage id="BUTTON.CUSTOMER.INDIVIDUAL.NEW" />
      </Link>
    </Stack>
  );
};
