import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { Alert } from "src/app/components/Feedback";
import { useAppDispatch, useAppSelector } from "src/app/hooks";

import {
  BackdropLoader,
  WizardForm,
  WizardFormStep,
  Error,
} from "src/app/components";
import { PageTitle } from "src/_metronic/layout/core";
import FormPermitInfo from "./Forms/FormPermitInfo";
import FormPermitProducts from "./Forms/FormPermitProducts";
import {
  permitBasicInfoValidateSchema,
  permitDetailsValidateSchema,
} from "./schemas";
import { defaultService } from "./helpers";
import { useSnackbar } from "notistack";
import { FormikValues } from "formik";
import { breadcrumbs } from "../../breadcrumbs";
import { salesAPI, saleActions } from "src/app/modules/sales/redux/saleSlice";

export const ServiceEdit = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const savedService = useAppSelector((state) => state.sales.savedService);

  const {
    data: service,
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetSaleByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [createService, { isLoading: isCreating }] =
    salesAPI.useCreateSaleMutation();

  function getInitialValue() {
    if (savedService?.customer) {
      return savedService;
    } else if (code) {
      return service;
    }
    return defaultService;
  }

  const [updateService, { isLoading: isUpdating }] =
    salesAPI.useUpdateSaleMutation();

  const backToServicesList = () => {
    dispatch(saleActions.clear());
    navigate(-1);
  };

  async function handleServiceCreation(values: FormikValues) {
    try {
      const response = await createService(values).unwrap();
      enqueueSnackbar("Service created successfully", {
        variant: "success",
      });
      navigate(`/services/${response.code}/detail`);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleServiceUpdate(values: FormikValues) {
    try {
      await updateService(values).unwrap();
      enqueueSnackbar("Service updated successfully", {
        variant: "success",
      });
      navigate(-1);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading || isCreating || isUpdating) {
    return <BackdropLoader />;
  }

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {code ? "Edit Service Sale" : `New Service Sale`}
      </PageTitle>
      <Card>
        <Alert
          show={Object.keys(savedService).length > 0}
          variant="warning"
          labelComponent="application"
          action={() => dispatch(saleActions.clear())}
        />
        <WizardForm
          initialValues={getInitialValue()}
          onSubmit={code ? handleServiceUpdate : handleServiceCreation}
          onCancel={backToServicesList}
        >
          <WizardFormStep
            labelTextId="WIZARD.PERMIT.BASIC_INFO"
            validationSchema={permitBasicInfoValidateSchema}
          >
            <FormPermitInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.PERMIT.DETAIL"
            validationSchema={permitDetailsValidateSchema}
          >
            <FormPermitProducts />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};
