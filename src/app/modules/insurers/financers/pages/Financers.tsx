import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { breadcrumbs } from "../breadcrumbs";
import { FinancersTable } from "../components/FinancersTable";
import { defaultFilter } from "../components/helpers";

const Financers = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Financers</PageTitle>
      <RequirePermission permissions={Permissions.FINANCERS_CREATE}>
        <Toolbar>
          <Link
            to="/financers/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.FINANCER.NEW" />
          </Link>
        </Toolbar>
      </RequirePermission>
      <Card className="p-8">
        <FilterUIProvider
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <FinancersTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};

export default Financers;
