
import { CUSTOMER_TYPES } from "src/app/constants/General";
import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";
export const businessSchema = Yup.object().shape({
  business: Yup.object().shape({
    name: Yup.string().required("Company Name is required."),
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      country: Yup.string(),
      zip: Yup.string(),
    }),
    email: Yup.string()
      .trim()
      .email("Must be a valid email address")
      .required("Email address is required."),
    fax: Yup.string(),
    industry: Yup.string(),
    primaryPhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number.")
      .required("Phone number is required."),
    primaryPhoneExtension: Yup.string(),
    website: Yup.string(),
    usDOT: Yup.string(),
    mcNumber: Yup.string(),
    txDMV: Yup.string(),
  }),
});

export const personalInfoSchema = Yup.object().shape({
  contact: Yup.object().shape({
    address: Yup.object().shape({
      address1: Yup.string(),
      address2: Yup.string(),
      city: Yup.string(),
      state: Yup.string(),
      country: Yup.string(),
      zip: Yup.string(),
    }),
    dob: Yup.mixed().nullable(),
    driverLicense: Yup.string(),
    email: Yup.string().trim().email("Must be a valid email address."),
    firstName: Yup.string().required("First Name is required."),
    language: Yup.string(),
    lastName: Yup.string().required("Last Name is required."),
    mobilePhone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    phone: Yup.string()
      .nullable()
      .trim()
      .matches(PHONE_REGEX, "Must be a valid phone number"),
    ssn: Yup.string(),
    timezone: Yup.string(),
    website: Yup.string(),
    driverLicenseState: Yup.string(),
    educationLevel: Yup.string(),
    employer: Yup.string(),
    employmentStatus: Yup.string(),
    ethnicity: Yup.string(),
    gender: Yup.string(),
    identityCardId: Yup.string(),
    identityCardType: Yup.string(),
    isInsured: Yup.boolean(),
    immigrantStatus: Yup.string(),
    hasHealthInsurance: Yup.boolean(),
    healthInsuranceCompany: Yup.string(),
    hasAutoInsurance: Yup.boolean(),
    autoInsuranceCompany: Yup.string(),
    hasLifeInsurance: Yup.boolean(),
    lifeInsuranceCompany: Yup.string(),
    hasHomeInsurance: Yup.boolean(),
    homeInsuranceCompany: Yup.string(),
    maritalStatus: Yup.string(),
    notes: Yup.string(),
    relationship: Yup.string(),

    vehicles: Yup.array().of(Yup.object()),
    yearlyGrossIncome: Yup.string(),
  }),
});

export const cohortSchema = Yup.object().shape({
  address: Yup.object().shape({
    address1: Yup.string(),
    address2: Yup.string(),
    city: Yup.string(),
    state: Yup.string(),
    country: Yup.string(),
    zip: Yup.string(),
  }),
  dob: Yup.mixed().nullable(),
  driverLicense: Yup.string(),
  email: Yup.string().trim().email("Must be a valid email address."),
  firstName: Yup.string().required("First Name is required."),
  language: Yup.string(),
  lastName: Yup.string().required("Last Name is required."),
  mobilePhone: Yup.string()
    .nullable()
    .trim()
    .matches(PHONE_REGEX, "Must be a valid phone number"),
  phone: Yup.string()
    .nullable()
    .trim()
    .matches(PHONE_REGEX, "Must be a valid phone number"),
  ssn: Yup.string(),
  timezone: Yup.string(),
  website: Yup.string(),
  driverLicenseState: Yup.string(),
  educationLevel: Yup.string(),
  employer: Yup.string(),
  employmentStatus: Yup.string(),
  ethnicity: Yup.string(),
  gender: Yup.string(),
  identityCardId: Yup.string(),
  identityCardType: Yup.string(),
  isInsured: Yup.boolean(),
  immigrantStatus: Yup.string(),
  hasHealthInsurance: Yup.boolean(),
  healthInsuranceCompany: Yup.string(),
  hasAutoInsurance: Yup.boolean(),
  autoInsuranceCompany: Yup.string(),
  hasLifeInsurance: Yup.boolean(),
  lifeInsuranceCompany: Yup.string(),
  hasHomeInsurance: Yup.boolean(),
  homeInsuranceCompany: Yup.string(),
  maritalStatus: Yup.string(),
  notes: Yup.string(),
  relationship: Yup.string(),
});

export const cohortsSchema = Yup.object().shape({
  cohorts: Yup.array().of(cohortSchema),
});

export const vehicleSchema = Yup.object().shape({
  vinNumber: Yup.string().required("Please enter a vin number"),
  licensePlate: Yup.string(),
  notes: Yup.string(),
  make: Yup.string(),
  type: Yup.string(),
  model: Yup.string(),
  year: Yup.string(),
  hasAutoInsurance: Yup.boolean(),
  isCommercial: Yup.boolean(),
  autoInsuranceCompany: Yup.string(),
  trim: Yup.string(),
  color: Yup.string(),
  condition: Yup.string(),
  ownershipStatus: Yup.string(),
  primaryUse: Yup.string(),
  drivingDaysPerWeek: Yup.number(),
  drivingMilesPerWeek: Yup.number(),
  annualMileage: Yup.string(),
});

export const vehiclesSchema = Yup.object().shape({
  vehicles: Yup.array().of(vehicleSchema),
});

export const customerSettingsSchema = Yup.object().shape({
  settings: Yup.object().shape({
    email: Yup.boolean(),
    sms: Yup.boolean(),
    phone: Yup.boolean(),
  }),
});

export const customerSchema = Yup.object().shape({
  type: Yup.string(),
  business: Yup.object().when("type", (value: CustomerType) => {
    return value === CUSTOMER_TYPES.BUSINESS ? businessSchema : Yup.object();
  }),
  contact: personalInfoSchema,
  cohorts: Yup.array().of(cohortSchema),
  vehicles: Yup.array().of(vehicleSchema),
  communication: customerSettingsSchema,
});
