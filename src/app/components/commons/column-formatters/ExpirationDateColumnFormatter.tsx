import { FormattedDate } from "react-intl";

export function ExpirationDateColumnFormatter(cellContent: any) {
  return (
    <div style={{ whiteSpace: "nowrap" }}>
      <FormattedDate value={cellContent} />
    </div>
  );
}
