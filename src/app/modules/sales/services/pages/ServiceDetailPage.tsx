import { Fragment, useEffect } from "react";
import { useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { Permissions } from "src/app/constants/Permissions";
import { BackdropLoader, Error } from "src/app/components";

import {
  PageDetailTopButtons,
  PermitMetricsAccordion,
  PermitOverview,
} from "src/app/components/Widgets";
import { breadcrumbs } from "../breadcrumbs";
import { ApplicationTable } from "../components/ApplicationTable";
import { salesAPI } from "src/app/modules/sales/redux/saleSlice";
import { useSnackbar } from "notistack";

export const ServiceDetailPage = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: service,
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetSaleByCodeQuery(
    `${code}?layout=FULL&withCustomer=true&withSeller=true`,
    {
      skip: !code,
      refetchOnFocus: true,
      refetchOnReconnect: true,
      refetchOnMountOrArgChange: true,
    }
  );

  const [deleteSale, { isLoading: isDeleting }] =
    salesAPI.useDeleteSaleMutation();

  const goToEditService = () => {
    navigate(`/services/${code}/edit`);
  };

  const openRenewServicePage = () => {
    navigate(`/services/${code}/renew`);
  };

  async function handleDelete() {
    try {
      await deleteSale(code).unwrap();
      enqueueSnackbar("Service deleted", {
        variant: "success",
      });
      navigate("/services");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.APPLICATION" })}
      </PageTitle>
      <Toolbar>
        <PageDetailTopButtons
          editFunction={goToEditService}
          renewFunction={openRenewServicePage}
          deleteFunction={handleDelete}
          editPermission={Permissions.APPLICATIONS_UPDATE}
          deletePermission={Permissions.APPLICATIONS_DELETE}
          componentLabel="application"
        />
      </Toolbar>

      <PermitOverview sale={service} />
      <div className="p-8 mb-8 bg-white">
        <PermitMetricsAccordion sale={service} />
      </div>
      <ApplicationTable applications={service.endorsements} />
    </Fragment>
  );
};
