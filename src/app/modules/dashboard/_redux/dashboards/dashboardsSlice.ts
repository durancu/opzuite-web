import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialDashboardsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  dashboardForEdit: undefined,
  lastError: null,
  chart: undefined,
};

export const callTypes = {
  list: "list",
  action: "action",
};

type QueryType = {
  code: string | undefined;
  queryParams: any;
};

export const dashboardAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getChartData: build.query<any, QueryType>({
      query: ({ code, queryParams }) => ({
        url: `/dashboards/charts/${code}`,
        method: "post",
        body: { queryParams },
      }),
    }),
  }),
  overrideExisting: false,
});

export const dashboardsSlice = createSlice({
  name: "dashboards",
  initialState: initialDashboardsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getDashboardById
    dashboardFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.dashboardForEdit = action.payload.dashboardForEdit;
      state.error = null;
    },
    dashboardCharDataFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.chart = action.payload.chart;
      state.error = null;
    },
    // getDashboardById
    dashboardResult: (state: any, action: any) => {
      state.actionsLoading = false;
      state.dashboardForEdit = action.payload.dashboard;
      state.error = null;
    },
    // findDashboards
    dashboardsFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
      state.actionsLoading = false;
    },
    // createDashboard
    dashboardCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // createDashboardRenew
    dashboardCreatedRenew: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // updateDashboard
    dashboardUpdated: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // deleteDashboard
    dashboardDeleted: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // deleteDashboards
    dashboardsDeleted: (state: any) => {
      state.error = null;
      state.actionsLoading = false;
    },
    // dashboardsUpdateState
    dashboardsStatusUpdated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map((entity: any) => {
        if (ids.findIndex((id: any) => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
  },
});
