/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const PolicyActionsColumnFormatter = (
  cellContent: any,
  sale: any,
  rowIndex: any,
  {
    openEditPolicyPage,
    openDeletePolicyDialog,
    openRenewPolicyPage,
    openDetailPolicyPage,
    openCertificates,
  }: any
) => (
  <TableActionsMenu
    readFunction={openDetailPolicyPage}
    editFunction={openEditPolicyPage}
    renewFunction={openRenewPolicyPage}
    deleteFunction={openDeletePolicyDialog}
    openCertificates={openCertificates}
    editPermission={Permissions.POLICIES_UPDATE}
    readPermission={Permissions.POLICIES_READ}
    renewPermission={Permissions.POLICIES_RENEW}
    deletePermission={Permissions.POLICIES_DELETE}
    certificatePermission={Permissions.POLICIES_CERTIFICATES}
    customerId={sale?.customerId}
    code={sale.code}
  />
);
