import { Fragment, useEffect } from "react";
import { ProgressBar } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { useBackdropUIContext } from "src/app/components/Backdrop";
import * as catalogActions from "src/app/components/Catalog/redux/catalogActions";
import {
  LocationDetails,
  LocationEmployees,
  LocationOverview,
  PageDetailTopButtons,
} from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch } from "src/app/hooks";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import * as actions from "../_redux/locationsActions";

export const Details = () => {
  const { code }: any = useParams();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const { locationLoading, locationForEdit } = useSelector(
    (state: any) => ({
      locationLoading: state.locations.actionsLoading,
      locationForEdit: state.locations.locationForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchLocation(code));
    dispatch(catalogActions.getAllCatalog());
    return () => {
      dispatch(actions.clearLocation());
    };
  }, [code, dispatch]);

  const { setIsLoading } = useBackdropUIContext();
  useEffect(() => {
    setIsLoading(locationLoading);
  }, [locationLoading, setIsLoading]);

  const goToEditLocation = () => {
    navigate(`/manager/locations/${code}/edit`);
  };

  const deleteLocation = () =>
    dispatch(actions.deleteLocation(code)).then(() => {
      navigate("/manager/locations");
    });

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Locations</PageTitle>
      {locationLoading && <ProgressBar />}

      {!locationLoading && locationForEdit && (
        <div>
          <div className="d-flex justify-content-between align-items-center py-4">
            <h3 className="fw-bolder">Location Details</h3>
            <PageDetailTopButtons
              editFunction={goToEditLocation}
              deleteFunction={deleteLocation}
              editPermission={Permissions.LOCATIONS_UPDATE}
              deletePermission={Permissions.LOCATIONS_DELETE}
              componentLabel="location"
            />
          </div>
          <LocationOverview location={locationForEdit} />

          <LocationDetails location={locationForEdit} />

          <LocationEmployees id={locationForEdit.id} />

          {/* <LocationPolicies
            location={locationForEdit}
            basePath={ADMIN_CONSOLE_ROUTES.poli}
            code={code}
          />

          <LocationApplications
            location={locationForEdit}
            history={history}
            code={code}
          /> */}
        </div>
      )}
    </Fragment>
  );
};
