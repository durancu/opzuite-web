import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialState = {
  savedPolicy: {} as Policy,
  savedService: {} as Service,
};

interface Payload {
  type: "savedPolicy" | "savedService";
  values: any;
}

export const salesAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllSales: build.query<Policy[] | Service[], QueryParams>({
      query: (queryParams) => ({
        url: "/sales/search",
        method: "post",
        body: { queryParams },
      }),
    }),
    getAllRenewals: build.query<Policy[], QueryParams>({
      query: (queryParams) => ({
        url: "/sales/renewals",
        method: "post",
        body: { queryParams },
      }),
    }),
    getSaleByCode: build.query<any, any>({
      query: (code) => `/sales/${code}`,
    }),
    createSale: build.mutation<any, any>({
      query: (data) => ({
        url: "/sales",
        method: "post",
        body: data,
      }),
    }),
    updateSale: build.mutation<any, any>({
      query: (data) => ({
        url: `/sales/${data.code}`,
        method: "put",
        body: data,
      }),
    }),
    renewSale: build.mutation<any, any>({
      query: (data) => ({
        url: "/sales/renew",
        method: "post",
        body: data,
      }),
    }),
    deleteSale: build.mutation<any, any>({
      query: (code) => ({
        url: `/sales/${code}`,
        method: "delete",
      }),
    }),
  }),
  overrideExisting: false,
});

export const saleSlice = createSlice({
  name: "sales",
  initialState: initialState,
  reducers: {
    save: (state, { payload }: { payload: Payload }) => {
      return {
        ...state,
        [payload.type]: {
          ...state[payload.type],
          ...payload.values,
        },
      };
    },
    clear: () => initialState,
  },
});

export const saleActions = saleSlice.actions;
export default saleSlice.reducer;
