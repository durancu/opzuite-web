export const SellerNameColumnFormatter = (row: any) => (
  <>{`${row?._id?.name}`}</>
);
