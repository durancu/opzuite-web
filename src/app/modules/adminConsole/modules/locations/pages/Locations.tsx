import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { LocationsTable } from "../components/LocationsTable";
import { breadcrumbs } from "../../../breadcrumbs";
import { defaultFilter } from "../components/helpers";

export const Locations = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
    changeIP: (code: string) => navigate(`${code}/change-ip`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Locations</PageTitle>
      <Card>
        <RequirePermission permissions={Permissions.LOCATIONS_CREATE}>
          <Card.Header>
            <Card.Title className="fw-bolder">Locations</Card.Title>
            <Link
              to="/manager/locations/new"
              className="btn btn-primary align-self-center"
            >
              <FormattedMessage id="BUTTON.LOCATION.NEW" />
            </Link>
          </Card.Header>
        </RequirePermission>
        <Card.Body>
          <FilterUIProvider
            context="locations"
            menuActions={menuActions}
            defaultFilter={defaultFilter}
          >
            <LocationsTable />
          </FilterUIProvider>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
