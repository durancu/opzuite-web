/* eslint-disable no-script-url */
import { TableActionsMenu } from "../../TableActionsMenu";
import { Permissions } from "src/app/constants/Permissions";

export const MGAActionsColumnFormatter = (
  cellContent: any,
  row: any,
  rowIndex: any,
  { openEditMgaPage, openDeleteMgaDialog, openDetailMgaPage }: any
) => (
  <TableActionsMenu
    readFunction={openDetailMgaPage}
    editFunction={openEditMgaPage}
    deleteFunction={openDeleteMgaDialog}
    editPermission={Permissions.APPLICATIONS_UPDATE}
    readPermission={Permissions.APPLICATIONS_READ}
    deletePermission={Permissions.APPLICATIONS_DELETE}
    code={row.code}
  />
);
