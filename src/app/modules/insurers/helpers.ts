const baseInsurer = {
  business: {
    address: {
      address1: "",
      address2: "",
      city: "",
      state: "",
      country: "USA",
      zip: "",
    },
    email: "",
    fax: "",
    industry: "",
    logo: "",
    name: "",
    otherPhones: [""],
    manager: {
      name: "",
      dob: null,
      ssn: "",
      driverLicense: "",
    },
    primaryPhone: null,
    primaryPhoneExtension: "",
    secondaryPhone: null,
    secondaryPhoneExtension: "",
    sector: "",
    startedAt: null,
    type: "",
    website: "",
  },
  contact: {
    address: {
      address1: "",
      address2: "",
      city: "",
      state: "",
      country: "USA",
      zip: "",
    },
    dob: null,
    email: "",
    firstName: "",
    language: "en",
    lastName: "",
    mobilePhone: null,
    phone: null,
    timezone: "US/Central",
    website: "",
  },
  commissions: [],
  company: "",
  type: "BROKER",
};

export const defaultFinancer = {
  ...baseInsurer,
  type: "FINANCER",
};

export const defaultCarrier = {
  ...baseInsurer,
  carriers: [],
  type: "CARRIER",
};

export const defaultMGA = baseInsurer;
