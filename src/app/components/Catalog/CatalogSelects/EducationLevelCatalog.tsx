export const EDUCATION_LEVEL_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "PRESCHOOL", name: "Preschool" },
  { id: "ELEMENTARY", name: "Elementary school" },
  { id: "MIDDLE", name: "Middle school" },
  { id: "HIGH", name: "High school" },
  { id: "ASSOCIATE", name: "Associate's level" },
  { id: "BACHELOR", name: "Bachelor’s level" },
  { id: "MASTER", name: "Master’s level" },
  { id: "DOCTOR", name: "Doctor's level" },
];
