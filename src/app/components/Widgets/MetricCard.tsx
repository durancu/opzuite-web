import { Card, CardGroup, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { formatAmount } from "../functions";
import { metricIdByTitle } from "../functions/metricIdByTitle";
import { getMetricColorByTitle } from "src/app/utils";

type MetricItem = {
  title: string;
  value: number;
};

type Props = {
  metrics: MetricItem[];
};

export const MetricCard = ({ metrics }: Props) => {
  return (
    <CardGroup className="d-flex gap-4">
      {metrics?.map(({ title, value }: any) => {
        return (
          <Card
            className="rounded border border-gray-300 text-center"
            style={{ minWidth: "150px", maxWidth: "150px" }}
            key={title}
          >
            <Card.Title
              className="px-2 py-4"
              style={{
                borderBottomStyle: "solid",
                borderBottomWidth: "3px",
                borderColor: getMetricColorByTitle(title),
              }}
            >
              <span className="fs-3">
                {formatAmount(value)}
                {title === "CHECKSUM SANITY" && (
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip>
                        Do the necesary adjustments to make this value equal to
                        zero (0).
                      </Tooltip>
                    }
                  >
                    <i className="ms-2 fas fa-question-circle text-dark "></i>
                  </OverlayTrigger>
                )}
                {title === "AGENCY COMMISSION" && (
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip>
                        This number doesn&apos;t include commissions from
                        coverage providers yet (only available after saving the
                        policy).
                      </Tooltip>
                    }
                  >
                    <i className="ms-2 fas fa-question-circle text-dark "></i>
                  </OverlayTrigger>
                )}
                {title === "AGENT COMMISSION" && (
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip>
                        This number doesn&apos;t include commissions from
                        premium amount yet (only available after month&apos;s
                        fiscal day).
                      </Tooltip>
                    }
                  >
                    <i className="ms-2 fas fa-question-circle text-dark "></i>
                  </OverlayTrigger>
                )}
              </span>
            </Card.Title>
            <Card.Body className="px-2 py-3">
              <FormattedMessage
                id={`TABLE.HEADER.TOTAL_${metricIdByTitle(title)}`}
              />
            </Card.Body>
          </Card>
        );
      })}
    </CardGroup>
  );
};
