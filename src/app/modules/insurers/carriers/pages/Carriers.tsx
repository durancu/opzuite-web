import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { CarriersTable } from "../components/CarriersTable";
import { defaultFilter } from "../components/helpers";
import { breadcrumbs } from "../breadcrumbs";

const Carriers = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Carriers</PageTitle>
      <RequirePermission permissions={Permissions.CARRIERS_CREATE}>
        <Toolbar>
          <Link
            to="/carriers/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.CARRIER.NEW" />
          </Link>
        </Toolbar>
      </RequirePermission>
      <Card className="p-8">
        <FilterUIProvider
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <CarriersTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};

export default Carriers;
