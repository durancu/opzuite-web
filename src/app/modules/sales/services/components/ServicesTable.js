import PropTypes from "prop-types";
import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { useLocation } from "react-router-dom";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { isPermitted } from "src/app/utils";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { useGetUserAuth } from "src/app/hooks";
import { salesAPI } from "../../redux/saleSlice";
import { FilterForm } from "./FilterForm";
import { initialValues, prepareFilter, sizePerPageList } from "./helpers";

export const ServicesTable = ({ showFilter = true }) => {
  const location = useLocation();
  const user = useGetUserAuth();
  const { context, menuActions, queryParams, updateQueryParams } =
    useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = salesAPI.useGetAllSalesQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "code",
      text: "InZuite ID",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CodeColumnHeaderFormatter,
      formatter: columnFormatters.CodeColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
      },
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "soldAt",
      text: "Date",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.DateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "customerName",
      text: "Customer",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CustomerColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedCustomerNameColumnFormatter,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "totalPremium",
      text: "Amount",
      sort: true,
      headerAlign: "left",
      align: "left",
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.TotalColumnHeaderFormatter,
      formatter: columnFormatters.AmountColumnFormatter,
      hidden: location.pathname.indexOf("permits") === -1,
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "sellerName",
      text: "Agent",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CoordinatorColumnHeaderFormatter,
      formatter: columnFormatters.SellerAndLocationColumnFormatter,
      hidden: !isPermitted(Permissions.APPLICATIONS, user.permissions),
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "left",
      align: "left",
      formatter: columnFormatters.PermitActionsColumnFormatter,
      formatExtraData: {
        openEditPermitPage: menuActions.edit,
        openDeletePermitDialog: menuActions.delete,
        openDetailPermitPage: menuActions.details,
      },
      hidden:
        context !== "sales" ||
        !isPermitted(
          [
            Permissions.APPLICATIONS_UPDATE,
            Permissions.APPLICATIONS_DELETE,
            Permissions.APPLICATIONS_READ,
          ],
          user.permissions
        ),
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      {showFilter && (
        <FilterPanel
          initialValues={initialValues}
          prepareFilter={prepareFilter}
          filterForm={<FilterForm />}
        />
      )}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-left overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="code"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "soldAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

ServicesTable.propTypes = {
  showFilter: PropTypes.bool,
};
