import { useEffect } from "react";
import { lowerFirst } from "lodash";
import { Modal, Button, Spinner } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { salesAPI } from "src/app/modules/sales/redux/saleSlice";
import { useSnackbar } from "notistack";

export const PolicyDeleteDialog = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [deleteSale, { isLoading }] = salesAPI.useDeleteSaleMutation();

  function goBack() {
    navigate("/policies");
  }

  // if !id we should close modal
  useEffect(() => {
    if (!code) goBack();
  }, [code]);

  async function handleDelete() {
    try {
      await deleteSale(code).unwrap();
      enqueueSnackbar("Policy deleted", {
        variant: "success",
      });
      navigate("/policies");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete policy"
      centered
      backdrop="static"
    >
      <Modal.Header closeButton>
        <Modal.Title id="delete policy">
          <FormattedMessage
            id="GENERAL.MODAL.TITLE.DELETE"
            values={{
              model: intl.formatMessage({
                id: "GENERAL.MODEL.POLICY.SINGULAR",
              }),
            }}
          />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormattedMessage
          id="GENERAL.MODAL.TEXT.SURE_WANT_TO_DELETE_PERMANENTLY.MODEL"
          values={{
            this: intl.formatMessage({
              id: "GENERAL.JOINT.THIS.MALE.SINGULAR",
            }),
            model: lowerFirst(
              intl.formatMessage({ id: "GENERAL.MODEL.POLICY.SINGULAR" })
            ),
          }}
        />

        <FormattedMessage
          id="GENERAL.MODAL.ACTION.DELETING"
          values={{
            model: intl.formatMessage({
              id: "GENERAL.MODEL.POLICY.SINGULAR",
            }),
          }}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={goBack} className="btn btn-light btn-elevate">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>

        <Button
          onClick={handleDelete}
          className="btn btn-danger btn-elevate"
          disabled={isLoading}
        >
          {isLoading ? (
            <div className="d-flex gap-3 align-items-center">
              <Spinner />
              <span>Processing</span>
            </div>
          ) : (
            <FormattedMessage id="BUTTON.DELETE" />
          )}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
