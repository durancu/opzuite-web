import clsx from "clsx";
import { Field, FieldArray, useFormikContext } from "formik";
import objectPath from "object-path";
import { Fragment, useEffect, useState } from "react";
import { Col, Form, InputGroup, Row } from "react-bootstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";

const Commissions = () => {
  const { setFieldValue, values, errors } = useFormikContext<any>();
  const [isAllCommissionsState, setIsAllCommissionsState] = useState(false);
  const [allCommissions, setAllCommissions] = useState("0");

  const intl = useIntl();
  const coverages: any = intl.messages["COVERAGES_TYPES"];

  useEffect(() => {
    const commissions = coverages.map((element: any) => ({
      coverage: element.id,
      percent: "0",
    }));

    if (!values?.commissions?.length) {
      setFieldValue("commissions", commissions);
    }
  }, [coverages, setFieldValue, values?.commissions]);

  return (
    <Fragment>
      <RequirePermission permissions={Permissions.MY_COMPANY}>
        <header className="mb-10">
          <h2 className="mb-4 text-primary">That&apos;s all for now! </h2>
          <p className="text-dark-suble">
            Please remind the company owner or someone authorized to set this
            institution commissions. Now you&apos;re ready to save and review.
          </p>
        </header>
      </RequirePermission>
      <RequirePermission permissions={Permissions.MY_COMPANY}>
        <Form.Group className="mb-4">
          <Form.Label>
            <FormattedMessage id="FORM.LABELS.SET_SAME_PERCENT" />
          </Form.Label>
          <Form.Check
            name="allCommissions"
            checked={isAllCommissionsState}
            onChange={() => {
              setIsAllCommissionsState(!isAllCommissionsState);
              const newAllValuesCommissions = values.commissions.map(
                (element: any) => ({ ...element, percent: allCommissions })
              );
              setFieldValue("commissions", newAllValuesCommissions);
            }}
            label={intl.formatMessage({ id: "FORM.LABELS.USE_SAME_PERCENT" })}
          />
        </Form.Group>
        <Form.Group hidden={!isAllCommissionsState}>
          <Form.Label>
            {intl.formatMessage({
              id: "FORM.LABELS.GENERAL_COMMISSION",
            })}
          </Form.Label>
          <InputGroup>
            <Form.Control
              min={0}
              type="number"
              onBlur={({ target }: any) => {
                if (isAllCommissionsState) {
                  setAllCommissions(`${target.value}`);
                  const newAllValuesCommissions = values.commissions.map(
                    (element: any) => ({
                      ...element,
                      percent: `${target.value}`,
                    })
                  );
                  setFieldValue("commissions", newAllValuesCommissions);
                }
              }}
            />
            <InputGroup.Text>%</InputGroup.Text>
          </InputGroup>
        </Form.Group>
        <Row hidden={isAllCommissionsState}>
          <FieldArray
            name="commissions"
            render={() => {
              return values?.commissions.map((element: any, index: number) => {
                const fieldName = `commissions.${index}`;
                const error = objectPath.get(errors, fieldName);
                return (
                  <Col md={3} key={index} className="my-4">
                    <Form.Group>
                      <Form.Label>{coverages[index].name}</Form.Label>
                      <InputGroup>
                        <Field
                          min={0}
                          type="number"
                          required
                          as={Form.Control}
                          name={`${fieldName}.percent`}
                          isInvalid={error ? true : false}
                        />
                        <InputGroup.Text
                          className={clsx({
                            "border-danger": error,
                          })}
                        >
                          %
                        </InputGroup.Text>
                      </InputGroup>

                      <Form.Text className="mt-1 d-block text-danger">
                        {error?.percent}
                      </Form.Text>
                      <Form.Text className="mt-1 d-block">
                        {coverages[index].description}
                      </Form.Text>
                    </Form.Group>
                  </Col>
                );
              });
            }}
          />
        </Row>
      </RequirePermission>
    </Fragment>
  );
};

export default Commissions;
