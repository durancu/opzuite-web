import { useState } from "react";
import { useIntl } from "react-intl";
import { Alert } from "react-bootstrap";
import parse from "html-react-parser";

export const InfoMessageWidget = () => {
  const [show, setShow] = useState(true);
  const intl = useIntl();
  const message = intl.formatMessage({
    id: "CONTENTS.HOME.WELCOME.PARAGRAPH1",
  });

  return (
    <Alert
      show={show}
      variant="primary"
      dismissible
      onClose={() => setShow(false)}
    >
      <div className="d-flex gap-4 align-items-start text-dark">
        <i className="bi bi-exclamation-circle fs-1"></i>
        <div>{parse(message)}</div>
      </div>
    </Alert>
  );
};
