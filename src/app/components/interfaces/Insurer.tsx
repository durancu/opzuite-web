export interface Insurer {
  id: string;
  code: string;
  business: any;
  commissions: any[];
  company: string;
  contact: any;
  createdBy?: string;
  type: string;
  updatedBy?: string;
}
