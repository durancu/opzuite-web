import { shallowEqual, useSelector } from "react-redux";
import { FormAutoComplete } from "../forms";
interface Props {
  helperText?: string;
  helperTextId?: string;
  labelText?: string;
  labelTextId?: string;
  fieldName?: string;
}

export const LocationsAutocompleteField = ({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.LOCATION",
  labelText,
  fieldName = "location",
}: Props) => {
  const { catalogs } = useSelector(
    (reduxState: any) => ({ catalogs: reduxState.catalogs }),
    shallowEqual
  );
  const { entities }: any = catalogs;
  return (
    <FormAutoComplete
      name={fieldName}
      labelText={labelText}
      helperTextId={helperTextId}
      labelTextId={labelTextId}
      helperText={helperText}
      options={entities?.locations || []}
    />
  );
};
