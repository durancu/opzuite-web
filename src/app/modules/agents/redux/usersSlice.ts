import { createSlice } from "@reduxjs/toolkit";
import { API } from "src/redux/api";

const initialAgentsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  userForEdit: undefined,
  lastError: null,
};

export const callTypes = {
  list: "list",
  action: "action",
};

export const userAPI = API.injectEndpoints({
  endpoints: (build) => ({
    getAllUsers: build.query<QueryParams, any>({
      query: (queryParams) => ({
        url: "/users/search",
        method: "post",
        body: { queryParams },
      }),
    }),
  }),
  overrideExisting: false,
});

export const usersSlice = createSlice({
  name: "users",
  initialState: initialAgentsState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getUserById
    userFetched: (state: any, action: any) => {
      state.actionsLoading = false;
      state.userForEdit = action.payload.userForEdit;
      state.error = null;
    },
    // findAgents
    usersFetched: (state: any, action: any) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createUser
    userCreated: (state: any) => {
      state.actionsLoading = false;
      state.error = null;
    },
    // updateUser
    userUpdated: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.userForEdit = action.payload.userForEdit;
    },
    // deleteUser
    userDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => el.id !== action.payload.id
      );
    },
    // deleteAgents
    usersDeleted: (state: any, action: any) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        (el: any) => !action.payload.ids.includes(el.id)
      );
    },
    // usersUpdateState
    usersStatusUpdated: (state: any, action: any) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map((entity: any) => {
        if (ids.findIndex((id: any) => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
  },
});
