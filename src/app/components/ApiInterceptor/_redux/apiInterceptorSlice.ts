import { createSlice } from "@reduxjs/toolkit";

const initialApiInterceptorState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  requestList: [],
  lastRequest: null,
  responseList: [],
  lastResponse: null,
  entities: null,
  lastError: undefined,
  isLoading: false,
};

export const callTypes = {
  list: "list",
  action: "action",
};

export const apiInterceptorsSlice = createSlice({
  name: "apiInterceptor",
  initialState: initialApiInterceptorState,
  reducers: {
    catchError: (state: any, action: any) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state: any, action: any) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    addResponse: (state: any, action: any) => {
      state.listLoading = false;
      state.actionsLoading = false;
      state.lastResponse = action.payload.response;
      // state.responseList.push(action.payload.response)
      state.error = null;
    },
    addError: (state: any, action: any) => {
      state.isLoading = false;
      state.listLoading = false;
      state.actionsLoading = false;
      if (action.payload.response.message === "UNAUTHORIZED_IP_ADDRESS") {
        //action.payload.response.stat
        action.payload.response.statusCode = 403.6;
      }
      state.lastError = action.payload.response;
      state.error = null;
    },
    addRequest: (state: any, action: any) => {
      state.listLoading = false;
      state.actionsLoading = false;
      state.lastRequest = action.payload.request;
      // state.requestList.push(action.payload.request)
      state.error = null;
    },
  },
});
