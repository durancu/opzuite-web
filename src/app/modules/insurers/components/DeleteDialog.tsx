import { useEffect } from "react";
import { Modal, Button, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { insurersAPI } from "../redux/insurerSlice";
import { useSnackbar } from "notistack";

interface Props {
  label?: string;
  path?: string; // callback path
}

// This component is shared with all insurer submodules(mgas, financers, carriers ..)
const DeleteDialog = ({ label = "financer", path = "/financers" }: Props) => {
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [deleteInsurer, { isLoading }] = insurersAPI.useDeleteInsurerMutation();

  function goBack() {
    navigate(path);
  }

  // if !id we should close modal
  useEffect(() => {
    if (!code) goBack();
  }, [code]);

  async function handleDelete() {
    try {
      await deleteInsurer(code).unwrap();
      enqueueSnackbar(`${label} deleted`, {
        variant: "success",
      });
      goBack();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  return (
    <Modal show centered backdrop="static" onHide={goBack}>
      <Modal.Header closeButton>
        <Modal.Title>
          <FormattedMessage
            id="GENERAL.MODAL.TITLE.DELETE"
            values={{
              model: label,
            }}
          />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormattedMessage
          id="GENERAL.MODAL.TEXT.SURE_WANT_TO_DELETE_PERMANENTLY.RECORD"
          values={{
            record: label,
          }}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={goBack} className="btn btn-light btn-elevate">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>

        <Button
          onClick={handleDelete}
          className="btn btn-danger btn-elevate"
          disabled={isLoading}
        >
          {isLoading ? (
            <div className="d-flex gap-3 align-items-center">
              <Spinner />
              <span>Processing</span>
            </div>
          ) : (
            <FormattedMessage id="BUTTON.DELETE" />
          )}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteDialog;
