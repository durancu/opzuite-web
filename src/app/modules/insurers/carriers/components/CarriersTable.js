import PropTypes from "prop-types";
import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  BackdropLoader,
  Error,
  FilterPanel,
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
} from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { insurersAPI } from "../../redux/insurerSlice";
import { defaultSorted, prepareFilter, sizePerPageList } from "./helpers";

export const CarriersTable = ({ showFilter = true }) => {
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetAllInsurersQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });
  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.NameColumnHeaderFormatter,
      formatter: columnFormatters.EnhancedInsurerWithLinkColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
        idField: "code",
        textField: "name",
        defaultText: "N/A",
      },
      align: "left",
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "createdBy",
      text: "Updated By",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CreatedByColumnHeaderFormatter,
      formatter: columnFormatters.CreatedByAndLocationColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "updatedAt",
      text: "Updated At",
      sort: true,
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.LastUpdateColumnHeaderFormatter,
      formatter: columnFormatters.DateTimeColumnFormatter,
      headerAlign: "left",
      align: "left",
      headerClasses: "fw-bold text-muted",
    },
    {
      dataField: "action",
      text: "",
      headerAlign: "right",
      align: "right",
      //headerFormatter: headerFormatters.ActionsColumnHeaderFormatter,
      formatter: columnFormatters.CarrierActionsColumnFormatter,
      formatExtraData: {
        openDetailCarrierPage: menuActions.details,
        openEditCarrierPage: menuActions.edit,
        openDeleteCarrierDialog: menuActions.delete,
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      {showFilter && <FilterPanel prepareFilter={prepareFilter} />}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-left overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="code"
                data={data.entities || []}
                columns={columns}
                defaultSorted={defaultSorted}
                onTableChange={getHandlerTableChange(updateQueryParams)}
                {...paginationTableProps}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

CarriersTable.propTypes = {
  showFilter: PropTypes.bool,
};
