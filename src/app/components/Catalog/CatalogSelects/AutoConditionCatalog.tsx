export const AUTO_CONDITION_CATALOG = [
  { id: "NOT_SPECIFIED", name: "Not Specified" },
  { id: "EXCELLENT", name: "Excellent" },
  { id: "GOOD", name: "Good" },
  { id: "BAD", name: "Bad" },
];
