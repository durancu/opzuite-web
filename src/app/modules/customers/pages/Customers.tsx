import { Fragment } from "react";
import { Card, Stack } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { FilterUIProvider } from "src/app/components";
import { breadcrumbs } from "../breadcrumbs";
import { CustomersTable } from "../components/CustomersTable";
import { defaultFilter } from "../components/helpers";

export const Customers = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string, type: string) => {
      navigate(`${type.toLocaleLowerCase()}/${code}/edit`);
    },
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
    renew: (code: string) => navigate(`${code}/renew`),
    certificates: (customerId: string) => {
      navigate(`/certificates/new?customer=${customerId}`);
    },
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Customers</PageTitle>
      <Toolbar>
        <Stack direction="horizontal" gap={3}>
          <Link
            to="customers/business/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.CUSTOMER.BUSINESS.NEW" />
          </Link>
          <Link
            to="customers/individual/new"
            className="btn btn-primary align-self-center"
          >
            <FormattedMessage id="BUTTON.CUSTOMER.INDIVIDUAL.NEW" />
          </Link>
        </Stack>
      </Toolbar>
      <Card className="p-8">
        <FilterUIProvider
          context="customers"
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <CustomersTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
