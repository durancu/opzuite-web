import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useField } from "formik";

interface Props {
  name: string;
}

export const RichTextEditor = ({ name }: Props) => {
  const [field, , helpers] = useField(name);

  function handleChange(value: string) {
    helpers.setValue(value);
  }
  return (
    <ReactQuill
      theme="snow"
      value={field.value}
      onChange={handleChange}
      className="mb-4 border"
    />
  );
};
