import { useState } from "react";

interface Props {
  children: any;
  item: object;
}

export const Repeater = (props: Props) => {
  const [items, setItems] = useState([props.item]);

  const incrementItems = () => {
    setItems([...items, props.item]);
  };

  const deletePolicyItem = (keys: number) => {
    setItems(items.filter((item: any, key: any) => key !== keys));
  };

  return (
    <>
      {items.map((item: any, key: any) => {
        return (
          <div className="form-group row" key={key}>
            {props.children}
            <div className="col-lg-1">
              <button
                type="button"
                onClick={() => {
                  deletePolicyItem(key);
                }}
                className="btn btn-outline-danger font-size-sm mt-8"
              >
                <i className="fa fa-trash"></i>
              </button>
            </div>
          </div>
        );
      })}
      <button
        type="button"
        className="btn btn-outline-primary"
        onClick={() => {
          incrementItems();
        }}
      >
        <i className="fa fa-plus" />
      </button>
    </>
  );
};
