import moment, { Moment } from "moment";

export const DEFAULT_FORMAT = "YYYY-MM-DD";
export const DEFAULT_DATEPICKER_FORMAT = "MM-dd-yyyy";

export const DateRangeEnum = {
  ALL: "",
  YESTERDAY: "YESTERDAY",
  TODAY: "TODAY",
  WEEK_TO_DATE: "WTD",
  LAST_WEEK: "LAST_WEEK",
  MONTH_TO_DATE: "MTD",
  LAST_MONTH: "LAST_MONTH",
  QUARTER_TO_DATE: "QTD",
  LAST_QUARTER: "LAST_QUARTER",
  YEAR_TO_DATE: "YTD",
  LAST_YEAR: "LAST_YEAR",
  CUSTOM: "CUSTOM",
  THIS_FISCAL_MONTH: "THIS_FISCAL_MONTH",
  LAST_FISCAL_MONTH: "LAST_FISCAL_MONTH",
};

export const dateRangeByName = (
  rangeName: any = DateRangeEnum.MONTH_TO_DATE
  //format: any = "YYYY-MM-DD"
) => {
  switch (rangeName) {
    case DateRangeEnum.YESTERDAY:
      return yesterday();
    case DateRangeEnum.THIS_FISCAL_MONTH:
      return thisPayPeriod();
    case DateRangeEnum.LAST_FISCAL_MONTH:
      return lastPayPeriod();
    case DateRangeEnum.WEEK_TO_DATE:
      return weekToDate();
    case DateRangeEnum.LAST_WEEK:
      return lastWeek();
    case DateRangeEnum.MONTH_TO_DATE:
      return monthToDate();
    case DateRangeEnum.LAST_MONTH:
      return lastMonth();
    case DateRangeEnum.QUARTER_TO_DATE:
      return quarterToDate();
    case DateRangeEnum.LAST_QUARTER:
      return lastQuarter();
    case DateRangeEnum.YEAR_TO_DATE:
      return yearToDate();
    case DateRangeEnum.LAST_YEAR:
      return lastYear();
    case DateRangeEnum.TODAY:
      return today();
    // case DateRangeEnum.ALL:
    default:
      return {
        dateFrom: null,
        dateTo: null,
      };
  }
};

export function thisPayPeriod() {
  const range = {
    start:
      moment().date() > 20
        ? moment().set("date", 21)
        : moment().subtract(1, "months").set("date", 21),
    end:
      moment().date() > 20
        ? moment().add(1, "months").set("date", 20)
        : moment().set("date", 20),
  };

  return {
    dateFrom: range.start.toDate(),
    dateTo: range.end.toDate(),
  };
}

export function lastPayPeriod() {
  const range = {
    start:
      moment().date() > 20
        ? moment().subtract(1, "months").set("date", 21)
        : moment().subtract(2, "months").set("date", 21),
    end:
      moment().date() > 20
        ? moment().set("date", 20)
        : moment().subtract(1, "months").set("date", 20),
  };

  return {
    dateFrom: range.start.toDate(),
    dateTo: range.end.toDate(),
  };
}

export function today() {
  return {
    //"dateFrom": moment().startOf('day').format(format ? format : DEFAULT_FORMAT),
    //"dateTo": moment().startOf('day').format(format ? format : DEFAULT_FORMAT),
    dateFrom: moment().startOf("day").toDate(),
    dateTo: moment().startOf("day").toDate(),
  };
}

export function yesterday() {
  return {
    dateFrom: moment().startOf("day").subtract(1, "day").toDate(),
    dateTo: moment().startOf("day").subtract(1, "day").toDate(),
  };
}

export function weekToDate() {
  return {
    dateFrom: moment().startOf("week").toDate(),
    dateTo: moment().toDate(),
  };
}

export function lastWeek() {
  return {
    dateFrom: moment().startOf("week").subtract(1, "week").toDate(),
    dateTo: moment().endOf("week").subtract(1, "week").endOf("week").toDate(),
  };
}

export function monthToDate() {
  return {
    dateFrom: moment().startOf("month").toDate(),
    dateTo: moment().toDate(),
  };
}

export function lastMonth() {
  return {
    dateFrom: moment().startOf("month").subtract(1, "month").toDate(),
    dateTo: moment()
      .endOf("month")
      .subtract(1, "month")
      .endOf("month")
      .toDate(),
  };
}

export function quarterToDate() {
  return {
    dateFrom: moment().startOf("quarter").toDate(),
    dateTo: moment().toDate(),
  };
}

export function lastQuarter() {
  return {
    dateFrom: moment().startOf("quarter").subtract(1, "quarter").toDate(),
    dateTo: moment()
      .endOf("quarter")
      .subtract(1, "quarter")
      .endOf("quarter")
      .toDate(),
  };
}

export function yearToDate() {
  return {
    dateFrom: moment().startOf("year").toDate(),
    dateTo: moment().toDate(),
  };
}

export function lastYear() {
  return {
    dateFrom: moment().startOf("year").subtract(1, "year").toDate(),
    dateTo: moment().endOf("year").subtract(1, "year").endOf("year").toDate(),
  };
}

export function addYearTo(date: Date) {
  let momentDate: Moment = moment(date);
  momentDate = momentDate.add(1, "year");
  return momentDate.toDate();
}

export function addHalfYearTo(date: Date) {
  let momentDate: Moment = moment(date);
  momentDate = momentDate.add(6, "month");
  return momentDate.toDate();
}

export function addQuarterTo(date: Date) {
  let momentDate: Moment = moment(date);
  momentDate = momentDate.add(1, "quarter");
  return momentDate.toDate();
}

export function addMonthTo(date: Date) {
  let momentDate: Moment = moment(date);
  momentDate = momentDate.add(1, "month");
  return momentDate.toDate();
}

export function expirationDateByRenewalFrequency(
  renewalFrequency: string,
  effectiveAt: Date
) {
  switch (renewalFrequency) {
    case "SEMI-ANNUAL":
      return addHalfYearTo(effectiveAt);
    case "QUARTERLY":
      return addQuarterTo(effectiveAt);
    case "MONTHLY":
      return addMonthTo(effectiveAt);
    case "ANNUAL":
    default:
      return addYearTo(effectiveAt);
  }
}
