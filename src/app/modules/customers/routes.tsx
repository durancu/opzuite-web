// routes module, to be imported i src/Routes
import { RouteObject } from "react-router-dom";
import { RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { CustomerDeleteDialog } from "./pages/CustomerDeleteDialog";
import { CustomerDetailPage } from "./pages/CustomerDetailPage";
import { Customers } from "./pages/Customers";
import { CustomerEdit } from "./pages/customer-edit/CustomerEdit";

export const customerRoutes: RouteObject[] = [
  {
    index: true,
    element: (
      <RequirePermission permissions={Permissions.CUSTOMERS_READ}>
        <Customers />
      </RequirePermission>
    ),
  },
  {
    path: ":type/new",
    element: (
      <RequirePermission permissions={Permissions.CUSTOMERS_CREATE}>
        <CustomerEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":type/:code/edit",
    element: (
      <RequirePermission permissions={Permissions.CUSTOMERS_UPDATE}>
        <CustomerEdit />
      </RequirePermission>
    ),
  },
  {
    path: ":code/detail",
    element: (
      <RequirePermission permissions={Permissions.CUSTOMERS_READ}>
        <CustomerDetailPage />
      </RequirePermission>
    ),
  },
  {
    path: ":code/delete",
    element: <CustomerDeleteDialog />,
  },
];
