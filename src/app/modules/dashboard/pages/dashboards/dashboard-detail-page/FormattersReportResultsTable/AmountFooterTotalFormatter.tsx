export const AmountFooterTotalFormatter = (
  column: any,
  colIndex: any,
  { text }: any
) => <span style={{ float: "right" }}>${text.toFixed(2)}</span>;
