import { Fragment, useEffect } from "react";
import { useSnackbar } from "notistack";
import { useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { BackdropLoader, Error } from "src/app/components";

import {
  FinancerDetails,
  FinancerOverview,
  PageDetailTopButtons,
} from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { insurersAPI } from "../../redux/insurerSlice";
import { breadcrumbs } from "../breadcrumbs";

const Details = () => {
  const intl = useIntl();
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: financer,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(`${code}?layout=FULL`, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [deleteFinancer, { isLoading: isDeleting }] =
    insurersAPI.useDeleteInsurerMutation();

  const goToEditFinancer = () => {
    navigate(`/financers/${code}/edit`);
  };

  async function handleDelete() {
    try {
      await deleteFinancer(code).unwrap();
      enqueueSnackbar("Financer deleted", {
        variant: "success",
      });
      navigate("/financers");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  useEffect(() => {
    if (!code) navigate(-1);
  }, [code]);

  if (isLoading || isDeleting) return <BackdropLoader />;

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.FINANCER" })}
      </PageTitle>
      <Toolbar>
        <PageDetailTopButtons
          editFunction={goToEditFinancer}
          deleteFunction={handleDelete}
          editPermission={Permissions.FINANCERS_UPDATE}
          deletePermission={Permissions.FINANCERS_DELETE}
          componentLabel="financer"
        />
      </Toolbar>

      <FinancerOverview financer={financer} />
      <FinancerDetails financer={financer} />
    </Fragment>
  );
};

export default Details;
