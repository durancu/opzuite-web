import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useIntl } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import {
  WizardForm,
  WizardFormStep,
  BackdropLoader,
  Error,
} from "src/app/components";
import {
  carrierBusinessValidationSchema,
  carrierCommissionsValidationSchema,
  carrierContactValidationSchema,
} from "./schema";
import {
  BusinessInfo,
  ContactInfo,
  Commissions,
} from "src/app/modules/insurers/components";
import { FormikValues } from "formik";
import { insurersAPI } from "src/app/modules/insurers/redux/insurerSlice";
import { useSnackbar } from "notistack";
import { defaultCarrier } from "src/app/modules/insurers/helpers";
import { breadcrumbs } from "../../breadcrumbs";

const Edit = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const {
    data: carrier,
    isLoading,
    isError,
    refetch,
  } = insurersAPI.useGetInsurerByCodeQuery(code, {
    skip: !code,
    refetchOnFocus: true,
    refetchOnReconnect: true,
    refetchOnMountOrArgChange: true,
  });

  const [createCarrier, { isLoading: isCreating }] =
    insurersAPI.useCreateInsurerMutation();

  const [updateCarrier, { isLoading: isUpdating }] =
    insurersAPI.useUpdateInsurerMutation();

  const intl = useIntl();
  const pageHeaderLangId = !code
    ? "FORM.TITLE.CARRIER.NEW"
    : "FORM.TITLE.CARRIER.EDIT";

  const backToCarriersList = () => {
    navigate("/carriers");
  };

  async function handleCreate(values: FormikValues) {
    try {
      const response = await createCarrier(values).unwrap();
      enqueueSnackbar("Carrier created successfully", {
        variant: "success",
      });
      navigate(`/carriers/${response.code}/detail`);
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  async function handleUpdate(values: FormikValues) {
    try {
      await updateCarrier(values).unwrap();
      enqueueSnackbar("Carrier updated successfully", {
        variant: "success",
      });
      backToCarriersList();
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  if (isLoading || isCreating || isUpdating) {
    return <BackdropLoader />;
  }

  if (isError) {
    return <Error show={isError} action={refetch} />;
  }

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: pageHeaderLangId })}
      </PageTitle>
      <Card>
        <WizardForm
          initialValues={code ? carrier : defaultCarrier}
          onSubmit={code ? handleUpdate : handleCreate}
          onCancel={backToCarriersList}
        >
          <WizardFormStep
            labelTextId="WIZARD.BUSINESS_INFO"
            validationSchema={carrierBusinessValidationSchema}
          >
            <BusinessInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.PERSONAL_INFO"
            validationSchema={carrierContactValidationSchema}
          >
            <ContactInfo />
          </WizardFormStep>
          <WizardFormStep
            labelTextId="WIZARD.COMMISSIONS"
            validationSchema={carrierCommissionsValidationSchema}
          >
            <Commissions />
          </WizardFormStep>
        </WizardForm>
      </Card>
    </Fragment>
  );
};

export default Edit;
