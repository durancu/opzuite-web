import { useSnackbar } from "notistack";
import { Fragment, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { FilterUIProvider } from "src/app/components/Filter";
import { PageDetailTopButtons } from "src/app/components/Widgets";
import { Permissions } from "src/app/constants/Permissions";
import { useAppDispatch } from "src/app/hooks";
import * as actions from "../../_redux/reportsActions";
import { ReportOverview } from "../../components/ReportOverview";

import { Card, ProgressBar } from "react-bootstrap";
import { useIntl } from "react-intl";
import { PageTitle } from "src/_metronic/layout/core";
import { breadcrumbs } from "src/app/modules/adminConsole/breadcrumbs";
import { DetailsTable } from "./components/DetailsTable";

export const Details = () => {
  const { code } = useParams();
  const intl = useIntl();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const { actionsLoading, reportForEdit, error, user } = useSelector(
    (state: any) => ({
      user: state.auth.user,
      actionsLoading: state.reports.actionsLoading,
      reportForEdit: state.reports.reportForEdit,
      error: state.reports.error,
    }),
    shallowEqual
  );

  const { enqueueSnackbar } = useSnackbar();
  useEffect(() => {
    error &&
      enqueueSnackbar(error, {
        variant: "error",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right",
        },
      });
    error && navigate(`/not-found`);
  }, [enqueueSnackbar, error]);

  const goToEditReport = () => {
    navigate(`/manager/reports/${code}/edit`);
  };

  const deleteReport = () =>
    dispatch(actions.deleteReport(code)).then(() => {
      navigate("/manager/reports");
    });

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>
        {intl.formatMessage({ id: "GENERAL.DETAILS.REPORT" })}
      </PageTitle>
      <div className="d-flex justify-content-between align-items-center py-4">
        <h3 className="fw-bolder">Report Details</h3>
        {reportForEdit?.createdBy._id === user.id && (
          <PageDetailTopButtons
            editFunction={goToEditReport}
            deleteFunction={deleteReport}
            editPermission={Permissions.REPORTS_UPDATE}
            deletePermission={Permissions.REPORTS_DELETE}
            componentLabel="report"
          />
        )}
      </div>
      {actionsLoading && <ProgressBar />}
      {reportForEdit && <ReportOverview report={reportForEdit} />}

      <Card className="mb-4">
        <Card.Header>
          <Card.Title as="h3" className="text-primary">
            Details
          </Card.Title>
        </Card.Header>

        <Card.Body className="p-8">
          <FilterUIProvider>
            <DetailsTable />
          </FilterUIProvider>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
