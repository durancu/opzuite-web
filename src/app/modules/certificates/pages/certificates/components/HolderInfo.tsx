import { Col, Row } from "react-bootstrap";
import {
  FormAutoComplete,
  FormInput,
  PhoneNumberInput,
} from "src/app/components";
import { useGetCatalog } from "src/app/components/Catalog/hooks";

export const HolderInfo = () => {
  // fetch catalogs
  const catalog = useGetCatalog();

  return (
    <div className="border-bottom mb-4">
      <Row className="mb-4">
        <Col>
          <FormInput name="business.name" labelTextId="FORM.LABELS.NAME" />
        </Col>

        <Col>
          <FormInput
            type="email"
            name="business.email"
            labelTextId="FORM.LABELS.EMAIL"
          />
        </Col>
        <Col>
          <PhoneNumberInput name="business.phone" labelTextId="FORM.LABELS.PHONE_NUMBER" />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormInput
            name="business.address.address1"
            labelTextId="FORM.LABELS.ADDRESS_LINE_1"
          />
        </Col>

        <Col>
          <FormInput
            name="business.address.address2"
            labelTextId="FORM.LABELS.ADDRESS_LINE_2"
          />
        </Col>
      </Row>

      <Row className="mb-4">
        <Col>
          <FormInput
            name="business.address.city"
            labelTextId="FORM.LABELS.CITY"
          />
        </Col>

        <Col>
          <FormAutoComplete
            name="business.address.state"
            labelTextId="FORM.LABELS.STATE"
            options={catalog?.states || []}
          />
        </Col>
      </Row>

      <Row>
        <Col>
          <FormInput
            name="business.address.zip"
            labelTextId="FORM.LABELS.ZIP_CODE"
          />
        </Col>
        <Col>
          <FormAutoComplete
            name="business.address.country"
            labelTextId="FORM.LABELS.COUNTRY"
            helperTextId="FORM.LABELS.HELPER.COUNTRIES"
            options={catalog?.countries || []}
          />
        </Col>
      </Row>
    </div>
  );
};
