type Align = "left" | "center" | "right";

export const AlignFormatter = (align: Align = "left") => align;
