import axios from "axios";

export const LOGIN_URL = "api/v2/auth/login";
export const REGISTER_URL = "api/v2/auth/register";
export const VERIFY_EMAIL_ADDRESS = "api/v2/auth/confirm-email";
export const REQUEST_PASSWORD_URL = "api/v2/auth/forgot-password";
export const RESET_PASSWORD_URL = "api/v2/auth/reset-password";

export const ME_URL = "api/v2/profile";

export function login(email: string, password: string) {
  return axios.post(LOGIN_URL, { email, password });
}

export function register(
  email: string,
  fullname: string,
  username: string,
  password: string
) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email: string) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function resetPassword(token: string, password: string) {
  return axios.post(RESET_PASSWORD_URL, { token, password });
}

export function confirmEmailAddress(confirmToken: string) {
  return axios.post(VERIFY_EMAIL_ADDRESS, { confirmToken });
}

export function getUserByToken() {
  return axios.get(
    `${ME_URL}?withRole=true&withCompany=true&withLocation=false&withSupervisor=false`
  );
}
