import { useFormik } from "formik";
import { useState } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link, Navigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import { resetPassword } from "../__mocks__/authCrud";

const initialValues = {
  token: "",
  password: "",
  confirmPassword: "",
};

function ResetPassword(props: any) {
  const { passwordToken }: any = useParams();
  const { intl } = props;
  const [isRequested, setIsRequested] = useState(false);
  const [message, setMessage] = useState<any>({
    text: "",
    type: "info",
  });

  const ForgotPasswordSchema = Yup.object().shape({
    password: Yup.string()
      .min(3, "Minimum 3 symbols")
      .max(50, "Maximum 50 symbols")
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      ),
    confirmPassword: Yup.string()
      .required(
        intl.formatMessage({
          id: "AUTH.VALIDATION.REQUIRED_FIELD",
        })
      )
      .when("password", {
        is: (val: string) => (val && val.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("password")],
          "Passwords don't match"
        ),
      }),
  });

  const getInputClasses = (fieldname: string) => {
    if (formik.touched[fieldname] && formik.errors[fieldname]) {
      return "is-invalid";
    }

    if (formik.touched[fieldname] && !formik.errors[fieldname]) {
      return "is-valid";
    }

    return "";
  };

  const formik: any = useFormik({
    initialValues,
    validationSchema: ForgotPasswordSchema,
    onSubmit: (values, { setStatus }) => {
      resetPassword(passwordToken, values.password).then((res: any) => {
        if (res.errorCode) {
          if (res.statusCode && res.statusCode === 404) {
            setStatus(
              "Invalid token or expired. Please click Forgot Password to request a new one."
            );
            setMessage({
              text: "Invalid token or expired. Please click Forgot Password to request a new one.",
              type: "error",
            });
          }
          setIsRequested(true);
        } else {
          setStatus("Password was resetted successfully.");
          setMessage({
            text: "Password was resetted successfully.",
            type: "success",
          });
          setIsRequested(true);
        }
      });
    },
  });

  return (
    <>
      {isRequested && <Navigate to="/auth/login" state={{ ...message }} />}
      {!isRequested && (
        <div className="login-form login-forgot" style={{ display: "block" }}>
          <div className="text-center mb-10 mb-lg-20">
            <h3 className="font-size-h1">Reset Password</h3>
            <div className="text-muted font-weight-bold">
              Enter your new password and confirmation
            </div>
          </div>
          <form
            onSubmit={formik.handleSubmit}
            className="form fv-plugins-bootstrap fv-plugins-framework animated animate__animated animate__backInUp"
          >
            {formik.status && (
              <div className="mb-10 alert alert-custom alert-light-danger alert-dismissible">
                <div className="alert-text font-weight-bold">
                  {formik.status}
                </div>
              </div>
            )}
            <div className="form-group fv-plugins-icon-container">
              <input
                type="password"
                className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
                  "password"
                )}`}
                name="password"
                placeholder="Your new password"
                {...formik.getFieldProps("password")}
              />
              {formik.touched.password && formik.errors.password ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">{formik.errors.password}</div>
                </div>
              ) : null}
            </div>
            <div className="form-group fv-plugins-icon-container">
              <input
                type="password"
                className={`form-control form-control-solid h-auto py-5 px-6 ${getInputClasses(
                  "password"
                )}`}
                name="confirmPassword"
                placeholder="Confirm your new password"
                {...formik.getFieldProps("confirmPassword")}
              />
              {formik.touched.confirmPassword &&
              formik.errors.confirmPassword ? (
                <div className="fv-plugins-message-container">
                  <div className="fv-help-block">
                    {formik.errors.confirmPassword}
                  </div>
                </div>
              ) : null}
            </div>
            <div className="form-group d-flex flex-wrap flex-center">
              <Link to="/auth">
                <button
                  type="button"
                  id="kt_login_forgot_cancel"
                  className="btn btn-light-dark font-weight-bold px-9 py-4 my-3 mx-5"
                >
                  <FormattedMessage id="BUTTON.CANCEL" />
                </button>
              </Link>
              <button
                id="kt_login_forgot_submit"
                type="submit"
                className="btn btn-dark font-weight-bold px-9 py-4 my-3 mx-5"
                disabled={formik.isSubmitting}
              >
                Submit
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
}

export default injectIntl(ResetPassword);
