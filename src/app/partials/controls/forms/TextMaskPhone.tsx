import MaskedInput from "react-text-mask";
import { PHONE_NUMBER_MASK } from "src/app/constants/RegularExpressions";

export const TextMaskPhone = (props: any) => {
  return (
    <MaskedInput
      {...props}
      ref={(ref: any) => ref}
      mask={PHONE_NUMBER_MASK}
      placeholderChar={"\u2000"}
    />
  );
};
