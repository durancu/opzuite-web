export const buildObjectByPath = (path: string, value: any) => {
  const keys = path.split(".");
  const result: any = [];
  let object = {};
  const i = 0;
  keys
    .slice()
    .reverse()
    .forEach((key: any) => {
      if (isNaN(parseInt(key))) {
        object = { [key]: i === 0 ? value : object };
      } else {
        result.push(object);
      }
    });
  return result;
};
