import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { RequirePermission, FilterUIProvider } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { breadcrumbs } from "../../../breadcrumbs";
import { AgentsTable } from "../components/AgentsTable";
import { defaultFilter } from "../components/helpers";

export const Agents = () => {
  const navigate = useNavigate();
  const userCreatePermissions = [Permissions.USERS, Permissions.USERS_CREATE];

  const menuActions = {
    edit: (id: string) => navigate(`${id}/edit`),
    details: (id: string) => navigate(`${id}/detail`),
    delete: (id: string) => navigate(`${id}/delete`),
    changePassword: (id: string) => navigate(`${id}/change-password`),
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Employees</PageTitle>
      <Card>
        <Card.Header>
          <Card.Title className="fw-bolder">Employees</Card.Title>
          <RequirePermission permissions={userCreatePermissions}>
            <Link
              to="/manager/users/new"
              className="btn btn-primary align-self-center"
            >
              <FormattedMessage id="BUTTON.EMPLOYEE.NEW" />
            </Link>
          </RequirePermission>
        </Card.Header>
        <Card.Body className="p-8">
          <FilterUIProvider
            context="users"
            menuActions={menuActions}
            defaultFilter={defaultFilter}
          >
            <AgentsTable />
          </FilterUIProvider>
        </Card.Body>
      </Card>
    </Fragment>
  );
};
