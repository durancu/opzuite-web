/**
 * Create React App entry point. This and `public/index.html` files can not be
 * changed or moved.
 */
import React from "react";
import ReactDOM from "react-dom/client";
import axios from "axios";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { RouterProvider } from "react-router-dom";

import * as _redux from "./redux";
import store, { persistor } from "./redux/store";

import { MetronicI18nProvider } from "./_metronic/i18n/Metronici18n";
import { LayoutSplashScreen } from "./_metronic/layout/core";
import router from "./Routes";

// style imports
import "./index.scss"; // Standard version
/**
 * Creates `axios-mock-adapter` instance for provided `axios` instance, add
 * basic Metronic mocks and returns it.
 *
 * @see https://github.com/ctimmerm/axios-mock-adapter
 */
/* const mock = */ //_redux.mockAxios(axios);

/**
 * Inject metronic interceptors for axios.
 *
 * @see https://github.com/axios/axios#interceptors
 */

axios.defaults.baseURL = process.env.REACT_APP_API_URL;
_redux.setupAxios(axios, store);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={<LayoutSplashScreen />}>
        <MetronicI18nProvider>
          <RouterProvider router={router} />
        </MetronicI18nProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>
);
