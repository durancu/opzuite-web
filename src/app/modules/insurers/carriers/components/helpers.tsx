import { FormikValues } from "formik";
export const CarrierStatusCssClasses = ["success", "info", ""];
export const CarrierStatusTitles = ["Selling", "Sold"];
export const CarrierConditionCssClasses = ["success", "danger", ""];
export const CarrierConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "code", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "20", value: 20 },
  { text: "50", value: 50 },
  { text: "100", value: 100 },
];

const searchColumns = ["email", "name", "phone"];

export const defaultFilter = {
  filter: {
    type: "CARRIER",
    name: "",
  },
  sortOrder: "asc",
  sortField: "name",
  pageNumber: 1,
  pageSize: 20,
};

export const prepareFilter = (values: FormikValues) => {
  // Init filter setting type
  const filter: any = {
    type: "CARRIER",
  };

  // check and remove empty or nullish filters
  for (const key in values) {
    if (values[key]) {
      filter[key] = values[key];
    }
  }

  return {
    filter,
    searchColumns,
    searchText: values.searchText,
    monthsToExpiration: 3,
    sortOrder: "desc",
    sortField: "soldAt",
    pageNumber: 1,
    pageSize: 20,
  };
};
