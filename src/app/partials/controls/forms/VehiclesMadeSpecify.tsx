import { FormControl, MenuItem, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import * as CATALOG from "../../../components/Catalog/CatalogSelects";

const getYearsCatalog = async () => {
  const data = [];
  const thisYear: number = new Date().getFullYear();

  for (let i = thisYear; i > thisYear - 25; i--) {
    data.push(i);
  }

  return data;
};

interface Props {
  handleChange: any;
  value: {
    year: string;
    make: string;
    model: string;
    type: string;
  };
}

export const VehiclesMadeSpecify = ({ handleChange, value }: Props) => {
  const [yearsList, setYearsList] = useState<any>([]);

  useEffect(() => {
    getYearsCatalog().then((res) => {
      setYearsList(res || []);
    });
  }, []);

  return (
    <>
      <Row className="form-group">
        <Col md={3}>
          <FormControl>
            <TextField
              select={true}
              name={`year`}
              label="Year"
              onChange={(e) => {
                handleChange(e);
              }}
              value={value.year}
            >
              {yearsList.length > 0 ? (
                yearsList.map((year: string) => (
                  <MenuItem key={year} value={year}>
                    {year}
                  </MenuItem>
                ))
              ) : (
                <MenuItem value="">Not have years</MenuItem>
              )}
            </TextField>
          </FormControl>
        </Col>
        <Col sm="3">
          <FormControl disabled={!value.year}>
            <TextField
              name={"make"}
              label="Make"
              onChange={(e) => {
                handleChange(e);
              }}
              value={value.make}
            />
          </FormControl>
        </Col>
        <Col sm="3">
          <FormControl>
            <TextField
              name={"model"}
              label="Model"
              onChange={(e) => {
                handleChange(e);
              }}
              value={value.model}
            />
          </FormControl>
        </Col>
        <Col sm="3">
          <FormControl>
            <TextField
              select={true}
              name={`type`}
              label="Type"
              onChange={(e) => {
                handleChange(e);
              }}
              value={value.type}
            >
              {CATALOG.AUTO_TYPE_COMMERCIAL_CATALOG.map(({ id, name }) => (
                <MenuItem key={id} value={id}>
                  {name}
                </MenuItem>
              ))}
            </TextField>
          </FormControl>
        </Col>
      </Row>
    </>
  );
};
