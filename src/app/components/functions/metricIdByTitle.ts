import { upperCase } from "lodash";

export const metricIdByTitle = (title: any) => {
  return upperCase(title).replace(/ /g, "_");
};
