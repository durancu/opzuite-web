import { useEffect } from "react";
import { Modal, Button, Spinner } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { useNavigate, useParams } from "react-router-dom";
import { salesAPI } from "src/app/modules/sales/redux/saleSlice";
import { useSnackbar } from "notistack";

export const ServiceDeleteDialog = () => {
  const { code } = useParams();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [deleteSale, { isLoading }] = salesAPI.useDeleteSaleMutation();

  function goBack() {
    navigate("/services");
  }

  // if !code we should close modal
  useEffect(() => {
    if (!code) goBack();
  }, [code]);

  async function handleDelete() {
    try {
      await deleteSale(code).unwrap();
      enqueueSnackbar("Service deleted", {
        variant: "success",
      });
      navigate("/services");
    } catch (error) {
      enqueueSnackbar("An error occured", {
        variant: "error",
      });
    }
  }

  return (
    <Modal
      show
      onHide={goBack}
      aria-labelledby="delete service"
      centered
      backdrop="static"
    >
      <Modal.Header closeButton>
        <Modal.Title id="delete service">Delete Service</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <span>Are you sure to permanently delete this service?</span>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={goBack} className="btn btn-light btn-elevate">
          <FormattedMessage id="BUTTON.CANCEL" />
        </Button>

        <Button
          onClick={handleDelete}
          className="btn btn-danger btn-elevate"
          disabled={isLoading}
        >
          {isLoading ? (
            <div className="d-flex gap-3 align-items-center">
              <Spinner />
              <span>Processing</span>
            </div>
          ) : (
            <FormattedMessage id="BUTTON.DELETE" />
          )}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
