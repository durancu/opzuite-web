import * as Yup from "yup";

// policy.items[item].details
export const detailSchema = Yup.object().shape({
  autoCoverageScope: Yup.string().oneOf(["ANY", "SCHEDULED"]),
  includeOwnedAutos: Yup.bool(),
  includeHiredAutos: Yup.bool(),
  includeNonOwnedAutos: Yup.bool(),
  includeOtherCoveredAutoA: Yup.bool(),
  otherCoveredAutoADescription: Yup.string().when("includeOtherCoveredAutoA", {
    is: true,
    then: (schema) => schema.required("Please enter a description"),
  }),
  includeOtherCoveredAutoB: Yup.bool(),
  otherCoveredAutoBDescription: Yup.string().when("includeOtherCoveredAutoB", {
    is: true,
    then: (schema) => schema.required("Please enter a description"),
  }),
  combinedSingleLimit: Yup.number(),
  bodilyInjuryPerPersonLimit: Yup.number(),
  bodilyInjuryPerAccidentLimit: Yup.number(),
  propertyDamagePerAccidentLimit: Yup.number(),
  coverageFormUsed: Yup.string().oneOf(["UMBRELLA", "EXCESS"]),
  policyBasedForm: Yup.string().oneOf(["CLAIMS", "OCCURENCE"]),
  includeOtherCoverageA: Yup.bool(),
  otherCoverageADescription: Yup.string().when("includeOtherCoverageA", {
    is: true,
    then: (schema) => schema.required("Please enter a description"),
  }),
  includeOtherCoverageB: Yup.bool(),
  otherCoverageBDescription: Yup.string().when("includeOtherCoverageB", {
    is: true,
    then: (schema) => schema.required("Please enter a description"),
  }),
  generalAggregateLimitAppliesPer: Yup.string().oneOf([
    "POLICY",
    "PROJECT",
    "LOCATION",
    "OTHER",
  ]),
  generalAggregateLimitAppliesPerOther: Yup.string().nullable(),
  additionalInsured: Yup.bool(),
  waiverOfSubrogation: Yup.bool(),
  hasDeductible: Yup.bool(),
  hasRetention: Yup.bool(),
  retention: Yup.number(),
  eachOccurrenceLimit: Yup.number(),
  damageToRentedPremisesLimit: Yup.number(),
  medicalExpensesLimit: Yup.number(),
  generalAggregateLimit: Yup.number(),
  productCompletedOperationsAggregateLimit: Yup.number(),
  other1LimitName: Yup.string(),
  other1LimitAmount: Yup.number(),
  other2LimitName: Yup.string(),
  other2LimitAmount: Yup.number(),
  hasExcludedPeople: Yup.bool(),
  excludedPeopleDescription: Yup.string().when("hasExcludedPeople", {
    is: true,
    then: (schema) => schema.required("Please enter a description"),
  }),
  perStatute: Yup.bool(),
  other: Yup.bool(),
  employerLiabilityEachAccidentLimit: Yup.number(),
  employerLiabilityEachEmployeeLimit: Yup.number(),
  employerLiabilityDiseasePolicyLimit: Yup.number(),
});

// policy.items[item].history
export const historySchema = Yup.object().shape({
  customer: Yup.string(),
  policy: Yup.object(),
  action: Yup.string(),
  deleted: Yup.string(),
  updatedAt: Yup.mixed(),
  updatedBy: Yup.string(),
});

// policy.items[item]
export const policyItemSchema = Yup.object().shape({
  amount: Yup.number(),
  broker: Yup.string(),
  carrier: Yup.string().required("Carrier is required"),
  premium: Yup.number(),
  product: Yup.string().required("Product is required"),
  seller: Yup.string().required("Seller is required"),
  status: Yup.string().required("Status is required"),
  lineOfBusiness: Yup.string().required("Line of Business is required"),
  deductible: Yup.number(),
  compensation: Yup.number(),
  limit: Yup.number(),
  updatedBy: Yup.string(),
  createdBy: Yup.string(),
  history: Yup.array().of(historySchema),
  details: detailSchema,
});

// endorsement.items[item].renewalFrequencies
export const renewalReferenceSchema = Yup.object().shape({
  customer: Yup.string(), //ID del customer de la venta a la que pertenece
  policy: Yup.object(), // Item que se va a reemplazar
  policyNumber: Yup.string(), // item.number
  renewedAt: Yup.mixed(), // today
  seller: Yup.string().nullable(), // USuario que realiza la accion de renovar (autenticado)
});

// endorsement.items[item]
export const endorsementItemSchema = Yup.object().shape({
  type: Yup.string().required("Type is required"),
  amount: Yup.number().required("Amount is required"),
  status: Yup.string(), // .required("Status is required.")
  seller: Yup.string().nullable(), //remove hard requirement as it breaks if user role is missing
  endorsedAt: Yup.date()
    .required("Date is required.")
    .typeError("Please enter a valid Date"),
  description: Yup.string(),
  followUpDate: Yup.mixed().nullable(),
  followUpPerson: Yup.string().nullable(),
  accountingClass: Yup.string().oneOf([
    "AGENT_COMMISSION",
    "AGENCY_COMMISION",
    "FEE_TAX",
    "FINANCING_DIRECT_BILL",
    "PAYABLE",
    "RECEIVABLE",
  ]),
  renewalReferences: Yup.array().of(renewalReferenceSchema),
  financerCompany: Yup.string(),
  // .when("accountingClass", {
  //   is: (value: string) => value === "FINANCING_DIRECT_BILL",
  //   then: (schema) =>
  //     schema.required("Field Finance Company (PFA) is required."),
  // }),
  otherDetails: Yup.object().when("accountingClass", {
    is: (value: string) => ["FEE_TAX", "PAYABLE"].includes(value),
    then: (schema) =>
      schema.shape({
        remitTo: Yup.string() /* .required("Remit To is required.") */,
        description: Yup.string(),
      }),
    otherwise: (schema) =>
      schema.shape({
        remitTo: Yup.string(),
        description: Yup.string(),
      }),
  }),
});

// policy.endorsements
export const endorsementSchema = Yup.object().shape({
  type: Yup.string().required("Type is required"),
  amount: Yup.number().required("Amount is required"),
  status: Yup.string().required("Status is required."),
  seller: Yup.string().nullable(), //remove hard requirement as it breaks if user role is missing
  endorsedAt: Yup.date()
    .required("Date is required.")
    .typeError("Please enter a valid Date"),
  description: Yup.string(),
  followUpDate: Yup.mixed().nullable(),
  followUpPerson: Yup.string().nullable(),
  items: Yup.array().of(endorsementItemSchema),
});

// policy
export const policyBasicInfoValidationSchema = Yup.object().shape({
  customer: Yup.string().required("Insured is required"),
  soldAt: Yup.mixed().required("Policy Date is required"),
  seller: Yup.string().nullable(), //remove hard requirement as it breaks if user role is missing
  status: Yup.string().required("Status is required"),
  effectiveAt: Yup.mixed(),
  expiresAt: Yup.mixed(),
  autoRenew: Yup.boolean(),
  cancelledAt: Yup.mixed(),
  number: Yup.string(),
});

export const policyDetailValidationSchema = Yup.object().shape({
  items: Yup.array().of(policyItemSchema),
  endorsements: Yup.array().of(endorsementSchema),
});
