import Prism from "prismjs";
import "prismjs/components/prism-json";
import Editor from "react-simple-code-editor";
import styled from "styled-components";
import "./theme.css";
export const JSONViewer = ({ data }: any) => {
  return (
    <StyledEditor
      value={JSON.stringify(data, null, 4)}
      highlight={(code: any) =>
        Prism.highlight(code || "", Prism.languages.json, "json")
      }
      padding={5}
      onValueChange={() => {
        // do nothing
      }}
    />
  );
};

const StyledEditor = styled(Editor)`
  background: black;
  font-family: "Fira code", "Fira Mono", monospace;
  font-size: 14px;
  min-height: 100px;

  & :focus {
    outline: none;
  }
`;
