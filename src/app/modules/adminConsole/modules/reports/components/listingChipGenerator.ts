import { startCase } from "lodash";
import moment from "moment";

const getOptionsInCatalog = (field: string, catalog: any) => {
  switch (field) {
    case "broker":
      return catalog?.brokers;
    case "carrier":
      return catalog?.carriers;
    case "country":
      return catalog?.countries;
    case "customer":
      return catalog?.customers;
    case "location":
      return catalog?.locations;
    case "seller":
      return catalog?.users;
    case "status":
      return catalog?.saleStatus;
    case "type":
      return catalog?.saleTypes;
    default:
      return [];
  }
};

export const listingChipGenerator = (object: any, catalog: any): any[] => {
  const listFilter: any[] = [];

  Object.entries(object).forEach(([key, value]: any) => {
    switch (key) {
      case "filters":
        value.length &&
          value.forEach(({ field, value }: any) => {
            listFilter.push({
              label: `${startCase(field)}: ${
                getOptionsInCatalog(field, catalog)?.find(
                  ({ id }: any) => id === value
                ).name
              }`,
            });
          });
        break;

      case "dateFrom":
        value &&
          listFilter.push({
            label: `From${value && `: ${moment(value).format("MM-DD-yyyy")}`}`,
          });
        break;

      case "dateTo":
        value &&
          listFilter.push({
            label: `To${value && `: ${moment(value).format("MM-DD-yyyy")}`}`,
          });
        break;

      default:
        break;
    }
  });

  return listFilter;
};

export const paramsChipListGenerator = (object: any, catalog: any): any[] => {
  const listFilter: any[] = [];

  Object.entries(object).forEach(([key, value]: any) => {
    switch (key) {
      case "model":
        value &&
          listFilter.push({
            label: `Data source: ${
              catalog?.reportFilterModel.find(({ id }: any) => id === value)
                ?.name
            }`,
            key,
          });

        break;
      case "groupByModel":
        value &&
          listFilter.push({
            label: `Grouped by: ${
              catalog?.reportFilterModel[0]?.groupByFields?.find(
                ({ id }: any) => id === value
              )?.name
            }`,
          });
        break;
      default:
        break;
    }
  });

  return listFilter;
};
