import { Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { GenericFunction } from "../../interfaces/FunctionInterfaces";
import { MODAL_EDIT_ACTION_TYPE } from "./ModalActionsTypes";

interface Props {
  tooltipText: string;
  className?: string;
  actionName: MODAL_EDIT_ACTION_TYPE;
  anotherActions?: GenericFunction;
}

export const ButtonModalEndorsementAction = ({
  actionName,
  anotherActions,
  tooltipText,
}: Props) => {
  return (
    <OverlayTrigger
      placement="top"
      overlay={<Tooltip className="text-capitalize">{tooltipText}</Tooltip>}
    >
      <Button variant="primary" className="px-4 py-2" onClick={anotherActions}>
        {actionName === "ADD" ? (
          <i className="fs-3 bi bi-arrow-repeat"></i>
        ) : (
          <i className="fs-3 bi bi-pencil-square"></i>
        )}
      </Button>
    </OverlayTrigger>
  );
};
