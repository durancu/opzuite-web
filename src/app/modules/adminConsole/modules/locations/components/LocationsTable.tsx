import { Fragment } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import {
  NoRecordsFoundMessage,
  getHandlerTableChange,
  sortCaret,
  useFilterUIContext,
  FilterPanel,
  BackdropLoader,
  Error,
} from "src/app/components";
import * as columnFormatters from "src/app/components/commons/column-formatters";
import * as headerFormatters from "src/app/components/commons/header-formatters";
import { BoostrapTableTotalPaginator } from "src/app/components/functions/BoostrapTableTotalPaginator";
import { Pagination } from "src/app/components/pagination";
import { sizePerPageList, prepareFilter } from "./helpers";
import { FilterForm } from "./FilterForm";
import { locationAPI } from "../_redux/locationsSlice";
import PropTypes from "prop-types";

export const LocationsTable = ({ showFilter = true }) => {
  const { menuActions, queryParams, updateQueryParams } = useFilterUIContext();

  const {
    data = {},
    isLoading,
    isError,
    refetch,
  } = locationAPI.useGetAllLocationsQuery(queryParams, {
    refetchOnMountOrArgChange: true,
  });

  // Table columns
  const columns = [
    {
      dataField: "name",
      text: "Name",
      sort: true,
      align: "left",
      headerAlign: "left",
      headerClasses: "fw-bold text-muted",
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.NameColumnHeaderFormatter,
      formatter: columnFormatters.LocationNameWithLinkColumnFormatter,
      formatExtraData: {
        openDetailPageFunction: menuActions.details,
        idField: "code",
        textField: "name",
        defaultText: "N/A",
      },
    },
    {
      dataField: "city",
      text: "City",
      headerClasses: "fw-bold text-muted",
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.CityColumnHeaderFormatter,
      formatter: columnFormatters.CityColumnFormatter,
    },
    {
      dataField: "country",
      text: "Country",
      sort: true,
      headerFormatter: headerFormatters.CountryColumnHeaderFormatter,
      formatter: columnFormatters.CountryColumnFormatter,
      headerClasses: "fw-bold text-muted",
      sortCaret: sortCaret,
    },
    {
      dataField: "email",
      text: "Email",
      sort: true,
      headerFormatter: headerFormatters.EmailColumnHeaderFormatter,
      formatter: columnFormatters.EmailColumnFormatter,
      headerClasses: "fw-bold text-muted",
      sortCaret: sortCaret,
    },
    {
      dataField: "phone",
      text: "Phone Number",
      sort: true,
      headerClasses: "fw-bold text-muted",
      sortCaret: sortCaret,
      headerFormatter: headerFormatters.PhoneColumnHeaderFormatter,
      formatter: columnFormatters.PhoneColumnFormatter,
    },

    {
      dataField: "action",
      text: "",
      headerAlign: "right",
      align: "right",
      formatter: columnFormatters.LocationActionsColumnFormatter,
      formatExtraData: {
        openDetailLocationPage: menuActions.details,
        openEditLocationPage: menuActions.edit,
        openDeleteLocationDialog: menuActions.delete,
        openChangeIpLocationDialog: menuActions.changeIP,
      },
      classes: "text-right pr-0",
      style: {
        minWidth: "100px",
      },
    },
  ];

  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: data.totalCount || 0,
    sizePerPageList: sizePerPageList,
    sizePerPage: queryParams.pageSize,
    page: queryParams.pageNumber,
    paginationTotalRenderer: BoostrapTableTotalPaginator,
  };

  if (isLoading) return <BackdropLoader />;

  return (
    <Fragment>
      {showFilter && (
        <FilterPanel
          prepareFilter={prepareFilter}
          filterForm={<FilterForm />}
        />
      )}
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination isLoading={isLoading} paginationProps={paginationProps}>
              <BootstrapTable
                {...paginationTableProps}
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={data.entities || []}
                columns={columns}
                defaultSorted={[{ dataField: "soldAt", order: "desc" }]}
                onTableChange={getHandlerTableChange(updateQueryParams)}
              />
              {!isError && <NoRecordsFoundMessage entities={data.entities} />}
              <Error show={isError} action={refetch} />
            </Pagination>
          );
        }}
      </PaginationProvider>
    </Fragment>
  );
};

LocationsTable.propTypes = {
  showFilter: PropTypes.bool,
};
