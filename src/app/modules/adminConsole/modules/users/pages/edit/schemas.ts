import { PHONE_REGEX } from "src/app/constants/RegularExpressions";
import * as Yup from "yup";

export const userGeneralValidationSchema = Yup.object().shape({
  //BASIC INFO
  email: Yup.string() ///-----
    .email("Must be a valid email address")
    .required("Email is required"),
  //PERSONAL INFO
  firstName: Yup.string()
    .min(2, "Minimum 1 character")
    .max(50, "Maximum 50 characters")
    .required("First name is required"),
  lastName: Yup.string()
    .min(2, "Minimum 1 character")
    .max(50, "Maximum 50 characters")
    .required("Last name is required"),
  phone: Yup.string()
    .nullable()
    .trim()
    .matches(PHONE_REGEX, "Must be a valid phone number"),
  mobilePhone: Yup.string()
    .nullable()
    .matches(PHONE_REGEX, "Must be a valid phone number")
    .required("Phone number is required"),
  dob: Yup.mixed(),
  company: Yup.string().when("code", {
    is: (code: string) => (code && code.length > 0 ? true : false),
    then: Yup.string().required("Company is required"),
    otherwise: Yup.string().nullable(),
  }),
  address: Yup.object().shape({
    address1: Yup.string(),
    address2: Yup.string(),
    city: Yup.string(),
    state: Yup.string(),
    country: Yup.string().required("Country is required"),
    zip: Yup.number(),
  }),
  role: Yup.mixed().required("Role is required"),
  /* password: Yup.string().when("code", {
    is: (code) => (code && code.length > 0 ? true : false),
    then: Yup.string(),
    otherwise: Yup.string()
      .min(8, "Minimum 8 character")
      .required("Password is required"),
  }),

  confirmPassword: Yup.string().when("code", {
    is: (code) => (code && code.length > 0 ? true : false),
    then: Yup.string(),
    otherwise: Yup.string()
      .required("Please confirm your password")
      .when("password", {
        is: (password) => (password && password.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref("password"), ""],
          "Password doesn't match"
        ),
      }),
  }), */
});

export const userAgentInfoValidationSchema = Yup.object().shape({
  //PERSONAL INFO
  location: Yup.string(), //.required("Employee's location/office is required"),
  //EMPLOYEE INFO
  employeeInfo: Yup.object().shape({
    endedAt: Yup.mixed().nullable(),
    startedAt: Yup.mixed().nullable(),
    position: Yup.string().required("Employee Position is required"),
    payFrequency: Yup.string(),
    payRate: Yup.number(),
    overtimeAuthorized: Yup.boolean(),
    overtimePayRate: Yup.number(),
    salaryFormula: Yup.string(),
  }),
  supervisor: Yup.string(), //.required("Supervisor is required"),
});

export const userSettingsValidationSchema = Yup.object().shape({
  emailNotification: Yup.boolean(),
  sendCopyToPersonalEmail: Yup.boolean(),
  youHaveNewNotifications: Yup.boolean(),
  youAreSentADirectMessage: Yup.boolean(),
  locationTargetReached: Yup.boolean(),
  newTeamMember: Yup.boolean(),
  employeeTargetReached: Yup.boolean(),
});
