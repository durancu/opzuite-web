import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { FormattedMessage } from "react-intl";
import { Link, useNavigate } from "react-router-dom";
import { PageTitle, Toolbar } from "src/_metronic/layout/core";
import { FilterUIProvider, RequirePermission } from "src/app/components";
import { Permissions } from "src/app/constants/Permissions";
import { breadcrumbs } from "../breadcrumbs";
import { PoliciesTable } from "../components/PoliciesTable";
import { defaultFilter } from "../components/helpers";

export const Policies = () => {
  const navigate = useNavigate();

  const menuActions = {
    edit: (code: string) => navigate(`${code}/edit`),
    details: (code: string) => navigate(`${code}/detail`),
    delete: (code: string) => navigate(`${code}/delete`),
    renew: (code: string) => navigate(`${code}/renew`),
    certificates: (customerId: string) => {
      navigate(`/certificates/new?customer=${customerId}`);
    },
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Policies</PageTitle>
      <Toolbar>
        <RequirePermission permissions={Permissions.POLICIES_CREATE}>
          <Link to="policies/new" className="btn btn-primary align-self-center">
            <FormattedMessage id="BUTTON.POLICY.NEW" />
          </Link>
        </RequirePermission>
      </Toolbar>
      <Card className="p-8">
        <FilterUIProvider
          context="sales"
          menuActions={menuActions}
          defaultFilter={defaultFilter}
        >
          <PoliciesTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
