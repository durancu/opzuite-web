import { useState, Fragment } from "react";
import { FieldArray, useFormikContext } from "formik";
import { Button, Accordion, Col, Row } from "react-bootstrap";
import { useGetCatalog } from "src/app/components/Catalog/hooks";
import {
  ButtonModalEndorsementAction,
  EndorsementType,
  ItemDeleteModal,
  ModalEndorsementActionType,
} from "src/app/components/EndorsementsComponents";
import { FakeField } from "src/app/partials/controls";
import { RepeaterPermitEndorsementItems } from "./RepeaterPermitEndorsementItems";
import { EndorsementModal } from "./components";
import { defaultEndorsement } from "./helpers";
import objectPath from "object-path";
import { MissingFieldsAlert } from "src/app/components";

const RepeaterPermitEndorsements = () => {
  const { values, errors, setFieldValue } = useFormikContext<any>();
  const { permitTypes, permitEndorsementStatus, employees } = useGetCatalog();

  const [localState, setLocalState] = useState<Record<string, any>>({
    type: "", // endorsementType
    action: "", // endorsement action, Add, edit ...
    index: null, // actual endorsement
    modal: false, // show or hide modal
  });

  function updateLocalState(key: string, value: boolean) {
    setLocalState((current) => {
      return { ...current, [key]: value };
    });
  }

  const addNewEndosement = () => {
    const updatedEndorsements = [...values.endorsements, defaultEndorsement];
    setFieldValue("endorsements", updatedEndorsements);
    setLocalState((current) => {
      return {
        ...current,
        type: EndorsementType.APPLICATION,
        action: ModalEndorsementActionType.ADD,
        index: updatedEndorsements.length - 1, // you don't want to edit the wrong one xd
        modal: true,
      };
    });
  };

  return (
    <Fragment>
      <EndorsementModal
        action={localState.action}
        index={localState.index}
        show={localState.modal}
        onHide={() => updateLocalState("modal", false)}
      />

      <FieldArray
        name="endorsements"
        render={(endorsements) => (
          <Accordion defaultActiveKey={["0", "1"]} flush alwaysOpen>
            {values.endorsements?.map((element: any, index: number) => {
              const applicationType = permitTypes.find(
                (e: any) => e.type === element.type
              );
              return (
                <Accordion.Item
                  key={index}
                  eventKey={index.toString()}
                  className="mb-4 border"
                >
                  <Accordion.Button className="px-6 py-4 bg-light-primary">
                    <div className="w-100 d-flex justify-content-between align-items-center me-4">
                      <h4 className="fw-bold">
                        {`Service A${index + 1}`}
                        {applicationType
                          ? " - ".concat(applicationType.name)
                          : ""}
                      </h4>

                      <div className="d-flex justify-content-between align-items-center gap-3">
                        <MissingFieldsAlert
                          show={objectPath.get(errors, `endorsements.${index}`)}
                        />
                        <ButtonModalEndorsementAction
                          className="mr-2"
                          actionName={ModalEndorsementActionType.EDIT}
                          tooltipText="Edit Endorsement"
                          anotherActions={() => {
                            setLocalState((current) => {
                              return {
                                ...current,
                                type: EndorsementType.APPLICATION,
                                action: ModalEndorsementActionType.EDIT,
                                index: index, // you don't want to edit the wrong one xd
                                modal: true,
                              };
                            });
                          }}
                        />
                        <ItemDeleteModal
                          itemTitle={`Service A${index + 1}`}
                          action={() => endorsements.remove(index)}
                        />
                      </div>
                    </div>
                  </Accordion.Button>
                  <Accordion.Body>
                    <Row className="mb-12">
                      <Col md={3}>
                        <FakeField
                          label="Name"
                          type="text"
                          value={element.name}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Requested on"
                          type="date"
                          value={element.endorsedAt}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Completed on"
                          type="date"
                          value={element.followUpDate}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Amount"
                          type="number"
                          value={element.amount}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Status"
                          type="select"
                          value={element.status}
                          options={permitEndorsementStatus}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Seller"
                          type="select"
                          value={element.seller}
                          options={employees}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Follow Up Person"
                          type="select"
                          value={element.followUpPerson}
                          options={employees}
                        />
                      </Col>
                      <Col md={3}>
                        <FakeField
                          label="Description"
                          type="text"
                          value={element.description}
                        />
                      </Col>
                    </Row>

                    <RepeaterPermitEndorsementItems endorsementIndex={index} />
                  </Accordion.Body>
                </Accordion.Item>
              );
            })}

            <Button
              variant="primary"
              size="sm"
              onClick={() => addNewEndosement()}
              className="d-flex gap-1 align-items-center my-4"
            >
              <i className="fs-3 bi bi-plus-circle-dotted"></i>
              <span> Custom Service</span>
            </Button>
          </Accordion>
        )}
      />
    </Fragment>
  );
};

export default RepeaterPermitEndorsements;
