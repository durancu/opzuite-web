import { ReactNode, Fragment } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

type Props = {
  children: ReactNode;
};

export const RequireAuth = ({ children }: Props) => {
  const { isAuthorized, lastError } = useSelector(
    (state: any) => ({
      isAuthorized: state.auth.user != null,
      lastError: state.apiInterceptor.lastError,
    }),
    shallowEqual
  );

  if (!isAuthorized) {
    return <Navigate to="/auth/login" />;
  } else if (lastError?.statusCode === 403) {
    return <Navigate to="/forbidden" />;
  } else if (lastError?.statusCode === 403.6) {
    return <Navigate to="/not-found-ip" />;
  }

  return <Fragment>{children}</Fragment>;
};
