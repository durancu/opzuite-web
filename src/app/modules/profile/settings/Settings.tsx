import { Fragment } from "react";
import { ProfileDetails } from "./ProfileDetails";
import { SignInMethod } from "./SignInMethod";
import { PageTitle } from "src/_metronic/layout/core";
import { breadcrumbs } from "../breadcrumbs";

export const Settings = () => {
  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Settings</PageTitle>
      <ProfileDetails />
      <SignInMethod />
    </Fragment>
  );
};
