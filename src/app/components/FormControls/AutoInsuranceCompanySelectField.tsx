import { FormHelperText, MenuItem } from "@mui/material";
import { useIntl } from "react-intl";
import * as CATALOG from "../Catalog/CatalogSelects";
import { Field } from "formik";
import { TextField } from "formik-material-ui";

interface Props {
  helperText?: string;
  helperTextId?: string;
  labelTextId?: string;
  labelText?: string;
  fieldName?: string;
  disabled: boolean;
}

export function AutoInsuranceCompanySelectField({
  helperText,
  helperTextId,
  labelTextId = "FORM.LABELS.AUTO_INSURANCE_COMPANY",
  labelText,
  fieldName = "autoInsuranceCompany",
  ...props
}: Props) {
  const intl = useIntl();

  return (
    <>
      <Field
        {...props}
        component={TextField}
        select={true}
        name={fieldName}
        label={labelText || `${intl.formatMessage({ id: labelTextId })}`}
      >
        {CATALOG.AUTO_AND_HOME_INSURANCE_CATALOG.map(({ id, name }) => (
          <MenuItem key={id} value={id}>
            {name}
          </MenuItem>
        ))}
      </Field>
      {helperText ? (
        <FormHelperText>{helperText}</FormHelperText>
      ) : helperTextId ? (
        <FormHelperText>
          {intl.formatMessage({ id: helperTextId })}
        </FormHelperText>
      ) : (
        ``
      )}
    </>
  );
}
