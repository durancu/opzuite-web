/**
 * High level router
 * Note: It's recommended to compose related routes in modules routes.tsx
 */
import { Navigate } from "react-router-dom";
import { createBrowserRouter, RouteObject } from "react-router-dom";
import { AuthLayout, AuthRoutes } from "./app/modules/auth";
import { ErrorsLayout, ErrorRoutes } from "./app/modules/errors";
import { MasterLayout } from "src/_metronic/layout/MasterLayout";
import { AppRoutes } from "./app/routes";
import { RequireAuth } from "./app/components/guards";
import { ErrorBoundary } from "./app/components";
import App from "./App";

// all app routes
const routes: RouteObject[] = [
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorBoundary />, // catch all not-found pages
    children: [
      { path: "error", element: <ErrorsLayout />, children: ErrorRoutes },
      {
        path: "not-found-ip",
        element: <Navigate to="/error/ip" replace />,
      },
      {
        path: "not-found",
        element: <Navigate to="/error" replace />,
      },
      { path: "forbidden", element: <Navigate to="/error/403" replace /> },
      {
        path: "auth",
        element: <AuthLayout />,
        children: AuthRoutes,
      },
      {
        element: (
          <RequireAuth>
            <MasterLayout />
          </RequireAuth>
        ),
        children: AppRoutes,
      },
    ],
  },
];

/**
 * Base URL of the website.
 * @see https://facebook.github.io/create-react-app/docs/using-the-public-folder
 * Override `basename` (e.g: `homepage` in `package.json`)
 */

const router = createBrowserRouter(routes, {
  basename: process.env.PUBLIC_URL,
});

export default router;
