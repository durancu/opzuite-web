import { Fragment } from "react";
import { Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { PageTitle } from "src/_metronic/layout/core";
import { FilterUIProvider } from "src/app/components";
import { BASE_FILTER_OPTIONS } from "src/app/constants/tables";
import { breadcrumbs } from "../breadcrumbs";
import { CertificatesTable } from "../components/CertificatesTable";

export const OverView = () => {
  const navigate = useNavigate();

  const menuActions = {
    //details: (code: string) => navigate(`${code}/detail`),
    // delete: (code: string) => navigate(`${code}/delete`),
    edit: (code: string) => {
      navigate(`/certificates/${code}/edit`);
    },
    preview: (code: string) => {
      navigate(`/certificates/${code}/preview`);
    },
  };

  return (
    <Fragment>
      <PageTitle breadcrumbs={breadcrumbs}>Certificates</PageTitle>
      <Card className="p-8">
        <FilterUIProvider
          menuActions={menuActions}
          defaultFilter={BASE_FILTER_OPTIONS}
        >
          <CertificatesTable />
        </FilterUIProvider>
      </Card>
    </Fragment>
  );
};
