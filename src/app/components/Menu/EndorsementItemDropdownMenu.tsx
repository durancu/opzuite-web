import { Dropdown } from "react-bootstrap";

interface Props {
  title?: string;
  options: Array<any>;
  itemAction: (itemType: any) => void;
}

export const EndorsementItemDropdownMenu = ({
  title,
  options,
  itemAction,
}: Props) => {
  return (
    <Dropdown>
      <Dropdown.Toggle variant="secondary" className="px-4 py-2">
        {title ? title : "Add New Detail"}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {options
          ?.filter((option: any) => option.id !== "AGENCY_COMMISSION")
          ?.map((option: any, index: number) => (
            <Dropdown.Item
              key={index}
              className="d-flex gap-4 align-items-center"
              onClick={() => itemAction && itemAction(option)}
            >
              <i
                className="bi bi-circle-fill"
                style={{ color: `${option.color}` }}
              ></i>
              <span>{option.name}</span>
            </Dropdown.Item>
          ))}
      </Dropdown.Menu>
    </Dropdown>
  );
};
